const TYPE_GIFT = 4; // Auto add bundle detail when add sku parent on POS
const TYPE_STOCK = 2; // Use for update stock
const TYPE_PICK_PRICE = 1; // Khong tich diem

module.exports = {
    TYPE_GIFT,
    TYPE_STOCK,
    TYPE_PICK_PRICE,
}