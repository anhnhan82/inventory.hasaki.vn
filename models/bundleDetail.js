'use strict';
const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
    class BundleDetail extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            
        }
    };
    BundleDetail.init({
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true // Automatically gets converted to SERIAL for postgres
        },
        bundle_id: DataTypes.INTEGER,
        sku: DataTypes.INTEGER,
        qty: DataTypes.INTEGER,
        discount: DataTypes.INTEGER,
        set_price: DataTypes.INTEGER,
        status: DataTypes.INTEGER,
    }, {
        sequelize,
        timestamps: false,
        modelName: 'BundleDetail',
        tableName: 'bundle_details',
    });
    return BundleDetail;
};
