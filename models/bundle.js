'use strict';
const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  
  class Bundle extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      // associations can be defined here
      Bundle.hasMany(models.BundleDetail, {
        foreignKey: 'bundle_id',
        //as: 'BundleDetail',
      })
    }
  };
  Bundle.init({
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    sku: DataTypes.INTEGER,
    qty: DataTypes.INTEGER,
    type: DataTypes.INTEGER,
    description: DataTypes.STRING,
  }, {
    //timestamps: false,
    sequelize,
    modelName: 'Bundle',
    tableName: 'bundles',
    underscored: true,
  });
  return Bundle;
};
