'use strict';

const fs = require('fs');
const path = require('path');
const Sequelize = require('sequelize');
const basename = path.basename(__filename);
const env = process.env.NODE_ENV || 'development';
const config = require(__dirname + '/../config/config.js')[env];

const logger = require('../utils/logger');

const db = {};
let sequelize;

if (config.use_env_variable) {
  sequelize = new Sequelize(process.env[config.use_env_variable], config);
} else {
  sequelize = new Sequelize(config.database, config.username, config.password, {
    //logging: console.log,                  // Default, displays the first parameter of the log function call
    //logging: (...msg) => console.log(msg), // Displays all log function call parameters
    logging: false,                        // Disables logging
    //logging: msg => logger.info(msg),     // Use custom logger (e.g. Winston or Bunyan), displays the first parameter
    //logging: logger.debug.bind(logger),     // Alternative way to use custom logger, displays all messages
    underscored: true,
    dialect: config.dialect,
    host: config.host,
    port: config.port,
    //benchmark: true,
    pool: {
      max: 10,
      min: 0,
      acquire: 30000,
      idle: 10000,
    }
  });
}

fs
  .readdirSync(__dirname)
  .filter(file => {
    return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
  })
  .forEach(file => {
    const model = require(path.join(__dirname, file))(sequelize, Sequelize.DataTypes);
    db[model.name] = model;
  });

Object.keys(db).forEach(modelName => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;
