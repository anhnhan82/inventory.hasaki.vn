'use strict';
const {
  Model
} = require('sequelize');



module.exports = (sequelize, DataTypes) => {
  class StockProduct extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  StockProduct.init({
    stockprod_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true // Automatically gets converted to SERIAL for postgres
    },
    stockprod_stock_id: DataTypes.INTEGER,
    stockprod_sku: DataTypes.INTEGER,
    stockprod_in_stock: DataTypes.INTEGER,
    stockprod_committed: DataTypes.INTEGER,
    stockprod_available: DataTypes.INTEGER,
    stockprod_in_comming: DataTypes.INTEGER,
    stockprod_utime: DataTypes.INTEGER,
  }, {
    timestamps: false,
    sequelize,
    modelName: 'StockProduct',
    tableName: 'stock_product',
  });
  return StockProduct;
};
