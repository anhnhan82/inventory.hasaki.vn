'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class StockInOut extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
        }
    };
    StockInOut.init({
        stockino_id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        stockino_stock_id: DataTypes.INTEGER,
        stockino_store_id: DataTypes.INTEGER,
        stockino_sku: DataTypes.INTEGER,
        stockino_in: DataTypes.INTEGER,
        stockino_in_comming: DataTypes.INTEGER,
        stockino_out: DataTypes.INTEGER,
        stockino_committed: DataTypes.INTEGER,
        stockino_source_id: DataTypes.INTEGER,
        stockino_ctime: DataTypes.BIGINT,
    }, {
        timestamps: false,
        sequelize,
        modelName: 'StockInOut',
        tableName: 'stock_inout',
    });
    return StockInOut;
};