'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class StockImportExport extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
        }
    };
    StockImportExport.init({
        stockimex_id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        stockimex_stock_id: DataTypes.INTEGER,
        stockimex_sku: DataTypes.INTEGER,
        stockimex_qty: DataTypes.INTEGER,
        stockimex_type: DataTypes.INTEGER,
        stockimex_user_id: DataTypes.INTEGER,
        stockimex_import_id: DataTypes.BIGINT,
        stockimex_ctime: DataTypes.INTEGER,
        stockimex_current_in_stock: DataTypes.INTEGER,
        stockimex_import_type: DataTypes.INTEGER,
        stockimex_source: DataTypes.INTEGER,
    }, {
        timestamps: false,
        sequelize,
        modelName: 'StockImportExport',
        tableName: 'stock_import_export',
    });
    return StockImportExport;
};