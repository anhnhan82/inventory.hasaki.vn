const express = require('express')
const app = express(), bodyParser = require('body-parser')
const port = 4000

const stockImportExportService = require('./services/stockImportExportService')

app.use(bodyParser.json());
app.get('/', async(req, res) => {
  const { User } = require('./models')
  const stockProductService = require('./services/stockProductService')
  //const arrProduct = await stockProductService.getList({})
  //res.send('Hello World!')
  //res.json(arrProduct)

  const stockImportExportService = require('./services/stockImportExportService')
  const result = stockImportExportService.insert({
    stock_id: 1001,
    product_sku: 205100136,
    qty: 1,
    type: 1, // Imp
    user_id: 1001,
    import_id: 21033011194,
    import_type: 1, // order
  });
  res.json(result)
})

app.get('/bundle', async(req, res) => {
  const bundleService = require('./services/bundleService')
  //const arrBundle = await bundleService.getInfo({sku:277900016})
  const arrBundle = await bundleService.getConversion({sku:204900015, qty: 1, type: 1, stock_id: 1005})
  //res.json(arrBundle)
  res.json({ data: {rows: arrBundle}})
})

app.post('/stock/import-export', async(req, res) => {
  console.log(req.body)
  const arrResult = await stockImportExportService.insert(req.body)
  res.json({ data: {rows: arrResult}})
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})

module.exports = app; // for testing