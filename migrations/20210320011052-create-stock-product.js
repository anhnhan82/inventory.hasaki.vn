'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('stock_products', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      stockId: {
        allowNull: false,
        type: Sequelize.INTEGER
      },
      productSku: {
        allowNull: false,
        type: Sequelize.INTEGER
      },
      /*
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      createdAt: {
        type: DataTypes.DATE,
        field: 'created_at'
      },
      */
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('stock_products');
  }
};