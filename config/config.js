// config.js
require('dotenv').config();
const { DB_CONNECTION, DB_HOST, DB_DATABASE, DB_USERNAME, DB_PASSWORD, DB_PORT } = process.env;

module.exports = {
  "development": {
    "username": DB_USERNAME,
    "password": DB_PASSWORD,
    "database": DB_DATABASE,
    "host": DB_HOST,
    "port": DB_PORT,
    "dialect": DB_CONNECTION,
    "logging": true
  },
  "test": {
    "username": DB_USERNAME,
    "password": DB_PASSWORD,
    "database": "nodejs",
    "host": DB_HOST,
    "port": DB_PORT,
    "dialect": DB_CONNECTION,
    "logging": true
  },
  "production": {
    "username": DB_USERNAME,
    "password": DB_PASSWORD,
    "database": DB_DATABASE,
    "host": DB_HOST,
    "port": DB_PORT,
    "dialect": DB_CONNECTION,
    "logging": true
  }
}
