const { Bundle, BundleDetail, sequelize } = require('../models')
const { Op } = require("sequelize")
const { TYPE_STOCK } = require('../constants/bundle')
const ImportExportConstant = require('../constants/importExport');
const stockProductService = require('./stockProductService');
const { log } = require('../utils/logger');

const getInfo = async (param) => {
    let cond = { sku: param.sku, type: TYPE_STOCK }
    const limit = 10, offset = 0;

    let result = { is_bundle: 0, detail: [], relation: [] }, arrSku = []
    const bundle = await Bundle.findOne({ limit: limit, offset: offset, where: cond, include: { model: BundleDetail, required: true } })
    if (bundle) {
        result.is_bundle = 1
        result.detail = bundle.BundleDetails
        for (let detail of bundle.BundleDetails) {
            arrSku.push(detail.sku)
        }
    } else {
        arrSku.push(param.sku)
    }
    result.relation = await Bundle.findAll({
        include: {
            model: BundleDetail,
            //where: { sku: { [Op.in]: arrSku } },
            required: true,
        }, where: {
            sku: { [Op.ne]: param.sku },
            type: TYPE_STOCK,
        }
    })

    return result
};


const getConversion = async (param) => {
    const bundle = await getInfo(param)

    let result = {}
    if (bundle && (!bundle.detail && !bundle.relation)) {
        return {}
    }
    result.is_bundle = bundle.is_bundle
    if (ImportExportConstant.TYPE_IMPORT == param.type) {
        result = await conversionImport(param, bundle)
    } else {
        result = await conversionExport(param, bundle)
    }
    //console.log(bundle)
    return result;

}
const conversionImport = async (param, bundle) => {
    let arrResult = [], arrAdj = {}
    // Is bundle
    if (bundle.is_bundle) {
        console.log('Is import bundle')

        if (bundle.detail) {
            for (let detail of bundle.detail) {
                intAdj = param.qty * detail.qty;
                arrAdj[detail.sku] = intAdj
                arrResult.push({ sku: detail.sku, qty: intAdj, conversion_value: detail.qty, source: detail.sku })
            }
            // Calculate item in other combo
            const arrRelation = await conversionCombo(ImportExportConstant.TYPE_IMPORT, param.stock_id, bundle, arrAdj)
            if (arrRelation) {
                for (let detail of arrRelation) {
                    arrResult.push({ sku: detail.sku, qty: detail.qty, conversion_value: detail.qty, source: detail.sku })
                }
            }
        }
    } else {
        console.log('Is simple')
        // Is not bundle
        let arrSku = [param.sku];
        arrAdj[param.sku] = param.qty
        console.log('BUNDLE', JSON.stringify(bundle))
        if (bundle.relation) {
            for (let relation of bundle.relation) {
                arrSku.push(relation.sku)
                for (let detail of relation.BundleDetails) {
                    arrSku.push(detail.sku)
                }
            }
            
        }
        let mappingStock = {}
        // Lay stock product co lien quan
        const arrStock = await stockProductService.getList({ stock_id: param.stock_id, sku: arrSku })
        if (arrStock) {
            for (let stock of arrStock) {
                mappingStock[stock.stockprod_sku] = stock.stockprod_in_stock
            }
        }
        console.log({ abc: mappingStock })

        // Lay ton kho hien tai
        let childQty = mappingStock[param.sku] || 0

        // Kiem tra xem voi so luong sku con import vao co du de tao combo moi khong?
        for (let combo of bundle.relation) {
            if (!combo.BundleDetails) {
                continue
            }
            let arrMapDetail = {}
            for (let detail of combo.BundleDetails) {
                arrMapDetail[detail.sku] = detail.qty
            }
            let comboQty = mappingStock[combo.sku] || 0
            let expectComboQty = parseInt(Math.floor(childQty / arrMapDetail[param.sku]))

            // Store list qty maybe change
            let arrQty = [];
            for (let detail of combo.BundleDetails) {
                // Tinh toan xem so luong combo co thay doi theo hay khong
                if (detail.qty > 0) {
                    let tmpChildStock = mappingStock[detail.sku] || 0
                    let expectComboQtyTmp
                    if (detail.sku == param.sku) {
                        expectComboQtyTmp = parseInt(Math.floor((tmpChildStock + param.qty) / detail.qty))
                    } else {
                        expectComboQtyTmp = parseInt(Math.floor((tmpChildStock) / detail.qty))
                    }
                    arrQty.push(expectComboQtyTmp)
                }
            }
            // Neu so luong combo thay doi
            const comboAdj = Math.min(...arrQty) || 0
            if (comboAdj > comboQty) {
                arrResult.push({ sku: combo.sku, qty: comboAdj - comboQty, conversion_value: comboAdj, source: param.sku })
            }
        }
    }

    return arrResult
}
const conversionExport = async (param, bundle) => {
    let arrResult = [], arrAdj = {}
    // Is bundle
    if (bundle.is_bundle) {
        if (bundle.detail) {
            for (let detail of bundle.detail) {
                intAdj = param.qty * detail.qty;
                arrAdj[detail.sku] = intAdj
                arrResult.push({ sku: detail.sku, qty: intAdj, conversion_value: detail.qty, source: detail.sku })
            }
            // Calculate item in other combo
            const arrRelation = await conversionCombo(ImportExportConstant.TYPE_EXPORT, param.stock_id, bundle, arrAdj)
            console.log(arrRelation)
            if (arrRelation) {
                for (let detail of arrRelation) {
                    arrResult.push({ sku: detail.sku, qty: detail.qty, conversion_value: detail.qty, source: detail.sku })
                }
            }
        }
    } else {
        // Is not bundle
        if (bundle.relation) {
            let arrSku = [param.sku];
            for (let relation of bundle.relation) {
                arrSku.push(relation.sku)
                for (let detail of relation.BundleDetails) {
                    arrSku.push(detail.sku)
                }
            }
            let mappingStock = {}
            // Lay stock product co lien quan
            const arrStock = await stockProductService.getList({ stock_id: param.stock_id, sku: arrSku })
            if (arrStock) {
                for (let stock of arrStock) {
                    mappingStock[stock.stockprod_sku] = stock.stockprod_in_stock
                }
            }
            const childQty = mappingStock[param.sku] || 0
            // Tim trong combo sku dang xuat ra con du de tao combo ko?
            for (let combo of bundle.relation) {
                let arrMapDetail = {}
                for (let detail of combo.BundleDetails) {
                    arrMapDetail[detail.sku] = detail.qty
                }
                // Neu co item trong combo can xuat
                if (arrMapDetail[param.sku]) {
                    const comboQty = mappingStock[combo.sku] || 0
                    const expectChildQty = comboQty * arrMapDetail[param.sku]
                    if (childQty - param.qty < expectChildQty) {
                        const adj = parseInt(Math.ceil((childQty - param.qty) / arrMapDetail[param.sku]))
                        let comboAdjQty = comboQty - adj
                        const reCheckChildQty = comboAdjQty * arrMapDetail[param.sku]
                        if (((reCheckChildQty != (childQty - param.qty))) && (reCheckChildQty - (childQty - param.qty)) < arrMapDetail[param.sku]) {
                            comboAdjQty = comboQty - Math.floor((childQty - param.qty) / arrMapDetail[param.sku]);
                        }
                        arrResult.push({ sku: combo.sku, qty: comboAdjQty, conversion_value: arrMapDetail[param.sku], source: param.sku })
                    }
                }
            }
        }

    }
    return arrResult
}
const conversionCombo = async (type, stockId, bundle, arrAdjust) => {
    let arrResult = []
    console.log(arrAdjust);

    for (let combo of bundle.relation) {
        let arrSku = [combo.sku];
        // if( combo.bundle_id == bundle.id ){
        // //chi lay cai combo item voi bundle_id bang combo bundle_id
        // //chi lay item cua combo chi dinh
        // arrSku = [combo.sku];
        // }
        // arrSku = [combo.sku];
        // Is bundle
        for (let detail of combo.BundleDetails) {
            arrSku.push(detail.sku)
        }
        let mappingStock = {}
        const arrStock = await stockProductService.getList({ stock_id: stockId, sku: arrSku })
        if (arrStock) {
            for (let stock of arrStock) {
                mappingStock[stock.stockprod_sku] = stock.stockprod_in_stock
            }
            // console.log(mappingStock);
        }
        const comboQty = mappingStock[combo.sku] || 0

        let comboAdj = 0
        let minImport = null;
        for (let detail of combo.BundleDetails) {
            const childQty = mappingStock[detail.sku] || 0 // qty cua sku child

            // Is Export
            if (type == ImportExportConstant.TYPE_EXPORT) {
                const childAdj = arrAdjust[detail.sku] || 0
                //console.log('Go HERRERRRR', JSON.stringify({child_adj: childAdj, arr_adj: arrAdjust}))
                if ((childQty - childAdj) < (comboQty * detail.qty)) {
                    let adj = Math.floor((childQty - childAdj) / detail.qty);
                    adj = comboQty - adj;
                    if (adj && adj > comboAdj) {
                        arrResult.push({ sku: combo.sku, qty: adj, conversion_value: detail.qty, source: detail.sku })
                    }
                }
            } else {
                // Import
                const childIn = arrAdjust[detail.sku] || 0
                const nextChildStock = Math.floor((childQty + childIn) / detail.qty)
                // console.log(childIn / detail.qty);
                if (minImport === null || minImport > nextChildStock) {
                    minImport = nextChildStock;
                }
                // if (arrAdjust[detail.sku]) {

                // if (nextChildStock > comboQty * detail.qty) {
                // const expectQty = parseInt(Math.floor(nextChildStock / detail.qty)) - comboQty;
                // //console.log('expectQtyexpectQtyexpectQty', {next_stock: nextChildStock, expect_qty:expectQty, stock_combo: comboQty})
                // if (expectQty > 0) {
                // // Lay min stock cua cac sku con
                // let arrChildStock = []
                // for (let child of combo.BundleDetails) {
                // if (detail.sku != child.sku) {
                // arrChildStock.push(mappingStock[child.sku] || 0)
                // }


                // }
                // // Neu stock nho nhat cua sku con ma lon hon stock combo
                // if (Math.min(...arrChildStock) >= expectQty || !arrChildStock) {
                // arrResult.push({ sku: combo.sku, qty: expectQty, conversion_value: detail.qty, source: 0 })
                // }
                // }
                // }

                // }
            }
        }
        // // neu co minImport != NULL => nghia la TH import
        if (minImport) {

            arrResult.push({ sku: combo.sku, qty: minImport - comboQty, source: 0 });
        }

    }
    if (type == ImportExportConstant.TYPE_EXPORT) {
        //arrResult

        arrResult = Object.values(arrResult.reduce(function (r, e) {
            if (!r[e.sku]) r[e.sku] = e;
            else if (e.qty > r[e.sku].qty) r[e.sku] = e;
            return r;
        }, {}))

    }
    // if (type == ImportExportConstant.TYPE_IMPORT) {
    // //arrResult

    // arrResult = Object.values(arrResult.reduce(function (r, e) {
    // if (!r[e.sku]) r[e.sku] = e;
    // else if (e.qty < r[e.sku].qty) r[e.sku] = e;
    // return r;
    // }, {}))


    // }
    //console.log(arrResult);
    return arrResult;

}

const getConversionInOut = async (param) => {
    
    let inOutType = 0, intQty = 0;
    if (param.committed) {
        inOutType = ImportExportConstant.TYPE_EXPORT
        intQty = parseInt(param.committed)
    } else if (param.in_comming) {
        inOutType = ImportExportConstant.TYPE_IMPORT
        intQty = parseInt(param.in_comming)
    }
    // Not valid param
    if (!inOutType || !intQty) {
    
        return []
        
    }

    const bundle = await getInfo(param)
    if (bundle && (!bundle.detail && !bundle.relation)) {
        return []
    }
    let arrResult = []
    let arrConversions
    
    // When in-comming
    if (inOutType === ImportExportConstant.TYPE_IMPORT) {
        arrConversions = await getConvertionIncomming(bundle, param.stock_id, param.sku, intQty)
     
    } else {
        // End When in-comming
        arrConversions = await getConvertionCommitted(bundle, param.stock_id, param.sku, intQty)
     
    }

    if (arrConversions) {
        for (let conversion of arrConversions) {
            arrResult.push({ sku: conversion.sku, qty: conversion.qty , stock_id : param.stock_id , store_id : param.store_id , type : inOutType })
        }
        
    }


    return arrResult
}

const getConvertionIncomming = async (bundle, stockId, inComSku, intQty) => {
    let arrResult = []
    let arrQty = {}
    if (bundle.is_bundle) {
        for (let detail of bundle.detail) {
            arrResult.push({ 'sku': detail.sku, 'qty': detail.qty * intQty })
            arrQty[detail.sku] = detail.qty * intQty
        }
    }

    for (let combo of bundle.relation) {
        let arrSkuStock = [combo.sku]
        let childStock, arrMapQty = {}
        for (let detail of combo.BundleDetails) {
            arrSkuStock.push(detail.sku)
            if(arrMapQty[detail.sku] = arrQty[detail.sku] ){
                arrQty[detail.sku]
            }else
            0
        }

        const arrStockTmp = await stockProductService.getList({ stock_id: stockId, sku: arrSkuStock })
        let mappingStock = {}
        if (arrStockTmp) {
            for (let stock of arrStockTmp) {
                mappingStock[stock.stockprod_sku] = stock.stockprod_in_stock
            }
        }

        let currentStockCombo = mappingStock[combo.sku] || 0
        // Bundle
        if (bundle.is_bundle) {
            childStock = Number.MAX_SAFE_INTEGER // Maximum int number 9007199254740991
            // Tim trong item, co du stock de tang so luong combo len hay ko?
            for (let detail of combo.BundleDetails) {
                //arrResult.push({ 'sku': detail.sku, 'qty': detail.qty })
                
                if (inComSku == combo.sku) {
                    continue
                }
                if (mappingStock[detail.sku] && detail.qty) {                  
                    let tmpChildStock = (mappingStock[detail.sku] + (arrMapQty[detail.sku] || 0)) / detail.qty;
                    if (tmpChildStock < childStock) {
                        childStock = tmpChildStock;
                    }   
                }
            }
        } else {
            // End Bundle
            childStock = Number.MAX_SAFE_INTEGER // Minimum int number 9007199254740991
            for (let detail of combo.BundleDetails) {
                if (detail.qty) {
                    if (detail.sku == inComSku) {
                        tmpChildStock = ((mappingStock[detail.sku] || 0) + intQty) / detail.qty
                    } else {
                        tmpChildStock = (mappingStock[detail.sku] || 0) / detail.qty
                    }
                    if (tmpChildStock < childStock) {
                        childStock = tmpChildStock;
                    }
                }
            }
        }

        if (childStock && childStock > currentStockCombo && childStock != Number.MAX_SAFE_INTEGER) {
            const intAjd = Math.floor(childStock - (currentStockCombo));
            if (intAjd) {
            arrResult.push({ 'sku': combo.sku, 'qty': intAjd })
            }
            

        }
    }
    return arrResult
}

const getConvertionCommitted = async (bundle, stockId, commitSku, intQty) => {
    let arrResult = [], arrMapQty = {}
    if (bundle.is_bundle) {
        for (let detail of bundle.detail) {
            arrResult.push({ 'sku': detail.sku, 'qty': detail.qty * intQty })
            arrMapQty[detail.sku] = detail.qty * intQty
        }
    }
    for (let combo of bundle.relation) {
        let arrSkuStock = [combo.sku], tmpChildStock = 0
        for (let detail of combo.BundleDetails) {
            arrSkuStock.push(detail.sku)
            //arrMapQty[detail.sku] = detail.qty * intQty
        }
        const arrStockTmp = await stockProductService.getList({ stock_id: stockId, sku: arrSkuStock })
        let mappingStock = {}
        if (arrStockTmp) {
            for (let stock of arrStockTmp) {
                mappingStock[stock.stockprod_sku] = stock.stockprod_in_stock
            }
        }

        let currentStockCombo = mappingStock[combo.sku] || 0
        childStock = Number.MAX_SAFE_INTEGER // Minimum int number 9007199254740991
        // Bundle
        if (bundle.is_bundle) {
            if (combo.sku == commitSku) {
                continue;
            }
            // Tim trong item, co du stock de tang so luong combo len hay ko?
            for (let detail of combo.BundleDetails) {
                if (detail.qty) {
                    tmpChildStock = (mappingStock[detail.sku] - (arrMapQty[detail.sku] || 0)) / detail.qty
                    if (tmpChildStock < childStock) {
                        childStock = tmpChildStock;
                    }
                    if (combo.sku == 422203809) {
                        console.log({ mappingStock: mappingStock, child: detail.sku, arrMapQty: arrMapQty })
                    }
                }
            }
        } else {
            // End Bundle
            for (let detail of combo.BundleDetails) {
                if (detail.qty) {
                    if (commitSku == detail.sku) {
                        tmpChildStock = (mappingStock[detail.sku] - intQty) / detail.qty
                    } else {
                        tmpChildStock = mappingStock[detail.sku] / detail.qty
                    }
                    if (tmpChildStock < childStock) {
                        childStock = tmpChildStock;
                    }
                }
            }
        }

        if (childStock < currentStockCombo) {
            const intAjd = Math.ceil(currentStockCombo - childStock);
            if (intAjd) {
                arrResult.push({ 'sku': combo.sku, 'qty': intAjd })
            }

        }
    }

    return arrResult
}
module.exports = {
    getInfo,
    getConversion,
    getConversionInOut,

}