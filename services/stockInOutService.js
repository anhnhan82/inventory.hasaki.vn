const { StockInOut, StockProduct,sequelize } = require('../models')
const { unixTimeStamp } = require('../utils/time')

const bundleService = require('./bundleService')


// const stockImportExportService = require('./stockImportExportService')
const ImportExportConstant = require('../constants/importExport');


function delay(delayms) {
    return new Promise(function (resolve, reject) {
        setTimeout(resolve, delayms);
    });
}

const insert = async (param) =>{
    const result = await sequelize.transaction({ skipLocked: true }, async (tran) =>{
        let stockInOut
        let intType = 0 , intQtyOut = 0 ,intQtyIn = 0;

        if (param.committed) {
            intQtyOut = parseInt(param.committed)
        } else if (param.in_comming) {
            intQtyIn = parseInt(param.in_comming)
        }
        
        try {
            // Get current stock product
            let [stockProduct, created] = await StockProduct.findOrCreate({
                where: {
                    stockprod_stock_id: param.stock_id,
                    stockprod_sku: param.sku,
                    
                },
                defaults: {
                    stockprod_in_stock: 0,
                    stockprod_committed: 0,
                    stockprod_available: 0,
                    stockprod_in_comming: 0,
                    stockprod_utime: unixTimeStamp(),
                },
                transaction: tran,
                // lock: true,
                // skipLocked: true
            })
            // Insert log
            stockInOut = await StockInOut.create({
                stockino_stock_id: param.stock_id,
                stockino_store_id: param.store_id,
                stockino_sku: param.sku,
                stockino_committed: intQtyOut,
                stockino_out: intQtyOut,
                stockino_in_comming: intQtyIn,
                stockino_in: intQtyIn,           
                stockino_ctime: unixTimeStamp(),
                stockino_source_id: param.source_id || 0,

            }, { transaction: tran });
            
            let arrUpdate = { stockprod_utime: unixTimeStamp() }
            // Import or export
            if (param.in_comming) {
                arrUpdate.stockprod_in_stock = sequelize.literal('stockprod_in_stock + ' + param.in_comming)
                arrUpdate.stockprod_available = sequelize.literal('stockprod_available + ' + param.in_comming)
                
            } else {
                arrUpdate.stockprod_in_stock = sequelize.literal('stockprod_in_stock - ' + param.committed)
                arrUpdate.stockprod_available = sequelize.literal('stockprod_available - ' + param.committed)
                
            }
            
            // Update stock
            
            await StockProduct.update(arrUpdate, { where: { stockprod_sku: stockProduct.stockprod_sku }, transaction: tran })
            
        }catch(e){
            console.log('e.message', e.message)
            
            if (e instanceof sequelize.DatabaseError ,
            
                e.parent
                && e.parent.code === "ER_LOCK_DEADLOCK")
                await tran.cleanup();
        }
        return stockInOut;
        
    })
    return result
}

const insertV2 = async (param) =>{
    const result = await sequelize.transaction({ skipLocked: true }, async (tran) =>{
        let stockInOut
        let  intQtyOut = 0 
        let  intQtyIn = 0;

        try {
            // Get current stock product
            let [stockProduct, created] = await StockProduct.findOrCreate({

                where: {
                    stockprod_stock_id: param.stock_id,
                    stockprod_sku: param.product_sku,
                    
                },
                defaults: {
                    stockprod_in_stock: 0,
                    stockprod_committed: 0,
                    stockprod_available: 0,
                    stockprod_in_comming: 0,
                    stockprod_utime: unixTimeStamp(),
                },
                transaction: tran,
                // lock: true,
                // skipLocked: true
            })
           
            // Insert log

            stockInOut = await StockInOut.create({
                stockino_stock_id: param.stock_id,
                stockino_store_id: param.store_id,
                stockino_sku: param.product_sku,
                stockino_committed:intQtyOut ,
                stockino_out:intQtyOut ,
                stockino_in_comming: intQtyIn,
                stockino_in: intQtyIn,           
                stockino_ctime: unixTimeStamp(),
                stockino_source_id: param.source_id || 0,

            }, { transaction: tran });
            
            let arrUpdate = { stockprod_utime: unixTimeStamp() }
            // Import or export
            

            // console.log(param);




            if (param.type == 1 ) {
                arrUpdate.stockprod_in_stock = sequelize.literal('stockprod_in_stock + ' + param.qty)
                arrUpdate.stockprod_available = sequelize.literal('stockprod_available + ' + param.qty)
               
            } else {
                arrUpdate.stockprod_in_stock = sequelize.literal('stockprod_in_stock - ' + param.qty)
                arrUpdate.stockprod_available = sequelize.literal('stockprod_available - ' + param.qty)
            }
           
            // Update stock
            
            const updateted =  await StockProduct.update(arrUpdate, { where: { stockprod_sku: stockProduct.stockprod_sku }, transaction: tran })
            
            
        }catch(e){
            console.log('e.message', e.message)
            
            if (e instanceof sequelize.DatabaseError,
            
                e.parent
                && e.parent.code === "ER_LOCK_DEADLOCK")
                await tran.cleanup();
        }
        return stockInOut;
        
    })
    return result
}

const tryInsert = async (param) =>{
    let result
    try {
        let index = 1
        while (index++ < 5) {   
            result = await insert(param); 
            if (result) {
                break;
            }
            await delay(100)
        }
        
    } catch (error) {
        console.log(error)
    }
    return result
}
const tryInsertV2 = async (param) =>{
    let result
    try {
        let index = 1
        while (index++ < 5) {
            
            
            result = await insertV2(param);
            // console.log({result: result});
            if (result) {
                break;
            }
            await delay(100)
        }
        
    } catch (error) {
        console.log(error)
    }
    return result
}



const getTotalInOut = async (param) =>{
    // console.log(param);
    const arrBundle = await bundleService.getConversionInOut({ sku: param.sku, committed: param.committed, in_comming:param.in_comming, stock_id: param.stock_id ,store_id : param.store_id })
    const inOut = await tryInsert (param)
    // console.log(arrBundle);
    if (inOut && arrBundle) {
        for (let bundle of arrBundle) {
            await tryInsertV2 ({stock_id: param.stock_id ,  product_sku: bundle.sku, qty: bundle.qty , store_id : param.store_id , type: bundle.type})
        }
    }
    
    return inOut

}

module.exports = {
    getTotalInOut
}

