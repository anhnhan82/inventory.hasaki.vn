const { StockProduct } = require('../models')
const { Op } = require("sequelize")

const getList = async (param) => {
    let cond = {}
    const limit = 10, offset = 0;
    if (param.stock_id) {
        cond.stockprod_stock_id = param.stock_id
    }
    if (param.sku) {
        cond.stockprod_sku = { [Op.in]: param.sku }
    }

    return await StockProduct.findAll({ limit: limit, offset: offset, where: cond, order: [['stockprod_stock_id', 'DESC'], ['stockprod_sku', 'DESC']] })
};

module.exports = {
    getList
}