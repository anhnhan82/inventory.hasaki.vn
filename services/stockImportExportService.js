const { StockImportExport, StockProduct, sequelize } = require('../models')
const { unixTimeStamp } = require('../utils/time')

const bundleService = require('./bundleService')
const ImportExportConstant = require('../constants/importExport');
//const { IMPORT_TYPE_PURCHASE_ORDER } = require('../constants/importExport');

const getList = async (param) => {
    let cond = {}, orderBy = []
    const limit = param.limit || 10, offset = param.offset = 0;
    if (param.stock_id) {
        cond.stockimex_stock_id = param.stock_id
    }
    if (param.sku) {
        cond.stockimex_sku = param.sku
    }
    if (param.type) {
        cond.stockimex_type = param.type
    }
    if (param.user_id) {
        cond.stockimex_user_id = param.user_id
    }
    if (param.import_id) {
        cond.stockimex_import_id = param.import_id
    }
    if (param.import_type) {
        cond.stockimex_import_type = param.import_type
    }
    if (param.source) {
        cond.stockimex_source = param.source
    }
    if (!param.order_by) {
        param.order_by = stockimex_id
    }
    orderBy.push([param.order_by, 'DESC'])

    return await StockProduct.findAll({ limit: limit, offset: offset, where: cond, order: orderBy })
};

const _insertV1 = async (param) => {
    const result = await sequelize.transaction({ skipLocked: true }, async (tran) => {
        let stockImpExp
        try {
            // Get current stock product
            
            let [stockProduct, created] = await StockProduct.findOrCreate({
                where: {
                    stockprod_stock_id: param.stock_id,
                    stockprod_sku: param.product_sku,
                },
                defaults: {
                    stockprod_in_stock: 0,
                    stockprod_committed: 0,
                    stockprod_available: 0,
                    stockprod_in_comming: 0,
                    stockprod_utime: unixTimeStamp(),
                },
                transaction: tran,
                //lock: true,
                //skipLocked: true
            });
            let inStock = stockProduct.stockprod_in_stock

            // Insert log
            stockImpExp = await StockImportExport.create({
                stockimex_stock_id: param.stock_id,
                stockimex_sku: param.product_sku,
                stockimex_qty: param.qty,
                stockimex_type: param.type,
                stockimex_user_id: param.user_id,
                stockimex_import_id: param.import_id,
                stockimex_ctime: unixTimeStamp(),
                stockimex_current_in_stock: inStock,
                stockimex_import_type: param.import_type,
                stockimex_source: param.source || 0,


                




            }, { transaction: tran });
            // console.log(stockProduct);
            let arrUpdate = { stockprod_utime: unixTimeStamp() }
            // Import or export
            if (param.type == 1) {
                arrUpdate.stockprod_in_stock = sequelize.literal('stockprod_in_stock + ' + param.qty)
                arrUpdate.stockprod_available = sequelize.literal('stockprod_available + ' + param.qty)
            } else {
                arrUpdate.stockprod_in_stock = sequelize.literal('stockprod_in_stock - ' + param.qty)
                arrUpdate.stockprod_available = sequelize.literal('stockprod_available - ' + param.qty)
            }
            // Update stock
            await StockProduct.update(arrUpdate, { where: { stockprod_stock_id: stockProduct.stockprod_stock_id }, transaction: tran })
            
        } catch (e) {
            console.log('e.message', e.message)
            if (e instanceof sequelize.DatabaseError
                && e.parent
                && e.parent.code === "ER_LOCK_DEADLOCK")
                await tran.cleanup();
        }

        return stockImpExp
    });

    return result
}

const _insertV2 = async (param) => {
    const tran = await sequelize.transaction({
        //lock: true,
        //skipLocked: false,
        autocommit:false
    })
    let stockImpExp, errorMessage = 'Success'
    try {

        // Get current stock product
        let [stockProduct, created] = await StockProduct.findOrCreate({
            where: {
                stockprod_stock_id: param.stock_id,
                stockprod_sku: param.product_sku,
            },
            defaults: {
                stockprod_in_stock: 0,
                stockprod_committed: 0,
                stockprod_available: 0,
                stockprod_in_comming: 0,
                stockprod_utime: unixTimeStamp(),
            },
            transaction: tran,
            lock: tran.LOCK.UPDATE
        });
        let inStock = stockProduct.stockprod_in_stock

        // Insert log
        stockImpExp = await StockImportExport.create({
            stockimex_stock_id: param.stock_id,
            stockimex_sku: param.product_sku,
            stockimex_qty: param.qty,
            stockimex_type: param.type,
            stockimex_user_id: param.user_id,
            stockimex_import_id: param.import_id,
            stockimex_ctime: unixTimeStamp(),
            stockimex_current_in_stock: inStock,
            stockimex_import_type: param.import_type,
            stockimex_source: param.source || 0,
        }, { transaction: tran });
        let arrUpdate = { stockprod_utime: unixTimeStamp() }
        // Import or export
        if (param.type == 1) {
            arrUpdate.stockprod_in_stock = sequelize.literal('stockprod_in_stock + ' + param.qty)
            arrUpdate.stockprod_available = sequelize.literal('stockprod_available + ' + param.qty)
        } else {
            arrUpdate.stockprod_in_stock = sequelize.literal('stockprod_in_stock - ' + param.qty)
            arrUpdate.stockprod_available = sequelize.literal('stockprod_available - ' + param.qty)
        }
        // Updat stock
        await StockProduct.update(arrUpdate, { where: { stockprod_stock_id: stockProduct.stockprod_stock_id }, transaction: tran })

        // Commit
        await tran.commit();
    } catch (error) {
        errorMessage = error.message
        console.error(error)
        await tran.rollback();
    }

    //return {error: errorMessage, data: stockImpExp}
    return errorMessage

}

const _insertV3 = async (param) => {
    try {
        const result = await sequelize.transaction(async (tran) => {

            // Get current stock product
            let [stockProduct, created] = await StockProduct.findOrCreate({
                where: {
                    stockprod_stock_id: param.stock_id,
                    stockprod_sku: param.product_sku,
                },
                defaults: {
                    stockprod_in_stock: 0,
                    stockprod_committed: 0,
                    stockprod_available: 0,
                    stockprod_in_comming: 0,
                    stockprod_utime: unixTimeStamp(),
                },
                transaction: tran,
                lock: tran.LOCK.UPDATE
            });
            let inStock = stockProduct.stockprod_in_stock
            // Insert log
            const stockImpExp = await StockImportExport.create({
                stockimex_stock_id: param.stock_id,
                stockimex_sku: param.product_sku,
                stockimex_qty: param.qty,
                stockimex_type: param.type,
                stockimex_user_id: param.user_id,
                stockimex_import_id: param.import_id,
                stockimex_ctime: unixTimeStamp(),
                stockimex_current_in_stock: inStock,
                stockimex_import_type: param.import_type,
                stockimex_source: param.source || 0,
            }, { transaction: tran });
            let arrUpdate = { stockprod_utime: unixTimeStamp() }
            // Import or export
            if (param.type == 1) {
                arrUpdate.stockprod_in_stock = sequelize.literal('stockprod_in_stock + ' + param.qty)
                arrUpdate.stockprod_available = sequelize.literal('stockprod_available + ' + param.qty)
            } else {
                arrUpdate.stockprod_in_stock = sequelize.literal('stockprod_in_stock - ' + param.qty)
                arrUpdate.stockprod_available = sequelize.literal('stockprod_available - ' + param.qty)
            }
            // Updat stock
            await StockProduct.update(arrUpdate, { where: { stockprod_stock_id: stockProduct.stockprod_stock_id }, transaction: tran })

            return stockImpExp

        });
        return result
    } catch (error) {
        console.log(error)
        // If the execution reaches this line, an error occurred.
        // The transaction has already been rolled back automatically by Sequelize!
        return null
    }
}

function delay(delayms) {
    return new Promise(function (resolve, reject) {
        setTimeout(resolve, delayms);
    });
}

const _tryInsert = async (param) => {
    let result
    try {
        let index = 1
        while (index++ < 5) {
            result = await _insertV1(param);
            if (result) {
                break;
            }
            await delay(2000)
        }
        
    } catch (error) {
        console.log(error)
    }
    return result
}


const insert = async (param) => {
    const arrBundle = await bundleService.getConversion({ sku: param.product_sku, qty: param.qty, type: param.type, stock_id: param.stock_id })
    const importExport = await _tryInsert(param)
    if (importExport && arrBundle) {
        for (let bundle of arrBundle) {
            await _tryInsert({ stock_id: param.stock_id, product_sku: bundle.sku, qty: bundle.qty, type: param.type, user_id: param.user_id, import_id: param.import_id, import_type: _getImportType(param.type) })
        }
    }
    return importExport
}

const _getImportType = function (type) {
    let autoType
    switch (type) {
        case ImportExportConstant.IMPORT_TYPE_ORDER:
        case ImportExportConstant.IMPORT_TYPE_RECEIPT:
            autoType = ImportExportConstant.IMPORT_TYPE_ADJUSTMENT_COMBO
            break
        case ImportExportConstant.IMPORT_TYPE_INTERNAL_TRANSFER:
            autoType = ImportExportConstant.IMPORT_TYPE_ADJUSTMENT_INTERNAL_TRANSFER
            break
        case ImportExportConstant.IMPORT_TYPE_PURCHASE_ORDER:
            autoType = ImportExportConstant.IMPORT_TYPE_ADJUSTMENT_PURCHASE_ORDER
            break
        case ImportExportConstant.IMPORT_TYPE_USED:
            autoType = ImportExportConstant.IMPORT_TYPE_ADJUSTMENT_USED
            break
        case ImportExportConstant.IMPORT_TYPE_LOST:
            autoType = ImportExportConstant.IMPORT_TYPE_ADJUSTMENT_LOST
            break
        case ImportExportConstant.IMPORT_TYPE_SYSTEM_ERROR:
            autoType = ImportExportConstant.IMPORT_TYPE_ADJUSTMENT_SYSTEM_ERROR
            break
        case ImportExportConstant.IMPORT_TYPE_MISSING:
            autoType = ImportExportConstant.IMPORT_TYPE_ADJUSTMENT_MISSING
            break
        default:
            autoType = type
    }
    return autoType;
}




module.exports = {
    insert,
    getList,
    _tryInsert,
    
}