const assert = require('assert');

const bundleService = require('../../services/bundleService')
const { Bundle, BundleDetail, StockProduct, sequelize } = require('../../models')

async function initCombo201600075() {
    const bundle = await Bundle.create({ sku: 201600075, type: 2, description: '3 x 100150041' });
    await BundleDetail.create({ bundle_id: bundle.id, sku: 100150041, qty: 3 });
}

async function cleanUpStock() {
    await StockProduct.destroy({ where: {}, truncate: true });
}
async function initStock(arrParam) {
    for (let param of arrParam) {
        await StockProduct.create({ stockprod_stock_id: 1005, stockprod_sku: param.sku, stockprod_in_stock: param.in_stock, stockprod_in_comming: 0, stockprod_available: param.in_stock, stockprod_committed: 0, stockprod_utime: Math.floor(+new Date() / 1000)});    
    }
    
}

async function initData() {
    await initCombo201600075()
}

describe('Combo simple 3', function () {

    before(async () => {
        await initData()
        await cleanUpStock()
    })
    beforeEach(async () => {
        await cleanUpStock()
    })
    after(async () => {
        await Bundle.destroy({ where: {}, truncate: true });
        await BundleDetail.destroy({ where: {}, truncate: true });
    })
    // beforeEach('Setting up the userList', function(done){
    //     console.log('beforeEach');
    //     done()
    //   });
    describe('Combo 201600075', function () { 
        // Run one test case: yarn mocha test/unit-conversion/importCombo1.spec.js -g 'Purchase combo with qty 1'
        it('Purchase combo with qty 1', async function () {
            const arrBundle = await bundleService.getConversion({ sku: 201600075, qty: 1, type: 1, stock_id: 1005 })
            assert(Array.isArray(arrBundle), 'Bundle error');
            assert(arrBundle.length == 1, 'Only one item');
            const item = arrBundle[0]
            assert(item.qty == 3, 'Must import  3');
            assert(item.sku == 100150041, 'Must import sku 100150041');
        });
        it('Purchase combo with qty 2', async function () {
            const arrBundle = await bundleService.getConversion({ sku: 201600075, qty: 2, type: 1, stock_id: 1005 })
            assert(Array.isArray(arrBundle), 'Bundle error');
            assert(arrBundle.length == 1, 'Only one item');
            const item = arrBundle[0]
            assert(item.qty == 6, 'Must import 6');
            assert(item.sku == 100150041, 'Must import sku 100150041');
        });
        it('Purchase combo with qty 5', async function () {
            const arrBundle = await bundleService.getConversion({ sku: 201600075, qty: 5, type: 1, stock_id: 1005 })
            assert(Array.isArray(arrBundle), 'Bundle error');
            assert(arrBundle.length == 1, 'Only one item');
            const item = arrBundle[0]
            assert(item.qty == 15, 'Must import 15');
            assert(item.sku == 100150041, 'Must import sku 100150041');
        });
        
        it('Sale combo with qty 1', async function () {
            await initStock([{ sku: 100150041, in_stock: 1 }, { sku: 201600075, in_stock: 3 }])
            const arrBundle = await bundleService.getConversion({ sku: 201600075, qty: 1, type: 2, stock_id: 1005 })
            assert(Array.isArray(arrBundle), 'Bundle out error');
            assert(arrBundle.length == 1, 'One sku export ');
            assert(arrBundle[0].qty == 3, 'Must export 3 sku 100150041');
            assert(arrBundle[0].sku == 100150041, 'Must export sku 100150041'); 
        });
        it('Sale combo with qty 2', async function () {
            await initStock([{ sku: 100150041, in_stock: 8 }, { sku: 201600075, in_stock: 2 }])
            const arrBundle = await bundleService.getConversion({ sku: 201600075, qty: 2, type: 2, stock_id: 1005 })
            assert(Array.isArray(arrBundle), 'Bundle out error');
            assert(arrBundle.length == 1, 'One sku export ');
            assert(arrBundle[0].qty == 6, 'Must export 6 sku 100150041');
            assert(arrBundle[0].sku == 100150041, 'Must export sku 100150041');
        });
        it('Sale combo with qty 3', async function () {
            await initStock([{ sku: 100150041, in_stock: 10 }, { sku: 201600075, in_stock: 3 }])
            const arrBundle = await bundleService.getConversion({ sku: 201600075, qty: 3, type: 2, stock_id: 1005 })
            assert(Array.isArray(arrBundle), 'Bundle out error');
            assert(arrBundle.length == 1, 'One sku export ');
            assert(arrBundle[0].qty == 9, 'Must export 9 sku 100150041');
            assert(arrBundle[0].sku == 100150041, 'Must export sku 100150041');
        });

        it('Intertransfer combo In qty 1', async function () {
            await initStock([{ sku: 100150041, in_stock: 3 }, { sku: 201600075, in_stock: 1 }])
            const arrBundle = await bundleService.getConversion({ sku: 201600075, qty: 1, type: 1, stock_id: 1005 })
            assert(Array.isArray(arrBundle), 'Bundle out error');
            assert(arrBundle.length == 1, 'One sku in ');
            assert(arrBundle[0].qty == 3, 'Must in 3 sku 100150041');
            assert(arrBundle[0].sku == 100150041, 'Must import sku 100150041');
        });
        it('Intertransfer combo In qty 2', async function () {
            await initStock([{ sku: 100150041, in_stock: 0 }, { sku: 201600075, in_stock: 0 }])
            const arrBundle = await bundleService.getConversion({ sku: 201600075, qty: 2, type: 1, stock_id: 1005 })
            assert(Array.isArray(arrBundle), 'Bundle out error');
            assert(arrBundle.length == 1, 'One sku in ');
            assert(arrBundle[0].qty == 6, 'Must in 3 sku 100150041');
            assert(arrBundle[0].sku == 100150041, 'Must import sku 100150041');
        });
        it('Intertransfer combo In qty 5', async function () {
            await initStock([{ sku: 100150041, in_stock: 2 }, { sku: 201600075, in_stock: 0 }])
            const arrBundle = await bundleService.getConversion({ sku: 201600075, qty: 5, type: 1, stock_id: 1005 })
            assert(Array.isArray(arrBundle), 'Bundle out error');
            assert(arrBundle.length == 1, 'One sku in ');
            assert(arrBundle[0].qty == 15, 'Must in 15 sku 100150041');
            assert(arrBundle[0].sku == 100150041, 'Must import sku 100150041');
        });
        
        it('Intertransfer combo Out qty 1', async function () {
            await initStock([{ sku: 100150041, in_stock: 3 }, { sku: 201600075, in_stock: 1 }])
            const arrBundle = await bundleService.getConversion({ sku: 201600075, qty: 1, type: 2, stock_id: 1005 })
            assert(Array.isArray(arrBundle), 'Bundle out error');
            assert(arrBundle.length == 1, 'One sku out');
            assert(arrBundle[0].qty == 3, 'Must out 3 sku 100150041');
            assert(arrBundle[0].sku == 100150041, 'Must export sku 100150041');
        });
        it('Intertransfer combo Out qty 1 exist 5', async function () {
            await initStock([{ sku: 100150041, in_stock: 5 }, { sku: 201600075, in_stock: 1 }])
            const arrBundle = await bundleService.getConversion({ sku: 201600075, qty: 1, type: 2, stock_id: 1005 })
            assert(Array.isArray(arrBundle), 'Bundle out error');
            assert(arrBundle.length == 1, 'One sku out');
            assert(arrBundle[0].qty == 3, 'Must out 3 sku 100150041');
            assert(arrBundle[0].sku == 100150041, 'Must export sku 100150041');
        });
        it('Intertransfer combo Out qty 2', async function () {
            await initStock([{ sku: 100150041, in_stock: 7 }, { sku: 201600075, in_stock: 2 }])
            const arrBundle = await bundleService.getConversion({ sku: 201600075, qty: 2, type: 2, stock_id: 1005 })
            assert(Array.isArray(arrBundle), 'Bundle out error');
            assert(arrBundle.length == 1, 'One sku out');
            assert(arrBundle[0].qty == 6, 'Must out 6 sku 100150041');
            assert(arrBundle[0].sku == 100150041, 'Must export sku 100150041');
        });
        it('Intertransfer combo Out qty 5', async function () {
            await initStock([{ sku: 100150041, in_stock: 15 }, { sku: 201600075, in_stock: 5 }])
            const arrBundle = await bundleService.getConversion({ sku: 201600075, qty: 5, type: 2, stock_id: 1005 })
            assert(Array.isArray(arrBundle), 'Bundle out error');
            assert(arrBundle.length == 1, 'One sku out');
            assert(arrBundle[0].qty == 15, 'Must out 6 sku 100150041');
            assert(arrBundle[0].sku == 100150041, 'Must export sku 100150041');
        });
    })

    describe('Sku 100150041', function () { 
        it('Purchase sku 100150041 with qty 1', async function () {
            await initStock([{ sku: 100150041, in_stock: 0 }, {sku:201600075, in_stock:0}])
            const arrBundle = await bundleService.getConversion({ sku: 100150041, qty: 1, type: 1, stock_id: 1005 })
            console.log(arrBundle)
        });
        it('Purchase sku 100150041 with qty 3', async function () {
            await initStock([{ sku: 100150041, in_stock: 0 }, {sku:201600075, in_stock:0}])
            const arrBundle = await bundleService.getConversion({ sku: 100150041, qty: 3, type: 1, stock_id: 1005 })
            assert(Array.isArray(arrBundle), 'Bundle error');
            assert(arrBundle.length == 1, 'One combo import');
            assert(arrBundle[0].qty == 1, 'Must import 1 combo 201600075');
            assert(arrBundle[0].sku == 201600075, 'Must import combo 201600075'); 
        });
        it('Purchase sku 100150041 with qty 5', async function () {
            await initStock([{ sku: 100150041, in_stock: 0 }, {sku:201600075, in_stock:0}])
            const arrBundle = await bundleService.getConversion({ sku: 100150041, qty: 5, type: 1, stock_id: 1005 })
            assert(Array.isArray(arrBundle), 'Bundle error');
            assert(arrBundle.length == 1, 'One combo import');
            assert(arrBundle[0].qty == 1, 'Must import 1 combo 201600075');
            assert(arrBundle[0].sku == 201600075, 'Must import combo 201600075'); 
        });
        it('Purchase sku 100150041 with qty 3 with exist 1 item', async function () {
            await initStock([{ sku: 100150041, in_stock: 1 }, {sku:201600075, in_stock:0}])
            const arrBundle = await bundleService.getConversion({ sku: 100150041, qty: 3, type: 1, stock_id: 1005 })
            assert(Array.isArray(arrBundle), 'Bundle error');
            assert(arrBundle.length == 1, 'One combo import');
            assert(arrBundle[0].qty == 1, 'Must import 1 combo 201600075');
            assert(arrBundle[0].sku == 201600075, 'Must import combo 201600075'); 
        });
        it('Purchase sku 100150041 with qty 5 with exist 1 item', async function () {
            await initStock([{ sku: 100150041, in_stock: 1 }, {sku:201600075, in_stock: 0}])
            const arrBundle = await bundleService.getConversion({ sku: 100150041, qty: 5, type: 1, stock_id: 1005 })
            console.log(arrBundle)
            assert(Array.isArray(arrBundle), 'Bundle error');
            assert(arrBundle.length == 1, 'One combo import');
            assert(arrBundle[0].qty == 2, 'Must import 2 combo 201600075');
            assert(arrBundle[0].sku == 201600075, 'Must import combo 201600075'); 
        });
        it('Purchase sku 100150041 with qty 4 with exist 2 item', async function () {
            await initStock([{ sku: 100150041, in_stock: 2 }, {sku:201600075, in_stock: 0}])
            const arrBundle = await bundleService.getConversion({ sku: 100150041, qty: 4, type: 1, stock_id: 1005 })
            console.log(arrBundle)
            assert(Array.isArray(arrBundle), 'Bundle error');
            assert(arrBundle.length == 1, 'One combo import');
            assert(arrBundle[0].qty == 2, 'Must import 2 combo 201600075');
            assert(arrBundle[0].sku == 201600075, 'Must import combo 201600075'); 
        });

        it('Sale sku qty 1', async function () {
            await initStock([{ sku: 100150041, in_stock: 1 }, { sku: 201600075, in_stock: 0 }])
            const arrBundle = await bundleService.getConversion({ sku: 100150041, qty: 1, type: 2, stock_id: 1005 })
            assert(Array.isArray(arrBundle), 'Sku in error');
            assert(arrBundle.length == 0, 'Must out combo');
        });
        it('Sale sku qty 1 with stock 2', async function () {
            await initStock([{ sku: 100150041, in_stock: 2 }, { sku: 201600075, in_stock: 0 }])
            const arrBundle = await bundleService.getConversion({ sku: 100150041, qty: 1, type: 2, stock_id: 1005 })
            assert(Array.isArray(arrBundle), 'Sku in error');
            assert(arrBundle.length == 0, 'Must out combo');
        });
        it('Sale sku qty 1 with stock 3', async function () {
            await initStock([{ sku: 100150041, in_stock: 3 }, { sku: 201600075, in_stock: 1 }])
            const arrBundle = await bundleService.getConversion({ sku: 100150041, qty: 1, type: 2, stock_id: 1005 })
            assert(Array.isArray(arrBundle), 'Sku in error');
            assert(arrBundle.length == 1, 'Must out combo');
            assert(arrBundle[0].qty == 1, 'Must out 1 combo 201600075');
            assert(arrBundle[0].sku == 201600075, 'Must out combo 201600075');
        });

        it('Sale sku qty 1 with stock 4', async function () {
            await initStock([{ sku: 100150041, in_stock: 4 }, { sku: 201600075, in_stock: 0 }])
            const arrBundle = await bundleService.getConversion({ sku: 100150041, qty: 1, type: 2, stock_id: 1005 })
            assert(Array.isArray(arrBundle), 'Sku in error');
            assert(arrBundle.length == 0, 'Must out combo');
        });
        it('Sale sku qty 1 with stock 6', async function () {
            await initStock([{ sku: 100150041, in_stock: 6 }, { sku: 201600075, in_stock: 2 }])
            const arrBundle = await bundleService.getConversion({ sku: 100150041, qty: 1, type: 2, stock_id: 1005 })
            assert(Array.isArray(arrBundle), 'Sku in error');
            assert(arrBundle.length == 1, 'Must out combo');
            assert(arrBundle[0].qty == 1, 'Must out 1 combo 201600075');
            assert(arrBundle[0].sku == 201600075, 'Must out combo 201600075');
        });
        it('Sale sku all', async function () {
            await initStock([{ sku: 100150041, in_stock: 9 }, { sku: 201600075, in_stock: 3 }])
            const arrBundle = await bundleService.getConversion({ sku: 100150041, qty: 9, type: 2, stock_id: 1005 })
            assert(Array.isArray(arrBundle), 'Sku in error');
            assert(arrBundle.length == 1, 'Must out combo');
            assert(arrBundle[0].qty == 3, 'Must out 1 combo 201600075');
            assert(arrBundle[0].sku == 201600075, 'Must out combo 201600075');
        });

        it('Intertransfer sku  In qty 1', async function () {
            await initStock([{ sku: 100150041, in_stock: 0 }, { sku: 201600075, in_stock: 0 }])
            const arrBundle = await bundleService.getConversion({ sku: 100150041, qty: 1, type: 1, stock_id: 1005 })
            assert(Array.isArray(arrBundle), 'Sku in error');
            assert(arrBundle.length == 0, 'No combo in');
        });
        it('Intertransfer sku In qty 3', async function () {
            await initStock([{ sku: 100150041, in_stock: 0 }, { sku: 201600075, in_stock: 0 }])
            const arrBundle = await bundleService.getConversion({ sku: 100150041, qty: 3, type: 1, stock_id: 1005 })
            assert(Array.isArray(arrBundle), 'Sku in error');
            assert(arrBundle.length == 1, 'Must combo in');
            assert(arrBundle[0].qty == 1, 'Must in 1 combo 201600075');
            assert(arrBundle[0].sku == 201600075, 'Must import combo 201600075');
        });
        it('Intertransfer sku In qty 6', async function () {
            await initStock([{ sku: 100150041, in_stock: 0 }, { sku: 201600075, in_stock: 0 }])
            const arrBundle = await bundleService.getConversion({ sku: 100150041, qty: 6, type: 1, stock_id: 1005 })
            assert(Array.isArray(arrBundle), 'Sku in error');
            assert(arrBundle.length == 1, 'Must combo in');
            assert(arrBundle[0].qty == 2, 'Must in 1 combo 201600075');
            assert(arrBundle[0].sku == 201600075, 'Must import combo 201600075');
        });
        it('Intertransfer sku  In qty 1 exist 2', async function () {
            await initStock([{ sku: 100150041, in_stock: 2 }, { sku: 201600075, in_stock: 0 }])
            const arrBundle = await bundleService.getConversion({ sku: 100150041, qty: 1, type: 1, stock_id: 1005 })
            assert(Array.isArray(arrBundle), 'Sku in error');
            assert(arrBundle.length == 1, 'Must combo in');
            assert(arrBundle[0].qty == 1, 'Must in 1 combo 201600075');
            assert(arrBundle[0].sku == 201600075, 'Must import combo 201600075');
        });
        it('Intertransfer sku  In qty 5 exist 2', async function () {
            await initStock([{ sku: 100150041, in_stock: 2 }, { sku: 201600075, in_stock: 0 }])
            const arrBundle = await bundleService.getConversion({ sku: 100150041, qty: 5, type: 1, stock_id: 1005 })
            assert(Array.isArray(arrBundle), 'Sku in error');
            assert(arrBundle.length == 1, 'Must combo in');
            assert(arrBundle[0].qty == 2, 'Must in 2 combo 201600075');
            assert(arrBundle[0].sku == 201600075, 'Must import combo 201600075');
        });

        it('Intertransfer sku Out qty 1', async function () {
            await initStock([{ sku: 100150041, in_stock: 3 }, { sku: 201600075, in_stock: 1 }])
            const arrBundle = await bundleService.getConversion({ sku: 100150041, qty: 1, type: 2, stock_id: 1005 })
            assert(Array.isArray(arrBundle), 'Sku in error');
            assert(arrBundle.length == 1, 'Must out combo');
            assert(arrBundle[0].qty == 1, 'Must in 2 combo 201600075');
            assert(arrBundle[0].sku == 201600075, 'Must import combo 201600075');
        });
        it('Intertransfer sku Out qty 3', async function () {
            await initStock([{ sku: 100150041, in_stock: 6 }, { sku: 201600075, in_stock: 2 }])
            const arrBundle = await bundleService.getConversion({ sku: 100150041, qty: 3, type: 2, stock_id: 1005 })
            assert(Array.isArray(arrBundle), 'Sku out error');
            assert(arrBundle.length == 1, 'Must out combo');
            assert(arrBundle[0].qty == 1, 'Must in 2 combo 201600075');
            assert(arrBundle[0].sku == 201600075, 'Must import combo 201600075');
        });
        it('Intertransfer sku Out qty 1 with stock 4', async function () {
            await initStock([{ sku: 100150041, in_stock: 4 }, { sku: 201600075, in_stock: 1 }])
            const arrBundle = await bundleService.getConversion({ sku: 100150041, qty: 1, type: 2, stock_id: 1005 })
            assert(Array.isArray(arrBundle), 'Sku out error');
            assert(arrBundle.length == 0, 'Must no combo');
        });
        it('Intertransfer sku Out qty 2 with stock 4', async function () {
            await initStock([{ sku: 100150041, in_stock: 4 }, { sku: 201600075, in_stock: 1 }])
            const arrBundle = await bundleService.getConversion({ sku: 100150041, qty: 2, type: 2, stock_id: 1005 })
            assert(Array.isArray(arrBundle), 'Sku out error');
            assert(arrBundle.length == 1, 'Must out combo');
            assert(arrBundle[0].qty == 1, 'Must in 2 combo 201600075');
            assert(arrBundle[0].sku == 201600075, 'Must import combo 201600075');
        });
        it('Intertransfer sku Out qty 1 with stock 6', async function () {
            await initStock([{ sku: 100150041, in_stock: 6 }, { sku: 201600075, in_stock: 2}])
            const arrBundle = await bundleService.getConversion({ sku: 100150041, qty: 1, type: 2, stock_id: 1005 })
            assert(Array.isArray(arrBundle), 'Sku out error');
            assert(arrBundle.length == 1, 'Must out combo');
            assert(arrBundle[0].qty == 1, 'Must in 2 combo 201600075');
            assert(arrBundle[0].sku == 201600075, 'Must import combo 201600075');
        });
        it('Intertransfer sku Out all', async function () {
            await initStock([{ sku: 100150041, in_stock: 9 }, { sku: 201600075, in_stock: 3}])
            const arrBundle = await bundleService.getConversion({ sku: 100150041, qty: 9, type: 2, stock_id: 1005 })
            console.log(arrBundle)
            assert(Array.isArray(arrBundle), 'Sku out error');
            assert(arrBundle.length == 1, 'Must out combo');
            assert(arrBundle[0].qty == 3, 'Must in 2 combo 201600075');
            assert(arrBundle[0].sku == 201600075, 'Must import combo 201600075');
        });
    })
})