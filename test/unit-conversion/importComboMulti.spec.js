const assert = require('assert');
const ImportExportConstant = require('../../constants/importExport');
const { Bundle, BundleDetail, StockProduct, sequelize } = require('../../models')
const bundleService = require('../../services/bundleService')

const SKU_COMBO_1 = 422203807
const SKU_COMBO_2 = 422203809
const SKU_COMBO_3 = 422203776

const SKU_CHILD = 243300007


async function initCombo() {
    const bundle = await Bundle.create({ sku: SKU_COMBO_1, type: 2, description: '2 x 243300007'});
    await BundleDetail.create({ bundle_id: bundle.id, sku: SKU_CHILD, qty: 2 });
    const bundle2 = await Bundle.create({ sku: SKU_COMBO_2, type: 2, description: '3 x 243300007'});
    await BundleDetail.create({ bundle_id: bundle2.id, sku: SKU_CHILD, qty: 3 });
    const bundle3 = await Bundle.create({ sku: SKU_COMBO_3, type: 2, description: '4 x 243300007'});
    await BundleDetail.create({ bundle_id: bundle3.id, sku: SKU_CHILD, qty: 4 });
}

async function initData() {
    await initCombo()
}

async function cleanUpStock() {
    await StockProduct.destroy({ where: {}, truncate: true });
}
async function initStock(arrParam) {
    for (let param of arrParam) {
        await StockProduct.create({ stockprod_stock_id: 1005, stockprod_sku: param.sku, stockprod_in_stock: param.in_stock, stockprod_in_comming: 0, stockprod_available: param.in_stock, stockprod_committed: 0, stockprod_utime: Math.floor(+new Date() / 1000)});    
    }
    
}


describe('Combo multi combo', function () {

    before(async () => {
        await initData()
    })
    beforeEach(async () => {
        await cleanUpStock()
    })
    after(async () => {
        await Bundle.destroy({ where: {}, truncate: true });
        await BundleDetail.destroy({ where: {}, truncate: true });
    })

    // Run one test case: yarn mocha test/unit-conversion/importComboMulti.spec.js -g 'Purchase 422203807 qty 1'
    describe('Combo 422203807', function () { 
        describe('Purchase 422203807', function () { 
            it('Purchase 422203807 qty 1', async function () {
                await initStock([{ sku: SKU_COMBO_1, in_stock: 0 }, { sku: SKU_COMBO_2, in_stock: 0 }, { sku: SKU_COMBO_3, in_stock: 0 } , { sku: SKU_CHILD, in_stock: 0 }])
                const arrBundle = await bundleService.getConversion({ sku: SKU_COMBO_1, qty: 1, type: ImportExportConstant.TYPE_IMPORT, stock_id: 1005 })
                console.log(arrBundle)
                assert(Array.isArray(arrBundle), 'Bundle error');
                const child = arrBundle.find(item => item.sku === SKU_CHILD)
                assert(child, 'Child 243300007 must import');
                assert(child.qty == 2, 'Must import 2');
            });
            // import 1 combo 422203807 -> 2 x 243300007 + exist 1 = 3  ==> 1 x 422203809(3), 0 x 422203776(4)
            it('Purchase 422203807 qty 1 exist 243300007', async function () {
                await initStock([{ sku: SKU_COMBO_1, in_stock: 0 }, { sku: SKU_COMBO_2, in_stock: 0 }, { sku: SKU_COMBO_3, in_stock: 0 } , { sku: SKU_CHILD, in_stock: 1 }])
                const arrBundle = await bundleService.getConversion({ sku: SKU_COMBO_1, qty: 1, type: ImportExportConstant.TYPE_IMPORT, stock_id: 1005 })
                console.log(arrBundle)
                assert(Array.isArray(arrBundle), 'Bundle error');
                const child = arrBundle.find(item => item.sku === SKU_CHILD)
                assert(child, 'Child 243300007 must import');
                assert(child.qty == 2, 'Must import 2');

                // Import 1 combo 422203809
                const combo2 = arrBundle.find(item => item.sku === SKU_COMBO_2)
                assert(combo2, 'Combo 422203809 must import');
                assert(combo2.qty == 1, 'Must import 1 combo 422203809');
                // No import combo 422203776
                const combo3 = arrBundle.find(item => item.sku === SKU_COMBO_3)
                assert(!combo3, 'No import combo 422203776');
            });

            // import 2 combo 422203807 -> 6 x 243300007 ==> 1 x 422203809(3), 1 x 422203776(4)
            it('Purchase 422203807 qty 2', async function () {
                await initStock([{ sku: SKU_COMBO_1, in_stock: 0 }, { sku: SKU_COMBO_2, in_stock: 0 }, { sku: SKU_COMBO_3, in_stock: 0 } , { sku: SKU_CHILD, in_stock: 0 }])
                const arrBundle = await bundleService.getConversion({ sku: SKU_COMBO_1, qty: 2, type: ImportExportConstant.TYPE_IMPORT, stock_id: 1005 })
                assert(Array.isArray(arrBundle), 'Bundle error');
                const child = arrBundle.find(item => item.sku === SKU_CHILD)
                const combo2 = arrBundle.find(item => item.sku === SKU_COMBO_2)
                const combo3 = arrBundle.find(item => item.sku === SKU_COMBO_3)
                assert(child, 'Child 243300007 must import');
                assert(child.qty == 4, 'Must import 4 child');

                assert(combo2, 'Combo 422203809 must import');
                assert(combo2.qty == 1, 'Must import 1 combo 422203809');

                assert(combo3, 'Combo 422203776 must import');
                assert(combo3.qty == 1, 'Must import 1 combo 422203776');
            });

            // import 7 combo 422203807 -> 14 x 243300007 + exist 1 243300007 = 15 ==> 5 x 422203809(3), 3 x 422203776(4)
            it('Purchase 422203807 qty 7 exist 1 243300007', async function () {
                await initStock([{ sku: SKU_COMBO_1, in_stock: 0 }, { sku: SKU_COMBO_2, in_stock: 0 }, { sku: SKU_COMBO_3, in_stock: 0 } , { sku: SKU_CHILD, in_stock: 1 }])
                const arrBundle = await bundleService.getConversion({ sku: SKU_COMBO_1, qty: 7, type: ImportExportConstant.TYPE_IMPORT, stock_id: 1005 })
                console.log(arrBundle)
                assert(Array.isArray(arrBundle), 'Bundle error');
                const child = arrBundle.find(item => item.sku === SKU_CHILD)
                const combo2 = arrBundle.find(item => item.sku === SKU_COMBO_2)
                const combo3 = arrBundle.find(item => item.sku === SKU_COMBO_3)
                assert(child, 'Child 243300007 must import');
                assert(child.qty == 14, 'Must import 14 child');

                assert(combo2, 'Combo 422203809 must import');
                assert(combo2.qty == 5, 'Must import 5 combo 422203809');

                assert(combo3, 'Combo 422203776 must import');
                assert(combo3.qty == 3, 'Must import 3 combo 422203776');
            });

            // import 4 combo 422203807 -> 8 x 243300007 + exist 1 243300007 = 9 ==> 3 x 422203809(3), 2 x 422203776(4)
            it('Purchase 422203807 qty 4 exist 1 243300007', async function () {
                await initStock([{ sku: SKU_COMBO_1, in_stock: 0 }, { sku: SKU_COMBO_2, in_stock: 0 }, { sku: SKU_COMBO_3, in_stock: 0 } , { sku: SKU_CHILD, in_stock: 1 }])
                const arrBundle = await bundleService.getConversion({ sku: SKU_COMBO_1, qty: 4, type: ImportExportConstant.TYPE_IMPORT, stock_id: 1005 })
                console.log(arrBundle)
                assert(Array.isArray(arrBundle), 'Bundle error');
                const child = arrBundle.find(item => item.sku === SKU_CHILD)
                const combo2 = arrBundle.find(item => item.sku === SKU_COMBO_2)
                const combo3 = arrBundle.find(item => item.sku === SKU_COMBO_3)
                assert(child, 'Child 243300007 must import');
                assert(child.qty == 8, 'Must import 8 child');

                assert(combo2, 'Combo 422203809 must import');
                assert(combo2.qty == 3, 'Must import 3 combo 422203809');

                assert(combo3, 'Combo 422203776 must import');
                assert(combo3.qty == 2, 'Must import 2 combo 422203776');
            });

            // Sale 1 422203807 --> out 2 sku 243300007 ==> out 1 x 422203809(3), 1 422203776(4)
            it('Sale 422203807 qty 1', async function () {
                await initStock([{ sku: SKU_COMBO_1, in_stock: 1 }, { sku: SKU_COMBO_2, in_stock: 1 }, { sku: SKU_COMBO_3, in_stock: 1 } , { sku: SKU_CHILD, in_stock: 4 }])
                const arrBundle = await bundleService.getConversion({ sku: SKU_COMBO_1, qty: 1, type: ImportExportConstant.TYPE_EXPORT, stock_id: 1005 })
                console.log(arrBundle)
                assert(Array.isArray(arrBundle), 'Bundle error');
                const child = arrBundle.find(item => item.sku === SKU_CHILD)
                const combo2 = arrBundle.find(item => item.sku === SKU_COMBO_2)
                const combo3 = arrBundle.find(item => item.sku === SKU_COMBO_3)
                assert(child, 'Child 243300007 must export');
                assert(child.qty == 2, 'Must export 2 child');

                assert(combo2, 'Combo 422203809 must export');
                assert(combo2.qty == 1, 'Must export 1 combo 422203809');

                assert(combo3, 'Combo 422203776 must import');
                assert(combo3.qty == 1, 'Must export 1 combo 422203776');
            });

            // Sale 2  243300007 --> out 4 sku 243300007 --> stock 11 => out 2 x 422203809(3), 1 x 422203776(4)
            it('Sale 422203807 qty 2 exist 15 243300007', async function () {
                await initStock([{ sku: SKU_COMBO_1, in_stock: 7 }, { sku: SKU_COMBO_2, in_stock: 5 }, { sku: SKU_COMBO_3, in_stock: 3 } , { sku: SKU_CHILD, in_stock: 15 }])
                const arrBundle = await bundleService.getConversion({ sku: SKU_COMBO_1, qty: 2, type: ImportExportConstant.TYPE_EXPORT, stock_id: 1005 })
                assert(Array.isArray(arrBundle), 'Bundle error');
                const child = arrBundle.find(item => item.sku === SKU_CHILD)
                const combo2 = arrBundle.find(item => item.sku === SKU_COMBO_2)
                const combo3 = arrBundle.find(item => item.sku === SKU_COMBO_3)
                assert(child, 'Child 243300007 must export');
                assert(child.qty == 4, 'Must export 4 child');

                assert(combo2, 'Combo 422203809 must export');
                assert(combo2.qty == 2, 'Must export 2 combo 422203809');

                assert(combo3, 'Combo 422203776 must export');
                assert(combo3.qty == 1, 'Must export 1 combo 422203776');
            });

            // Sale 7 combo 243300007 --> out 14 sku 243300007 --> stock 1 => out 5 x 422203809(3), 3 x 422203776(4)
            it('Sale 422203807 all', async function () {
                await initStock([{ sku: SKU_COMBO_1, in_stock: 7 }, { sku: SKU_COMBO_2, in_stock: 5 }, { sku: SKU_COMBO_3, in_stock: 3 } , { sku: SKU_CHILD, in_stock: 15 }])
                const arrBundle = await bundleService.getConversion({ sku: SKU_COMBO_1, qty: 7, type: ImportExportConstant.TYPE_EXPORT, stock_id: 1005 })
                assert(Array.isArray(arrBundle), 'Bundle error');
                const child = arrBundle.find(item => item.sku === SKU_CHILD)
                const combo2 = arrBundle.find(item => item.sku === SKU_COMBO_2)
                const combo3 = arrBundle.find(item => item.sku === SKU_COMBO_3)
                assert(child, 'Child 243300007 must export');
                assert(child.qty == 14, 'Must export 4 child');

                assert(combo2, 'Combo 422203809 must export');
                assert(combo2.qty == 5, 'Must export 2 combo 422203809');

                assert(combo3, 'Combo 422203776 must export');
                assert(combo3.qty == 3, 'Must export 1 combo 422203776');
            });
        })

        describe('Purchase 422203809', function () { 
            it('Purchase 422203809 qty 1', async function () {
                await initStock([{ sku: SKU_COMBO_1, in_stock: 0 }, { sku: SKU_COMBO_2, in_stock: 0 }, { sku: SKU_COMBO_3, in_stock: 0 } , { sku: SKU_CHILD, in_stock: 0 }])
                const arrBundle = await bundleService.getConversion({ sku: SKU_COMBO_2, qty: 1, type: ImportExportConstant.TYPE_IMPORT, stock_id: 1005 })
                console.log(arrBundle)
                assert(Array.isArray(arrBundle), 'Bundle error');
                const child = arrBundle.find(item => item.sku === SKU_CHILD)
                const combo1 = arrBundle.find(item => item.sku === SKU_COMBO_1)
                const combo2 = arrBundle.find(item => item.sku === SKU_COMBO_2)
                assert(child, 'Child 243300007 must import');
                assert(child.qty == 3, 'Must import 3');

                assert(combo1, 'Combo 422203807 must import');
                assert(combo1.qty == 1, 'Must import 1');

                assert(!combo2, 'No import 422203776');
            });

            // Buy 1 x 422203809 -> in 3 x 243300007 --> 4 ==> in 2 x 422203807(2), 1 x 422203776(4)
            it('Purchase 422203809 qty 1 exist 1 243300007', async function () {
                await initStock([{ sku: SKU_COMBO_1, in_stock: 0 }, { sku: SKU_COMBO_2, in_stock: 0 }, { sku: SKU_COMBO_3, in_stock: 0 } , { sku: SKU_CHILD, in_stock: 1 }])
                const arrBundle = await bundleService.getConversion({ sku: SKU_COMBO_2, qty: 1, type: ImportExportConstant.TYPE_IMPORT, stock_id: 1005 })
                console.log(arrBundle)
                assert(Array.isArray(arrBundle), 'Bundle error');
                const child = arrBundle.find(item => item.sku === SKU_CHILD)
                const combo1 = arrBundle.find(item => item.sku === SKU_COMBO_1)
                const combo2 = arrBundle.find(item => item.sku === SKU_COMBO_3)
                assert(child, 'Child 243300007 must import');
                assert(child.qty == 3, 'Must import 3');

                // 2 combo 2
                assert(combo1, 'Combo 422203807 must import');
                assert(combo1.qty == 2, 'Must import 2');

                // 1 combo 4
                assert(combo2, 'Combo 422203776 must import');
                assert(combo2.qty == 1, 'Must import 1');
            });

            // Buy 4 x 422203809 -> in 3 x 243300007 --> 12 + 2 ==> in 6 x 422203807(2), 3 x 422203776(4)
            it('Purchase 422203809 qty 4 exist 1 combo 422203807', async function () {
                await initStock([{ sku: SKU_COMBO_1, in_stock: 1 }, { sku: SKU_COMBO_2, in_stock: 0 }, { sku: SKU_COMBO_3, in_stock: 0 } , { sku: SKU_CHILD, in_stock: 2 }])
                const arrBundle = await bundleService.getConversion({ sku: SKU_COMBO_2, qty: 4, type: ImportExportConstant.TYPE_IMPORT, stock_id: 1005 })
                console.log(arrBundle)
                assert(Array.isArray(arrBundle), 'Bundle error');
                const child = arrBundle.find(item => item.sku === SKU_CHILD)
                const combo1 = arrBundle.find(item => item.sku === SKU_COMBO_1)
                const combo2 = arrBundle.find(item => item.sku === SKU_COMBO_3)
                assert(child, 'Child 243300007 must import');
                assert(child.qty == 12, 'Must import 12');

                // 2 combo 2
                assert(combo1, 'Combo 422203807 must import');
                assert(combo1.qty == 6, 'Must import 6');

                // 1 combo 4
                assert(combo2, 'Combo 422203776 must import');
                assert(combo2.qty == 3, 'Must import 3');
            });
            // Buy 6 x 422203809 -> in 3 x 243300007 --> 18 + 2 ==> in 9 x 422203807(2), 5 x 422203776(4)
            it('Purchase 422203809 qty 6 exist 1 combo 422203807', async function () {
                await initStock([{ sku: SKU_COMBO_1, in_stock: 1 }, { sku: SKU_COMBO_2, in_stock: 0 }, { sku: SKU_COMBO_3, in_stock: 0 } , { sku: SKU_CHILD, in_stock: 2 }])
                const arrBundle = await bundleService.getConversion({ sku: SKU_COMBO_2, qty: 6, type: ImportExportConstant.TYPE_IMPORT, stock_id: 1005 })
                console.log(arrBundle)
                assert(Array.isArray(arrBundle), 'Bundle error');
                const child = arrBundle.find(item => item.sku === SKU_CHILD)
                const combo1 = arrBundle.find(item => item.sku === SKU_COMBO_1)
                const combo2 = arrBundle.find(item => item.sku === SKU_COMBO_3)
                assert(child, 'Child 243300007 must import');
                assert(child.qty == 18, 'Must import 18');

                // 2 combo 2
                assert(combo1, 'Combo 422203807 must import');
                assert(combo1.qty == 9, 'Must import 9');

                // 1 combo 4
                assert(combo2, 'Combo 422203776 must import');
                assert(combo2.qty == 5, 'Must import 5');
            });

            // Sale 1 x 422203809 -> out 3 243300007--> avail 3 243300007 ==>  out 2 x 422203807, 1 x 422203776
            it('Sale 1 422203809', async function () {
                await initStock([{ sku: SKU_COMBO_1, in_stock: 3 }, { sku: SKU_COMBO_2, in_stock: 2 }, { sku: SKU_COMBO_3, in_stock: 1 } , { sku: SKU_CHILD, in_stock: 6 }])
                const arrBundle = await bundleService.getConversion({ sku: SKU_COMBO_2, qty: 1, type: ImportExportConstant.TYPE_EXPORT, stock_id: 1005 })
                console.log(arrBundle)
                assert(Array.isArray(arrBundle), 'Bundle error');
                const child = arrBundle.find(item => item.sku === SKU_CHILD)
                const combo1 = arrBundle.find(item => item.sku === SKU_COMBO_1)
                const combo2 = arrBundle.find(item => item.sku === SKU_COMBO_3)
                assert(child, 'Child 243300007 must export');
                assert(child.qty == 3, 'Must export 3');

                // 2 combo 2
                assert(combo1, 'Combo 422203807 must export');
                assert(combo1.qty == 2, 'Must export 2');

                // 1 combo 4
                assert(combo2, 'Combo 422203776 must export');
                assert(combo2.qty == 1, 'Must export 2');
            });

            // sale 6 x 422203809 -> out 18 243300007 --> avail 2 x 243300007 ==> out 18 x 243300007, 9 x 422203807, 5 x 422203776
            it('Sale all 422203809', async function () {
                await initStock([{ sku: SKU_COMBO_1, in_stock: 10 }, { sku: SKU_COMBO_2, in_stock: 6 }, { sku: SKU_COMBO_3, in_stock: 5 } , { sku: SKU_CHILD, in_stock: 20 }])
                const arrBundle = await bundleService.getConversion({ sku: SKU_COMBO_2, qty: 6, type: ImportExportConstant.TYPE_EXPORT, stock_id: 1005 })
                console.log(arrBundle)
                assert(Array.isArray(arrBundle), 'Bundle error');
                const child = arrBundle.find(item => item.sku === SKU_CHILD)
                const combo1 = arrBundle.find(item => item.sku === SKU_COMBO_1)
                const combo2 = arrBundle.find(item => item.sku === SKU_COMBO_3)
                assert(child, 'Child 243300007 must export');
                assert(child.qty == 18, 'Must export 18');

                // 2 combo 2
                assert(combo1, 'Combo 422203807 must export');
                assert(combo1.qty == 9, 'Must export 9');

                // 1 combo 4
                assert(combo2, 'Combo 422203776 must export');
                assert(combo2.qty == 5, 'Must export 5');
            });

            // Sale 8 x 422203809 -> out 24 x 243300007 ==> out 24 x 243300007, 12 x 422203807, 6 x 422203776
            it('Sale over 422203809', async function () {
                await initStock([{ sku: SKU_COMBO_1, in_stock: 10 }, { sku: SKU_COMBO_2, in_stock: 6 }, { sku: SKU_COMBO_3, in_stock: 5 } , { sku: SKU_CHILD, in_stock: 20 }])
                const arrBundle = await bundleService.getConversion({ sku: SKU_COMBO_2, qty: 8, type: ImportExportConstant.TYPE_EXPORT, stock_id: 1005 })
                console.log(arrBundle)
                assert(Array.isArray(arrBundle), 'Bundle error');
                const child = arrBundle.find(item => item.sku === SKU_CHILD)
                const combo1 = arrBundle.find(item => item.sku === SKU_COMBO_1)
                const combo2 = arrBundle.find(item => item.sku === SKU_COMBO_3)
                assert(child, 'Child 243300007 must export');
                assert(child.qty == 24, 'Must export 24');

                // 2 combo 2
                assert(combo1, 'Combo 422203807 must export');
                assert(combo1.qty == 12, 'Must export 12');

                // 1 combo 4
                assert(combo2, 'Combo 422203776 must export');
                assert(combo2.qty == 6, 'Must export 6');
            });
        })
        
        

    })
    
})