const assert = require('assert');
const ImportExportConstant = require('../../constants/importExport');
const { Bundle, BundleDetail, StockProduct, sequelize, StockImportExport } = require('../../models')
const bundleService = require('../../services/bundleService')

const SKU_COMBO = 204900015
const SKU_CHILD_1 = 100160018
const SKU_CHILD_2 = 204900005

async function initCombo204900015() {
    const bundle = await Bundle.create({ sku: SKU_COMBO, type: 2, description: '1 x 100160018, 1 x 204900005'});
    await BundleDetail.create({ bundle_id: bundle.id, sku: SKU_CHILD_1, qty: 1 });
    await BundleDetail.create({ bundle_id: bundle.id, sku: SKU_CHILD_2, qty: 1 });
}

async function initData() {
    await initCombo204900015()
}

async function cleanUpStock() {
    await StockProduct.destroy({ where: {}, truncate: true });
    await StockImportExport.destroy({ where: {}, truncate: true });
}
async function initStock(arrParam) {
    for (let param of arrParam) {
        await StockProduct.create({ stockprod_stock_id: 1005, stockprod_sku: param.sku, stockprod_in_stock: param.in_stock, stockprod_in_comming: 0, stockprod_available: param.in_stock, stockprod_committed: 0, stockprod_utime: Math.floor(+new Date() / 1000)});    
    }
    
}


describe('Combo (1 x 100160018, 1 x 204900005)', function () {

    before(async () => {
        await initData()
    })
    beforeEach(async () => {
        await cleanUpStock()
    })
    after(async () => {
        await Bundle.destroy({ where: {}, truncate: true });
        await BundleDetail.destroy({ where: {}, truncate: true });
    })

    describe('Combo 204900015', function () { 
        // Run one test case: yarn mocha test/unit-conversion/importCombo2.spec.js -g 'Purchase 204900015 qty 1'
        it('Purchase 204900015 qty 1', async function () {
            await initStock([{ sku: SKU_COMBO, in_stock: 1 }, { sku: SKU_CHILD_1, in_stock: 1 }, { sku: SKU_CHILD_2, in_stock: 1 }])
            const arrBundle = await bundleService.getConversion({ sku: SKU_COMBO, qty: 1, type: ImportExportConstant.TYPE_IMPORT, stock_id: 1005 })
            assert(Array.isArray(arrBundle), 'Bundle error');
            assert(arrBundle.length == 2, 'Must import 2 childs');
            const child1 = arrBundle.find(item => item.sku === SKU_CHILD_1)
            const child2 = arrBundle.find(item => item.sku === SKU_CHILD_2)
            assert(child1, 'Child ' + SKU_CHILD_1 + ' must import');
            assert(child2, 'Child ' + SKU_CHILD_2 + ' must import');

            assert(child1.qty == 1, 'Must import 1');
            assert(child1.sku == SKU_CHILD_1, 'Must import SKU_CHILD_1');

            assert(child2.qty == 1, 'Must import 1');
            assert(child2.sku == SKU_CHILD_2, 'Must import SKU_CHILD_2');
        });

        it('Purchase 204900015 qty 3', async function () {
            await initStock([{ sku: SKU_COMBO, in_stock: 0 }, { sku: SKU_CHILD_1, in_stock: 0 }, { sku: SKU_CHILD_2, in_stock: 0 }])
            const arrBundle = await bundleService.getConversion({ sku: SKU_COMBO, qty: 3, type: ImportExportConstant.TYPE_IMPORT, stock_id: 1005 })
            assert(Array.isArray(arrBundle), 'Bundle error');
            assert(arrBundle.length == 2, 'Must import 2 childs');
            const child1 = arrBundle.find(item => item.sku === SKU_CHILD_1)
            const child2 = arrBundle.find(item => item.sku === SKU_CHILD_2)
            assert(child1, 'Child ' + SKU_CHILD_1 + ' must import');
            assert(child2, 'Child ' + SKU_CHILD_2 + ' must import');

            assert(child1.qty == 3, 'Must import 2');
            assert(child1.sku == SKU_CHILD_1, 'Must import SKU_CHILD_1');

            assert(child2.qty == 3, 'Must import 2');
            assert(child2.sku == SKU_CHILD_2, 'Must import SKU_CHILD_2');
        });

        it('Sale 204900015 qty 1', async function () {
            await initStock([{ sku: SKU_COMBO, in_stock: 5 }, { sku: SKU_CHILD_1, in_stock: 5 }, { sku: SKU_CHILD_2, in_stock: 5 }])
            const arrBundle = await bundleService.getConversion({ sku: SKU_COMBO, qty: 1, type: ImportExportConstant.TYPE_EXPORT, stock_id: 1005 })
            assert(Array.isArray(arrBundle), 'Bundle error');
            assert(arrBundle.length == 2, 'Must export 2 childs');
            const child1 = arrBundle.find(item => item.sku === SKU_CHILD_1)
            const child2 = arrBundle.find(item => item.sku === SKU_CHILD_2)
            assert(child1, 'Child ' + SKU_CHILD_1 + ' must export');
            assert(child2, 'Child ' + SKU_CHILD_2 + ' must export');

            assert(child1.qty == 1, 'Must import 1');
            assert(child1.sku == SKU_CHILD_1, 'Must export SKU_CHILD_1');

            assert(child2.qty == 1, 'Must import 1');
            assert(child2.sku == SKU_CHILD_2, 'Must export SKU_CHILD_2');
        });
        
        it('Sale 204900015 qty 3', async function () {
            await initStock([{ sku: SKU_COMBO, in_stock: 3 }, { sku: SKU_CHILD_1, in_stock: 3 }, { sku: SKU_CHILD_2, in_stock: 3 }])
            const arrBundle = await bundleService.getConversion({ sku: SKU_COMBO, qty: 3, type: ImportExportConstant.TYPE_EXPORT, stock_id: 1005 })
            assert(Array.isArray(arrBundle), 'Bundle error');
            assert(arrBundle.length == 2, 'Must export 2 childs');
            const child1 = arrBundle.find(item => item.sku === SKU_CHILD_1)
            const child2 = arrBundle.find(item => item.sku === SKU_CHILD_2)
            assert(child1, 'Child ' + SKU_CHILD_1 + ' must export');
            assert(child2, 'Child ' + SKU_CHILD_2 + ' must export');

            assert(child1.qty == 3, 'Must import 1');
            assert(child1.sku == SKU_CHILD_1, 'Must export SKU_CHILD_1');

            assert(child2.qty == 3, 'Must import 1');
            assert(child2.sku == SKU_CHILD_2, 'Must export SKU_CHILD_2');
        });

        it('Sale 204900015 qty 3 exist 6 100160018', async function () {
            await initStock([{ sku: SKU_COMBO, in_stock: 3 }, { sku: SKU_CHILD_1, in_stock: 3 }, { sku: SKU_CHILD_2, in_stock: 3 }])
            const arrBundle = await bundleService.getConversion({ sku: SKU_COMBO, qty: 3, type: ImportExportConstant.TYPE_EXPORT, stock_id: 1005 })
            assert(Array.isArray(arrBundle), 'Bundle error');
            assert(arrBundle.length == 2, 'Must export 2 childs');
            const child1 = arrBundle.find(item => item.sku === SKU_CHILD_1)
            const child2 = arrBundle.find(item => item.sku === SKU_CHILD_2)
            assert(child1, 'Child ' + SKU_CHILD_1 + ' must export');
            assert(child2, 'Child ' + SKU_CHILD_2 + ' must export');

            assert(child1.qty == 3, 'Must import 1');
            assert(child1.sku == SKU_CHILD_1, 'Must export SKU_CHILD_1');

            assert(child2.qty == 3, 'Must import 1');
            assert(child2.sku == SKU_CHILD_2, 'Must export SKU_CHILD_2');
        });

        it('Internal out 204900015 qty 1', async function () {
            await initStock([{ sku: SKU_COMBO, in_stock: 10 }, { sku: SKU_CHILD_1, in_stock: 5 }, { sku: SKU_CHILD_2, in_stock: 5 }])
            const arrBundle = await bundleService.getConversion({ sku: SKU_COMBO, qty: 1, type: ImportExportConstant.TYPE_EXPORT, stock_id: 1005 })
            assert(Array.isArray(arrBundle), 'Bundle error');
            assert(arrBundle.length == 2, 'Must export 2 childs');
            const child1 = arrBundle.find(item => item.sku === SKU_CHILD_1)
            const child2 = arrBundle.find(item => item.sku === SKU_CHILD_2)
            assert(child1, 'Child ' + SKU_CHILD_1 + ' must export');
            assert(child2, 'Child ' + SKU_CHILD_2 + ' must export');

            assert(child1.qty == 1, 'Must import 1');
            assert(child1.sku == SKU_CHILD_1, 'Must export SKU_CHILD_1');

            assert(child2.qty == 1, 'Must import 1');
            assert(child2.sku == SKU_CHILD_2, 'Must export SKU_CHILD_2');
        });

        it('Internal out 204900015 qty 3', async function () {
            await initStock([{ sku: SKU_COMBO, in_stock: 6 }, { sku: SKU_CHILD_1, in_stock: 3 }, { sku: SKU_CHILD_2, in_stock: 3 }])
            const arrBundle = await bundleService.getConversion({ sku: SKU_COMBO, qty: 3, type: ImportExportConstant.TYPE_EXPORT, stock_id: 1005 })
            assert(Array.isArray(arrBundle), 'Bundle error');
            assert(arrBundle.length == 2, 'Must export 2 childs');
            const child1 = arrBundle.find(item => item.sku === SKU_CHILD_1)
            const child2 = arrBundle.find(item => item.sku === SKU_CHILD_2)
            assert(child1, 'Child ' + SKU_CHILD_1 + ' must export');
            assert(child2, 'Child ' + SKU_CHILD_2 + ' must export');

            assert(child1.qty == 3, 'Must import 1');
            assert(child1.sku == SKU_CHILD_1, 'Must export SKU_CHILD_1');

            assert(child2.qty == 3, 'Must import 1');
            assert(child2.sku == SKU_CHILD_2, 'Must export SKU_CHILD_2');
        });

        
    })
    
    describe('Sku 100160018', function () { 
        it('Purchase 100160018 qty 1', async function () {
            await initStock([{ sku: SKU_COMBO, in_stock: 0 }, { sku: SKU_CHILD_1, in_stock: 0 }, { sku: SKU_CHILD_2, in_stock: 0 }])
            const arrBundle = await bundleService.getConversion({ sku: SKU_CHILD_1, qty: 1, type: ImportExportConstant.TYPE_IMPORT, stock_id: 1005 })
            console.log(arrBundle)
            assert(arrBundle.length == 0, 'No combo import');
            
        });
        it('Purchase 100160018 qty 1 with exist 2 204900005', async function () {
            await initStock([{ sku: SKU_COMBO, in_stock: 0 }, { sku: SKU_CHILD_1, in_stock: 0 }, { sku: SKU_CHILD_2, in_stock: 2 }])
            const arrBundle = await bundleService.getConversion({ sku: SKU_CHILD_1, qty: 1, type: ImportExportConstant.TYPE_IMPORT, stock_id: 1005 })
            assert(arrBundle.length == 1, 'Must import combo');
            assert(arrBundle[0].qty == 1, 'Must import 1 combo ' + SKU_COMBO);
            assert(arrBundle[0].sku == SKU_COMBO, 'Must import combo ' + SKU_COMBO); 
        });
        it('Purchase 100160018 qty 1 with exist 2 204900005, 1 100160018', async function () {
            await initStock([{ sku: SKU_COMBO, in_stock: 1 }, { sku: SKU_CHILD_1, in_stock: 1 }, { sku: SKU_CHILD_2, in_stock: 5 }])
            const arrBundle = await bundleService.getConversion({ sku: SKU_CHILD_1, qty: 1, type: ImportExportConstant.TYPE_IMPORT, stock_id: 1005 })
            console.log(arrBundle)
            assert(arrBundle.length == 1, 'Must import combo');
            assert(arrBundle[0].qty == 1, 'Must import 1 combo ' + SKU_COMBO);
            assert(arrBundle[0].sku == SKU_COMBO, 'Must import combo ' + SKU_COMBO); 
        });
        it('Purchase 100160018 qty 4 with exist 1 204900005', async function () {
            await initStock([{ sku: SKU_COMBO, in_stock: 1 }, { sku: SKU_CHILD_1, in_stock: 1 }, { sku: SKU_CHILD_2, in_stock: 5 }])
            const arrBundle = await bundleService.getConversion({ sku: SKU_CHILD_1, qty: 4, type: ImportExportConstant.TYPE_IMPORT, stock_id: 1005 })
            console.log(arrBundle)
            assert(arrBundle.length == 1, 'Must import combo');
            assert(arrBundle[0].qty == 4, 'Must import 1 combo ' + SKU_COMBO);
            assert(arrBundle[0].sku == SKU_COMBO, 'Must import combo ' + SKU_COMBO); 
        });
        it('Purchase 100160018 qty 2 with exist 2 204900005', async function () {
            await initStock([{ sku: SKU_COMBO, in_stock: 0 }, { sku: SKU_CHILD_1, in_stock: 0 }, { sku: SKU_CHILD_2, in_stock: 2 }])
            const arrBundle = await bundleService.getConversion({ sku: SKU_CHILD_1, qty: 2, type: ImportExportConstant.TYPE_IMPORT, stock_id: 1005 })
            assert(arrBundle.length == 1, 'Must import combo');
            assert(arrBundle[0].qty == 2, 'Must import 1 combo ' + SKU_COMBO);
            assert(arrBundle[0].sku == SKU_COMBO, 'Must import combo ' + SKU_COMBO); 
        });
        // Import 6 sku 100160018, co san 5 sku 204900005 ==> tang 5 combo
        it('Purchase 100160018 qty 6 with exist 5 204900005', async function () {
            await initStock([{ sku: SKU_COMBO, in_stock: 0 }, { sku: SKU_CHILD_1, in_stock: 0 }, { sku: SKU_CHILD_2, in_stock: 5 }])
            const arrBundle = await bundleService.getConversion({ sku: SKU_CHILD_1, qty: 6, type: ImportExportConstant.TYPE_IMPORT, stock_id: 1005 })
            console.log(arrBundle)
            assert(arrBundle.length == 1, 'Must import combo');
            assert(arrBundle[0].qty == 5, 'Must import 1 combo ' + SKU_COMBO);
            assert(arrBundle[0].sku == SKU_COMBO, 'Must import combo ' + SKU_COMBO); 
        });
        it('Purchase 100160018 qty 3 with exist 1 204900005', async function () {
            await initStock([{ sku: SKU_COMBO, in_stock: 0 }, { sku: SKU_CHILD_1, in_stock: 0 }, { sku: SKU_CHILD_2, in_stock: 1 }])
            const arrBundle = await bundleService.getConversion({ sku: SKU_CHILD_1, qty: 3, type: ImportExportConstant.TYPE_IMPORT, stock_id: 1005 })
            assert(arrBundle.length == 1, 'Must import combo');
            assert(arrBundle[0].qty == 1, 'Must import 1 combo ' + SKU_COMBO);
            assert(arrBundle[0].sku == SKU_COMBO, 'Must import combo ' + SKU_COMBO); 
        });

        it('Sale 100160018 qty 1', async function () {
            await initStock([{ sku: SKU_COMBO, in_stock: 0 }, { sku: SKU_CHILD_1, in_stock: 1 }, { sku: SKU_CHILD_2, in_stock: 0 }])
            const arrBundle = await bundleService.getConversion({ sku: SKU_CHILD_1, qty: 1, type: ImportExportConstant.TYPE_EXPORT, stock_id: 1005 })
            console.log(arrBundle)
            assert(arrBundle.length == 0, 'No sale combo');
        });

        it('Sale 100160018 qty 1 with exist 1 combo', async function () {
            await initStock([{ sku: SKU_COMBO, in_stock: 1 }, { sku: SKU_CHILD_1, in_stock: 1 }, { sku: SKU_CHILD_2, in_stock:2 }])
            const arrBundle = await bundleService.getConversion({ sku: SKU_CHILD_1, qty: 1, type: ImportExportConstant.TYPE_EXPORT, stock_id: 1005 })
            console.log(arrBundle)
            assert(arrBundle.length == 1, 'Must import combo');
            assert(arrBundle[0].qty == 1, 'Must import 1 combo ' + SKU_COMBO);
            assert(arrBundle[0].sku == SKU_COMBO, 'Must import combo ' + SKU_COMBO); 
        });
        it('Sale 100160018 qty 2 with exist 3 combo', async function () {
            await initStock([{ sku: SKU_COMBO, in_stock: 3 }, { sku: SKU_CHILD_1, in_stock: 3 }, { sku: SKU_CHILD_2, in_stock: 3 }])
            const arrBundle = await bundleService.getConversion({ sku: SKU_CHILD_1, qty: 2, type: ImportExportConstant.TYPE_EXPORT, stock_id: 1005 })
            console.log(arrBundle)
            assert(arrBundle.length == 1, 'Must import combo');
            assert(arrBundle[0].qty == 2, 'Must import 1 combo ' + SKU_COMBO);
            assert(arrBundle[0].sku == SKU_COMBO, 'Must import combo ' + SKU_COMBO); 
        });

        it('Sale 100160018 qty 2 with exist 3 combo, 5 100160018', async function () {
            await initStock([{ sku: SKU_COMBO, in_stock: 3 }, { sku: SKU_CHILD_1, in_stock: 5 }, { sku: SKU_CHILD_2, in_stock: 3 }])
            const arrBundle = await bundleService.getConversion({ sku: SKU_CHILD_1, qty: 2, type: ImportExportConstant.TYPE_EXPORT, stock_id: 1005 })
            console.log(arrBundle)
            assert(arrBundle.length == 0, 'No sale combo');
        });
    })

    describe('Sku 204900005', function () { 
        it('Purchase 204900005 qty 1', async function () {
            await initStock([{ sku: SKU_COMBO, in_stock: 0 }, { sku: SKU_CHILD_1, in_stock: 0 }, { sku: SKU_CHILD_2, in_stock: 0 }])
            const arrBundle = await bundleService.getConversion({ sku: SKU_CHILD_2, qty: 1, type: ImportExportConstant.TYPE_IMPORT, stock_id: 1005 })
            console.log(arrBundle)
            assert(arrBundle.length == 0, 'No combo import');
        });
        it('Purchase 204900005 qty 1 exist 1 100160018', async function () {
            await initStock([{ sku: SKU_COMBO, in_stock: 0 }, { sku: SKU_CHILD_1, in_stock: 2 }, { sku: SKU_CHILD_2, in_stock: 0 }])
            const arrBundle = await bundleService.getConversion({ sku: SKU_CHILD_2, qty: 1, type: ImportExportConstant.TYPE_IMPORT, stock_id: 1005 })
            console.log(arrBundle)
            assert(arrBundle.length == 1, 'Must import combo');
            assert(arrBundle[0].qty == 1, 'Must import 1 combo ' + SKU_COMBO);
            assert(arrBundle[0].sku == SKU_COMBO, 'Must import combo ' + SKU_COMBO); 
        });

        it('Purchase 204900005 qty 5 exist 2 100160018', async function () {
            await initStock([{ sku: SKU_COMBO, in_stock: 0 }, { sku: SKU_CHILD_1, in_stock: 2 }, { sku: SKU_CHILD_2, in_stock: 0 }])
            const arrBundle = await bundleService.getConversion({ sku: SKU_CHILD_2, qty: 2, type: ImportExportConstant.TYPE_IMPORT, stock_id: 1005 })
            assert(arrBundle.length == 1, 'Must import combo');
            assert(arrBundle[0].qty == 2, 'Must import 2 combo ' + SKU_COMBO);
            assert(arrBundle[0].sku == SKU_COMBO, 'Must import combo ' + SKU_COMBO); 
        });

        it('Purchase 204900005 qty 4 exist 4 100160018', async function () {
            await initStock([{ sku: SKU_COMBO, in_stock: 0 }, { sku: SKU_CHILD_1, in_stock: 4 }, { sku: SKU_CHILD_2, in_stock: 0 }])
            const arrBundle = await bundleService.getConversion({ sku: SKU_CHILD_2, qty: 4, type: ImportExportConstant.TYPE_IMPORT, stock_id: 1005 })
            assert(arrBundle.length == 1, 'Must import combo');
            assert(arrBundle[0].qty == 4, 'Must import 4 combo ' + SKU_COMBO);
            assert(arrBundle[0].sku == SKU_COMBO, 'Must import combo ' + SKU_COMBO); 
        });

        it('Sale 204900005 qty 1', async function () {
            await initStock([{ sku: SKU_COMBO, in_stock: 2 }, { sku: SKU_CHILD_1, in_stock: 2 }, { sku: SKU_CHILD_2, in_stock: 2 }])
            const arrBundle = await bundleService.getConversion({ sku: SKU_CHILD_2, qty: 1, type: ImportExportConstant.TYPE_EXPORT, stock_id: 1005 })
            assert(arrBundle.length == 1, 'Must export combo');
            assert(arrBundle[0].qty == 1, 'Must import 1 combo ' + SKU_COMBO);
        });
        it('Sale 204900005 qty 1 exist 10', async function () {
            await initStock([{ sku: SKU_COMBO, in_stock: 2 }, { sku: SKU_CHILD_1, in_stock: 2 }, { sku: SKU_CHILD_2, in_stock: 10 }])
            const arrBundle = await bundleService.getConversion({ sku: SKU_CHILD_2, qty: 1, type: ImportExportConstant.TYPE_EXPORT, stock_id: 1005 })
            assert(arrBundle.length == 0, 'No export combo');
        });

        it('Sale 204900005 qty 2 exist 5 100160018', async function () {
            await initStock([{ sku: SKU_COMBO, in_stock: 2 }, { sku: SKU_CHILD_1, in_stock: 5 }, { sku: SKU_CHILD_2, in_stock: 2 }])
            const arrBundle = await bundleService.getConversion({ sku: SKU_CHILD_2, qty: 2, type: ImportExportConstant.TYPE_EXPORT, stock_id: 1005 })
            assert(arrBundle.length == 1, 'Must export combo');
            assert(arrBundle[0].qty == 2, 'Must import 1 combo ' + SKU_COMBO);
        });

        it('Sale 204900005 over', async function () {
            await initStock([{ sku: SKU_COMBO, in_stock: 2 }, { sku: SKU_CHILD_1, in_stock: 2 }, { sku: SKU_CHILD_2, in_stock: 2 }])
            const arrBundle = await bundleService.getConversion({ sku: SKU_CHILD_2, qty: 5, type: ImportExportConstant.TYPE_EXPORT, stock_id: 1005 })
            console.log(arrBundle)
            assert(arrBundle.length == 1, 'Must export combo');
            assert(arrBundle[0].qty == 5, 'Must export 5 combo ' + SKU_COMBO);
        });
    })
})