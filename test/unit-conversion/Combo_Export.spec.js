const assert = require('assert');
const { LOADIPHLPAPI } = require('dns');
const ImportExportConstant = require('../../constants/importExport');
const { Bundle, BundleDetail, StockProduct, sequelize } = require('../../models')
const bundleService = require('../../services/bundleService')
        const SKU_CHILD1 = 100160014
        const SKU_CHILD2 = 100100015
        const SKU_CHILD3 = 100170026
        const SKU_CHILD4 = 100170027
        const SKU_CHILD5 = 100170028
        const SKU_COMBO1 = 204900015
        const SKU_COMBO2 = 204900016
        const SKU_COMBO3 = 204900017
        const SKU_COMBO4 = 204900018
        const SKU_COMBO5 = 204900019
        const SKU_COMBO6 = 204900020
        const SKU_COMBO7 = 204900021
        const SKU_COMBO8 = 204900022
        const SKU_COMBO9 = 204900023
        const SKU_COMBO10 = 204900024
async function initCombo() {
    const bundle1 = await Bundle.create({ sku: SKU_COMBO1, type: 2, description: '100160014 + 100100015' });
    await BundleDetail.create({ bundle_id: bundle1.id, sku: SKU_CHILD1, qty: 1 });
    await BundleDetail.create({ bundle_id: bundle1.id, sku: SKU_CHILD2, qty: 1 });

    const bundle2 = await Bundle.create({ sku: SKU_COMBO2, type: 2, description: '100160014 + 100100015 + 100170026' });
    await BundleDetail.create({ bundle_id: bundle2.id, sku: SKU_CHILD1, qty: 1 });
    await BundleDetail.create({ bundle_id: bundle2.id, sku: SKU_CHILD2, qty: 1 });
    await BundleDetail.create({ bundle_id: bundle2.id, sku: SKU_CHILD3, qty: 1 });

    const bundle3 = await Bundle.create({ sku: SKU_COMBO3, type: 2, description: '100160014 + 100100015 + 100170026 +100170027' });
    await BundleDetail.create({ bundle_id: bundle3.id, sku: SKU_CHILD1, qty: 1 });
    await BundleDetail.create({ bundle_id: bundle3.id, sku: SKU_CHILD2, qty: 1 });
    await BundleDetail.create({ bundle_id: bundle3.id, sku: SKU_CHILD3, qty: 1 });
    await BundleDetail.create({ bundle_id: bundle3.id, sku: SKU_CHILD4, qty: 1 });

    const bundle4 = await Bundle.create({ sku: SKU_COMBO4, type: 2, description: '100160014 + 100100015 + 100170026 +100170027 + 100170028' });
    await BundleDetail.create({ bundle_id: bundle4.id, sku: SKU_CHILD1, qty: 1 });
    await BundleDetail.create({ bundle_id: bundle4.id, sku: SKU_CHILD2, qty: 1 });
    await BundleDetail.create({ bundle_id: bundle4.id, sku: SKU_CHILD3, qty: 1 });
    await BundleDetail.create({ bundle_id: bundle4.id, sku: SKU_CHILD4, qty: 1 });
    await BundleDetail.create({ bundle_id: bundle4.id, sku: SKU_CHILD5, qty: 1 });

    const bundle5 = await Bundle.create({ sku: SKU_COMBO5, type: 2, description: '100160014 + 4 x 100100015' });
    await BundleDetail.create({ bundle_id: bundle5.id, sku: SKU_CHILD1, qty: 1 });
    await BundleDetail.create({ bundle_id: bundle5.id, sku: SKU_CHILD2, qty: 4 });

    const bundle6 = await Bundle.create({ sku: SKU_COMBO6, type: 2, description: ' 3 x 100160014 + 3 x 100100015' });
    await BundleDetail.create({ bundle_id: bundle6.id, sku: SKU_CHILD1, qty: 3 });
    await BundleDetail.create({ bundle_id: bundle6.id, sku: SKU_CHILD2, qty: 3 });

    const bundle7 = await Bundle.create({ sku: SKU_COMBO7, type: 2, description: ' 3 x 100160014 + 3 x 100100015' });
    await BundleDetail.create({ bundle_id: bundle7.id, sku: SKU_CHILD1, qty: 8 });
    await BundleDetail.create({ bundle_id: bundle7.id, sku: SKU_CHILD2, qty: 3 });

    const bundle8 = await Bundle.create({ sku: SKU_COMBO8, type: 2, description: '100160014 + 100100015 + 2 x 100170026' });
    await BundleDetail.create({ bundle_id: bundle8.id, sku: SKU_CHILD1, qty: 1 });
    await BundleDetail.create({ bundle_id: bundle8.id, sku: SKU_CHILD2, qty: 1 });
    await BundleDetail.create({ bundle_id: bundle8.id, sku: SKU_CHILD3, qty: 2 });

    const bundle9 = await Bundle.create({ sku: SKU_COMBO9, type: 2, description: '100160014 + 100100015 + 3 x 100170026' });
    await BundleDetail.create({ bundle_id: bundle9.id, sku: SKU_CHILD1, qty: 1 });
    await BundleDetail.create({ bundle_id: bundle9.id, sku: SKU_CHILD2, qty: 1 });
    await BundleDetail.create({ bundle_id: bundle9.id, sku: SKU_CHILD3, qty: 3 });

    const bundle10 = await Bundle.create({ sku: SKU_COMBO10, type: 2, description: '2 x 100160014 + 2 x 100100015 + 4 x 100170026 + 2 x 100170027' });
    await BundleDetail.create({ bundle_id: bundle10.id, sku: SKU_CHILD1, qty: 2 });
    await BundleDetail.create({ bundle_id: bundle10.id, sku: SKU_CHILD2, qty: 2 });
    await BundleDetail.create({ bundle_id: bundle10.id, sku: SKU_CHILD3, qty: 4 });
    await BundleDetail.create({ bundle_id: bundle10.id, sku: SKU_CHILD4, qty: 2 });
}
async function initData() {
    await initCombo()
}
async function cleanUpStock() {
    await StockProduct.destroy({ where: {}, truncate: true });
}
async function initStock(arrParam) {
    for (let param of arrParam) {
        await StockProduct.create({ stockprod_stock_id: 1005, stockprod_sku: param.sku, stockprod_in_stock: param.in_stock, stockprod_in_comming: 0, stockprod_available: param.in_stock, stockprod_committed: 0, stockprod_utime: Math.floor(+new Date() / 1000) });
    }   
}
describe('Combo multi combo', function () {
    before(async () => {
        await initData()
    })
    beforeEach(async () => {
        await cleanUpStock()
    })
    after(async () => {
        await Bundle.destroy({ where: {}, truncate: true });
        await BundleDetail.destroy({ where: {}, truncate: true });
    })
    describe('Combo', function () {
        describe('Export combo', function () {
            it('Export 204900015 qty 160', async function () {
                await initStock([
                    { sku: SKU_COMBO1, in_stock: 160 },
                    { sku: SKU_COMBO2, in_stock: 160 },
                    { sku: SKU_COMBO3, in_stock: 158 },
                    { sku: SKU_COMBO4, in_stock: 158 },
                    { sku: SKU_COMBO5, in_stock: 40 },
                    { sku: SKU_COMBO6, in_stock: 53 },
                    { sku: SKU_COMBO7, in_stock: 21 },
                    { sku: SKU_COMBO8, in_stock: 93 },
                    { sku: SKU_COMBO9, in_stock: 62 },
                    { sku: SKU_COMBO10, in_stock: 46 },
                    { sku: SKU_CHILD1, in_stock: 173 } , 
                    { sku: SKU_CHILD2, in_stock: 160 } , 
                    { sku: SKU_CHILD3, in_stock: 186 } ,
                    { sku: SKU_CHILD4, in_stock: 158 } ,
                    { sku: SKU_CHILD5, in_stock: 210 }])
                const arrBundle = await bundleService.getConversion({ sku: SKU_COMBO1, qty: 160, type: ImportExportConstant.TYPE_EXPORT, stock_id: 1005 })
                assert(Array.isArray(arrBundle), 'Bundle error');
               // console.log(arrBundle);
               const child1 = arrBundle.find(item => item.sku === SKU_CHILD1)
               assert(child1, 'Combo 100100014 must export');
               assert(child1.qty == 160, 'Must export 160');

               const child2 = arrBundle.find(item => item.sku === SKU_CHILD2)
               assert(child2, 'Combo 100100015 must export');
               assert(child2.qty == 160, 'Must export 160');

               const combo2 = arrBundle.find(item => item.sku === SKU_COMBO2)
               assert(combo2, 'Combo 204900016 must export');
               assert(combo2.qty == 160, 'Must export 160');

               const combo3 = arrBundle.find(item => item.sku === SKU_COMBO3)
               assert(combo3, 'Combo 204900017 must export');
               assert(combo3.qty == 158, 'Must export 158');

               const combo4 = arrBundle.find(item => item.sku === SKU_COMBO4)
               assert(combo4, 'Combo 204900018 must export');
               assert(combo4.qty == 158, 'Must export 158');

               const combo5 = arrBundle.find(item => item.sku === SKU_COMBO5)
               assert(combo5, 'Combo 204900019 must export');
               assert(combo5.qty == 40, 'Must export 40');

               const combo6 = arrBundle.find(item => item.sku === SKU_COMBO6)
               assert(combo6, 'Combo 204900020 must export');
               assert(combo6.qty == 53, 'Must export 53');

               const combo7 = arrBundle.find(item => item.sku === SKU_COMBO7)
               assert(combo7, 'Combo 204900021 must export');
               assert(combo7.qty == 21, 'Must export 21');

               const combo8 = arrBundle.find(item => item.sku === SKU_COMBO8)
               assert(combo8, 'Combo 204900022 must export');
               assert(combo8.qty ==93, 'Must export 93');

               const combo9 = arrBundle.find(item => item.sku === SKU_COMBO9)
               assert(combo9, 'Combo 204900023 must export');
               assert(combo9.qty == 62, 'Must export 62');

               const combo10 = arrBundle.find(item => item.sku === SKU_COMBO10)
               assert(combo10, 'Combo 204900021 must export');
               assert(combo10.qty == 46, 'Must export 46');
            })
            it('Export 204900016 with qty 160', async function () {
                await initStock([{ sku: SKU_COMBO1, in_stock: 160 },
                { sku: SKU_COMBO2, in_stock: 160 },
                { sku: SKU_COMBO3, in_stock: 158 },
                { sku: SKU_COMBO4, in_stock: 158 },
                { sku: SKU_COMBO5, in_stock: 40 },
                { sku: SKU_COMBO6, in_stock: 53 },
                { sku: SKU_COMBO7, in_stock: 21 },
                { sku: SKU_COMBO8, in_stock: 93 },
                { sku: SKU_COMBO9, in_stock: 62 },
                { sku: SKU_COMBO10, in_stock: 46 },
                { sku: SKU_CHILD1, in_stock: 173 },
                { sku: SKU_CHILD2, in_stock: 160 },
                { sku: SKU_CHILD3, in_stock: 186 },
                { sku: SKU_CHILD4, in_stock: 158 },
                { sku: SKU_CHILD5, in_stock: 210 }])
                const arrBundle = await bundleService.getConversion({ sku: SKU_COMBO2, qty: 160, type: ImportExportConstant.TYPE_EXPORT, stock_id: 1005 })
                assert(Array.isArray(arrBundle), 'Bundle error');
               // console.log(arrBundle)
                const child1 = arrBundle.find(item => item.sku === SKU_CHILD1)
                assert(child1, 'Child1 100160014 must export');
                assert(child1.qty == 160, 'Must export 160');
    
                const child2 = arrBundle.find(item => item.sku === SKU_CHILD2)
                assert(child2, 'Child2 100100015 must export');
                assert(child2.qty == 160, 'Must export 160');
    
                const child3 = arrBundle.find(item => item.sku === SKU_CHILD3)
                assert(child3, 'Child3 100170026 must export');
                assert(child3.qty == 160, 'Must export 160');
    
                const combo1 = arrBundle.find(item => item.sku === SKU_COMBO1)
                assert(combo1, 'Combo 204900015 must export');
                assert(combo1.qty == 160, 'Must export 160');
    
                const combo3 = arrBundle.find(item => item.sku === SKU_COMBO3)
                assert(combo3, 'Combo 204900017 must export');
                assert(combo3.qty == 158, 'Must export 158');
    
                const combo4 = arrBundle.find(item => item.sku === SKU_COMBO4)
                assert(combo4, 'Combo 204900018 must export');
                assert(combo4.qty == 158, 'Must export 158');
    
                const combo5 = arrBundle.find(item => item.sku === SKU_COMBO5)
                assert(combo5, 'Combo 204900019 must export');
                assert(combo5.qty == 40, 'Must export 40');
    
                const combo6 = arrBundle.find(item => item.sku === SKU_COMBO6)
                assert(combo6, 'Combo 204900020 must export');
                assert(combo6.qty == 53, 'Must export 53');
    
                const combo7 = arrBundle.find(item => item.sku === SKU_COMBO7)
                assert(combo7, 'Combo 204900021 must export');
                assert(combo7.qty == 21, 'Must export 21');
    
                const combo8 = arrBundle.find(item => item.sku === SKU_COMBO8)
                assert(combo8, 'Combo 204900022 must export');
                assert(combo8.qty == 93, 'Must export 93');
    
                const combo9 = arrBundle.find(item => item.sku === SKU_COMBO9)
                assert(combo9, 'Combo 204900023 must export');
                assert(combo9.qty == 62, 'Must export 62');
    
                const combo10 = arrBundle.find(item => item.sku === SKU_COMBO10)
                assert(combo10, 'Combo 204900024 must export');
                assert(combo10.qty == 46, 'Must export 46');
    
            });
            it('Export 204900017 with qty 100', async function () {
                await initStock([{ sku: SKU_COMBO1, in_stock: 160 },
                { sku: SKU_COMBO2, in_stock: 160 },
                { sku: SKU_COMBO3, in_stock: 158 },
                { sku: SKU_COMBO4, in_stock: 158 },
                { sku: SKU_COMBO5, in_stock: 40 },
                { sku: SKU_COMBO6, in_stock: 53 },
                { sku: SKU_COMBO7, in_stock: 21 },
                { sku: SKU_COMBO8, in_stock: 93 },
                { sku: SKU_COMBO9, in_stock: 62 },
                { sku: SKU_COMBO10, in_stock: 46 },
                { sku: SKU_CHILD1, in_stock: 173 },
                { sku: SKU_CHILD2, in_stock: 160 },
                { sku: SKU_CHILD3, in_stock: 186 },
                { sku: SKU_CHILD4, in_stock: 158 },
                { sku: SKU_CHILD5, in_stock: 210 }])
                const arrBundle = await bundleService.getConversion({ sku: SKU_COMBO3, qty: 100, type: ImportExportConstant.TYPE_EXPORT, stock_id: 1005 })
               // console.log(arrBundle)
                assert(Array.isArray(arrBundle), 'Bundle error');
    
                const child1 = arrBundle.find(item => item.sku === SKU_CHILD1)
                assert(child1, 'Child1 100160014 must export');
                assert(child1.qty == 100, 'Must export 100');
    
                const child2 = arrBundle.find(item => item.sku === SKU_CHILD2)
                assert(child2, 'Child1 100100015 must export');
                assert(child2.qty == 100, 'Must export 100');
    
                const child3 = arrBundle.find(item => item.sku === SKU_CHILD3)
                assert(child3, 'Child3 100170026 must export');
                assert(child3.qty == 100, 'Must export 100');
    
                const child4 = arrBundle.find(item => item.sku === SKU_CHILD4)
                assert(child4, 'Child4 100170027 must export');
                assert(child4.qty == 100, 'Must export 100');
    
                const combo1 = arrBundle.find(item => item.sku === SKU_COMBO1)
                assert(combo1, 'Combo 204900015 must export');
                assert(combo1.qty == 100, 'Must export 200');
    
                const combo2 = arrBundle.find(item => item.sku === SKU_COMBO2)
                assert(combo2, 'Combo 204900016 must export');
                assert(combo2.qty == 100, 'Must export 100');
    
                const combo4 = arrBundle.find(item => item.sku === SKU_COMBO4)
                assert(combo4, 'Combo 204900018 must export');
                assert(combo4.qty == 100, 'Must export 100');
    
                const combo5 = arrBundle.find(item => item.sku === SKU_COMBO5)
                assert(combo5, 'Combo 204900019 must export');
                assert(combo5.qty == 25, 'Must export 25');
    
                const combo6 = arrBundle.find(item => item.sku === SKU_COMBO6)
                assert(combo6, 'Combo 204900020 must export');
                assert(combo6.qty == 33, 'Must export 33');
    
                const combo7 = arrBundle.find(item => item.sku === SKU_COMBO7)
                assert(combo7, 'Combo 204900021 must export');
                assert(combo7.qty == 12, 'Must export 12');
    
                const combo8 = arrBundle.find(item => item.sku === SKU_COMBO8)
                assert(combo8, 'Combo 204900022 must export');
                assert(combo8.qty == 50, 'Must export 50');
    
                const combo9 = arrBundle.find(item => item.sku === SKU_COMBO9)
                assert(combo9, 'Combo 204900023 must export');
                assert(combo9.qty == 34, 'Must export 34');
    
                const combo10 = arrBundle.find(item => item.sku === SKU_COMBO10)
                assert(combo10, 'Combo 204900024 must export');
                assert(combo10.qty == 25, 'Must export 25');
    
            });
            it('Export 204900018 with qty 130', async function () {
                await initStock([{ sku: SKU_COMBO1, in_stock: 160 },
                { sku: SKU_COMBO2, in_stock: 160 },
                { sku: SKU_COMBO3, in_stock: 158 },
                { sku: SKU_COMBO4, in_stock: 158 },
                { sku: SKU_COMBO5, in_stock: 40 },
                { sku: SKU_COMBO6, in_stock: 53 },
                { sku: SKU_COMBO7, in_stock: 21 },
                { sku: SKU_COMBO8, in_stock: 93 },
                { sku: SKU_COMBO9, in_stock: 62 },
                { sku: SKU_COMBO10, in_stock: 46 },
                { sku: SKU_CHILD1, in_stock: 173 },
                { sku: SKU_CHILD2, in_stock: 160 },
                { sku: SKU_CHILD3, in_stock: 186 },
                { sku: SKU_CHILD4, in_stock: 158 },
                { sku: SKU_CHILD5, in_stock: 210 }])
                const arrBundle = await bundleService.getConversion({ sku: SKU_COMBO4, qty: 130, type: ImportExportConstant.TYPE_EXPORT, stock_id: 1005 })
               // console.log(arrBundle)
                assert(Array.isArray(arrBundle), 'Bundle error');
    
                const child1 = arrBundle.find(item => item.sku === SKU_CHILD1)
                assert(child1, 'Child1 100100015 must export');
                assert(child1.qty == 130, 'Must export 130');
    
                const child2 = arrBundle.find(item => item.sku === SKU_CHILD2)
                assert(child2, 'Child2 100100015 must export');
                assert(child2.qty == 130, 'Must export 130');
    
                const child3 = arrBundle.find(item => item.sku === SKU_CHILD3)
                assert(child3, 'Child3 100170026 must export');
                assert(child3.qty == 130, 'Must export 130');
    
                const child4 = arrBundle.find(item => item.sku === SKU_CHILD4)
                assert(child4, 'Combo 100170027 must export');
                assert(child4.qty == 130, 'Must export 130');
    
                const child5 = arrBundle.find(item => item.sku === SKU_CHILD5)
                assert(child5, 'Child5  100170028 must export');
                assert(child5.qty == 130, 'Must export 130');
    
                const combo1 = arrBundle.find(item => item.sku === SKU_COMBO1)
                assert(combo1, 'Combo 204900015 must export');
                assert(combo1.qty == 130, 'Must export 130');
    
                const combo2 = arrBundle.find(item => item.sku === SKU_COMBO2)
                assert(combo2, 'Combo 204900016 must export');
                assert(combo2.qty == 130, 'Must export 130');
    
                const combo3 = arrBundle.find(item => item.sku === SKU_COMBO3)
                assert(combo3, 'Combo 204900017 must export');
                assert(combo3.qty == 130, 'Must export 130');
    
                const combo5 = arrBundle.find(item => item.sku === SKU_COMBO5)
                assert(combo5, 'Combo 204900019 must export');
                assert(combo5.qty == 33, 'Must export 33');
    
                const combo6 = arrBundle.find(item => item.sku === SKU_COMBO6)
                assert(combo6, 'Combo 204900020 must export');
                assert(combo6.qty == 43, 'Must export 43');
    
                const combo7 = arrBundle.find(item => item.sku === SKU_COMBO7)
                assert(combo7, 'Combo 204900021 must export');
                assert(combo7.qty == 16, 'Must export 16');
    
                const combo8 = arrBundle.find(item => item.sku === SKU_COMBO8)
                assert(combo8, 'Combo 204900022 must export');
                assert(combo8.qty == 65, 'Must export 65');
    
                const combo9 = arrBundle.find(item => item.sku === SKU_COMBO9)
                assert(combo9, 'Combo 204900023 must export');
                assert(combo9.qty == 44, 'Must export 44');
    
                const combo10 = arrBundle.find(item => item.sku === SKU_COMBO10)
                assert(combo10, 'Combo 204900024 must export');
                assert(combo10.qty == 32, 'Must export 32');
            });
            it('Export 204900019 with qty 35', async function () {
                await initStock([{ sku: SKU_COMBO1, in_stock: 160 },
                { sku: SKU_COMBO2, in_stock: 160 },
                { sku: SKU_COMBO3, in_stock: 158 },
                { sku: SKU_COMBO4, in_stock: 158 },
                { sku: SKU_COMBO5, in_stock: 40 },
                { sku: SKU_COMBO6, in_stock: 53 },
                { sku: SKU_COMBO7, in_stock: 21 },
                { sku: SKU_COMBO8, in_stock: 93 },
                { sku: SKU_COMBO9, in_stock: 62 },
                { sku: SKU_COMBO10, in_stock: 46 },
                { sku: SKU_CHILD1, in_stock: 173 },
                { sku: SKU_CHILD2, in_stock: 160 },
                { sku: SKU_CHILD3, in_stock: 186 },
                { sku: SKU_CHILD4, in_stock: 158 },
                { sku: SKU_CHILD5, in_stock: 210 }])
                const arrBundle = await bundleService.getConversion({ sku: SKU_COMBO5, qty: 35, type: ImportExportConstant.TYPE_EXPORT, stock_id: 1005 })
               // console.log(arrBundle)
                assert(Array.isArray(arrBundle), 'Bundle error');
    
                const child1 = arrBundle.find(item => item.sku === SKU_CHILD1)
                assert(child1, 'Child1 100160014 must export');
                assert(child1.qty == 35, 'Must export 35');
    
                const child2 = arrBundle.find(item => item.sku === SKU_CHILD2)
                assert(child2, 'Child2 100100015 must export');
                assert(child2.qty == 140, 'Must export 140');
    
    
                const combo1 = arrBundle.find(item => item.sku === SKU_COMBO1)
                assert(combo1, 'Combo 204900015 must export');
                assert(combo1.qty == 140, 'Must export 140');
    
                const combo2 = arrBundle.find(item => item.sku === SKU_COMBO2)
                assert(combo2, 'Combo 204900016 must export');
                assert(combo2.qty == 140, 'Must export 140');
    
                const combo3 = arrBundle.find(item => item.sku === SKU_COMBO3)
                assert(combo3, 'Combo 204900017 must export');
                assert(combo3.qty == 138, 'Must export 138');
    
                const combo4 = arrBundle.find(item => item.sku === SKU_COMBO4)
                assert(combo4, 'Combo 204900018 must export');
                assert(combo4.qty == 138, 'Must export 138');
    
                const combo6 = arrBundle.find(item => item.sku === SKU_COMBO6)
                assert(combo6, 'Combo 204900020 must export');
                assert(combo6.qty == 47, 'Must export 47');
    
                const combo7 = arrBundle.find(item => item.sku === SKU_COMBO7)
                assert(combo7, 'Combo 204900021 must export');
                assert(combo7.qty == 15, 'Must export 15');
    
                const combo8 = arrBundle.find(item => item.sku === SKU_COMBO8)
                assert(combo8, 'Combo 204900022 must export');
                assert(combo8.qty == 73, 'Must export 73');
    
                const combo9 = arrBundle.find(item => item.sku === SKU_COMBO9)
                assert(combo9, 'Combo 204900023 must export');
                assert(combo9.qty == 42, 'Must export 42');
    
                const combo10 = arrBundle.find(item => item.sku === SKU_COMBO10)
                assert(combo10, 'Combo 204900024 must export');
                assert(combo10.qty == 36, 'Must export 36');
            });
            it('Export 204900020 with qty 50', async function () {
                await initStock([{ sku: SKU_COMBO1, in_stock: 160 },
                { sku: SKU_COMBO2, in_stock: 160 },
                { sku: SKU_COMBO3, in_stock: 158 },
                { sku: SKU_COMBO4, in_stock: 158 },
                { sku: SKU_COMBO5, in_stock: 40 },
                { sku: SKU_COMBO6, in_stock: 53 },
                { sku: SKU_COMBO7, in_stock: 21 },
                { sku: SKU_COMBO8, in_stock: 93 },
                { sku: SKU_COMBO9, in_stock: 62 },
                { sku: SKU_COMBO10, in_stock: 46 },
                { sku: SKU_CHILD1, in_stock: 173 },
                { sku: SKU_CHILD2, in_stock: 160 },
                { sku: SKU_CHILD3, in_stock: 186 },
                { sku: SKU_CHILD4, in_stock: 158 },
                { sku: SKU_CHILD5, in_stock: 210 }])
                const arrBundle = await bundleService.getConversion({ sku: SKU_COMBO6, qty: 50, type: ImportExportConstant.TYPE_EXPORT, stock_id: 1005 })
               // console.log(arrBundle)
                assert(Array.isArray(arrBundle), 'Bundle error');
            
                const child1 = arrBundle.find(item => item.sku === SKU_CHILD1)
                assert(child1, 'Child1 100160014 must export');
                assert(child1.qty == 150, 'Must export 150');
    
                const child2 = arrBundle.find(item => item.sku === SKU_CHILD2)
                assert(child2, 'Child2 100100015 must export');
                assert(child2.qty == 150, 'Must export 150');
    
                const combo1 = arrBundle.find(item => item.sku === SKU_COMBO1)
                assert(combo1, 'Combo 204900015 must export');
                assert(combo1.qty == 150, 'Must export 150');
    
                const combo2 = arrBundle.find(item => item.sku === SKU_COMBO2)
                assert(combo2, 'Combo 204900016 must export');
                assert(combo2.qty == 150, 'Must export 150');
    
                const combo3 = arrBundle.find(item => item.sku === SKU_COMBO3)
                assert(combo3, 'Combo 204900017 must export');
                assert(combo3.qty == 148, 'Must export 148');
    
                const combo4 = arrBundle.find(item => item.sku === SKU_COMBO4)
                assert(combo4, 'Combo 204900018 must export');
                assert(combo4.qty == 148, 'Must export 148');
    
                const combo5 = arrBundle.find(item => item.sku === SKU_COMBO5)
                assert(combo5, 'Combo 204900019 must export');
                assert(combo5.qty == 38, 'Must export 38');
    
                const combo7 = arrBundle.find(item => item.sku === SKU_COMBO7)
                assert(combo7, 'Combo 204900021 must export');
                assert(combo7.qty == 19, 'Must export 19');
    
                const combo8 = arrBundle.find(item => item.sku === SKU_COMBO8)
                assert(combo8, 'Combo 204900022 must export');
                assert(combo8.qty == 83, 'Must export 83');
    
                const combo9 = arrBundle.find(item => item.sku === SKU_COMBO9)
                assert(combo9, 'Combo 204900023 must export');
                assert(combo9.qty == 52, 'Must export 52');
    
                const combo10 = arrBundle.find(item => item.sku === SKU_COMBO10)
                assert(combo10, 'Combo 204900024 must export');
                assert(combo10.qty == 41, 'Must export 41');
            });
            it('Export 204900021 with qty 20', async function () {
                await initStock([{ sku: SKU_COMBO1, in_stock: 160 },
                { sku: SKU_COMBO2, in_stock: 160 },
                { sku: SKU_COMBO3, in_stock: 158 },
                { sku: SKU_COMBO4, in_stock: 158 },
                { sku: SKU_COMBO5, in_stock: 40 },
                { sku: SKU_COMBO6, in_stock: 53 },
                { sku: SKU_COMBO7, in_stock: 21 },
                { sku: SKU_COMBO8, in_stock: 93 },
                { sku: SKU_COMBO9, in_stock: 62 },
                { sku: SKU_COMBO10, in_stock: 46 },
                { sku: SKU_CHILD1, in_stock: 173 },
                { sku: SKU_CHILD2, in_stock: 160 },
                { sku: SKU_CHILD3, in_stock: 186 },
                { sku: SKU_CHILD4, in_stock: 158 },
                { sku: SKU_CHILD5, in_stock: 210 }])
                const arrBundle = await bundleService.getConversion({ sku: SKU_COMBO7, qty: 20, type: ImportExportConstant.TYPE_EXPORT, stock_id: 1005 })
              // console.log(arrBundle)
                assert(Array.isArray(arrBundle), 'Bundle error');
    
                const child1 = arrBundle.find(item => item.sku === SKU_CHILD1)
                assert(child1, 'Child1 100160014 must export');
                assert(child1.qty == 160, 'Must export 160');
    
                const child2 = arrBundle.find(item => item.sku === SKU_CHILD2)
                assert(child2, 'Child2 100100015 must export');
                assert(child2.qty == 60, 'Must export 60');
    
                const combo1 = arrBundle.find(item => item.sku === SKU_COMBO1)
                assert(combo1, 'Combo 204900015 must export');
                assert(combo1.qty == 147, 'Must export 147');
    
                const combo2 = arrBundle.find(item => item.sku === SKU_COMBO2)
                assert(combo2, 'Combo 204900017 must export');
                assert(combo2.qty == 147, 'Must export 147');
    
                const combo3 = arrBundle.find(item => item.sku === SKU_COMBO3)
                assert(combo3, 'Combo 204900017 must export');
                assert(combo3.qty == 145, 'Must export 145');
    
                const combo4 = arrBundle.find(item => item.sku === SKU_COMBO4)
                assert(combo4, 'Combo 204900018 must export');
                assert(combo4.qty == 145, 'Must export 145');
    
                const combo5 = arrBundle.find(item => item.sku === SKU_COMBO5)
                assert(combo5, 'Combo 204900019 must export');
                assert(combo5.qty == 27, 'Must export 27');
    
                const combo6 = arrBundle.find(item => item.sku === SKU_COMBO6)
                assert(combo6, 'Combo 204900020 must export');
                assert(combo6.qty == 49, 'Must export 49');
    
                const combo8 = arrBundle.find(item => item.sku === SKU_COMBO8)
                assert(combo8, 'Combo 204900022 must export');
                assert(combo8.qty == 80, 'Must export 80');
    
                const combo9 = arrBundle.find(item => item.sku === SKU_COMBO9)
                assert(combo9, 'Combo 204900023 must export');
                assert(combo9.qty == 49, 'Must export 49');
    
                const combo10 = arrBundle.find(item => item.sku === SKU_COMBO10)
                assert(combo10, 'Combo 204900021 must export');
                assert(combo10.qty == 40, 'Must export 40');
            });
            it('Export 204900022 with qty 93', async function () {
                await initStock([{ sku: SKU_COMBO1, in_stock: 160 },
                { sku: SKU_COMBO2, in_stock: 160 },
                { sku: SKU_COMBO3, in_stock: 158 },
                { sku: SKU_COMBO4, in_stock: 158 },
                { sku: SKU_COMBO5, in_stock: 40 },
                { sku: SKU_COMBO6, in_stock: 53 },
                { sku: SKU_COMBO7, in_stock: 21 },
                { sku: SKU_COMBO8, in_stock: 93 },
                { sku: SKU_COMBO9, in_stock: 62 },
                { sku: SKU_COMBO10, in_stock: 46 },
                { sku: SKU_CHILD1, in_stock: 173 },
                { sku: SKU_CHILD2, in_stock: 160 },
                { sku: SKU_CHILD3, in_stock: 186 },
                { sku: SKU_CHILD4, in_stock: 158 },
                { sku: SKU_CHILD5, in_stock: 210 }])
                const arrBundle = await bundleService.getConversion({ sku: SKU_COMBO8, qty: 93, type: ImportExportConstant.TYPE_EXPORT, stock_id: 1005 })
               // console.log(arrBundle)
                assert(Array.isArray(arrBundle), 'Bundle error');
    
                const child1 = arrBundle.find(item => item.sku === SKU_CHILD1)
                assert(child1, 'Child1 100160014 must export');
                assert(child1.qty == 93, 'Must export 93');
    
                const child2 = arrBundle.find(item => item.sku === SKU_CHILD2)
                assert(child2, 'Child2 100100015 must export');
                assert(child2.qty == 93, 'Must export 93');
    
                const child3 = arrBundle.find(item => item.sku === SKU_CHILD3)
                assert(child3, 'Child3 100100015 must export');
                assert(child3.qty == 186, 'Must export 186');
    
                const combo1 = arrBundle.find(item => item.sku === SKU_COMBO1)
                assert(combo1, 'Combo 204900015 must export');
                assert(combo1.qty == 93, 'Must export 93');
    
                const combo2 = arrBundle.find(item => item.sku === SKU_COMBO2)
                assert(combo2, 'Combo 204900017 must export');
                assert(combo2.qty == 160, 'Must export 160');
    
                const combo3 = arrBundle.find(item => item.sku === SKU_COMBO3)
                assert(combo3, 'Combo 204900018 must export');
                assert(combo3.qty == 158, 'Must export 158');
    
                const combo4 = arrBundle.find(item => item.sku === SKU_COMBO4)
                assert(combo4, 'Combo 204900019 must export');
                assert(combo4.qty == 158, 'Must export 158');
    
                const combo5 = arrBundle.find(item => item.sku === SKU_COMBO5)
                assert(combo5, 'Combo 204900020 must export');
                assert(combo5.qty == 24, 'Must export 24');
    
                const combo6 = arrBundle.find(item => item.sku === SKU_COMBO6)
                assert(combo6, 'Combo 204900021 must export');
                assert(combo6.qty == 31, 'Must export 31');
    
                const combo7 = arrBundle.find(item => item.sku === SKU_COMBO7)
                assert(combo7, 'Combo 204900022 must export');
                assert(combo7.qty == 11, 'Must export 11');
    
                const combo9 = arrBundle.find(item => item.sku === SKU_COMBO9)
                assert(combo9, 'Combo 204900023 must export');
                assert(combo9.qty == 62, 'Must export 62');
    
                const combo10 = arrBundle.find(item => item.sku === SKU_COMBO10)
                assert(combo10, 'Combo 204900021 must export');
                assert(combo10.qty == 46, 'Must export 46');
            });
            it('Export 204900023 with qty 58', async function () {
                await initStock([{ sku: SKU_COMBO1, in_stock: 160 },
                { sku: SKU_COMBO2, in_stock: 160 },
                { sku: SKU_COMBO3, in_stock: 158 },
                { sku: SKU_COMBO4, in_stock: 158 },
                { sku: SKU_COMBO5, in_stock: 40 },
                { sku: SKU_COMBO6, in_stock: 53 },
                { sku: SKU_COMBO7, in_stock: 21 },
                { sku: SKU_COMBO8, in_stock: 93 },
                { sku: SKU_COMBO9, in_stock: 62 },
                { sku: SKU_COMBO10, in_stock: 46 },
                { sku: SKU_CHILD1, in_stock: 173 },
                { sku: SKU_CHILD2, in_stock: 160 },
                { sku: SKU_CHILD3, in_stock: 186 },
                { sku: SKU_CHILD4, in_stock: 158 },
                { sku: SKU_CHILD5, in_stock: 210 }])
                const arrBundle = await bundleService.getConversion({ sku: SKU_COMBO9, qty: 58, type: ImportExportConstant.TYPE_EXPORT, stock_id: 1005 })
               // console.log(arrBundle)
                assert(Array.isArray(arrBundle), 'Bundle error');
    
                const child1 = arrBundle.find(item => item.sku === SKU_CHILD1)
                assert(child1, 'Child1 100160014 must export');
                assert(child1.qty == 58, 'Must export 58');
    
                const child2 = arrBundle.find(item => item.sku === SKU_CHILD2)
                assert(child2, 'Child2 100100015 must export');
                assert(child2.qty == 58, 'Must export 58');
    
                const child3 = arrBundle.find(item => item.sku === SKU_CHILD3)
                assert(child3, 'Child3 100170026 must export');
                assert(child3.qty == 174, 'Must export 174');
    
                const combo1 = arrBundle.find(item => item.sku === SKU_COMBO1)
                assert(combo1, 'Combo 204900015 must export');
                assert(combo1.qty == 58, 'Must export 58');
    
                const combo2 = arrBundle.find(item => item.sku === SKU_COMBO2)
                assert(combo2, 'Combo 204900017 must export');
                assert(combo2.qty == 148, 'Must export 148');
    
                const combo3 = arrBundle.find(item => item.sku === SKU_COMBO3)
                assert(combo3, 'Combo 204900018 must export');
                assert(combo3.qty == 146, 'Must export 146');
    
                const combo4 = arrBundle.find(item => item.sku === SKU_COMBO4)
                assert(combo4, 'Combo 204900019 must export');
                assert(combo4.qty == 146, 'Must export 146');
    
                const combo5 = arrBundle.find(item => item.sku === SKU_COMBO5)
                assert(combo5, 'Combo 204900020 must export');
                assert(combo5.qty == 15, 'Must export 15');
    
                const combo6 = arrBundle.find(item => item.sku === SKU_COMBO6)
                assert(combo6, 'Combo 204900021 must export');
                assert(combo6.qty == 19, 'Must export 19');
    
                const combo7 = arrBundle.find(item => item.sku === SKU_COMBO7)
                assert(combo7, 'Combo 204900022 must export');
                assert(combo7.qty == 7, 'Must export 7');
    
                const combo8 = arrBundle.find(item => item.sku === SKU_COMBO8)
                assert(combo8, 'Combo 204900023 must export');
                assert(combo8.qty == 87, 'Must export 87');
    
                const combo10 = arrBundle.find(item => item.sku === SKU_COMBO10)
                assert(combo10, 'Combo 204900024 must export');
                assert(combo10.qty == 43, 'Must export 43');
            });
            it('Export 204900024 with qty 46', async function () {
                await initStock([{ sku: SKU_COMBO1, in_stock: 160 },
                { sku: SKU_COMBO2, in_stock: 160 },
                { sku: SKU_COMBO3, in_stock: 158 },
                { sku: SKU_COMBO4, in_stock: 158 },
                { sku: SKU_COMBO5, in_stock: 40 },
                { sku: SKU_COMBO6, in_stock: 53 },
                { sku: SKU_COMBO7, in_stock: 21 },
                { sku: SKU_COMBO8, in_stock: 93 },
                { sku: SKU_COMBO9, in_stock: 62 },
                { sku: SKU_COMBO10, in_stock: 46 },
                { sku: SKU_CHILD1, in_stock: 173 },
                { sku: SKU_CHILD2, in_stock: 160 },
                { sku: SKU_CHILD3, in_stock: 186 },
                { sku: SKU_CHILD4, in_stock: 158 },
                { sku: SKU_CHILD5, in_stock: 210 }])
                const arrBundle = await bundleService.getConversion({ sku: SKU_COMBO10, qty: 46, type: ImportExportConstant.TYPE_EXPORT, stock_id: 1005 })
               console.log(arrBundle)
                assert(Array.isArray(arrBundle), 'Bundle error');
                const child1 = arrBundle.find(item => item.sku === SKU_CHILD1)
                assert(child1, 'Child1 100160014 must export');
                assert(child1.qty == 92, 'Must export 92');
    
                const child2 = arrBundle.find(item => item.sku === SKU_CHILD2)
                assert(child2, 'Child2 100100015 must export');
                assert(child2.qty == 92, 'Must export 92');
    
                const child3 = arrBundle.find(item => item.sku === SKU_CHILD3)
                assert(child3, 'Child3 100170026 must export');
                assert(child3.qty == 184, 'Must export 184');
    
                const child4 = arrBundle.find(item => item.sku === SKU_CHILD4)
                assert(child4, 'Child4 100170027 must export');
                assert(child4.qty == 92, 'Must export 92');
    
                const combo1 = arrBundle.find(item => item.sku === SKU_COMBO1)
                assert(combo1, 'Combo 204900015 must export');
                assert(combo1.qty == 92, 'Must export 92');
    
                const combo2 = arrBundle.find(item => item.sku === SKU_COMBO2)
                assert(combo2, 'Combo 204900016 must export');
                assert(combo2.qty == 158, 'Must export 158');
    
                const combo3 = arrBundle.find(item => item.sku === SKU_COMBO3)
                assert(combo3, 'Combo 204900017 must export');
                assert(combo3.qty == 156, 'Must export 156');
    
                const combo4 = arrBundle.find(item => item.sku === SKU_COMBO4)
                assert(combo4, 'Combo 204900018 must export');
                assert(combo4.qty == 156, 'Must export 156');
    
                const combo5 = arrBundle.find(item => item.sku === SKU_COMBO5)
                assert(combo5, 'Combo 204900019 must export');
                assert(combo5.qty == 23, 'Must export 23');
    
                const combo6 = arrBundle.find(item => item.sku === SKU_COMBO6)
                assert(combo6, 'Combo 204900020 must export');
                assert(combo6.qty == 31, 'Must export 31');
    
                const combo7 = arrBundle.find(item => item.sku === SKU_COMBO7)
                assert(combo7, 'Combo 204900021 must export');
                assert(combo7.qty == 11, 'Must export 11');
    
                const combo8 = arrBundle.find(item => item.sku === SKU_COMBO8)
                assert(combo8, 'Combo 204900022 must export');
                assert(combo8.qty == 92, 'Must export 92');
    
                const combo9 = arrBundle.find(item => item.sku === SKU_COMBO9)
                assert(combo9, 'Combo 204900023 must export');
                assert(combo9.qty == 62, 'Must export 62');
            });
        })
    })
})