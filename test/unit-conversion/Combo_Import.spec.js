const assert = require('assert');
const ImportExportConstant = require('../../constants/importExport');
const bundleService = require('../../services/bundleService')
const { Bundle, BundleDetail, StockProduct, sequelize } = require('../../models')
const SKU_CHILD1 = 100160014
const SKU_CHILD2 = 100100015
const SKU_CHILD3 = 100170026
const SKU_CHILD4 = 100170027
const SKU_CHILD5 = 100170028
const SKU_COMBO1 = 204900015
const SKU_COMBO2 = 204900016
const SKU_COMBO3 = 204900017
const SKU_COMBO4 = 204900018
const SKU_COMBO5 = 204900019
const SKU_COMBO6 = 204900020
const SKU_COMBO7 = 204900021
const SKU_COMBO8 = 204900022
const SKU_COMBO9 = 204900023
const SKU_COMBO10 = 204900024

async function initCombo() {
    const bundle1 = await Bundle.create({ sku: SKU_COMBO1, type: 2, description: '100160014 + 100100015' });
    await BundleDetail.create({ bundle_id: bundle1.id, sku: SKU_CHILD1, qty: 1 });
    await BundleDetail.create({ bundle_id: bundle1.id, sku: SKU_CHILD2, qty: 1 });

    const bundle2 = await Bundle.create({ sku: SKU_COMBO2, type: 2, description: '100160014 + 100100015 + 100170026' });
    await BundleDetail.create({ bundle_id: bundle2.id, sku: SKU_CHILD1, qty: 1 });
    await BundleDetail.create({ bundle_id: bundle2.id, sku: SKU_CHILD2, qty: 1 });
    await BundleDetail.create({ bundle_id: bundle2.id, sku: SKU_CHILD3, qty: 1 });

    const bundle3 = await Bundle.create({ sku: SKU_COMBO3, type: 2, description: '100160014 + 100100015 + 100170026 +100170027' });
    await BundleDetail.create({ bundle_id: bundle3.id, sku: SKU_CHILD1, qty: 1 });
    await BundleDetail.create({ bundle_id: bundle3.id, sku: SKU_CHILD2, qty: 1 });
    await BundleDetail.create({ bundle_id: bundle3.id, sku: SKU_CHILD3, qty: 1 });
    await BundleDetail.create({ bundle_id: bundle3.id, sku: SKU_CHILD4, qty: 1 });

    const bundle4 = await Bundle.create({ sku: SKU_COMBO4, type: 2, description: '100160014 + 100100015 + 100170026 +100170027 + 100170028' });
    await BundleDetail.create({ bundle_id: bundle4.id, sku: SKU_CHILD1, qty: 1 });
    await BundleDetail.create({ bundle_id: bundle4.id, sku: SKU_CHILD2, qty: 1 });
    await BundleDetail.create({ bundle_id: bundle4.id, sku: SKU_CHILD3, qty: 1 });
    await BundleDetail.create({ bundle_id: bundle4.id, sku: SKU_CHILD4, qty: 1 });
    await BundleDetail.create({ bundle_id: bundle4.id, sku: SKU_CHILD5, qty: 1 });

    const bundle5 = await Bundle.create({ sku: SKU_COMBO5, type: 2, description: '100160014 + 4 x 100100015' });
    await BundleDetail.create({ bundle_id: bundle5.id, sku: SKU_CHILD1, qty: 1 });
    await BundleDetail.create({ bundle_id: bundle5.id, sku: SKU_CHILD2, qty: 4 });

    const bundle6 = await Bundle.create({ sku: SKU_COMBO6, type: 2, description: ' 3 x 100160014 + 3 x 100100015' });
    await BundleDetail.create({ bundle_id: bundle6.id, sku: SKU_CHILD1, qty: 3 });
    await BundleDetail.create({ bundle_id: bundle6.id, sku: SKU_CHILD2, qty: 3 });

    const bundle7 = await Bundle.create({ sku: SKU_COMBO7, type: 2, description: ' 3 x 100160014 + 3 x 100100015' });
    await BundleDetail.create({ bundle_id: bundle7.id, sku: SKU_CHILD1, qty: 8 });
    await BundleDetail.create({ bundle_id: bundle7.id, sku: SKU_CHILD2, qty: 3 });

    const bundle8 = await Bundle.create({ sku: SKU_COMBO8, type: 2, description: '100160014 + 100100015 + 2 x 100170026' });
    await BundleDetail.create({ bundle_id: bundle8.id, sku: SKU_CHILD1, qty: 1 });
    await BundleDetail.create({ bundle_id: bundle8.id, sku: SKU_CHILD2, qty: 1 });
    await BundleDetail.create({ bundle_id: bundle8.id, sku: SKU_CHILD3, qty: 2 });

    const bundle9 = await Bundle.create({ sku: SKU_COMBO9, type: 2, description: '100160014 + 100100015 + 3 x 100170026' });
    await BundleDetail.create({ bundle_id: bundle9.id, sku: SKU_CHILD1, qty: 1 });
    await BundleDetail.create({ bundle_id: bundle9.id, sku: SKU_CHILD2, qty: 1 });
    await BundleDetail.create({ bundle_id: bundle9.id, sku: SKU_CHILD3, qty: 3 });

    const bundle10 = await Bundle.create({ sku: SKU_COMBO10, type: 2, description: '2 x 100160014 + 2 x 100100015 + 4 x 100170026 + 2 x 100170027' });
    await BundleDetail.create({ bundle_id: bundle10.id, sku: SKU_CHILD1, qty: 2 });
    await BundleDetail.create({ bundle_id: bundle10.id, sku: SKU_CHILD2, qty: 2 });
    await BundleDetail.create({ bundle_id: bundle10.id, sku: SKU_CHILD3, qty: 4 });
    await BundleDetail.create({ bundle_id: bundle10.id, sku: SKU_CHILD4, qty: 2 });

}
async function initData() {
    await initCombo()
}
async function cleanUpStock() {
    await StockProduct.destroy({ where: {}, truncate: true });
}
async function initStock(arrParam) {
    for (let param of arrParam) {
        await StockProduct.create({ stockprod_stock_id: 1005, stockprod_sku: param.sku, stockprod_in_stock: param.in_stock, stockprod_in_comming: 0, stockprod_available: param.in_stock, stockprod_committed: 0, stockprod_utime: Math.floor(+new Date() / 1000)});    
    }
    
}
describe('Combo multi combo', function () {

    before(async () => {
        await initData()
    })
    beforeEach(async () => {
        await cleanUpStock()
    })
    after(async () => {
        await Bundle.destroy({ where: {}, truncate: true });
        await BundleDetail.destroy({ where: {}, truncate: true });
    })
    // Run one test case: yarn mocha test/unit-conversion/Combo_Import.spec.js -g 'Import 204900015 with qty 110'
    describe('Combo ', function () { 
        describe('Import Combo', function () { 
        // Run one test case: yarn mocha --grep "Sales 204900015 with qty 23" test/in-out/Nhap-committing-mix.spec.js
        it('Import 204900015 with qty 110', async function () {
            await initStock([{ sku: SKU_COMBO1, in_stock: 160 },
            { sku: SKU_COMBO2, in_stock: 160 },
            { sku: SKU_COMBO3, in_stock: 158 },
            { sku: SKU_COMBO4, in_stock: 158 },
            { sku: SKU_COMBO5, in_stock: 40 },
            { sku: SKU_COMBO6, in_stock: 53 },
            { sku: SKU_COMBO7, in_stock: 21 },
            { sku: SKU_COMBO8, in_stock: 93 },
            { sku: SKU_COMBO9, in_stock: 62 },
            { sku: SKU_COMBO10, in_stock: 46 },
            { sku: SKU_CHILD1, in_stock: 173 },
            { sku: SKU_CHILD2, in_stock: 160 },
            { sku: SKU_CHILD3, in_stock: 186 },
            { sku: SKU_CHILD4, in_stock: 158 },
            { sku: SKU_CHILD5, in_stock: 210 }])
            const arrBundle = await bundleService.getConversion({ sku: SKU_COMBO1, qty: 110, type: ImportExportConstant.TYPE_IMPORT, stock_id: 1005 })
            console.log(arrBundle)
            assert(Array.isArray(arrBundle), 'Bundle error');
            const child1 = arrBundle.find(item => item.sku === SKU_CHILD1)
            assert(child1, 'Combo 100100015 must import');

            assert(child1.qty == 110, 'Must import 110 ');


            const child2 = arrBundle.find(item => item.sku === SKU_CHILD1)
            assert(child2, 'Combo 100100015 must import');
            assert(child2.qty == 110, 'Must import 110 ');

            const combo2 = arrBundle.find(item => item.sku === SKU_COMBO2)
            assert(combo2, 'Combo 204900016 must import');
            assert(combo2.qty == 26, 'Must import 26');
            
            const combo5 = arrBundle.find(item => item.sku === SKU_COMBO5)
            assert(combo5, 'Combo 240900019 not import');
            assert(combo5.qty == 27, 'Must import 27');

            const combo6 = arrBundle.find(item => item.sku === SKU_COMBO6)
            assert(combo6, 'Combo 204900020 must import');
            assert(combo6.qty == 37, 'Must import 37');
            
            const combo7 = arrBundle.find(item => item.sku === SKU_COMBO7)
            assert(combo7, 'Combo 204900021 must import');
            assert(combo7.qty == 14, 'Must import 14');
        });
        it('Import 204900016 with qty 170', async function () {
            await initStock([{ sku: SKU_COMBO1, in_stock: 160 },
            { sku: SKU_COMBO2, in_stock: 160 },
            { sku: SKU_COMBO3, in_stock: 158 },
            { sku: SKU_COMBO4, in_stock: 158 },
            { sku: SKU_COMBO5, in_stock: 40 },
            { sku: SKU_COMBO6, in_stock: 53 },
            { sku: SKU_COMBO7, in_stock: 21 },
            { sku: SKU_COMBO8, in_stock: 93 },
            { sku: SKU_COMBO9, in_stock: 62 },
            { sku: SKU_COMBO10, in_stock: 46 },
            { sku: SKU_CHILD1, in_stock: 173 },
            { sku: SKU_CHILD2, in_stock: 160 },
            { sku: SKU_CHILD3, in_stock: 186 },
            { sku: SKU_CHILD4, in_stock: 158 },
            { sku: SKU_CHILD5, in_stock: 210 }])
            const arrBundle = await bundleService.getConversion({ sku: SKU_COMBO2, qty: 170, type: ImportExportConstant.TYPE_IMPORT, stock_id: 1005 })
            //console.log(arrBundle)
            assert(Array.isArray(arrBundle), 'Bundle error');
            
            const child1 = arrBundle.find(item => item.sku === SKU_CHILD1)
            assert(child1, 'Combo 100100015 must import');
            assert(child1.qty == 170, 'Must import 170 ');

            const child2 = arrBundle.find(item => item.sku === SKU_CHILD2)
            assert(child2, 'Combo 100100015 must import');
            assert(child2.qty == 170, 'Must import 170 ');

            const child3 = arrBundle.find(item => item.sku === SKU_CHILD3)
            assert(child3, 'Combo 100100015 must import');
            assert(child3.qty == 170, 'Must import 170');

            const combo1 = arrBundle.find(item => item.sku === SKU_COMBO1)
            assert(combo1, 'Combo 204900016 must import');
            assert(combo1.qty == 170, 'Must import 170');
            
            const combo5 = arrBundle.find(item => item.sku === SKU_COMBO5)
            assert(combo5, 'Combo 240900019 not import');
            assert(combo5.qty == 42, 'Must import 43');

            const combo6 = arrBundle.find(item => item.sku === SKU_COMBO6)
            assert(combo6, 'Combo 204900020 must import');
            assert(combo6.qty == 57, 'Must import 57');
            
            const combo7 = arrBundle.find(item => item.sku === SKU_COMBO7)
            assert(combo7, 'Combo 204900021 must import');
            assert(combo7.qty == 21, 'Must import 22');

            const combo8 = arrBundle.find(item => item.sku === SKU_COMBO8)
            assert(combo8, 'Combo 204900022 must import');
            assert(combo8.qty == 85, 'Must import 85');

            const combo9 = arrBundle.find(item => item.sku === SKU_COMBO9)
            assert(combo9, 'Combo 204900023 must import');
            assert(combo9.qty == 56, 'Must import 57');

            const combo10 = arrBundle.find(item => item.sku === SKU_COMBO10)
            assert(combo10, 'Combo 204900024 must import');
            assert(combo10.qty == 33, 'Must import 33');
        });
        it('Import 204900017 with qty 138', async function () {
            await initStock([{ sku: SKU_COMBO1, in_stock: 160 },
            { sku: SKU_COMBO2, in_stock: 160 },
            { sku: SKU_COMBO3, in_stock: 158 },
            { sku: SKU_COMBO4, in_stock: 158 },
            { sku: SKU_COMBO5, in_stock: 40 },
            { sku: SKU_COMBO6, in_stock: 53 },
            { sku: SKU_COMBO7, in_stock: 21 },
            { sku: SKU_COMBO8, in_stock: 93 },
            { sku: SKU_COMBO9, in_stock: 62 },
            { sku: SKU_COMBO10, in_stock: 46 },
            { sku: SKU_CHILD1, in_stock: 173 },
            { sku: SKU_CHILD2, in_stock: 160 },
            { sku: SKU_CHILD3, in_stock: 186 },
            { sku: SKU_CHILD4, in_stock: 158 },
            { sku: SKU_CHILD5, in_stock: 210 }])
            const arrBundle = await bundleService.getConversion({ sku: SKU_COMBO3, qty: 138, type: ImportExportConstant.TYPE_IMPORT, stock_id: 1005 })
            //console.log(arrBundle)
            assert(Array.isArray(arrBundle), 'Bundle error');
            
            const child1 = arrBundle.find(item => item.sku === SKU_CHILD1)
            assert(child1, 'Combo 100100015 must import');
            assert(child1.qty == 138, 'Must import 138');

            const child2 = arrBundle.find(item => item.sku === SKU_CHILD2)
            assert(child2, 'Combo 100100015 must import');
            assert(child2.qty == 138, 'Must import 138');

            const child3 = arrBundle.find(item => item.sku === SKU_CHILD3)
            assert(child3, 'Combo 100100015 must import');
            assert(child3.qty == 138, 'Must import 138');

            const child4 = arrBundle.find(item => item.sku === SKU_CHILD4)
            assert(child4, 'Combo 100100015 must import');
            assert(child4.qty == 138, 'Must import 138');

            const combo1 = arrBundle.find(item => item.sku === SKU_COMBO1)
            assert(combo1, 'Combo 204900015 must import');
            assert(combo1.qty == 138, 'Must import 138');
            
            const combo2 = arrBundle.find(item => item.sku === SKU_COMBO2)
            assert(combo2, 'Combo 204900016 must import');
            assert(combo2.qty == 138, 'Must import 138');

            const combo4 = arrBundle.find(item => item.sku === SKU_COMBO4)
            assert(combo4, 'Combo 240900018 not import');
            assert(combo4.qty == 52, 'Must import 52');

            const combo5 = arrBundle.find(item => item.sku === SKU_COMBO5)
            assert(combo5, 'Combo 240900019 not import');
            assert(combo5.qty == 34, 'Must import 34');

            const combo6 = arrBundle.find(item => item.sku === SKU_COMBO6)
            assert(combo6, 'Combo 204900020 must import');
            assert(combo6.qty == 46, 'Must import 46');
            
            const combo7 = arrBundle.find(item => item.sku === SKU_COMBO7)
            assert(combo7, 'Combo 204900021 must import');
            assert(combo7.qty == 17, 'Must import 17');

            const combo8 = arrBundle.find(item => item.sku === SKU_COMBO8)
            assert(combo8, 'Combo 204900021 must import');
            assert(combo8.qty == 69, 'Must import 69');

            const combo9 = arrBundle.find(item => item.sku === SKU_COMBO9)
            assert(combo9, 'Combo 204900021 must import');
            assert(combo9.qty == 46, 'Must import 46');

            const combo10 = arrBundle.find(item => item.sku === SKU_COMBO10)
            assert(combo10, 'Combo 204900021 must import');
            assert(combo10.qty == 35, 'Must import 35');
        });
        it('Import 204900018 with qty 158', async function () {
            await initStock([{ sku: SKU_COMBO1, in_stock: 160 },
            { sku: SKU_COMBO2, in_stock: 160 },
            { sku: SKU_COMBO3, in_stock: 158 },
            { sku: SKU_COMBO4, in_stock: 158 },
            { sku: SKU_COMBO5, in_stock: 40 },
            { sku: SKU_COMBO6, in_stock: 53 },
            { sku: SKU_COMBO7, in_stock: 21 },
            { sku: SKU_COMBO8, in_stock: 93 },
            { sku: SKU_COMBO9, in_stock: 62 },
            { sku: SKU_COMBO10, in_stock: 46 },
            { sku: SKU_CHILD1, in_stock: 173 },
            { sku: SKU_CHILD2, in_stock: 160 },
            { sku: SKU_CHILD3, in_stock: 186 },
            { sku: SKU_CHILD4, in_stock: 158 },
            { sku: SKU_CHILD5, in_stock: 210 }])
            const arrBundle = await bundleService.getConversion({ sku: SKU_COMBO4, qty: 158, type: ImportExportConstant.TYPE_IMPORT, stock_id: 1005 })
            //console.log(arrBundle)
            assert(Array.isArray(arrBundle), 'Bundle error');
            
            const child1 = arrBundle.find(item => item.sku === SKU_CHILD1)
            assert(child1, 'Combo 100100015 must import');
            assert(child1.qty == 158, 'Must import 158');

            const child2 = arrBundle.find(item => item.sku === SKU_CHILD2)
            assert(child2, 'Combo 100100015 must import');
            assert(child2.qty == 158, 'Must import 158');

            const child3 = arrBundle.find(item => item.sku === SKU_CHILD3)
            assert(child3, 'Combo 100100015 must import');
            assert(child3.qty == 158, 'Must import 158');

            const child4 = arrBundle.find(item => item.sku === SKU_CHILD4)
            assert(child4, 'Combo 100100015 must import');
            assert(child4.qty == 158, 'Must import 158');

            const child5 = arrBundle.find(item => item.sku === SKU_CHILD5)
            assert(child5, 'Combo 100100015 must import');
            assert(child5.qty == 158, 'Must import 158');

            const combo1 = arrBundle.find(item => item.sku === SKU_COMBO1)
            assert(combo1, 'Combo 204900016 must import');
            assert(combo1.qty == 158, 'Must import 158');
            
            const combo2 = arrBundle.find(item => item.sku === SKU_COMBO2)
            assert(combo2, 'Combo 204900016 must import');
            assert(combo2.qty == 158, 'Must import 158');

            const combo3 = arrBundle.find(item => item.sku === SKU_COMBO3)
            assert(combo3, 'Combo 240900017 not import');
            assert(combo3.qty == 158, 'Must import 158');

            const combo5 = arrBundle.find(item => item.sku === SKU_COMBO5)
            assert(combo5, 'Combo 240900019 not import');
            assert(combo5.qty == 39, 'Must import 39');

            const combo6 = arrBundle.find(item => item.sku === SKU_COMBO6)
            assert(combo6, 'Combo 204900020 must import');
            assert(combo6.qty == 53, 'Must import 53');
            
            const combo7 = arrBundle.find(item => item.sku === SKU_COMBO7)
            assert(combo7, 'Combo 204900021 must import');
            assert(combo7.qty == 20, 'Must import 20');

            const combo8 = arrBundle.find(item => item.sku === SKU_COMBO8)
            assert(combo8, 'Combo 204900021 must import');
            assert(combo8.qty == 79, 'Must import 79');

            const combo9 = arrBundle.find(item => item.sku === SKU_COMBO9)
            assert(combo9, 'Combo 204900021 must import');
            assert(combo9.qty == 52, 'Must import 53');

            const combo10 = arrBundle.find(item => item.sku === SKU_COMBO10)
            assert(combo10, 'Combo 204900021 must import');
            assert(combo10.qty == 40, 'Must import 40');
        });
        it('Import 204900019 with qty 39', async function () {
            await initStock([{ sku: SKU_COMBO1, in_stock: 160 },
            { sku: SKU_COMBO2, in_stock: 160 },
            { sku: SKU_COMBO3, in_stock: 158 },
            { sku: SKU_COMBO4, in_stock: 158 },
            { sku: SKU_COMBO5, in_stock: 40 },
            { sku: SKU_COMBO6, in_stock: 53 },
            { sku: SKU_COMBO7, in_stock: 21 },
            { sku: SKU_COMBO8, in_stock: 93 },
            { sku: SKU_COMBO9, in_stock: 62 },
            { sku: SKU_COMBO10, in_stock: 46 },
            { sku: SKU_CHILD1, in_stock: 173 },
            { sku: SKU_CHILD2, in_stock: 160 },
            { sku: SKU_CHILD3, in_stock: 186 },
            { sku: SKU_CHILD4, in_stock: 158 },
            { sku: SKU_CHILD5, in_stock: 210 }])
            const arrBundle = await bundleService.getConversion({ sku: SKU_COMBO5, qty: 39, type: ImportExportConstant.TYPE_IMPORT, stock_id: 1005 })
           // console.log(arrBundle)
            assert(Array.isArray(arrBundle), 'Bundle error');
            
            const child1 = arrBundle.find(item => item.sku === SKU_CHILD1)
            assert(child1, 'Combo 100100015 must import');
            assert(child1.qty == 39, 'Must import 39');

            const child2 = arrBundle.find(item => item.sku === SKU_CHILD2)
            assert(child2, 'Combo 100100015 must import');
            assert(child2.qty == 156, 'Must import 156');

            const child3 = arrBundle.find(item => item.sku === SKU_CHILD3)
            assert(!child3, 'Combo 100100015 must import');

            const child4 = arrBundle.find(item => item.sku === SKU_CHILD4)
            assert(!child4, 'Combo 100100015 must import');

            const child5 = arrBundle.find(item => item.sku === SKU_CHILD5)
            assert(!child5, 'Combo 100100015 must import');

            const combo1 = arrBundle.find(item => item.sku === SKU_COMBO1)
            assert(combo1, 'Combo 204900016 must import');
            assert(combo1.qty == 52, 'Must import 52');
            
            const combo2 = arrBundle.find(item => item.sku === SKU_COMBO2)
            assert(combo2, 'Combo 204900016 must import');
            assert(combo2.qty == 26, 'Must import 26');

            const combo6 = arrBundle.find(item => item.sku === SKU_COMBO6)
            assert(combo6, 'Combo 204900020 must import');
            assert(combo6.qty == 17, 'Must import 17');
            
            const combo7 = arrBundle.find(item => item.sku === SKU_COMBO7)
            assert(combo7, 'Combo 204900021 must import');
            assert(combo7.qty == 5, 'Must import 5');

            // const combo8 = arrBundle.find(item => item.sku === SKU_COMBO8)
            // assert(!combo8, 'Combo 204900021 must import');

            // const combo9 = arrBundle.find(item => item.sku === SKU_COMBO9)
            // assert(!combo9, 'Combo 204900021 must import');

            // const combo10 = arrBundle.find(item => item.sku === SKU_COMBO10)
            // assert(!combo10, 'Combo 204900021 must import');
        });
        it('Import 204900020 with qty 47', async function () {
            await initStock([{ sku: SKU_COMBO1, in_stock: 160 },
            { sku: SKU_COMBO2, in_stock: 160 },
            { sku: SKU_COMBO3, in_stock: 158 },
            { sku: SKU_COMBO4, in_stock: 158 },
            { sku: SKU_COMBO5, in_stock: 40 },
            { sku: SKU_COMBO6, in_stock: 53 },
            { sku: SKU_COMBO7, in_stock: 21 },
            { sku: SKU_COMBO8, in_stock: 93 },
            { sku: SKU_COMBO9, in_stock: 62 },
            { sku: SKU_COMBO10, in_stock: 46 },
            { sku: SKU_CHILD1, in_stock: 173 },
            { sku: SKU_CHILD2, in_stock: 160 },
            { sku: SKU_CHILD3, in_stock: 186 },
            { sku: SKU_CHILD4, in_stock: 158 },
            { sku: SKU_CHILD5, in_stock: 210 }])
            const arrBundle = await bundleService.getConversion({ sku: SKU_COMBO6, qty: 47, type: ImportExportConstant.TYPE_IMPORT, stock_id: 1005 })
          //  console.log(arrBundle)
            assert(Array.isArray(arrBundle), 'Bundle error');
            
            const child1 = arrBundle.find(item => item.sku === SKU_CHILD1)
            assert(child1, 'Combo 100100015 must import');
            assert(child1.qty == 141, 'Must import 141');

            const child2 = arrBundle.find(item => item.sku === SKU_CHILD2)
            assert(child2, 'Combo 100100015 must import');
            assert(child2.qty == 141, 'Must import 141');

            const child3 = arrBundle.find(item => item.sku === SKU_CHILD3)
            assert(!child3, 'Combo 100100015 must import');

            const child4 = arrBundle.find(item => item.sku === SKU_CHILD4)
            assert(!child4, 'Combo 100100015 must import');

            const child5 = arrBundle.find(item => item.sku === SKU_CHILD5)
            assert(!child5, 'Combo 100100015 must import');

            const combo1 = arrBundle.find(item => item.sku === SKU_COMBO1)
            assert(combo1, 'Combo 204900015 must import');
            assert(combo1.qty == 141, 'Must import 141');
            
            const combo2 = arrBundle.find(item => item.sku === SKU_COMBO2)
            assert(combo2, 'Combo 204900016 must import');
            assert(combo2.qty == 26, 'Must import 26');

            // const combo3 = arrBundle.find(item => item.sku === SKU_COMBO3)
            // assert(!combo3, 'Combo 240900017 not import');

            const combo5 = arrBundle.find(item => item.sku === SKU_COMBO5)
            assert(combo5, 'Combo 240900019 not import');
            assert(combo5.qty == 35, 'Must import 36');
            
            const combo7 = arrBundle.find(item => item.sku === SKU_COMBO7)
            assert(combo7, 'Combo 204900021 must import');
            assert(combo7.qty == 18, 'Must import 18');

            // const combo8 = arrBundle.find(item => item.sku === SKU_COMBO8)
            // assert(!combo8, 'Combo 204900021 must import');

            // const combo9 = arrBundle.find(item => item.sku === SKU_COMBO9)
            // assert(!combo9, 'Combo 204900021 must import');

            // const combo10 = arrBundle.find(item => item.sku === SKU_COMBO10)
            // assert(!combo10, 'Combo 204900021 must import');
        });
        it('Import 204900021 with qty 53', async function () {
            await initStock([{ sku: SKU_COMBO1, in_stock: 160 },
            { sku: SKU_COMBO2, in_stock: 160 },
            { sku: SKU_COMBO3, in_stock: 158 },
            { sku: SKU_COMBO4, in_stock: 158 },
            { sku: SKU_COMBO5, in_stock: 40 },
            { sku: SKU_COMBO6, in_stock: 53 },
            { sku: SKU_COMBO7, in_stock: 21 },
            { sku: SKU_COMBO8, in_stock: 93 },
            { sku: SKU_COMBO9, in_stock: 62 },
            { sku: SKU_COMBO10, in_stock: 46 },
            { sku: SKU_CHILD1, in_stock: 173 },
            { sku: SKU_CHILD2, in_stock: 160 },
            { sku: SKU_CHILD3, in_stock: 186 },
            { sku: SKU_CHILD4, in_stock: 158 },
            { sku: SKU_CHILD5, in_stock: 210 }])
            const arrBundle = await bundleService.getConversion({ sku: SKU_COMBO7, qty: 53, type: ImportExportConstant.TYPE_IMPORT, stock_id: 1005 })
            // console.log(arrBundle)
            assert(Array.isArray(arrBundle), 'Bundle error');
            
            const child1 = arrBundle.find(item => item.sku === SKU_CHILD1)
            assert(child1, 'Combo 100100015 must import');
            assert(child1.qty == 424, 'Must import 424');

            const child2 = arrBundle.find(item => item.sku === SKU_CHILD2)
            assert(child2, 'Combo 100100015 must import');
            assert(child2.qty == 159, 'Must import 159');

            const child3 = arrBundle.find(item => item.sku === SKU_CHILD3)
            assert(!child3, 'Combo 100100015 must import');

            const child4 = arrBundle.find(item => item.sku === SKU_CHILD4)
            assert(!child4, 'Combo 100100015 must import');

            const child5 = arrBundle.find(item => item.sku === SKU_CHILD5)
            assert(!child5, 'Combo 100100015 must import');

            const combo1 = arrBundle.find(item => item.sku === SKU_COMBO1)
            assert(combo1, 'Combo 204900015 must import');
            assert(combo1.qty == 159, 'Must import 159');
        
            const combo2 = arrBundle.find(item => item.sku === SKU_COMBO2)
            assert(combo2, 'Combo 204900016 must import');
            assert(combo2.qty == 26, 'Must import 26');

            // const combo3 = arrBundle.find(item => item.sku === SKU_COMBO3)
            // assert(!combo3, 'Combo 240900017 not import');

            const combo5 = arrBundle.find(item => item.sku === SKU_COMBO5)
            assert(combo5, 'Combo 240900019 not import');
            assert(combo5.qty == 39, 'Must import 39');

            const combo6 = arrBundle.find(item => item.sku === SKU_COMBO6)
            assert(combo6, 'Combo 240900020 not import');
            assert(combo6.qty == 53, 'Must import 53');

            // const combo8 = arrBundle.find(item => item.sku === SKU_COMBO8)
            // assert(!combo8, 'Combo 204900021 must import');

            // const combo9 = arrBundle.find(item => item.sku === SKU_COMBO9)
            // assert(!combo9, 'Combo 204900021 must import');

            // const combo10 = arrBundle.find(item => item.sku === SKU_COMBO10)
            // assert(!combo10, 'Combo 204900021 must import');
        });
        it('Import 204900022 with qty 59', async function () {
            await initStock([{ sku: SKU_COMBO1, in_stock: 160 },
            { sku: SKU_COMBO2, in_stock: 160 },
            { sku: SKU_COMBO3, in_stock: 158 },
            { sku: SKU_COMBO4, in_stock: 158 },
            { sku: SKU_COMBO5, in_stock: 40 },
            { sku: SKU_COMBO6, in_stock: 53 },
            { sku: SKU_COMBO7, in_stock: 21 },
            { sku: SKU_COMBO8, in_stock: 93 },
            { sku: SKU_COMBO9, in_stock: 62 },
            { sku: SKU_COMBO10, in_stock: 46 },
            { sku: SKU_CHILD1, in_stock: 173 },
            { sku: SKU_CHILD2, in_stock: 160 },
            { sku: SKU_CHILD3, in_stock: 186 },
            { sku: SKU_CHILD4, in_stock: 158 },
            { sku: SKU_CHILD5, in_stock: 210 }])
            const arrBundle = await bundleService.getConversion({ sku: SKU_COMBO8, qty: 59, type: ImportExportConstant.TYPE_IMPORT, stock_id: 1005 })
            // console.log(arrBundle)
            assert(Array.isArray(arrBundle), 'Bundle error');
            
            const child1 = arrBundle.find(item => item.sku === SKU_CHILD1)
            assert(child1, 'Combo 100100015 must import');
            assert(child1.qty == 59, 'Must import 59');

            const child2 = arrBundle.find(item => item.sku === SKU_CHILD1)
            assert(child2, 'Combo 100100015 must import');
            assert(child2.qty == 59, 'Must import 59');

            const child3 = arrBundle.find(item => item.sku === SKU_CHILD3)
            assert(child3, 'Combo 100100015 must import');
            assert(child3.qty == 118, 'Must import 118');

            const child4 = arrBundle.find(item => item.sku === SKU_CHILD4)
            assert(!child4, 'Combo 100100015 must import');

            const combo1 = arrBundle.find(item => item.sku === SKU_COMBO1)
            assert(combo1, 'Combo 204900016 must import');
            assert(combo1.qty == 59, 'Must import 59');
            
            const combo2 = arrBundle.find(item => item.sku === SKU_COMBO2)
            assert(combo2, 'Combo 204900016 must import');
            assert(combo2.qty == 59, 'Must import 59');

            // const combo3 = arrBundle.find(item => item.sku === SKU_COMBO3)
            // assert(!combo3, 'Combo 204900017 must import');

            // const combo4 = arrBundle.find(item => item.sku === SKU_COMBO4)
            // assert(!combo4, 'Combo 204900018 must import');

            const combo5 = arrBundle.find(item => item.sku === SKU_COMBO5)
            assert(combo5, 'Combo 240900019 not import');
            assert(combo5.qty == 14, 'Must import 14');

            const combo6 = arrBundle.find(item => item.sku === SKU_COMBO6)
            assert(combo6, 'Combo 204900020 must import');
            assert(combo6.qty == 20, 'Must import 20');
            
            const combo7 = arrBundle.find(item => item.sku === SKU_COMBO7)
            assert(combo7, 'Combo 204900021 must import');
            assert(combo7.qty == 8, 'Must import 8');

            const combo9 = arrBundle.find(item => item.sku === SKU_COMBO9)
            assert(combo9, 'Combo 204900021 must import');
            assert(combo9.qty == 39, 'Must import 39');

            const combo10 = arrBundle.find(item => item.sku === SKU_COMBO10)
            assert(combo10, 'Combo 204900021 must import');
            assert(combo10.qty == 30, 'Must import 30');
        });
        it('Import 204900023 with qty 63', async function () {
            await initStock([{ sku: SKU_COMBO1, in_stock: 160 },
            { sku: SKU_COMBO2, in_stock: 160 },
            { sku: SKU_COMBO3, in_stock: 158 },
            { sku: SKU_COMBO4, in_stock: 158 },
            { sku: SKU_COMBO5, in_stock: 40 },
            { sku: SKU_COMBO6, in_stock: 53 },
            { sku: SKU_COMBO7, in_stock: 21 },
            { sku: SKU_COMBO8, in_stock: 93 },
            { sku: SKU_COMBO9, in_stock: 62 },
            { sku: SKU_COMBO10, in_stock: 46 },
            { sku: SKU_CHILD1, in_stock: 173 },
            { sku: SKU_CHILD2, in_stock: 160 },
            { sku: SKU_CHILD3, in_stock: 186 },
            { sku: SKU_CHILD4, in_stock: 158 },
            { sku: SKU_CHILD5, in_stock: 210 }])
            const arrBundle = await bundleService.getConversion({ sku: SKU_COMBO9, qty: 63, type: ImportExportConstant.TYPE_IMPORT, stock_id: 1005 })
           // console.log(arrBundle)
            assert(Array.isArray(arrBundle), 'Bundle error');
            
            const child1 = arrBundle.find(item => item.sku === SKU_CHILD1)
            assert(child1, 'Combo 100100015 must import');
            assert(child1.qty == 63, 'Must import 63 ');

            const child2 = arrBundle.find(item => item.sku === SKU_CHILD1)
            assert(child2, 'Combo 100100015 must import');
            assert(child2.qty == 63, 'Must import 63');

            const child3 = arrBundle.find(item => item.sku === SKU_CHILD3)
            assert(child3, 'Combo 100100015 must import');
            assert(child3.qty == 189, 'Must import 189');

            const child4 = arrBundle.find(item => item.sku === SKU_CHILD4)
            assert(!child4, 'Combo 100100015 must import');

            const combo1 = arrBundle.find(item => item.sku === SKU_COMBO1)
            assert(combo1, 'Combo 204900016 must import');
            assert(combo1.qty == 63, 'Must import 63');
            
            const combo2 = arrBundle.find(item => item.sku === SKU_COMBO2)
            assert(combo2, 'Combo 204900016 must import');
            assert(combo2.qty == 63, 'Must import 63');

            // const combo3 = arrBundle.find(item => item.sku === SKU_COMBO3)
            // assert(!combo3, 'Combo 204900016 must import');

            // const combo4 = arrBundle.find(item => item.sku === SKU_COMBO4)
            // assert(!combo4, 'Combo 204900018 must import');

            const combo5 = arrBundle.find(item => item.sku === SKU_COMBO5)
            assert(combo5, 'Combo 240900019 not import');
            assert(combo5.qty == 15, 'Must import 15');

            const combo6 = arrBundle.find(item => item.sku === SKU_COMBO6)
            assert(combo6, 'Combo 204900020 must import');
            assert(combo6.qty == 21, 'Must import 21');
            
            const combo7 = arrBundle.find(item => item.sku === SKU_COMBO7)
            assert(combo7, 'Combo 204900021 must import');
            assert(combo7.qty == 8, 'Must import 8');

            const combo8 = arrBundle.find(item => item.sku === SKU_COMBO8)
            assert(combo8, 'Combo 204900021 must import');
            assert(combo8.qty == 94, 'Must import 94');

            const combo10 = arrBundle.find(item => item.sku === SKU_COMBO10)
            assert(combo10, 'Combo 204900024 must import');
            assert(combo10.qty == 33, 'Must import 33');
        });
        it('Import 204900024 with qty 76', async function () {
            await initStock([{ sku: SKU_COMBO1, in_stock: 160 },
            { sku: SKU_COMBO2, in_stock: 160 },
            { sku: SKU_COMBO3, in_stock: 158 },
            { sku: SKU_COMBO4, in_stock: 158 },
            { sku: SKU_COMBO5, in_stock: 40 },
            { sku: SKU_COMBO6, in_stock: 53 },
            { sku: SKU_COMBO7, in_stock: 21 },
            { sku: SKU_COMBO8, in_stock: 93 },
            { sku: SKU_COMBO9, in_stock: 62 },
            { sku: SKU_COMBO10, in_stock: 46 },
            { sku: SKU_CHILD1, in_stock: 173 },
            { sku: SKU_CHILD2, in_stock: 160 },
            { sku: SKU_CHILD3, in_stock: 186 },
            { sku: SKU_CHILD4, in_stock: 158 },
            { sku: SKU_CHILD5, in_stock: 210 }])
            const arrBundle = await bundleService.getConversion({ sku: SKU_COMBO10, qty: 76, type: ImportExportConstant.TYPE_IMPORT, stock_id: 1005 })
           // console.log(arrBundle)
            assert(Array.isArray(arrBundle), 'Bundle error');
            
            const child1 = arrBundle.find(item => item.sku === SKU_CHILD1)
            assert(child1, 'Combo 100100015 must import');
            assert(child1.qty == 152, 'Must import 152 ');

            const child2 = arrBundle.find(item => item.sku === SKU_CHILD1)
            assert(child2, 'Combo 100100015 must import');
            assert(child2.qty == 152, 'Must import 152');

            const child3 = arrBundle.find(item => item.sku === SKU_CHILD3)
            assert(child3, 'Combo 100100015 must import');
            assert(child3.qty == 304, 'Must import 304');

            const child4 = arrBundle.find(item => item.sku === SKU_CHILD4)
            assert(child4, 'Combo 100100015 must import');
            assert(child4.qty == 152, 'Must import 152');

            const combo1 = arrBundle.find(item => item.sku === SKU_COMBO1)
            assert(combo1, 'Combo 204900016 must import');
            assert(combo1.qty == 152, 'Must import 152');
            
            const combo2 = arrBundle.find(item => item.sku === SKU_COMBO2)
            assert(combo2, 'Combo 204900016 must import');
            assert(combo2.qty == 152, 'Must import 152');

            const combo3 = arrBundle.find(item => item.sku === SKU_COMBO3)
            assert(combo3, 'Combo 204900016 must import');
            assert(combo3.qty == 152, 'Must import 152');

            const combo4 = arrBundle.find(item => item.sku === SKU_COMBO4)
            assert(combo4, 'Combo 204900016 must import');
            assert(combo4.qty == 52, 'Must import 52');

            const combo5 = arrBundle.find(item => item.sku === SKU_COMBO5)
            assert(combo5, 'Combo 240900019 not import');
            assert(combo5.qty == 38, 'Must import 38');

            const combo6 = arrBundle.find(item => item.sku === SKU_COMBO6)
            assert(combo6, 'Combo 204900020 must import');
            assert(combo6.qty == 51, 'Must import 51');
            
            const combo7 = arrBundle.find(item => item.sku === SKU_COMBO7)
            assert(combo7, 'Combo 204900021 must import');
            assert(combo7.qty == 19, 'Must import 19');

            const combo8 = arrBundle.find(item => item.sku === SKU_COMBO8)
            assert(combo8, 'Combo 204900022 must import');
            assert(combo8.qty == 152, 'Must import 152');

            const combo9 = arrBundle.find(item => item.sku === SKU_COMBO9)
            assert(combo9, 'Combo 204900023 must import');
            assert(combo9.qty == 101, 'Must import 101');

        });
    });
});
});