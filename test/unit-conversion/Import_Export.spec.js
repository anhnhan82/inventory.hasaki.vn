const assert = require('assert');
const ImportExportConstant = require('../../constants/importExport');
const { Bundle, BundleDetail, StockProduct, sequelize } = require('../../models')
const bundleService = require('../../services/bundleService')
const SKU_CHILD1 = 209000024;
const SKU_CHILD2 = 209000025;
const SKU_CHILD3 = 209000026;
const SKU_COMBO1 = 422200040;
const SKU_COMBO2 = 422200041;
const SKU_COMBO3 = 422200042;
async function initCombo() {
    const bundle1 = await Bundle.create({ sku: SKU_COMBO1, type: 2, description: '209000024 + 209000025' });
    await BundleDetail.create({ bundle_id: bundle1.id, sku: SKU_CHILD1, qty: 1 });
    await BundleDetail.create({ bundle_id: bundle1.id, sku: SKU_CHILD2, qty: 1 });
    const bundle2 = await Bundle.create({ sku: SKU_COMBO2, type: 2, description: '209000025 + 209000016' });
    await BundleDetail.create({ bundle_id: bundle2.id, sku: SKU_CHILD2, qty: 1 });
    await BundleDetail.create({ bundle_id: bundle2.id, sku: SKU_CHILD3, qty: 1 });
    const bundle3 = await Bundle.create({ sku: SKU_COMBO3, type: 2, description: '3 x 209000024' });
    await BundleDetail.create({ bundle_id: bundle3.id, sku: SKU_CHILD1, qty: 3});
}
async function initData() {
    await initCombo()
}
async function cleanUpStock() {
    await StockProduct.destroy({ where: {}, truncate: true });
}
async function initStock(arrParam) {
    for (let param of arrParam) {
        await StockProduct.create({ stockprod_stock_id: 1005, stockprod_sku: param.sku, stockprod_in_stock: param.in_stock, stockprod_in_comming: 0, stockprod_available: param.in_stock, stockprod_committed: 0, stockprod_utime: Math.floor(+new Date() / 1000) });
    }
}
describe('Combo multi combo', function () {

    before(async () => {
        await initData()
    })
    beforeEach(async () => {
        await cleanUpStock()  
    })
    after(async () => {
        await Bundle.destroy({ where: {}, truncate: true });
        await BundleDetail.destroy({ where: {}, truncate: true });
    })
    // Run one test case: yarn mocha test/unit-conversion/importComboMulti.spec.js -g 'Purchase 422203807 qty 1'
    describe('Combo', function () {
    //EXPORT
        it('Export 12 COMBO1', async function () {
            await initStock([
                { sku: SKU_COMBO1, in_stock: 29 },
                { sku: SKU_COMBO2, in_stock: 29 },
                { sku: SKU_COMBO3, in_stock: 16 },
                { sku: SKU_CHILD1, in_stock: 48 },
                { sku: SKU_CHILD2, in_stock: 29 },
                { sku: SKU_CHILD3, in_stock: 264 }])
            const arrBundle = await bundleService.getConversion({ sku: SKU_COMBO1, qty: 12, type: ImportExportConstant.TYPE_EXPORT, stock_id: 1005 })
            assert(Array.isArray(arrBundle), 'Bundle error');
           console.log(arrBundle);
            const child1 = arrBundle.find(item => item.sku === SKU_CHILD1)
            assert(child1, 'Child 209000024 must export');
            assert(child1.qty == 12, 'Must export 12');

            const child2 = arrBundle.find(item => item.sku === SKU_CHILD2)
            assert(child2, 'Child 209000025 must import');
            assert(child2.qty == 12, 'Must export 12');

            const combo2 = arrBundle.find(item => item.sku === SKU_COMBO2)
            assert(combo2, 'Child 422200041 must export');
            assert(combo2.qty == 12, 'Must export 12');

            const combo3 = arrBundle.find(item => item.sku === SKU_COMBO3)
            assert(combo3, 'Child 422200042 must export');
            assert(combo3.qty == 4, 'Must export 4');
        })
        it('Export 15 COMBO2', async function () {
            await initStock([
                { sku: SKU_COMBO1, in_stock: 29 },
                { sku: SKU_COMBO2, in_stock: 29 },
                { sku: SKU_COMBO3, in_stock: 16 },
                { sku: SKU_CHILD1, in_stock: 48 },
                { sku: SKU_CHILD2, in_stock: 29 },
                { sku: SKU_CHILD3, in_stock: 264 }])
            const arrBundle = await bundleService.getConversion({ sku: SKU_COMBO2, qty: 15, type: ImportExportConstant.TYPE_EXPORT, stock_id: 1005 })
            assert(Array.isArray(arrBundle), 'Bundle error');
            console.log(arrBundle);
            const child2 = arrBundle.find(item => item.sku === SKU_CHILD2)
            assert(child2, 'Child 209000025 must export');
            assert(child2.qty == 15, 'Must export 15');

            const child3 = arrBundle.find(item => item.sku === SKU_CHILD3)
            assert(child3, 'Child 209000026 must export');
            assert(child3.qty == 15, 'Must export 15');

            const combo1 = arrBundle.find(item => item.sku === SKU_COMBO1)
            assert(combo1, 'Child 422200040 must export');
            assert(combo1.qty == 15, 'Must export 15');
        })
        it('Export 12 COMBO3', async function () {
            await initStock([
                { sku: SKU_COMBO1, in_stock: 29 },
                { sku: SKU_COMBO2, in_stock: 29 },
                { sku: SKU_COMBO3, in_stock: 16 },
                { sku: SKU_CHILD1, in_stock: 48 },
                { sku: SKU_CHILD2, in_stock: 29 },
                { sku: SKU_CHILD3, in_stock: 264 }])
            const arrBundle = await bundleService.getConversion({ sku: SKU_COMBO3, qty: 12, type: ImportExportConstant.TYPE_EXPORT, stock_id: 1005 })
            assert(Array.isArray(arrBundle), 'Bundle error');
            const child1 = arrBundle.find(item => item.sku === SKU_CHILD1)
            assert(child1, 'Child 209000024 must export');
            assert(child1.qty == 36, 'Must export 36');

            const combo1 = arrBundle.find(item => item.sku === SKU_COMBO1)
            assert(combo1, 'Child 422200040 must export');
            assert(combo1.qty == 17, 'Must export 17');
        })
        //IMPORT
        it('IMPORT 28 COMBO1', async function () {
            await initStock([
                { sku: SKU_COMBO1, in_stock: 29 },
                { sku: SKU_COMBO2, in_stock: 29 },
                { sku: SKU_COMBO3, in_stock: 16 },
                { sku: SKU_CHILD1, in_stock: 48 },
                { sku: SKU_CHILD2, in_stock: 29 },
                { sku: SKU_CHILD3, in_stock: 264 }])
            const arrBundle = await bundleService.getConversion({ sku: SKU_COMBO1, qty: 200, type: ImportExportConstant.TYPE_IMPORT, stock_id: 1005 })
            assert(Array.isArray(arrBundle), 'Bundle error');
            //console.log(arrBundle)
            const child1 = arrBundle.find(item => item.sku === SKU_CHILD1)
            assert(child1, 'Child 209000024 must import');
            assert(child1.qty == 200, 'Must export 200');

            const child2 = arrBundle.find(item => item.sku === SKU_CHILD2)
            assert(child2, 'Child 209000025 must import');
            assert(child2.qty == 200, 'Must import 200');

            const combo2 = arrBundle.find(item => item.sku === SKU_COMBO2)
            assert(combo2, 'Child 422200041 must import');
            assert(combo2.qty == 200, 'Must import 200');

            const combo3 = arrBundle.find(item => item.sku === SKU_COMBO3)
            assert(combo3, 'Child 422200042 must import');
            assert(combo3.qty == 66, 'Must import 66');
            console.log(arrBundle)
        })
        it('IMPORT 18 CHILD1', async function () {
            // await cleanUpStock()
            await initStock([
                { sku: SKU_COMBO1, in_stock: 29 },
                { sku: SKU_COMBO2, in_stock: 29 },
                { sku: SKU_COMBO3, in_stock: 16 },
                { sku: SKU_CHILD1, in_stock: 48 },
                { sku: SKU_CHILD2, in_stock: 29 },
                { sku: SKU_CHILD3, in_stock: 264 }])
            const arrBundle = await bundleService.getConversion({ sku: SKU_CHILD1, qty: 180, type: ImportExportConstant.TYPE_IMPORT, stock_id: 1005 })
            assert(Array.isArray(arrBundle), 'Bundle error');
            console.log(arrBundle)
            // const combo3 = arrBundle.find(item => item.sku === SKU_COMBO3)
            // assert(combo3, 'Child 422200042 must import');
            // assert(combo3.qty == 6, 'Must import 6');
        })
        it('IMPORT 25 CHILD2', async function () {
            // await cleanUpStock()
            await initStock([
                { sku: SKU_COMBO1, in_stock: 29 },
                { sku: SKU_COMBO2, in_stock: 29 },
                { sku: SKU_COMBO3, in_stock: 16 },
                { sku: SKU_CHILD1, in_stock: 48 },
                { sku: SKU_CHILD2, in_stock: 29 },
                { sku: SKU_CHILD3, in_stock: 264 }])
            const arrBundle = await bundleService.getConversion({ sku: SKU_CHILD2, qty: 25, type: ImportExportConstant.TYPE_IMPORT, stock_id: 1005 })
            assert(Array.isArray(arrBundle), 'Bundle error');
            console.log(arrBundle)
            const combo1 = arrBundle.find(item => item.sku === SKU_COMBO1)
            assert(combo1, 'Child 422200040 must import');
            assert(combo1.qty == 19, 'Must import 15');

            const combo2 = arrBundle.find(item => item.sku === SKU_COMBO2)
            assert(combo2, 'Child 422200041 must import');
            assert(combo2.qty == 25, 'Must import 15');
        })
        it('IMPORT 23 COMBO2', async function () {
            await initStock([
                { sku: SKU_COMBO1, in_stock: 29 },
                { sku: SKU_COMBO2, in_stock: 29 },
                { sku: SKU_COMBO3, in_stock: 16 },
                { sku: SKU_CHILD1, in_stock: 48 },
                { sku: SKU_CHILD2, in_stock: 29 },
                { sku: SKU_CHILD3, in_stock: 264 }])
            const arrBundle = await bundleService.getConversion({ sku: SKU_COMBO2, qty: 23, type: ImportExportConstant.TYPE_IMPORT, stock_id: 1005 })
            assert(Array.isArray(arrBundle), 'Bundle error');
            console.log(arrBundle)
            const child2 = arrBundle.find(item => item.sku === SKU_CHILD2)
            assert(child2, 'Child 209000025 must import');
            assert(child2.qty == 23, 'Must import 23');

            const child3 = arrBundle.find(item => item.sku === SKU_CHILD3)
            assert(child3, 'Child 209000026 must import');
            assert(child3.qty == 23, 'Must import 23');

            const combo1 = arrBundle.find(item => item.sku === SKU_COMBO1)
            assert(combo1, 'Child 422200040 must import');
            assert(combo1.qty == 19, 'Must import 19');
        })
        it('IMPORT 9 Combo3', async function () {
            // await cleanUpStock()
            await initStock([
                { sku: SKU_COMBO1, in_stock: 29 },
                { sku: SKU_COMBO2, in_stock: 29 },
                { sku: SKU_COMBO3, in_stock: 16 },
                { sku: SKU_CHILD1, in_stock: 48 },
                { sku: SKU_CHILD2, in_stock: 29 },
                { sku: SKU_CHILD3, in_stock: 264 }])
            const arrBundle = await bundleService.getConversion({ sku: SKU_COMBO3, qty: 9, type: ImportExportConstant.TYPE_IMPORT, stock_id: 1005 })
            assert(Array.isArray(arrBundle), 'Bundle error');
            console.log(arrBundle)
            const child1 = arrBundle.find(item => item.sku === SKU_CHILD1)
            assert(child1, 'Child 209000024 must import');
            assert(child1.qty == 27, 'Must import 27');

        })

        // it('IMPORT TEST', async function () {
        //     await initStock([
        //         { sku: SKU_COMBO1, in_stock: -9 },
        //         { sku: SKU_COMBO2, in_stock: -10 },
        //         { sku: SKU_CHILD1, in_stock: 12 },
        //         { sku: SKU_CHILD2, in_stock: -9 },
        //         { sku: SKU_CHILD3, in_stock: -10 }])
        //     const arrBundle = await bundleService.getConversion({ sku: SKU_COMBO1, qty: 5, type: ImportExportConstant.TYPE_EXPORT, stock_id: 1005 })
        //     const arrBundle1 = await bundleService.getConversion({ sku: SKU_COMBO2, qty: 3, type: ImportExportConstant.TYPE_EXPORT, stock_id: 1005 })
        //     assert(Array.isArray(arrBundle), 'Bundle error');
        //     console.log(arrBundle)
        //     console.log(arrBundle1)
        // //     const child1 = arrBundle.find(item => item.sku === SKU_CHILD1)
        // //     assert(child1, 'Child 209000024 must import');
        // //     assert(child1.qty == 200, 'Must export 200');

        // //     const child2 = arrBundle.find(item => item.sku === SKU_CHILD2)
        // //     assert(child2, 'Child 209000025 must import');
        // //     assert(child2.qty == 200, 'Must import 200');

        // //     const child2 = arrBundle.find(item => item.sku === SKU_CHILD2)
        // //     assert(child2, 'Child 209000025 must import');
        // //     assert(child2.qty == 200, 'Must import 200');
        //})
    })
})