const assert = require('assert');
const importExportService = require('../../services/stockImportExportService')
const ImportExportConstant = require('../../constants/importExport');
const bundleService = require('../../services/bundleService')
const { Bundle, BundleDetail, StockProduct,StockImportExport, sequelize } = require('../../models')
        const SKU_CHILD1 = 209000024; 
        const SKU_CHILD2 = 209000013;  
        const SKU_COMBO1 = 422200040; 
        const SKU_COMBO2 = 422200041;
        const SKU_COMBO3 = 422200042;
        const SOURCE_ID_1 = 21042813200;
async function initCombo() {
}
async function cleanUpStock() {
    await StockProduct.destroy({ where: {}, truncate: true });
    await StockImportExport.destroy({ where: {}, truncate: true });
}
async function initStock(arrParam) {
    for (let param of arrParam) {
        await StockProduct.create({ stockprod_stock_id: 1005, stockprod_sku: param.sku, stockprod_in_stock: param.in_stock, stockprod_in_comming: 0, stockprod_available: param.in_stock, stockprod_committed: 0, stockprod_utime: Math.floor(+new Date() / 1000)});    
    }
}
async function initImportExport(arrParam) {
    for (let param of arrParam) {
        await StockImportExport.create({
            stockimex_stock_id: 1005, stockprod_sku: param.sku, stockprod_in_stock: param.in_stock, stockprod_in_comming: 0, stockprod_available: param.in_stock, stockprod_committed: 0, stockprod_utime: Math.floor(+new Date() / 1000)
        });    
    }
}
async function initData() {
    await initCombo()
}
describe('Complex combo', function () {

    before(async () => {
        await initData()
        await cleanUpStock()
    })
    beforeEach(async () => {
        await cleanUpStock()
    })
    after(async () => {
        await Bundle.destroy({ where: {}, truncate: true });
        await BundleDetail.destroy({ where: {}, truncate: true });
    })
    it('Export_Import le', async function () {
        await initStock([
            { sku: SKU_COMBO1, in_stock: 8 },
            { sku: SKU_COMBO2, in_stock: 4 },
            { sku: SKU_COMBO3, in_stock: 2 },
            { sku: SKU_CHILD1, in_stock: 8 },
            { sku: SKU_CHILD2, in_stock: 10 }])
        const QTY = 1;
        const stockProductOrginal = await StockProduct.findOne({ where: { stockprod_sku: SKU_COMBO2, stockprod_stock_id: 1005 } })
        await importExportService.insert({ stock_id: 1005, product_sku: SKU_COMBO2, qty: QTY, type: ImportExportConstant.TYPE_EXPORT, user_id: 1001, import_id: SOURCE_ID_1, import_type: ImportExportConstant.IMPORT_TYPE_PURCHASE_ORDER })
        const stockProduct = await StockProduct.findOne({ where: { stockprod_sku: SKU_COMBO2, stockprod_stock_id: 1005 } })
        assert((stockProductOrginal.stockprod_in_stock - QTY) === stockProduct.stockprod_in_stock, 'Must import 1');
        //console.log(stockProduct.stockprod_in_stock)
        
        await cleanUpStock()
        await initStock([{ sku: SKU_COMBO3, in_stock: 2 }])
        const stockProductOrginal2 = await StockProduct.findOne({ where: { stockprod_sku: SKU_COMBO3, stockprod_stock_id: 1005 } })
        await importExportService.insert({ stock_id: 1005, product_sku: SKU_COMBO3, qty: QTY, type: ImportExportConstant.TYPE_IMPORT, user_id: 1001, import_id: SOURCE_ID_1, import_type: ImportExportConstant.IMPORT_TYPE_PURCHASE_ORDER })
        const stockProduct2 = await StockProduct.findOne({ where: { stockprod_sku: SKU_COMBO3, stockprod_stock_id: 1005 } })
        assert((stockProductOrginal2.stockprod_in_stock + QTY) === stockProduct2.stockprod_in_stock, 'Must import 18');
        //console.log(stockProduct2.stockprod_in_stock)

        await initStock([{ sku: SKU_CHILD1, in_stock: 8 }])
        const stockProductOrginal3 = await StockProduct.findOne({ where: { stockprod_sku: SKU_CHILD1, stockprod_stock_id: 1005 } })
        await importExportService.insert({ stock_id: 1005, product_sku: SKU_CHILD1, qty: QTY, type: ImportExportConstant.TYPE_EXPORT, user_id: 1001, import_id: SOURCE_ID_1, import_type: ImportExportConstant.IMPORT_TYPE_PURCHASE_ORDER })
        const stockProduct3 = await StockProduct.findOne({ where: { stockprod_sku: SKU_CHILD1, stockprod_stock_id: 1005 } })
        assert((stockProductOrginal3.stockprod_in_stock - QTY) === stockProduct3.stockprod_in_stock, 'Must import 1');
        //console.log(stockProduct3.stockprod_in_stock)

        await initStock([{ sku: SKU_CHILD2, in_stock: 8 }])
        const stockProductOrginal4 = await StockProduct.findOne({ where: { stockprod_sku: SKU_CHILD2, stockprod_stock_id: 1005 } })
        await importExportService.insert({ stock_id: 1005, product_sku: SKU_CHILD2, qty: QTY, type: ImportExportConstant.TYPE_IMPORT, user_id: 1001, import_id: SOURCE_ID_1, import_type: ImportExportConstant.IMPORT_TYPE_CANCEL })
        const stockProducOut4 = await StockProduct.findOne({ where: { stockprod_sku: SKU_CHILD2, stockprod_stock_id: 1005 } })
        assert((stockProductOrginal4.stockprod_in_stock + QTY) === stockProducOut4.stockprod_in_stock, 'Must import 18');
        //console.log(stockProducOut4.stockprod_in_stock)
        
        await cleanUpStock()
        await initStock([{ sku: SKU_CHILD1, in_stock: 10 }])
        const stockProductOrginal5 = await StockProduct.findOne({ where: { stockprod_sku: SKU_CHILD1, stockprod_stock_id: 1005 } })
        await importExportService.insert({ stock_id: 1005, product_sku: SKU_CHILD1, qty: QTY, type: ImportExportConstant.TYPE_EXPORT, user_id: 1001, import_id: SOURCE_ID_1, import_type: ImportExportConstant.IMPORT_TYPE_PURCHASE_ORDER })
        const stockProducOut5 = await StockProduct.findOne({ where: { stockprod_sku: SKU_CHILD1, stockprod_stock_id: 1005 } })
        assert((stockProductOrginal5.stockprod_in_stock - QTY) === stockProducOut5.stockprod_in_stock, 'Must import 18');
        //console.log(stockProducOut5.stockprod_in_stock)
    });
    it('Import_Export combo', async function () {
        await initStock([
            { sku: SKU_COMBO1, in_stock: 8 },
            { sku: SKU_COMBO2, in_stock: 4 },
            { sku: SKU_COMBO3, in_stock: 2 },
            { sku: SKU_CHILD1, in_stock: 8 },
            { sku: SKU_CHILD2, in_stock: 10 }])
    // import CB1: 209000024 + 209000013    
        const QTY1 = 6;
        const QTY_OUT = 4;
        const stockProductOrginal = await StockProduct.findOne({ where: { stockprod_sku: SKU_COMBO1, stockprod_stock_id: 1005 } })
        await importExportService.insert({ stock_id: 1005, product_sku: SKU_COMBO1, qty: QTY1, type: ImportExportConstant.TYPE_IMPORT, user_id: 1001, import_id: SOURCE_ID_1, import_type: ImportExportConstant.IMPORT_TYPE_PURCHASE_ORDER })
        const stockProduct = await StockProduct.findOne({ where: { stockprod_sku: SKU_COMBO1, stockprod_stock_id: 1005 } })
        assert((stockProductOrginal.stockprod_in_stock + QTY1) === stockProduct.stockprod_in_stock, 'Must import 18');
        //console.log(stockProduct.stockprod_in_stock)
        await importExportService.insert({ stock_id: 1005, product_sku: SKU_COMBO1, qty: QTY_OUT, type: ImportExportConstant.TYPE_EXPORT, user_id: 1001, import_id: SOURCE_ID_1, import_type: ImportExportConstant.IMPORT_TYPE_PURCHASE_ORDER })
        const stockProductOut = await StockProduct.findOne({ where: { stockprod_sku: SKU_COMBO1, stockprod_stock_id: 1005 } })
        assert((stockProductOrginal.stockprod_in_stock + QTY1 - QTY_OUT) === stockProductOut.stockprod_in_stock, 'Must import 18');
        //console.log(stockProductOut.stockprod_in_stock)
    // Combo 1
        const QTY2 = 98; // sl import
        const QTY_OUT2 = 57; // sl export
        const stockProductOrginal1 = await StockProduct.findOne({ where: { stockprod_sku: SKU_COMBO1, stockprod_stock_id: 1005 } })
        await importExportService.insert({ stock_id: 1005, product_sku: SKU_COMBO1, qty: QTY2, type: ImportExportConstant.TYPE_IMPORT, user_id: 1001, import_id: SOURCE_ID_1, import_type: ImportExportConstant.IMPORT_TYPE_INTERNAL_TRANSFER })
        const stockProduct1 = await StockProduct.findOne({ where: { stockprod_sku: SKU_COMBO1, stockprod_stock_id: 1005 } })
        assert((stockProductOrginal1.stockprod_in_stock + QTY2) === stockProduct1.stockprod_in_stock, 'Must import 258');
        //console.log(stockProduct1.stockprod_in_stock)

        await importExportService.insert({ stock_id: 1005, product_sku: SKU_COMBO1, qty: QTY_OUT2, type: ImportExportConstant.TYPE_EXPORT, user_id: 1001, import_id: SOURCE_ID_1, import_type: ImportExportConstant.IMPORT_TYPE_PURCHASE_ORDER })
        const stockProductOut1 = await StockProduct.findOne({ where: { stockprod_sku: SKU_COMBO1, stockprod_stock_id: 1005 } })
        assert((stockProductOrginal1.stockprod_in_stock + QTY2 - QTY_OUT2) === stockProductOut1.stockprod_in_stock, 'Must Export 201');
        //console.log(stockProductOut1.stockprod_in_stock)
    // //CB2
        await cleanUpStock()
        const QTY4 = 4;
        const QTY_OUT4 = 3;
        await initStock([{ sku: SKU_COMBO2, in_stock: 4 }]);
        const stockProductOrginal7 = await StockProduct.findOne({ where: { stockprod_sku: SKU_COMBO2, stockprod_stock_id: 1005 } })
        await importExportService.insert({ stock_id: 1005, product_sku: SKU_COMBO2, qty: QTY_OUT4, type: ImportExportConstant.TYPE_EXPORT, user_id: 1001, import_id: SOURCE_ID_1, import_type: ImportExportConstant.IMPORT_TYPE_ORDER })
        const stockProductOut7 = await StockProduct.findOne({ where: { stockprod_sku: SKU_COMBO2, stockprod_stock_id: 1005 } })
        assert((stockProductOrginal7.stockprod_in_stock - QTY_OUT4) === stockProductOut7.stockprod_in_stock, 'Must export 1');
        //console.log(stockProductOut7.stockprod_in_stock);
        await importExportService.insert({ stock_id: 1005, product_sku: SKU_COMBO2, qty: QTY4, type: ImportExportConstant.TYPE_IMPORT, user_id: 1001, import_id: SOURCE_ID_1, import_type: ImportExportConstant.IMPORT_TYPE_PURCHASE_ORDER })
        const stockProduct7 = await StockProduct.findOne({ where: { stockprod_sku: SKU_COMBO2, stockprod_stock_id: 1005 } })
        assert((stockProductOrginal7.stockprod_in_stock - QTY_OUT4 + QTY4) === stockProduct7.stockprod_in_stock, 'Must import 5');
        //console.log(stockProduct7.stockprod_in_stock);    
    ////CB3  
        const QTY5 = 2;
        const QTY_OUT5 = 0;
        await initStock([{ sku: SKU_COMBO3, in_stock: 2 }]);
        const stockProductOrginal8 = await StockProduct.findOne({ where: { stockprod_sku: SKU_COMBO3, stockprod_stock_id: 1005 } })
        await importExportService.insert({ stock_id: 1005, product_sku: SKU_COMBO3, qty: QTY_OUT5, type: ImportExportConstant.TYPE_EXPORT, user_id: 1001, import_id: SOURCE_ID_1, import_type: ImportExportConstant.IMPORT_TYPE_ORDER })
        const stockProductOut8 = await StockProduct.findOne({ where: { stockprod_sku: SKU_COMBO3, stockprod_stock_id: 1005 } })
        assert((stockProductOrginal8.stockprod_in_stock - QTY_OUT5) === stockProductOut8.stockprod_in_stock, 'Must export 2');
        //console.log(stockProductOut8.stockprod_in_stock);
        await importExportService.insert({ stock_id: 1005, product_sku: SKU_COMBO3, qty: QTY5, type: ImportExportConstant.TYPE_IMPORT, user_id: 1001, import_id: SOURCE_ID_1, import_type: ImportExportConstant.IMPORT_TYPE_PURCHASE_ORDER })
        const stockProduct8 = await StockProduct.findOne({ where: { stockprod_sku: SKU_COMBO3, stockprod_stock_id: 1005 } })
        assert((stockProductOrginal8.stockprod_in_stock - QTY_OUT5 + QTY5) === stockProduct8.stockprod_in_stock, 'Must import 0');
        // console.log(stockProduct8.stockprod_in_stock);
        
    });
    it('Import_Export SKU', async function () {
        await initStock([
            { sku: SKU_COMBO1, in_stock: 8 },
            { sku: SKU_COMBO2, in_stock: 4 },
            { sku: SKU_COMBO3, in_stock: 2 },
            { sku: SKU_CHILD1, in_stock: 8 },
            { sku: SKU_CHILD2, in_stock: 10 }])
    //Child1
        const QTY1 = 2; // sl import
        const QTY_OUT1 = 4; // sl export
        await initStock([{ sku: SKU_CHILD1, in_stock: 8 }]);
        const stockProductOrginal = await StockProduct.findOne({ where: { stockprod_sku: SKU_CHILD1, stockprod_stock_id: 1005 } })
        await importExportService.insert({ stock_id: 1005, product_sku: SKU_CHILD1, qty: QTY_OUT1, type: ImportExportConstant.TYPE_EXPORT, user_id: 1001, import_id: SOURCE_ID_1, import_type: ImportExportConstant.IMPORT_TYPE_ORDER })
        const stockProductOut = await StockProduct.findOne({ where: { stockprod_sku: SKU_CHILD1, stockprod_stock_id: 1005 } })
        assert((stockProductOrginal.stockprod_in_stock - QTY_OUT1) === stockProductOut.stockprod_in_stock, 'Must export 6');
       // console.log(stockProductOut.stockprod_in_stock);
        await importExportService.insert({ stock_id: 1005, product_sku: SKU_CHILD1, qty: QTY1, type: ImportExportConstant.TYPE_IMPORT, user_id: 1001, import_id: SOURCE_ID_1, import_type: ImportExportConstant.IMPORT_TYPE_PURCHASE_ORDER })
        const stockProduct = await StockProduct.findOne({ where: { stockprod_sku: SKU_CHILD1, stockprod_stock_id: 1005 } })
        assert((stockProductOrginal.stockprod_in_stock - QTY_OUT1 + QTY1) === stockProduct.stockprod_in_stock, 'Must import 8');
        //console.log(stockProduct.stockprod_in_stock);
    // Child2
        await cleanUpStock()
        await initStock([{ sku: SKU_CHILD2, in_stock: 10 }]);
        const stockProductOrginal2 = await StockProduct.findOne({ where: { stockprod_sku: SKU_CHILD2, stockprod_stock_id: 1005 } })
        await importExportService.insert({ stock_id: 1005, product_sku: SKU_CHILD2, qty: QTY_OUT1, type: ImportExportConstant.TYPE_EXPORT, user_id: 1001, import_id: SOURCE_ID_1, import_type: ImportExportConstant.IMPORT_TYPE_ORDER })
        const stockProductOut2 = await StockProduct.findOne({ where: { stockprod_sku: SKU_CHILD2, stockprod_stock_id: 1005 } })
        assert((stockProductOrginal2.stockprod_in_stock - QTY_OUT1) === stockProductOut2.stockprod_in_stock, 'Must export 3');
       // console.log(stockProductOut2.stockprod_in_stock);
        await importExportService.insert({ stock_id: 1005, product_sku: SKU_CHILD2, qty: QTY1, type: ImportExportConstant.TYPE_IMPORT, user_id: 1001, import_id: SOURCE_ID_1, import_type: ImportExportConstant.IMPORT_TYPE_PURCHASE_ORDER })
        const stockProduct2 = await StockProduct.findOne({ where: { stockprod_sku: SKU_CHILD2, stockprod_stock_id: 1005 } })
        assert((stockProductOrginal2.stockprod_in_stock - QTY_OUT1 + QTY1) === stockProduct2.stockprod_in_stock, 'Must import 2');
        //console.log(stockProduct2.stockprod_in_stock);
    });
    it('Import combo', async function () {
        await initStock([
            { sku: SKU_COMBO1, in_stock: 8 },
            { sku: SKU_COMBO2, in_stock: 4 },
            { sku: SKU_COMBO3, in_stock: 2 },
            { sku: SKU_CHILD1, in_stock: 8 },
            { sku: SKU_CHILD2, in_stock: 15 }])
        const QTY1 = 7;
        const QTY_OUT = 4;
    // export CB1
        const stockProductOrginal = await StockProduct.findOne({ where: { stockprod_sku: SKU_COMBO1, stockprod_stock_id: 1005 } })
        await importExportService.insert({ stock_id: 1005, product_sku: SKU_COMBO1, qty: QTY_OUT, type: ImportExportConstant.TYPE_EXPORT, user_id: 1001, import_id: SOURCE_ID_1, import_type: ImportExportConstant.IMPORT_TYPE_PURCHASE_ORDER })
        const stockProductOut = await StockProduct.findOne({ where: { stockprod_sku: SKU_COMBO1, stockprod_stock_id: 1005 } })
        assert((stockProductOrginal.stockprod_in_stock - QTY_OUT) === stockProductOut.stockprod_in_stock, 'Must export 4');
        //console.log(stockProductOut.stockprod_in_stock)
    // import CB2:
        await cleanUpStock()
        await initStock([{ sku: SKU_COMBO2, in_stock: 4 }])
        const stockProductOrginal1 = await StockProduct.findOne({ where: { stockprod_sku: SKU_COMBO2, stockprod_stock_id: 1005 } })
        await importExportService.insert({ stock_id: 1005, product_sku: SKU_COMBO2, qty: QTY1, type: ImportExportConstant.TYPE_IMPORT, user_id: 1001, import_id: SOURCE_ID_1, import_type: ImportExportConstant.IMPORT_TYPE_PURCHASE_ORDER })
        const stockProduct1 = await StockProduct.findOne({ where: { stockprod_sku: SKU_COMBO2, stockprod_stock_id: 1005 } })
        assert((stockProductOrginal1.stockprod_in_stock + QTY1) === stockProduct1.stockprod_in_stock, 'Must import 15');
        //console.log(stockProduct1.stockprod_in_stock)
    // import CB3: 3 x 209000024
        //await cleanUpStock()
        await initStock([{ sku: SKU_COMBO3, in_stock: 2 }])
        const stockProductOrginal2 = await StockProduct.findOne({ where: { stockprod_sku: SKU_COMBO3, stockprod_stock_id: 1005 } })
        await importExportService.insert({ stock_id: 1005, product_sku: SKU_COMBO3, qty: QTY1, type: ImportExportConstant.TYPE_IMPORT, user_id: 1001, import_id: SOURCE_ID_1, import_type: ImportExportConstant.IMPORT_TYPE_PURCHASE_ORDER })
        const stockProduct2 = await StockProduct.findOne({ where: { stockprod_sku: SKU_COMBO3, stockprod_stock_id: 1005 } })
        assert((stockProductOrginal2.stockprod_in_stock + QTY1) === stockProduct2.stockprod_in_stock, 'Must import 13');
        //console.log(stockProduct2.stockprod_in_stock)
    // Export Sku_child1:
        //await cleanUpStock()
        await initStock([{ sku: SKU_CHILD1, in_stock: 8 }])
        const stockProductOrginal3 = await StockProduct.findOne({ where: { stockprod_sku: SKU_CHILD1, stockprod_stock_id: 1005 } })
        await importExportService.insert({ stock_id: 1005, product_sku: SKU_CHILD1, qty: QTY_OUT, type: ImportExportConstant.TYPE_EXPORT, user_id: 1001, import_id: SOURCE_ID_1, import_type: ImportExportConstant.IMPORT_TYPE_PURCHASE_ORDER })
        const stockProducOut3 = await StockProduct.findOne({ where: { stockprod_sku: SKU_CHILD1, stockprod_stock_id: 1005 } })
        assert((stockProductOrginal3.stockprod_in_stock - QTY_OUT) === stockProducOut3.stockprod_in_stock, 'Must export 4');
        //console.log(stockProducOut3.stockprod_in_stock)
    // Export SKU_Child2:
        //await cleanUpStock()
        await initStock([{ sku: SKU_CHILD2, in_stock: 15 }])
        const stockProductOrginal4 = await StockProduct.findOne({ where: { stockprod_sku: SKU_CHILD2, stockprod_stock_id: 1005 } })
        await importExportService.insert({ stock_id: 1005, product_sku: SKU_CHILD2, qty: QTY_OUT, type: ImportExportConstant.TYPE_EXPORT, user_id: 1001, import_id: SOURCE_ID_1, import_type: ImportExportConstant.IMPORT_TYPE_CANCEL })
        const stockProducOut4 = await StockProduct.findOne({ where: { stockprod_sku: SKU_CHILD2, stockprod_stock_id: 1005 } })
        assert((stockProductOrginal4.stockprod_in_stock - QTY_OUT) === stockProducOut4.stockprod_in_stock, 'Must export 11');
        //console.log(stockProducOut4.stockprod_in_stock)
    });
});