const assert = require('assert');
const ImportExportConstant = require('../../constants/importExport');
const { Bundle, BundleDetail, StockProduct, StockImportExport } = require('../../models')
const importExportService = require('../../services/stockImportExportService')

async function initData() {
    
}

async function cleanUp() {
    await StockProduct.destroy({ where: {}, truncate: true });
    await StockImportExport.destroy({ where: {}, truncate: true });
}
async function initStock(arrParam) {
    for (let param of arrParam) {
        await StockProduct.create({ stockprod_stock_id: 1005, stockprod_sku: param.sku, stockprod_in_stock: param.in_stock, stockprod_in_comming: 0, stockprod_available: param.in_stock, stockprod_committed: 0, stockprod_utime: Math.floor(+new Date() / 1000)});    
    }
}
async function initImportExport(arrParam) {
    for (let param of arrParam) {
        await StockImportExport.create({
            stockimex_stock_id: 1005, stockprod_sku: param.sku, stockprod_in_stock: param.in_stock, stockprod_in_comming: 0, stockprod_available: param.in_stock, stockprod_committed: 0, stockprod_utime: Math.floor(+new Date() / 1000)
        });    
    }
}

describe('Simple import export', function () {

    before(async () => {
        await initData()
    })
    beforeEach(async () => {
        await cleanUp()
    })
    after(async () => {
        await cleanUp()
    })

    it('Import without exist stock', async function () { 
        const SKU = 100160018;
        const SOURCE_ID = 21102104280153;
        await importExportService.insert({ stock_id: 1005, product_sku: 100160018, qty: 1, type: 1, user_id: 1001, import_id: SOURCE_ID, import_type: 1 });
        const stockProduct = await StockProduct.findOne({ where: { stockprod_sku: SKU, stockprod_stock_id: 1005 } })
        assert(stockProduct !== undefined, 'Stock not found');
        assert(stockProduct.stockprod_in_stock === 1, 'In-stock is not valid');
        assert(stockProduct.stockprod_available === 1, 'Available is not valid');
        assert(stockProduct.stockprod_committed === 0, 'Committed is not valid');
        assert(stockProduct.stockprod_in_comming === 0, 'Im-comming is not valid');
        
        const importExport = await StockImportExport.findOne({ where: { stockimex_stock_id: 1005, stockimex_sku: SKU, stockimex_type: 1, stockimex_import_id: SOURCE_ID } })
        assert(importExport !== undefined, 'ImportExport not found');
        assert(importExport.stockimex_qty === 1, 'Must import 1');
    })
    it('Import with stock 1', async function () { 
        const SKU = 100160018;
        const SOURCE_ID = 21102104280153;
        //Init stock
        await initStock([{ sku: SKU, in_stock: 1 }]);
        await importExportService.insert({ stock_id: 1005, product_sku: 100160018, qty: 1, type: 1, user_id: 1001, import_id: SOURCE_ID, import_type: 1 });
        const stockProduct = await StockProduct.findOne({ where: { stockprod_sku: SKU, stockprod_stock_id: 1005 } })
        assert(stockProduct !== undefined, 'Stock not found');
        assert(stockProduct.stockprod_in_stock === 2, 'In-stock is not valid');
        assert(stockProduct.stockprod_available === 2, 'Available is not valid');
        assert(stockProduct.stockprod_committed === 0, 'Committed is not valid');
        assert(stockProduct.stockprod_in_comming === 0, 'Im-comming is not valid');
        
        const importExport = await StockImportExport.findOne({ where: { stockimex_stock_id: 1005, stockimex_sku: SKU, stockimex_type: 1, stockimex_import_id: SOURCE_ID } })
        assert(importExport !== undefined, 'ImportExport not found');
        assert(importExport.stockimex_qty === 1, 'Must import 1');
    })

    it('Import with stock 2', async function () { 
        const SKU = 100160018;
        const SOURCE_ID = 21102104280153;
        //Init stock
        await initStock([{ sku: SKU, in_stock: 2 }]);
        await importExportService.insert({ stock_id: 1005, product_sku: 100160018, qty: 8, type: 1, user_id: 1001, import_id: SOURCE_ID, import_type: 1 });
        const stockProduct = await StockProduct.findOne({ where: { stockprod_sku: SKU, stockprod_stock_id: 1005 } })
        assert(stockProduct !== undefined, 'Stock not found');
        assert(stockProduct.stockprod_in_stock === 10, 'In-stock is not valid');
        assert(stockProduct.stockprod_available === 10, 'Available is not valid');
        assert(stockProduct.stockprod_committed === 0, 'Committed is not valid');
        assert(stockProduct.stockprod_in_comming === 0, 'Im-comming is not valid');
        
        const importExport = await StockImportExport.findOne({ where: { stockimex_stock_id: 1005, stockimex_sku: SKU, stockimex_type: 1, stockimex_import_id: SOURCE_ID } })
        assert(importExport !== undefined, 'ImportExport not found');
        assert(importExport.stockimex_qty === 8, 'Must import 8');
    })

    it('Export without stock', async function () { 
        const SKU = 100160018;
        const SOURCE_ID = 21102104280153;
        await importExportService.insert({ stock_id: 1005, product_sku: 100160018, qty: 2, type: 2, user_id: 1001, import_id: SOURCE_ID, import_type: 1 });
        const stockProduct = await StockProduct.findOne({ where: { stockprod_sku: SKU, stockprod_stock_id: 1005 } })
        console.log(stockProduct)
        assert(stockProduct !== undefined, 'Stock not found');
        assert(stockProduct.stockprod_in_stock === -2, 'In-stock is not valid');
        assert(stockProduct.stockprod_available === -2, 'Available is not valid');
        assert(stockProduct.stockprod_committed === 0, 'Committed is not valid');
        assert(stockProduct.stockprod_in_comming === 0, 'Im-comming is not valid');
        
        const importExport = await StockImportExport.findOne({ where: { stockimex_stock_id: 1005, stockimex_sku: SKU, stockimex_type: 2, stockimex_import_id: SOURCE_ID } })
        assert(importExport !== undefined, 'ImportExport not found');
        assert(importExport.stockimex_qty === 2, 'Must import 1');
    })

    it('Export with stock 2', async function () { 
        const SKU = 100160018;
        const SOURCE_ID = 21102104280153;
        await initStock([{ sku: SKU, in_stock: 10 }]);

        await importExportService.insert({ stock_id: 1005, product_sku: 100160018, qty: 2, type: 2, user_id: 1001, import_id: SOURCE_ID, import_type: 1 });
        const stockProduct = await StockProduct.findOne({ where: { stockprod_sku: SKU, stockprod_stock_id: 1005 } })
        console.log(stockProduct)
        assert(stockProduct !== undefined, 'Stock not found');
        assert(stockProduct.stockprod_in_stock === 8, 'In-stock is not valid');
        assert(stockProduct.stockprod_available === 8, 'Available is not valid');
        assert(stockProduct.stockprod_committed === 0, 'Committed is not valid');
        assert(stockProduct.stockprod_in_comming === 0, 'Im-comming is not valid');
        
        const importExport = await StockImportExport.findOne({ where: { stockimex_stock_id: 1005, stockimex_sku: SKU, stockimex_type: 2, stockimex_import_id: SOURCE_ID } })
        assert(importExport !== undefined, 'ImportExport not found');
        assert(importExport.stockimex_qty === 2, 'Must import 1');
    })
})