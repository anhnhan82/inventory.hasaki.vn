const assert = require('assert');
const ImportExportConstant = require('../../constants/importExport');
const { Bundle, BundleDetail, StockProduct, StockImportExport } = require('../../models')
const importExportService = require('../../services/stockImportExportService')
async function initData() { 
}

async function cleanUp() {
    await StockProduct.destroy({ where: {}, truncate: true });
    await StockImportExport.destroy({ where: {}, truncate: true });
}
async function initStock(arrParam) {
    for (let param of arrParam) {
        await StockProduct.create({ stockprod_stock_id: 1005, stockprod_sku: param.sku, stockprod_in_stock: param.in_stock, stockprod_in_comming: 0, stockprod_available: param.in_stock, stockprod_committed: 0, stockprod_utime: Math.floor(+new Date() / 1000)});    
    }
}
async function initImportExport(arrParam) {
    for (let param of arrParam) {
        await StockImportExport.create({
            stockimex_stock_id: 1005, stockprod_sku: param.sku, stockprod_in_stock: param.in_stock, stockprod_in_comming: 0, stockprod_available: param.in_stock, stockprod_committed: 0, stockprod_utime: Math.floor(+new Date() / 1000)
        });    
    }
}
describe('Simple import export', function () {

    before(async () => {
        await initData()
    })
    beforeEach(async () => {
        await cleanUp()
    })
    after(async () => {
        await cleanUp()
    })
    //yarn mocha --grep "Mix export/import" test/stock-import-export/Main.spec.js
    //không khai báo combo 
    it('Import SKU 209000024 with qty 21', async function () { 
        const SKU_CHILD1 = 209000011; //73
        const SKU_CHILD2 = 209000012;  //108
        const SKU_CHILD3 = 209000013;  // 222
        const SKU_CHILD4 = 209000014;  //- 41
        const SKU_CHILD5 = 209000015;  // 21
        const SKU_COMBO1 = 422200040; //-40 = 100550128 + 205600184 + 205600183 + 205600182 + 205600181
        const SOURCE_ID_1 = 21042813200
        const QTY = 21;
            await initStock([{ sku: SKU_CHILD1, in_stock: 73 }]);
            const stockProductOrginal1 = await StockProduct.findOne({ where: { stockprod_sku: SKU_CHILD1, stockprod_stock_id: 1005 } })
            await importExportService.insert({ stock_id:1005, product_sku: SKU_CHILD1, qty: QTY, type: ImportExportConstant.TYPE_IMPORT, user_id: 1001, import_id: SOURCE_ID_1, import_type: ImportExportConstant.IMPORT_TYPE_USED})
            const stockProduct1 = await StockProduct.findOne({ where: { stockprod_sku: SKU_CHILD1, stockprod_stock_id: 1005 } })
            assert((stockProductOrginal1.stockprod_in_stock + QTY) === stockProduct1.stockprod_in_stock, 'Must import 21');
            console.log(stockProduct1.stockprod_in_stock);

            await initStock([{ sku: SKU_CHILD2, in_stock: 108 }]);
            const stockProductOrginal2 = await StockProduct.findOne({ where: { stockprod_sku: SKU_CHILD2, stockprod_stock_id: 1005 } })
            await importExportService.insert({ stock_id:1005, product_sku: SKU_CHILD2, qty: QTY, type: ImportExportConstant.TYPE_IMPORT, user_id: 1001, import_id: SOURCE_ID_1, import_type: ImportExportConstant.IMPORT_TYPE_ADJUSTMENT_USED })
            const stockProduct2 = await StockProduct.findOne({ where: { stockprod_sku: SKU_CHILD2, stockprod_stock_id: 1005 } })
            assert((stockProductOrginal2.stockprod_in_stock + QTY) === stockProduct2.stockprod_in_stock, 'Must import 21');
            console.log(stockProduct2.stockprod_in_stock);

            await initStock([{ sku: SKU_CHILD3, in_stock: 222 }]);
            const stockProductOrginal3 = await StockProduct.findOne({ where: { stockprod_sku: SKU_CHILD3, stockprod_stock_id: 1005 } })
            await importExportService.insert({ stock_id:1005, product_sku: SKU_CHILD3, qty: QTY, type: ImportExportConstant.TYPE_IMPORT, user_id: 1001, import_id: SOURCE_ID_1, import_type: ImportExportConstant.IMPORT_TYPE_ADJUSTMENT_USED })
            const stockProduct3 = await StockProduct.findOne({ where: { stockprod_sku: SKU_CHILD3, stockprod_stock_id: 1005 } })
            assert((stockProductOrginal3.stockprod_in_stock + QTY) === stockProduct3.stockprod_in_stock, 'Must import 21');
            console.log(stockProduct3.stockprod_in_stock);

            await initStock([{ sku: SKU_CHILD4, in_stock: -41 }]);
            const stockProductOrginal4 = await StockProduct.findOne({ where: { stockprod_sku: SKU_CHILD4, stockprod_stock_id: 1005 } })
            await importExportService.insert({ stock_id:1005, product_sku: SKU_CHILD4, qty: QTY, type: ImportExportConstant.TYPE_IMPORT, user_id: 1001, import_id: SOURCE_ID_1, import_type: ImportExportConstant.IMPORT_TYPE_ADJUSTMENT_USED })
            const stockProduct4 = await StockProduct.findOne({ where: { stockprod_sku: SKU_CHILD4, stockprod_stock_id: 1005 } })
            assert((stockProductOrginal4.stockprod_in_stock + QTY) === stockProduct4.stockprod_in_stock, 'Must import 21');
            console.log(stockProduct4.stockprod_in_stock);

            await initStock([{ sku: SKU_CHILD5, in_stock: 21 }]);
            const stockProductOrginal5 = await StockProduct.findOne({ where: { stockprod_sku: SKU_CHILD5, stockprod_stock_id: 1005 } })
            await importExportService.insert({ stock_id:1005, product_sku: SKU_CHILD5, qty: QTY, type: ImportExportConstant.TYPE_IMPORT, user_id: 1001, import_id: SOURCE_ID_1, import_type: ImportExportConstant.IMPORT_TYPE_ADJUSTMENT_USED })
            const stockProduct5 = await StockProduct.findOne({ where: { stockprod_sku: SKU_CHILD5, stockprod_stock_id: 1005 } })
            assert((stockProductOrginal5.stockprod_in_stock + QTY) === stockProduct5.stockprod_in_stock, 'Must import 21');
            console.log(stockProduct5.stockprod_in_stock);

            await initStock([{ sku: SKU_COMBO1, in_stock: -40 }]);
            const stockProductOrginal6 = await StockProduct.findOne({ where: { stockprod_sku: SKU_COMBO1, stockprod_stock_id: 1005 } })
            await importExportService.insert({ stock_id:1005, product_sku: SKU_COMBO1, qty: QTY, type: ImportExportConstant.TYPE_IMPORT, user_id: 1001, import_id: SOURCE_ID_1, import_type: ImportExportConstant.IMPORT_TYPE_ADJUSTMENT_USED })
            const stockProduct6 = await StockProduct.findOne({ where: { stockprod_sku: SKU_COMBO1, stockprod_stock_id: 1005 } })
            assert((stockProductOrginal6.stockprod_in_stock + QTY) === stockProduct6.stockprod_in_stock, 'Must import 21');
            console.log(stockProduct6.stockprod_in_stock); 
    })
    it('Mix import - export with qty', async function () { 
        const SKU_CHILD1 = 209000024;
        const SKU_CHILD2 = 209000013;
        const SKU_COMBO1 = 422200040;
        const SKU_COMBO2 = 422200041;
        const SKU_COMBO3 = 422200042;
        const SOURCE_ID_1 = 21042813200
        const QTY = 3; // sl int
        const QTY_OUT = 4; // SL out
//
        await initStock([{ sku: SKU_CHILD1, in_stock: 8 }]);
        const stockProductOrginal1 = await StockProduct.findOne({ where: { stockprod_sku: SKU_CHILD1, stockprod_stock_id: 1005 } })
        await importExportService.insert({ stock_id:1005, product_sku: SKU_CHILD1, qty: QTY, type: ImportExportConstant.TYPE_IMPORT, user_id: 1001, import_id: SOURCE_ID_1, import_type: ImportExportConstant.IMPORT_TYPE_ADJUSTMENT })
        const stockProduct1 = await StockProduct.findOne({ where: { stockprod_sku: SKU_CHILD1, stockprod_stock_id: 1005 } })
        assert((stockProductOrginal1.stockprod_in_stock + QTY) === stockProduct1.stockprod_in_stock, 'Must import 3');
        console.log(stockProduct1.stockprod_in_stock);

        await importExportService.insert({ stock_id: 1005, product_sku: SKU_CHILD1, qty: QTY_OUT, type: ImportExportConstant.TYPE_EXPORT, user_id: 1001, import_id: SOURCE_ID_1, import_type: ImportExportConstant.IMPORT_TYPE_ADJUSTMENT})
        const stockProductOut1 = await StockProduct.findOne({ where: { stockprod_sku: SKU_CHILD1, stockprod_stock_id: 1005 } })
        assert((stockProductOrginal1.stockprod_in_stock + QTY - QTY_OUT) === stockProductOut1.stockprod_in_stock, 'Must export 4');
        console.log(stockProductOut1.stockprod_in_stock);

        await initStock([{ sku: SKU_CHILD2, in_stock: 10 }]);
        const stockProductOrginal2 = await StockProduct.findOne({ where: { stockprod_sku: SKU_CHILD2, stockprod_stock_id: 1005 } })
        await importExportService.insert({ stock_id:1005, product_sku: SKU_CHILD2, qty: QTY, type: ImportExportConstant.TYPE_IMPORT, user_id: 1001, import_id: SOURCE_ID_1, import_type: ImportExportConstant.IMPORT_TYPE_ADJUSTMENT })
        const stockProduct2 = await StockProduct.findOne({ where: { stockprod_sku: SKU_CHILD2, stockprod_stock_id: 1005 } })
        assert((stockProductOrginal2.stockprod_in_stock + QTY) === stockProduct2.stockprod_in_stock, 'Must import 3');
        console.log(stockProduct2.stockprod_in_stock);

        await importExportService.insert({ stock_id: 1005, product_sku: SKU_CHILD2, qty: QTY_OUT, type: ImportExportConstant.TYPE_EXPORT, user_id: 1001, import_id: SOURCE_ID_1, import_type: ImportExportConstant.IMPORT_TYPE_ADJUSTMENT})
        const stockProductOut2 = await StockProduct.findOne({ where: { stockprod_sku: SKU_CHILD2, stockprod_stock_id: 1005 } })
        assert((stockProductOrginal2.stockprod_in_stock + QTY - QTY_OUT) === stockProductOut2.stockprod_in_stock, 'Must export 4');
        console.log(stockProductOut2.stockprod_in_stock);
//
        await initStock([{ sku: SKU_COMBO1, in_stock: 10 }]);
        const stockProductOrginal3 = await StockProduct.findOne({ where: { stockprod_sku: SKU_COMBO1, stockprod_stock_id: 1005 } })
        await importExportService.insert({ stock_id:1005, product_sku: SKU_COMBO1, qty: QTY, type: ImportExportConstant.TYPE_IMPORT, user_id: 1001, import_id: SOURCE_ID_1, import_type: ImportExportConstant.IMPORT_TYPE_ADJUSTMENT })
        const stockProduct3 = await StockProduct.findOne({ where: { stockprod_sku: SKU_COMBO1, stockprod_stock_id: 1005 } })
        assert((stockProductOrginal3.stockprod_in_stock + QTY) === stockProduct3.stockprod_in_stock, 'Must import 3');
        console.log(stockProduct3.stockprod_in_stock);

        await importExportService.insert({ stock_id: 1005, product_sku: SKU_COMBO1, qty: QTY_OUT, type: ImportExportConstant.TYPE_EXPORT, user_id: 1001, import_id: SOURCE_ID_1, import_type: ImportExportConstant.IMPORT_TYPE_ADJUSTMENT})
        const stockProductOut3 = await StockProduct.findOne({ where: { stockprod_sku: SKU_COMBO1, stockprod_stock_id: 1005 } })
        assert((stockProductOrginal3.stockprod_in_stock + QTY - QTY_OUT) === stockProductOut3.stockprod_in_stock, 'Must export 4');
        console.log(stockProductOut3.stockprod_in_stock);
//
        const QTY1 = 1;
        const QTY_OUT1 =2;
        await initStock([{ sku: SKU_COMBO2, in_stock: 4 }]);
        const stockProductOrginal4 = await StockProduct.findOne({ where: { stockprod_sku: SKU_COMBO2, stockprod_stock_id: 1005 } })
        await importExportService.insert({ stock_id:1005, product_sku: SKU_COMBO2, qty: QTY1, type: ImportExportConstant.TYPE_IMPORT, user_id: 1001, import_id: SOURCE_ID_1, import_type: ImportExportConstant.IMPORT_TYPE_ADJUSTMENT })
        const stockProduct4 = await StockProduct.findOne({ where: { stockprod_sku: SKU_COMBO2, stockprod_stock_id: 1005 } }) 
        assert((stockProductOrginal4.stockprod_in_stock + QTY1) === stockProduct4.stockprod_in_stock, 'Must import 1');
        console.log(stockProduct4.stockprod_in_stock);

        await importExportService.insert({ stock_id: 1005, product_sku: SKU_COMBO2, qty: QTY_OUT1, type: ImportExportConstant.TYPE_EXPORT, user_id: 1001, import_id: SOURCE_ID_1, import_type: ImportExportConstant.IMPORT_TYPE_ADJUSTMENT})
        const stockProductOut4 = await StockProduct.findOne({ where: { stockprod_sku: SKU_COMBO2, stockprod_stock_id: 1005 } })
        assert((stockProductOrginal4.stockprod_in_stock + QTY1 - QTY_OUT1) === stockProductOut4.stockprod_in_stock, 'Must export 2');
        console.log(stockProductOut4.stockprod_in_stock);
//       
        const QTY3 = 1;
        const QTY_OUT3 =1;
        await initStock([{ sku: SKU_COMBO3, in_stock: 2 }]);
        const stockProductOrginal5 = await StockProduct.findOne({ where: { stockprod_sku: SKU_COMBO3, stockprod_stock_id: 1005 } })
        await importExportService.insert({ stock_id:1005, product_sku: SKU_COMBO3, qty: QTY3, type: ImportExportConstant.TYPE_IMPORT, user_id: 1001, import_id: SOURCE_ID_1, import_type: ImportExportConstant.IMPORT_TYPE_ADJUSTMENT })
        const stockProduct5 = await StockProduct.findOne({ where: { stockprod_sku: SKU_COMBO3, stockprod_stock_id: 1005 } })
        assert((stockProductOrginal5.stockprod_in_stock + QTY3) === stockProduct5.stockprod_in_stock, 'Must import 1');
        console.log(stockProduct5.stockprod_in_stock);

        await importExportService.insert({ stock_id: 1005, product_sku: SKU_COMBO3, qty: QTY_OUT3, type: ImportExportConstant.TYPE_EXPORT, user_id: 1001, import_id: SOURCE_ID_1, import_type: ImportExportConstant.IMPORT_TYPE_ADJUSTMENT})
        const stockProductOut5 = await StockProduct.findOne({ where: { stockprod_sku: SKU_COMBO3, stockprod_stock_id: 1005 } })
        assert((stockProductOrginal5.stockprod_in_stock + QTY3 - QTY_OUT3) === stockProductOut5.stockprod_in_stock, 'Must export 1');
        console.log(stockProductOut5.stockprod_in_stock);
    })
})