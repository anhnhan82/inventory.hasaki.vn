const assert = require('assert');
const ImportExportConstant = require('../../constants/importExport');
const { Bundle, BundleDetail, StockProduct, StockImportExport } = require('../../models')
const importExportService = require('../../services/stockImportExportService')

async function initData() {
    
}

async function cleanUp() {
    await StockProduct.destroy({ where: {}, truncate: true });
    await StockImportExport.destroy({ where: {}, truncate: true });
}
async function initStock(arrParam) {
    for (let param of arrParam) {
        await StockProduct.create({ stockprod_stock_id: 1005, stockprod_sku: param.sku, stockprod_in_stock: param.in_stock, stockprod_in_comming: 0, stockprod_available: param.in_stock, stockprod_committed: 0, stockprod_utime: Math.floor(+new Date() / 1000)});    
    }
}
async function initImportExport(arrParam) {
    for (let param of arrParam) {
        await StockImportExport.create({
            stockimex_stock_id: 1005, stockprod_sku: param.sku, stockprod_in_stock: param.in_stock, stockprod_in_comming: 0, stockprod_available: param.in_stock, stockprod_committed: 0, stockprod_utime: Math.floor(+new Date() / 1000)
        });    
    }
}

describe('Simple import export', function () {

    before(async () => {
        await initData()
    })
    beforeEach(async () => {
        await cleanUp()
    })
    after(async () => {
        await cleanUp()
    })

    // Run one test case: yarn mocha test/stock-import-export/multiple.spec.js -g 'Mix import/export'
    it('Mix import/export', async function () { 
        const SOURCE_ID_1 = 21042813200
        const SOURCE_ID_2 = 21042813198
        const SOURCE_ID_3 = 21442104280037
        const SOURCE_ID_4 = 21462104280028
        const SOURCE_ID_5 = 21422104280080
        const SOURCE_ID_6 = 10012104280340
        const SOURCE_ID_7 = 10012104280336
        const SOURCE_ID_8 = 10012104280329

        const QTY = 10;
        const QTY_OUT = 2;
        await initStock([{ sku: 100160018, in_stock: 1 }]);
        const stockProductOrginal = await StockProduct.findOne({ where: { stockprod_sku: 100160018, stockprod_stock_id: 1005 } })
        await importExportService.insert({ stock_id:1005, product_sku: 100160018, qty: QTY, type: ImportExportConstant.TYPE_IMPORT, user_id: 1001, import_id: SOURCE_ID_3, import_type: ImportExportConstant.IMPORT_TYPE_PURCHASE_ORDER })
        const stockProduct = await StockProduct.findOne({ where: { stockprod_sku: 100160018, stockprod_stock_id: 1005 } })
        assert((stockProductOrginal.stockprod_in_stock + QTY) === stockProduct.stockprod_in_stock, 'Must import 10');

        await importExportService.insert({ stock_id: 1005, product_sku: 100160018, qty: QTY_OUT, type: ImportExportConstant.TYPE_EXPORT, user_id: 1001, import_id: SOURCE_ID_1, import_type: ImportExportConstant.IMPORT_TYPE_ORDER })
        const stockProductOut = await StockProduct.findOne({ where: { stockprod_sku: 100160018, stockprod_stock_id: 1005 } })
        assert((stockProductOrginal.stockprod_in_stock + QTY - QTY_OUT) === stockProductOut.stockprod_in_stock, 'Must export 2');
    })
    
})