const assert = require('assert');

const bundleService = require('../../services/bundleService')
const { Bundle, BundleDetail, StockProduct, sequelize } = require('../../models')
const SKU_CHILD1 = 100160014
const SKU_CHILD2 = 100100015
const SKU_CHILD3 = 100170026
const SKU_CHILD4 = 100170027
const SKU_CHILD5 = 100170028
const SKU_COMBO1 = 204900015
const SKU_COMBO2 = 204900016
const SKU_COMBO3 = 204900017
const SKU_COMBO4 = 204900018
const SKU_COMBO5 = 204900019
const SKU_COMBO6 = 204900020
const SKU_COMBO7 = 204900021
const SKU_COMBO8 = 204900022
const SKU_COMBO9 = 204900023
const SKU_COMBO10 = 204900024
async function initCombo201600075() {
    const bundle1 = await Bundle.create({ sku: SKU_COMBO1, type: 2, description: '100160014 + 100100015' });
    await BundleDetail.create({ bundle_id: bundle1.id, sku: SKU_CHILD1, qty: 1 });
    await BundleDetail.create({ bundle_id: bundle1.id, sku: SKU_CHILD2, qty: 1 });

    const bundle2 = await Bundle.create({ sku: SKU_COMBO2, type: 2, description: '100160014 + 100100015 + 100170026' });
    await BundleDetail.create({ bundle_id: bundle2.id, sku: SKU_CHILD1, qty: 1 });
    await BundleDetail.create({ bundle_id: bundle2.id, sku: SKU_CHILD2, qty: 1 });
    await BundleDetail.create({ bundle_id: bundle2.id, sku: SKU_CHILD3, qty: 1 });

    const bundle3 = await Bundle.create({ sku: SKU_COMBO3, type: 2, description: '100160014 + 100100015 + 100170026 +100170027' });
    await BundleDetail.create({ bundle_id: bundle3.id, sku: SKU_CHILD1, qty: 1 });
    await BundleDetail.create({ bundle_id: bundle3.id, sku: SKU_CHILD2, qty: 1 });
    await BundleDetail.create({ bundle_id: bundle3.id, sku: SKU_CHILD3, qty: 1 });
    await BundleDetail.create({ bundle_id: bundle3.id, sku: SKU_CHILD4, qty: 1 });

    const bundle4 = await Bundle.create({ sku: SKU_COMBO4, type: 2, description: '100160014 + 100100015 + 100170026 +100170027 + 100170028' });
    await BundleDetail.create({ bundle_id: bundle4.id, sku: SKU_CHILD1, qty: 1 });
    await BundleDetail.create({ bundle_id: bundle4.id, sku: SKU_CHILD2, qty: 1 });
    await BundleDetail.create({ bundle_id: bundle4.id, sku: SKU_CHILD3, qty: 1 });
    await BundleDetail.create({ bundle_id: bundle4.id, sku: SKU_CHILD4, qty: 1 });
    await BundleDetail.create({ bundle_id: bundle4.id, sku: SKU_CHILD5, qty: 1 });

    const bundle5 = await Bundle.create({ sku: SKU_COMBO5, type: 2, description: '100160014 + 4 x 100100015' });
    await BundleDetail.create({ bundle_id: bundle5.id, sku: SKU_CHILD1, qty: 1 });
    await BundleDetail.create({ bundle_id: bundle5.id, sku: SKU_CHILD2, qty: 4 });

    const bundle6 = await Bundle.create({ sku: SKU_COMBO6, type: 2, description: ' 3 x 100160014 + 3 x 100100015' });
    await BundleDetail.create({ bundle_id: bundle6.id, sku: SKU_CHILD1, qty: 3 });
    await BundleDetail.create({ bundle_id: bundle6.id, sku: SKU_CHILD2, qty: 3 });

    const bundle7 = await Bundle.create({ sku: SKU_COMBO7, type: 2, description: ' 3 x 100160014 + 3 x 100100015' });
    await BundleDetail.create({ bundle_id: bundle7.id, sku: SKU_CHILD1, qty: 8 });
    await BundleDetail.create({ bundle_id: bundle7.id, sku: SKU_CHILD2, qty: 3 });

    const bundle8 = await Bundle.create({ sku: SKU_COMBO8, type: 2, description: '100160014 + 100100015 + 2 x 100170026' });
    await BundleDetail.create({ bundle_id: bundle8.id, sku: SKU_CHILD1, qty: 1 });
    await BundleDetail.create({ bundle_id: bundle8.id, sku: SKU_CHILD2, qty: 1 });
    await BundleDetail.create({ bundle_id: bundle8.id, sku: SKU_CHILD3, qty: 2 });

    const bundle9 = await Bundle.create({ sku: SKU_COMBO9, type: 2, description: '100160014 + 100100015 + 3 x 100170026' });
    await BundleDetail.create({ bundle_id: bundle9.id, sku: SKU_CHILD1, qty: 1 });
    await BundleDetail.create({ bundle_id: bundle9.id, sku: SKU_CHILD2, qty: 1 });
    await BundleDetail.create({ bundle_id: bundle9.id, sku: SKU_CHILD3, qty: 3 });

    const bundle10 = await Bundle.create({ sku: SKU_COMBO10, type: 2, description: '2 x 100160014 + 2 x 100100015 + 4 x 100170026 + 2 x 100170027' });
    await BundleDetail.create({ bundle_id: bundle10.id, sku: SKU_CHILD1, qty: 2 });
    await BundleDetail.create({ bundle_id: bundle10.id, sku: SKU_CHILD2, qty: 2 });
    await BundleDetail.create({ bundle_id: bundle10.id, sku: SKU_CHILD3, qty: 4 });
    await BundleDetail.create({ bundle_id: bundle10.id, sku: SKU_CHILD4, qty: 2 });
}

async function cleanUpStock() {
    await StockProduct.destroy({ where: {}, truncate: true });
}
async function initStock(arrParam) {
    for (let param of arrParam) {
        await StockProduct.create({ stockprod_stock_id: 1005, stockprod_sku: param.sku, stockprod_in_stock: param.in_stock, stockprod_in_comming: 0, stockprod_available: param.in_stock, stockprod_committed: 0, stockprod_utime: Math.floor(+new Date() / 1000)});    
    }
    
}

async function initData() {
    await initCombo201600075()
}

describe('Combo simple 3', function () {

    before(async () => {
        await initData()
        await cleanUpStock()
    })
    beforeEach(async () => {
        await cleanUpStock()
    })
    after(async () => {
        await Bundle.destroy({ where: {}, truncate: true });
        await BundleDetail.destroy({ where: {}, truncate: true });
    })
    describe('Combo 201600075', function () {
        // Run one test case:   yarn mocha --grep "Purchase combo with qty 46" test/in-out/ngoc-committing.spec.js
        it('Purchase SKU_Child2 with qty 17', async function () {
            await initStock([{ sku: SKU_COMBO1, in_stock: 160 },
                { sku: SKU_COMBO2, in_stock: 160 },
                { sku: SKU_COMBO3, in_stock: 158 },
                { sku: SKU_COMBO4, in_stock: 158 },
                { sku: SKU_COMBO5, in_stock: 40 },
                { sku: SKU_COMBO6, in_stock: 53 },
                { sku: SKU_COMBO7, in_stock: 21 },
                { sku: SKU_COMBO8, in_stock: 93 },
                { sku: SKU_COMBO9, in_stock: 62 },
                { sku: SKU_COMBO10, in_stock: 46 },
                { sku: SKU_CHILD1, in_stock: 173 },
                { sku: SKU_CHILD2, in_stock: 160 },
                { sku: SKU_CHILD3, in_stock: 186 },
                { sku: SKU_CHILD4, in_stock: 158 },
                { sku: SKU_CHILD5, in_stock: 210 }])
            const arrBundle = await bundleService.getConversionInOut({ sku: SKU_CHILD2, in_comming: 17 , stock_id: 1005 })
            assert(Array.isArray(arrBundle), 'Bundle error');
           // console.log(arrBundle);
            const combo1 = arrBundle.find(item => item.sku === SKU_COMBO1)
            assert(combo1, 'Child 204900015 must in-comming');
            assert(combo1.qty == 13, 'Must in-comming 13');

            const combo2 = arrBundle.find(item => item.sku === SKU_COMBO2)
            assert(combo2, 'Child 204900016 must in-comming');
            assert(combo2.qty == 13, 'Must in-comming 13');

            const combo5 = arrBundle.find(item => item.sku === SKU_COMBO5)
            assert(combo5, 'Child 204900019 must in-comming');
            assert(combo5.qty == 4, 'Must in-comming 4');

            const combo6 = arrBundle.find(item => item.sku === SKU_COMBO6)
            assert(combo6, 'Child 204900020 must in-comming');
            assert(combo6.qty == 4, 'Must in-comming 4');
        });  
        it('Purchase SKU_Child4 with qty 26', async function () {
            await initStock([{ sku: SKU_COMBO1, in_stock: 160 },
                { sku: SKU_COMBO2, in_stock: 160 },
                { sku: SKU_COMBO3, in_stock: 158 },
                { sku: SKU_COMBO4, in_stock: 158 },
                { sku: SKU_COMBO5, in_stock: 40 },
                { sku: SKU_COMBO6, in_stock: 53 },
                { sku: SKU_COMBO7, in_stock: 21 },
                { sku: SKU_COMBO8, in_stock: 93 },
                { sku: SKU_COMBO9, in_stock: 62 },
                { sku: SKU_COMBO10, in_stock: 46 },
                { sku: SKU_CHILD1, in_stock: 173 },
                { sku: SKU_CHILD2, in_stock: 160 },
                { sku: SKU_CHILD3, in_stock: 186 },
                { sku: SKU_CHILD4, in_stock: 158 },
                { sku: SKU_CHILD5, in_stock: 210 }])
            const arrBundle = await bundleService.getConversionInOut({ sku: SKU_CHILD4, in_comming: 26, stock_id: 1005 })
            assert(Array.isArray(arrBundle), 'Bundle error');
            //console.log(arrBundle);

            const combo3 = arrBundle.find(item => item.sku === SKU_COMBO3)
            assert(combo3, 'Child 204900017 must in-comming');
            assert(combo3.qty == 2, 'Must in-comming 2');

            const combo4 = arrBundle.find(item => item.sku === SKU_COMBO4)
            assert(combo4, 'Child 204900018 must in-comming');
            assert(combo4.qty == 2, 'Must in-comming 2');
        })  
        // yarn mocha --grep "Purchase SKU_Child4 with qty 26" test/in-out/est-committing.spec.js
    })
})