const assert = require('assert');
const bundleService = require('../../services/bundleService')
const { Bundle, BundleDetail, StockProduct, sequelize } = require('../../models')
const SKU_CHILD1 = 100160014
const SKU_CHILD2 = 100100015
const SKU_CHILD3 = 100170026
const SKU_CHILD4 = 100170027
const SKU_CHILD5 = 100170028
const SKU_COMBO1 = 204900015
const SKU_COMBO2 = 204900016
const SKU_COMBO3 = 204900017
const SKU_COMBO4 = 204900018
const SKU_COMBO5 = 204900019
const SKU_COMBO6 = 204900020
const SKU_COMBO7 = 204900021
const SKU_COMBO8 = 204900022
const SKU_COMBO9 = 204900023
const SKU_COMBO10 = 204900024

async function initCombo() {
    const bundle1 = await Bundle.create({ sku: SKU_COMBO1, type: 2, description: '100160014 + 100100015' });
    await BundleDetail.create({ bundle_id: bundle1.id, sku: SKU_CHILD1, qty: 1 });
    await BundleDetail.create({ bundle_id: bundle1.id, sku: SKU_CHILD2, qty: 1 });

    const bundle2 = await Bundle.create({ sku: SKU_COMBO2, type: 2, description: '100160014 + 100100015 + 100170026' });
    await BundleDetail.create({ bundle_id: bundle2.id, sku: SKU_CHILD1, qty: 1 });
    await BundleDetail.create({ bundle_id: bundle2.id, sku: SKU_CHILD2, qty: 1 });
    await BundleDetail.create({ bundle_id: bundle2.id, sku: SKU_CHILD3, qty: 1 });

    const bundle3 = await Bundle.create({ sku: SKU_COMBO3, type: 2, description: '100160014 + 100100015 + 100170026 +100170027' });
    await BundleDetail.create({ bundle_id: bundle3.id, sku: SKU_CHILD1, qty: 1 });
    await BundleDetail.create({ bundle_id: bundle3.id, sku: SKU_CHILD2, qty: 1 });
    await BundleDetail.create({ bundle_id: bundle3.id, sku: SKU_CHILD3, qty: 1 });
    await BundleDetail.create({ bundle_id: bundle3.id, sku: SKU_CHILD4, qty: 1 });

    const bundle4 = await Bundle.create({ sku: SKU_COMBO4, type: 2, description: '100160014 + 100100015 + 100170026 +100170027 + 100170028' });
    await BundleDetail.create({ bundle_id: bundle4.id, sku: SKU_CHILD1, qty: 1 });
    await BundleDetail.create({ bundle_id: bundle4.id, sku: SKU_CHILD2, qty: 1 });
    await BundleDetail.create({ bundle_id: bundle4.id, sku: SKU_CHILD3, qty: 1 });
    await BundleDetail.create({ bundle_id: bundle4.id, sku: SKU_CHILD4, qty: 1 });
    await BundleDetail.create({ bundle_id: bundle4.id, sku: SKU_CHILD5, qty: 1 });

    const bundle5 = await Bundle.create({ sku: SKU_COMBO5, type: 2, description: '100160014 + 4 x 100100015' });
    await BundleDetail.create({ bundle_id: bundle5.id, sku: SKU_CHILD1, qty: 1 });
    await BundleDetail.create({ bundle_id: bundle5.id, sku: SKU_CHILD2, qty: 4 });

    const bundle6 = await Bundle.create({ sku: SKU_COMBO6, type: 2, description: ' 3 x 100160014 + 3 x 100100015' });
    await BundleDetail.create({ bundle_id: bundle6.id, sku: SKU_CHILD1, qty: 3 });
    await BundleDetail.create({ bundle_id: bundle6.id, sku: SKU_CHILD2, qty: 3 });

    const bundle7 = await Bundle.create({ sku: SKU_COMBO7, type: 2, description: ' 3 x 100160014 + 3 x 100100015' });
    await BundleDetail.create({ bundle_id: bundle7.id, sku: SKU_CHILD1, qty: 8 });
    await BundleDetail.create({ bundle_id: bundle7.id, sku: SKU_CHILD2, qty: 3 });

    const bundle8 = await Bundle.create({ sku: SKU_COMBO8, type: 2, description: '100160014 + 100100015 + 2 x 100170026' });
    await BundleDetail.create({ bundle_id: bundle8.id, sku: SKU_CHILD1, qty: 1 });
    await BundleDetail.create({ bundle_id: bundle8.id, sku: SKU_CHILD2, qty: 1 });
    await BundleDetail.create({ bundle_id: bundle8.id, sku: SKU_CHILD3, qty: 2 });

    const bundle9 = await Bundle.create({ sku: SKU_COMBO9, type: 2, description: '100160014 + 100100015 + 3 x 100170026' });
    await BundleDetail.create({ bundle_id: bundle9.id, sku: SKU_CHILD1, qty: 1 });
    await BundleDetail.create({ bundle_id: bundle9.id, sku: SKU_CHILD2, qty: 1 });
    await BundleDetail.create({ bundle_id: bundle9.id, sku: SKU_CHILD3, qty: 3 });

    const bundle10 = await Bundle.create({ sku: SKU_COMBO10, type: 2, description: '2 x 100160014 + 2 x 100100015 + 4 x 100170026 + 2 x 100170027' });
    await BundleDetail.create({ bundle_id: bundle10.id, sku: SKU_CHILD1, qty: 2 });
    await BundleDetail.create({ bundle_id: bundle10.id, sku: SKU_CHILD2, qty: 2 });
    await BundleDetail.create({ bundle_id: bundle10.id, sku: SKU_CHILD3, qty: 4 });
    await BundleDetail.create({ bundle_id: bundle10.id, sku: SKU_CHILD4, qty: 2 });

}
async function cleanUpStock() {
    await StockProduct.destroy({ where: {}, truncate: true });
}
async function initStock(arrParam) {
    for (let param of arrParam) {
        await StockProduct.create({ stockprod_stock_id: 1005, stockprod_sku: param.sku, stockprod_in_stock: param.in_stock, stockprod_in_comming: 0, stockprod_available: param.in_stock, stockprod_committed: 0, stockprod_utime: Math.floor(+new Date() / 1000) });
    }
}
async function initData() {
    await initCombo()
}

describe('Complex combo', function () {

    before(async () => {
        await initData()
        await cleanUpStock()
    })
    beforeEach(async () => {
        await cleanUpStock()
    })
    after(async () => {
        await Bundle.destroy({ where: {}, truncate: true });
        await BundleDetail.destroy({ where: {}, truncate: true });
    })
    describe('Combo ', function () {
        // Run one test case: yarn mocha test/in-out/committed-combo-mix.spec.js -g 'Committed combo with qty 1'
    // Bán 67 sku 100160014 
        it('Sales 100160014 with qty 67', async function () {
            await initStock([{ sku: SKU_COMBO1, in_stock: 160 },
            { sku: SKU_COMBO2, in_stock: 160 },
            { sku: SKU_COMBO3, in_stock: 158 },
            { sku: SKU_COMBO4, in_stock: 158 },
            { sku: SKU_COMBO5, in_stock: 40 },
            { sku: SKU_COMBO6, in_stock: 53 },
            { sku: SKU_COMBO7, in_stock: 21 },
            { sku: SKU_COMBO8, in_stock: 93 },
            { sku: SKU_COMBO9, in_stock: 62 },
            { sku: SKU_COMBO10, in_stock: 46 },
            { sku: SKU_CHILD1, in_stock: 173 },
            { sku: SKU_CHILD2, in_stock: 160 },
            { sku: SKU_CHILD3, in_stock: 186 },
            { sku: SKU_CHILD4, in_stock: 158 },
            { sku: SKU_CHILD5, in_stock: 210 }])
            const arrBundle = await bundleService.getConversionInOut({ sku: SKU_CHILD1, committed: 67, stock_id: 1005 })
            //console.log(arrBundle)
            assert(Array.isArray(arrBundle), 'Bundle error');

            const child2 = arrBundle.find(item => item.sku === SKU_CHILD2)
            assert(!child2, 'Combo 100100015 must committed');
        
            const child3 = arrBundle.find(item => item.sku === SKU_CHILD3)
            assert(!child3, 'Combo 100170026 must committed');
          
            const child4 = arrBundle.find(item => item.sku === SKU_CHILD4)
            assert(!child4, 'Combo 100170027 must committed');

            const child5 = arrBundle.find(item => item.sku === SKU_CHILD5)
            assert(!child5, 'Combo 100170028 must committed');

            const combo1 = arrBundle.find(item => item.sku === SKU_COMBO1)
            assert(combo1, 'Combo 204900015 must committed');
            assert(combo1.qty == 54, 'Must committed 54');

            const combo2 = arrBundle.find(item => item.sku === SKU_COMBO2)
            assert(combo2, 'Combo 204900016 must committed');
            assert(combo2.qty == 54, 'Must committed 54');

            const combo3 = arrBundle.find(item => item.sku === SKU_COMBO3)
            assert(combo3, 'Combo 204900017 must committed');
            assert(combo3.qty == 52, 'Must committed 52');

            const combo4 = arrBundle.find(item => item.sku === SKU_COMBO4)
            assert(combo4, 'Combo 204900018 must committed');
            assert(combo4.qty == 52, 'Must committed 52');
            
            const combo5 = arrBundle.find(item => item.sku === SKU_COMBO5)
            assert(!combo5, 'Combo 240900019 not committed');

            const combo6 = arrBundle.find(item => item.sku === SKU_COMBO6)
            assert(combo6, 'Combo 204900020 must committed');
            assert(combo6.qty == 18, 'Must committed 18');

            const combo7 = arrBundle.find(item => item.sku === SKU_COMBO7)
            assert(combo7, 'Combo 204900021 must committed');
            assert(combo7.qty == 8, 'Must committed 8');

            const combo8 = arrBundle.find(item => item.sku === SKU_COMBO8)
            assert(!combo8, 'Combo 240900022 not committed');

            const combo9 = arrBundle.find(item => item.sku === SKU_COMBO9)
            assert(!combo9, 'Combo 240900023 not committed');

            const combo10 = arrBundle.find(item => item.sku === SKU_COMBO10)
            assert(!combo10, 'Combo 240900024 not committed');
        });
        // Bán 173 sku 100160014 
        it('Sales 100160014 with qty 173', async function () {
            await initStock([{ sku: SKU_COMBO1, in_stock: 160 },
            { sku: SKU_COMBO2, in_stock: 160 },
            { sku: SKU_COMBO3, in_stock: 158 },
            { sku: SKU_COMBO4, in_stock: 158 },
            { sku: SKU_COMBO5, in_stock: 40 },
            { sku: SKU_COMBO6, in_stock: 53 },
            { sku: SKU_COMBO7, in_stock: 21 },
            { sku: SKU_COMBO8, in_stock: 93 },
            { sku: SKU_COMBO9, in_stock: 62 },
            { sku: SKU_COMBO10, in_stock: 46 },
            { sku: SKU_CHILD1, in_stock: 173 },
            { sku: SKU_CHILD2, in_stock: 160 },
            { sku: SKU_CHILD3, in_stock: 186 },
            { sku: SKU_CHILD4, in_stock: 158 },
            { sku: SKU_CHILD5, in_stock: 210 }])
            const arrBundle = await bundleService.getConversionInOut({ sku: SKU_CHILD1, committed: 173, stock_id: 1005 })
            //console.log(arrBundle)
            assert(Array.isArray(arrBundle), 'Bundle error');

            const combo1 = arrBundle.find(item => item.sku === SKU_COMBO1)
            assert(combo1, 'Combo 204900015 must committed');
            assert(combo1.qty == 160, 'Must committed 160');

            const combo2 = arrBundle.find(item => item.sku === SKU_COMBO2)
            assert(combo2, 'Combo 204900016 must committed');
            assert(combo2.qty == 160, 'Must committed 160');

            const combo3 = arrBundle.find(item => item.sku === SKU_COMBO3)
            assert(combo3, 'Combo 204900017 must committed');
            assert(combo3.qty == 158, 'Must committed 158');

            const combo4 = arrBundle.find(item => item.sku === SKU_COMBO4)
            assert(combo4, 'Combo 204900018 must committed');
            assert(combo4.qty == 158, 'Must committed 158');

            const combo5 = arrBundle.find(item => item.sku === SKU_COMBO5)
            assert(combo5, 'Combo 240900019 must committed');
            assert(combo5.qty == 40, 'Must committed 40');

            const combo6 = arrBundle.find(item => item.sku === SKU_COMBO6)
            assert(combo6, 'Combo 204900020 must committed');
            assert(combo6.qty == 53, 'Must committed 53');

            const combo7 = arrBundle.find(item => item.sku === SKU_COMBO7)
            assert(combo7, 'Combo 204900021 must committed');
            assert(combo7.qty == 21, 'Must committed 21');

            const combo8 = arrBundle.find(item => item.sku === SKU_COMBO8)
            assert(combo8, 'Combo 240900022 not committed');
            assert(combo8.qty == 93, 'Must committed 93');

            const combo9 = arrBundle.find(item => item.sku === SKU_COMBO9)
            assert(combo9, 'Combo 240900023 not committed');
            assert(combo9.qty == 62, 'Must committed 62');

            const combo10 = arrBundle.find(item => item.sku === SKU_COMBO10)
            assert(combo10, 'Combo 240900024 not committed');
            assert(combo10.qty == 46, 'Must committed 46');
        });
        // Bán 200 sku 100160014 
        it('Sales 100160014 with qty 200', async function () {
            await initStock([{ sku: SKU_COMBO1, in_stock: 160 },
            { sku: SKU_COMBO2, in_stock: 160 },
            { sku: SKU_COMBO3, in_stock: 158 },
            { sku: SKU_COMBO4, in_stock: 158 },
            { sku: SKU_COMBO5, in_stock: 40 },
            { sku: SKU_COMBO6, in_stock: 53 },
            { sku: SKU_COMBO7, in_stock: 21 },
            { sku: SKU_COMBO8, in_stock: 93 },
            { sku: SKU_COMBO9, in_stock: 62 },
            { sku: SKU_COMBO10, in_stock: 46 },
            { sku: SKU_CHILD1, in_stock: 173 },
            { sku: SKU_CHILD2, in_stock: 160 },
            { sku: SKU_CHILD3, in_stock: 186 },
            { sku: SKU_CHILD4, in_stock: 158 },
            { sku: SKU_CHILD5, in_stock: 210 }])
            const arrBundle = await bundleService.getConversionInOut({ sku: SKU_CHILD1, committed: 200, stock_id: 1005 })
            //console.log(arrBundle)
            assert(Array.isArray(arrBundle), 'Bundle error');

            const combo1 = arrBundle.find(item => item.sku === SKU_COMBO1)
            assert(combo1, 'Combo 204900015 must committed');
            assert(combo1.qty == 187, 'Must committed 187');

            const combo2 = arrBundle.find(item => item.sku === SKU_COMBO2)
            assert(combo2, 'Combo 204900016 must committed');
            assert(combo2.qty == 187, 'Must committed 187');

            const combo3 = arrBundle.find(item => item.sku === SKU_COMBO3)
            assert(combo3, 'Combo 204900017 must committed');
            assert(combo3.qty == 185, 'Must committed 185');

            const combo4 = arrBundle.find(item => item.sku === SKU_COMBO4)
            assert(combo4, 'Combo 204900018 must committed');
            assert(combo4.qty == 185, 'Must committed 185');

            const combo5 = arrBundle.find(item => item.sku === SKU_COMBO5)
            assert(combo5, 'Combo 240900019 must committed');
            assert(combo5.qty == 67, 'Must committed 67');

            const combo6 = arrBundle.find(item => item.sku === SKU_COMBO6)
            assert(combo6, 'Combo 204900020 must committed');
            assert(combo6.qty == 62, 'Must committed 62');

            const combo7 = arrBundle.find(item => item.sku === SKU_COMBO7)
            assert(combo7, 'Combo 204900021 must committed');
            assert(combo7.qty == 25, 'Must committed 24');

            const combo8 = arrBundle.find(item => item.sku === SKU_COMBO8)
            assert(combo8, 'Combo 240900022 not committed');
            assert(combo8.qty == 120, 'Must committed 120');

            const combo9 = arrBundle.find(item => item.sku === SKU_COMBO9)
            assert(combo9, 'Combo 240900023 not committed');
            assert(combo9.qty == 89, 'Must committed 89');

            const combo10 = arrBundle.find(item => item.sku === SKU_COMBO10)
            assert(combo10, 'Combo 240900024 not committed');
            assert(combo10.qty == 60, 'Must committed 59');
        });
        // Bán 97 sku 100160015 
        it('Sales 100100015 with qty 97', async function () {
            await initStock([{ sku: SKU_COMBO1, in_stock: 160 },
            { sku: SKU_COMBO2, in_stock: 160 },
            { sku: SKU_COMBO3, in_stock: 158 },
            { sku: SKU_COMBO4, in_stock: 158 },
            { sku: SKU_COMBO5, in_stock: 40 },
            { sku: SKU_COMBO6, in_stock: 53 },
            { sku: SKU_COMBO7, in_stock: 21 },
            { sku: SKU_COMBO8, in_stock: 93 },
            { sku: SKU_COMBO9, in_stock: 62 },
            { sku: SKU_COMBO10, in_stock: 46 },
            { sku: SKU_CHILD1, in_stock: 173 },
            { sku: SKU_CHILD2, in_stock: 160 },
            { sku: SKU_CHILD3, in_stock: 186 },
            { sku: SKU_CHILD4, in_stock: 158 },
            { sku: SKU_CHILD5, in_stock: 210 }])
            const arrBundle = await bundleService.getConversionInOut({ sku: SKU_CHILD2, committed: 97, stock_id: 1005 })
           // console.log(arrBundle)
            assert(Array.isArray(arrBundle), 'Bundle error');

            const combo1 = arrBundle.find(item => item.sku === SKU_COMBO1)
            assert(combo1, 'Combo 204900015 must committed');
            assert(combo1.qty == 97, 'Must committed 97');

            const combo2 = arrBundle.find(item => item.sku === SKU_COMBO2)
            assert(combo2, 'Combo 204900016 must committed');
            assert(combo2.qty == 97, 'Must committed 97');

            const combo3 = arrBundle.find(item => item.sku === SKU_COMBO3)
            assert(combo3, 'Combo 204900017 must committed');
            assert(combo3.qty == 95, 'Must committed 95');

            const combo4 = arrBundle.find(item => item.sku === SKU_COMBO4)
            assert(combo4, 'Combo 204900018 must committed');
            assert(combo4.qty == 95, 'Must committed 95');

            const combo5 = arrBundle.find(item => item.sku === SKU_COMBO5)
            assert(combo5, 'Combo 204900019 must committed');
            assert(combo5.qty == 25, 'Must committed 25');

            const combo6 = arrBundle.find(item => item.sku === SKU_COMBO6)
            assert(combo6, 'Combo 204900020 must committed');
            assert(combo6.qty == 32, 'Must committed 32');

            const combo7 = arrBundle.find(item => item.sku === SKU_COMBO7)
            assert(!combo7, 'Combo 204900021 not committed');

            const combo8 = arrBundle.find(item => item.sku === SKU_COMBO8)
            assert(combo8, 'Combo 204900022 must committed');
            assert(combo8.qty == 30, 'Must committed 30');

            const combo9 = arrBundle.find(item => item.sku === SKU_COMBO9)
            assert(!combo9, 'Combo 240900023 not committed');

            const combo10 = arrBundle.find(item => item.sku === SKU_COMBO10)
            assert(combo10, 'Combo 204900021 must committed');
            assert(combo10.qty == 15, 'Must committed 15');

        });
        // Bán 160 sku 100160015 
        it('Sales 100100015 with qty 160', async function () {
            await initStock([{ sku: SKU_COMBO1, in_stock: 160 },
            { sku: SKU_COMBO2, in_stock: 160 },
            { sku: SKU_COMBO3, in_stock: 158 },
            { sku: SKU_COMBO4, in_stock: 158 },
            { sku: SKU_COMBO5, in_stock: 40 },
            { sku: SKU_COMBO6, in_stock: 53 },
            { sku: SKU_COMBO7, in_stock: 21 },
            { sku: SKU_COMBO8, in_stock: 93 },
            { sku: SKU_COMBO9, in_stock: 62 },
            { sku: SKU_COMBO10, in_stock: 46 },
            { sku: SKU_CHILD1, in_stock: 173 },
            { sku: SKU_CHILD2, in_stock: 160 },
            { sku: SKU_CHILD3, in_stock: 186 },
            { sku: SKU_CHILD4, in_stock: 158 },
            { sku: SKU_CHILD5, in_stock: 210 }])
            const arrBundle = await bundleService.getConversionInOut({ sku: SKU_CHILD2, committed: 160, stock_id: 1005 })
          //  console.log(arrBundle)
            assert(Array.isArray(arrBundle), 'Bundle error');
            const combo1 = arrBundle.find(item => item.sku === SKU_COMBO1)
            assert(combo1, 'Combo 204900015 must committed');
            assert(combo1.qty == 160, 'Must committed 160');

            const combo2 = arrBundle.find(item => item.sku === SKU_COMBO2)
            assert(combo2, 'Combo 204900016 must committed');
            assert(combo2.qty == 160, 'Must committed 160');

            const combo3 = arrBundle.find(item => item.sku === SKU_COMBO3)
            assert(combo3, 'Combo 204900017 must committed');
            assert(combo3.qty == 158, 'Must committed 158');

            const combo4 = arrBundle.find(item => item.sku === SKU_COMBO4)
            assert(combo4, 'Combo 204900018 must committed');
            assert(combo4.qty == 158, 'Must committed 158');

            const combo5 = arrBundle.find(item => item.sku === SKU_COMBO5)
            assert(combo5, 'Combo 204900019 must committed');
            assert(combo5.qty == 40, 'Must committed 40');

            const combo6 = arrBundle.find(item => item.sku === SKU_COMBO6)
            assert(combo6, 'Combo 204900020 must committed');
            assert(combo6.qty == 53, 'Must committed 53');

            const combo7 = arrBundle.find(item => item.sku === SKU_COMBO7)
            assert(combo7, 'Combo 204900021 not committed');
            assert(combo7.qty == 21, 'Must committed 21');

            const combo8 = arrBundle.find(item => item.sku === SKU_COMBO8)
            assert(combo8, 'Combo 204900022 must committed');
            assert(combo8.qty == 93, 'Must committed 93');

            const combo9 = arrBundle.find(item => item.sku === SKU_COMBO9)
            assert(combo9, 'Combo 240900023 not committed');
            assert(combo9.qty == 62, 'Must committed 62');

            const combo10 = arrBundle.find(item => item.sku === SKU_COMBO10)
            assert(combo10, 'Combo 204900024 must committed');
            assert(combo10.qty == 46, 'Must committed 46');

        });
        // Bán 120 sku 100170026 
        it('Sales 100170026 with qty 120', async function () {
            await initStock([{ sku: SKU_COMBO1, in_stock: 160 },
            { sku: SKU_COMBO2, in_stock: 160 },
            { sku: SKU_COMBO3, in_stock: 158 },
            { sku: SKU_COMBO4, in_stock: 158 },
            { sku: SKU_COMBO5, in_stock: 40 },
            { sku: SKU_COMBO6, in_stock: 53 },
            { sku: SKU_COMBO7, in_stock: 21 },
            { sku: SKU_COMBO8, in_stock: 93 },
            { sku: SKU_COMBO9, in_stock: 62 },
            { sku: SKU_COMBO10, in_stock: 46 },
            { sku: SKU_CHILD1, in_stock: 173 },
            { sku: SKU_CHILD2, in_stock: 160 },
            { sku: SKU_CHILD3, in_stock: 186 },
            { sku: SKU_CHILD4, in_stock: 158 },
            { sku: SKU_CHILD5, in_stock: 210 }])
            const arrBundle = await bundleService.getConversionInOut({ sku: SKU_CHILD3, committed: 120, stock_id: 1005 })
            //console.log(arrBundle)
            assert(Array.isArray(arrBundle), 'Bundle error');
            const combo1 = arrBundle.find(item => item.sku === SKU_COMBO1)
            assert(!combo1, 'Combo 204900015 not committed');

            const combo2 = arrBundle.find(item => item.sku === SKU_COMBO2)
            assert(combo2, 'Combo 204900016 must committed');
            assert(combo2.qty == 94, 'Must committed 94');

            const combo3 = arrBundle.find(item => item.sku === SKU_COMBO3)
            assert(combo3, 'Combo 204900017 must committed');
            assert(combo3.qty == 92, 'Must committed 92');

            const combo4 = arrBundle.find(item => item.sku === SKU_COMBO4)
            assert(combo4, 'Combo 204900018 must committed');
            assert(combo4.qty == 92, 'Must committed 92');

            const combo5 = arrBundle.find(item => item.sku === SKU_COMBO5)
            assert(!combo5, 'Combo 204900019 not committed');

            const combo6 = arrBundle.find(item => item.sku === SKU_COMBO6)
            assert(!combo6, 'Combo 204900020 not committed');

            const combo7 = arrBundle.find(item => item.sku === SKU_COMBO7)
            assert(!combo7, 'Combo 204900021 not committed');

            const combo8 = arrBundle.find(item => item.sku === SKU_COMBO8)
            assert(combo8, 'Combo 204900022 must committed');
            assert(combo8.qty == 60, 'Must committed 60');

            const combo9 = arrBundle.find(item => item.sku === SKU_COMBO9)
            assert(combo9, 'Combo 240900023 not committed');
            assert(combo9.qty == 40, 'Must committed 40');

            const combo10 = arrBundle.find(item => item.sku === SKU_COMBO10)
            assert(combo10, 'Combo 204900024 must committed');
            assert(combo10.qty == 30, 'Must committed 30');

        });
        // Bán 158 sku 100170027 
        it('Sales 100170027 with qty 158', async function () {
            await initStock([{ sku: SKU_COMBO1, in_stock: 160 },
            { sku: SKU_COMBO2, in_stock: 160 },
            { sku: SKU_COMBO3, in_stock: 158 },
            { sku: SKU_COMBO4, in_stock: 158 },
            { sku: SKU_COMBO5, in_stock: 40 },
            { sku: SKU_COMBO6, in_stock: 53 },
            { sku: SKU_COMBO7, in_stock: 21 },
            { sku: SKU_COMBO8, in_stock: 93 },
            { sku: SKU_COMBO9, in_stock: 62 },
            { sku: SKU_COMBO10, in_stock: 46 },
            { sku: SKU_CHILD1, in_stock: 173 },
            { sku: SKU_CHILD2, in_stock: 160 },
            { sku: SKU_CHILD3, in_stock: 186 },
            { sku: SKU_CHILD4, in_stock: 158 },
            { sku: SKU_CHILD5, in_stock: 210 }])
            const arrBundle = await bundleService.getConversionInOut({ sku: SKU_CHILD4, committed: 158, stock_id: 1005 })
            //console.log(arrBundle)
            assert(Array.isArray(arrBundle), 'Bundle error');
        
            const combo1 = arrBundle.find(item => item.sku === SKU_COMBO1)
            assert(!combo1, 'Combo 204900015 not committed');

            const combo2 = arrBundle.find(item => item.sku === SKU_COMBO2)
            assert(!combo2, 'Combo 204900016 not committed');

            const combo3 = arrBundle.find(item => item.sku === SKU_COMBO3)
            assert(combo3, 'Combo 204900017 must committed');
            assert(combo3.qty == 158, 'Must committed 158');

            const combo4 = arrBundle.find(item => item.sku === SKU_COMBO4)
            assert(combo4, 'Combo 204900018 must committed');
            assert(combo4.qty == 158, 'Must committed 158');

            const combo5 = arrBundle.find(item => item.sku === SKU_COMBO5)
            assert(!combo5, 'Combo 204900019 not committed');

            const combo6 = arrBundle.find(item => item.sku === SKU_COMBO6)
            assert(!combo6, 'Combo 204900020 not committed');

            const combo7 = arrBundle.find(item => item.sku === SKU_COMBO7)
            assert(!combo7, 'Combo 204900021 not committed');

            const combo8 = arrBundle.find(item => item.sku === SKU_COMBO8)
            assert(!combo8, 'Combo 204900022 must committed');

            const combo9 = arrBundle.find(item => item.sku === SKU_COMBO9)
            assert(!combo9, 'Combo 240900023 not committed');

            const combo10 = arrBundle.find(item => item.sku === SKU_COMBO10)
            assert(combo10, 'Combo 204900024 must committed');
            assert(combo10.qty == 46, 'Must committed 46');

        });
        // Bán 230 sku 100170028
        it('Sales 100170028 with qty 230', async function () {
            await initStock([{ sku: SKU_COMBO1, in_stock: 160 },
            { sku: SKU_COMBO2, in_stock: 160 },
            { sku: SKU_COMBO3, in_stock: 158 },
            { sku: SKU_COMBO4, in_stock: 158 },
            { sku: SKU_COMBO5, in_stock: 40 },
            { sku: SKU_COMBO6, in_stock: 53 },
            { sku: SKU_COMBO7, in_stock: 21 },
            { sku: SKU_COMBO8, in_stock: 93 },
            { sku: SKU_COMBO9, in_stock: 62 },
            { sku: SKU_COMBO10, in_stock: 46 },
            { sku: SKU_CHILD1, in_stock: 173 },
            { sku: SKU_CHILD2, in_stock: 160 },
            { sku: SKU_CHILD3, in_stock: 186 },
            { sku: SKU_CHILD4, in_stock: 158 },
            { sku: SKU_CHILD5, in_stock: 210 }])
            const arrBundle = await bundleService.getConversionInOut({ sku: SKU_CHILD5, committed: 230, stock_id: 1005 })
            //console.log(arrBundle)
            assert(Array.isArray(arrBundle), 'Bundle error');
            const combo1 = arrBundle.find(item => item.sku === SKU_COMBO1)
            assert(!combo1, 'Combo 204900015 not committed');

            const combo2 = arrBundle.find(item => item.sku === SKU_COMBO2)
            assert(!combo2, 'Combo 204900016 not committed');

            const combo3 = arrBundle.find(item => item.sku === SKU_COMBO3)
            assert(!combo3, 'Combo 204900017 must committed');

            const combo4 = arrBundle.find(item => item.sku === SKU_COMBO4)
            assert(combo4, 'Combo 204900018 must committed');
            assert(combo4.qty == 178, 'Must committed 178');

            const combo5 = arrBundle.find(item => item.sku === SKU_COMBO5)
            assert(!combo5, 'Combo 204900019 not committed');

            const combo6 = arrBundle.find(item => item.sku === SKU_COMBO6)
            assert(!combo6, 'Combo 204900020 not committed');

            const combo7 = arrBundle.find(item => item.sku === SKU_COMBO7)
            assert(!combo7, 'Combo 204900021 not committed');

            const combo8 = arrBundle.find(item => item.sku === SKU_COMBO8)
            assert(!combo8, 'Combo 204900022 must committed');

            const combo9 = arrBundle.find(item => item.sku === SKU_COMBO9)
            assert(!combo9, 'Combo 240900023 not committed');

            const combo10 = arrBundle.find(item => item.sku === SKU_COMBO10)
            assert(!combo10, 'Combo 204900024 must committed');

        });
        //Bán204900015 số lượng 100 
        it('Sales 204900015 with qty 100', async function () {
            await initStock([{ sku: SKU_COMBO1, in_stock: 160 },
            { sku: SKU_COMBO2, in_stock: 160 },
            { sku: SKU_COMBO3, in_stock: 158 },
            { sku: SKU_COMBO4, in_stock: 158 },
            { sku: SKU_COMBO5, in_stock: 40 },
            { sku: SKU_COMBO6, in_stock: 53 },
            { sku: SKU_COMBO7, in_stock: 21 },
            { sku: SKU_COMBO8, in_stock: 93 },
            { sku: SKU_COMBO9, in_stock: 62 },
            { sku: SKU_COMBO10, in_stock: 46 },
            { sku: SKU_CHILD1, in_stock: 173 },
            { sku: SKU_CHILD2, in_stock: 160 },
            { sku: SKU_CHILD3, in_stock: 186 },
            { sku: SKU_CHILD4, in_stock: 158 },
            { sku: SKU_CHILD5, in_stock: 210 }])
            const arrBundle = await bundleService.getConversionInOut({ sku: SKU_COMBO1, committed: 100, stock_id: 1005 })
            //console.log(arrBundle)
            assert(Array.isArray(arrBundle), 'Bundle error');

            const child1 = arrBundle.find(item => item.sku === SKU_CHILD1)
            assert(child1, 'Combo 100100015 must committed');
            assert(child1.qty == 100, 'Must committed 100');

            const child2 = arrBundle.find(item => item.sku === SKU_CHILD2)
            assert(child2, 'Combo 100100015 must committed');
            assert(child2.qty == 100, 'Must committed 100');

            const combo2 = arrBundle.find(item => item.sku === SKU_COMBO2)
            assert(combo2, 'Combo 204900016 must committed');
            assert(combo2.qty == 100, 'Must committed 100');

            const combo3 = arrBundle.find(item => item.sku === SKU_COMBO3)
            assert(combo3, 'Combo 204900017 must committed');
            assert(combo3.qty == 98, 'Must committed 98');

            const combo4 = arrBundle.find(item => item.sku === SKU_COMBO4)
            assert(combo4, 'Combo 204900018 must committed');
            assert(combo4.qty == 98, 'Must committed 98');

            const combo5 = arrBundle.find(item => item.sku === SKU_COMBO5)
            assert(combo5, 'Combo 204900019 must committed');
            assert(combo5.qty == 25, 'Must committed 25');

            const combo6 = arrBundle.find(item => item.sku === SKU_COMBO6)
            assert(combo6, 'Combo 204900020 must committed');
            assert(combo6.qty == 33, 'Must committed 33');

            const combo7 = arrBundle.find(item => item.sku === SKU_COMBO7)
            assert(combo7, 'Combo 204900021 must committed');
            assert(combo7.qty == 12, 'Must committed 12');

            const combo8 = arrBundle.find(item => item.sku === SKU_COMBO8)
            assert(combo8, 'Combo 204900022 must committed');
            assert(combo8.qty == 33, 'Must committed 33');

            const combo9 = arrBundle.find(item => item.sku === SKU_COMBO9)
            assert(combo9, 'Combo 204900023 must committed');
            assert(combo9.qty == 2, 'Must committed 2');

            const combo10 = arrBundle.find(item => item.sku === SKU_COMBO10)
            assert(combo10, 'Combo 204900024 must committed');
            assert(combo10.qty == 16, 'Must committed 16');

        });
        //Bán204900015 số lượng 160 
        it('Sales 204900015 with qty 160', async function () {
            await initStock([{ sku: SKU_COMBO1, in_stock: 160 },
            { sku: SKU_COMBO2, in_stock: 160 },
            { sku: SKU_COMBO3, in_stock: 158 },
            { sku: SKU_COMBO4, in_stock: 158 },
            { sku: SKU_COMBO5, in_stock: 40 },
            { sku: SKU_COMBO6, in_stock: 53 },
            { sku: SKU_COMBO7, in_stock: 21 },
            { sku: SKU_COMBO8, in_stock: 93 },
            { sku: SKU_COMBO9, in_stock: 62 },
            { sku: SKU_COMBO10, in_stock: 46 },
            { sku: SKU_CHILD1, in_stock: 173 },
            { sku: SKU_CHILD2, in_stock: 160 },
            { sku: SKU_CHILD3, in_stock: 186 },
            { sku: SKU_CHILD4, in_stock: 158 },
            { sku: SKU_CHILD5, in_stock: 210 }])
            const arrBundle = await bundleService.getConversionInOut({ sku: SKU_COMBO1, committed: 160, stock_id: 1005 })
            //console.log(arrBundle)
            assert(Array.isArray(arrBundle), 'Bundle error');

            const child1 = arrBundle.find(item => item.sku === SKU_CHILD1)
            assert(child1, 'Child1 100160014 must committed');
            assert(child1.qty == 160, 'Must committed 160');

            const child2 = arrBundle.find(item => item.sku === SKU_CHILD2)
            assert(child2, 'Child1 100100015 must committed');
            assert(child2.qty == 160, 'Must committed 160');

            const combo2 = arrBundle.find(item => item.sku === SKU_COMBO2)
            assert(combo2, 'Combo 204900016 must committed');
            assert(combo2.qty == 160, 'Must committed 160');

            const combo3 = arrBundle.find(item => item.sku === SKU_COMBO3)
            assert(combo3, 'Combo 204900017 must committed');
            assert(combo3.qty == 158, 'Must committed 158');

            const combo4 = arrBundle.find(item => item.sku === SKU_COMBO4)
            assert(combo4, 'Combo 204900018 must committed');
            assert(combo4.qty == 158, 'Must committed 158');

            const combo5 = arrBundle.find(item => item.sku === SKU_COMBO5)
            assert(combo5, 'Combo 204900019 must committed');
            assert(combo5.qty == 40, 'Must committed 40');

            const combo6 = arrBundle.find(item => item.sku === SKU_COMBO6)
            assert(combo6, 'Combo 204900020 must committed');
            assert(combo6.qty == 53, 'Must committed 53');

            const combo7 = arrBundle.find(item => item.sku === SKU_COMBO7)
            assert(combo7, 'Combo 204900021 must committed');
            assert(combo7.qty == 21, 'Must committed 21');

            const combo8 = arrBundle.find(item => item.sku === SKU_COMBO8)
            assert(combo8, 'Combo 204900022 must committed');
            assert(combo8.qty == 93, 'Must committed 93');

            const combo9 = arrBundle.find(item => item.sku === SKU_COMBO9)
            assert(combo9, 'Combo 204900023 must committed');
            assert(combo9.qty == 62, 'Must committed 62');

            const combo10 = arrBundle.find(item => item.sku === SKU_COMBO10)
            assert(combo10, 'Combo 204900024 must committed');
            assert(combo10.qty == 46, 'Must committed 46');

        });
        //Bán204900015 số lượng 200 lớn hơn Stock 
        it('Sales 204900015 with qty 200', async function () {
            await initStock([{ sku: SKU_COMBO1, in_stock: 160 },
            { sku: SKU_COMBO2, in_stock: 160 },
            { sku: SKU_COMBO3, in_stock: 158 },
            { sku: SKU_COMBO4, in_stock: 158 },
            { sku: SKU_COMBO5, in_stock: 40 },
            { sku: SKU_COMBO6, in_stock: 53 },
            { sku: SKU_COMBO7, in_stock: 21 },
            { sku: SKU_COMBO8, in_stock: 93 },
            { sku: SKU_COMBO9, in_stock: 62 },
            { sku: SKU_COMBO10, in_stock: 46 },
            { sku: SKU_CHILD1, in_stock: 173 },
            { sku: SKU_CHILD2, in_stock: 160 },
            { sku: SKU_CHILD3, in_stock: 186 },
            { sku: SKU_CHILD4, in_stock: 158 },
            { sku: SKU_CHILD5, in_stock: 210 }])
            const arrBundle = await bundleService.getConversionInOut({ sku: SKU_COMBO1, committed: 200, stock_id: 1005 })
            //console.log(arrBundle)
            assert(Array.isArray(arrBundle), 'Bundle error');

            const child1 = arrBundle.find(item => item.sku === SKU_CHILD1)
            assert(child1, 'Child1 100160014 must committed');
            assert(child1.qty == 200, 'Must committed 200');

            const child2 = arrBundle.find(item => item.sku === SKU_CHILD2)
            assert(child2, 'Child2 100100015 must committed');
            assert(child2.qty == 200, 'Must committed 200');

            const combo2 = arrBundle.find(item => item.sku === SKU_COMBO2)
            assert(combo2, 'Combo 204900016 must committed');
            assert(combo2.qty == 200, 'Must committed 200');

            const combo3 = arrBundle.find(item => item.sku === SKU_COMBO3)
            assert(combo3, 'Combo 204900017 must committed');
            assert(combo3.qty == 198, 'Must committed 198');

            const combo4 = arrBundle.find(item => item.sku === SKU_COMBO4)
            assert(combo4, 'Combo 204900018 must committed');
            assert(combo4.qty == 198, 'Must committed 198');

            const combo5 = arrBundle.find(item => item.sku === SKU_COMBO5)
            assert(combo5, 'Combo 204900019 must committed');
            assert(combo5.qty == 67, 'Must committed 67');

            const combo6 = arrBundle.find(item => item.sku === SKU_COMBO6)
            assert(combo6, 'Combo 204900020 must committed');
            assert(combo6.qty == 67, 'Must committed 67');

            const combo7 = arrBundle.find(item => item.sku === SKU_COMBO7)
            assert(combo7, 'Combo 204900021 must committed');
            assert(combo7.qty == 35, 'Must committed 35');

            const combo8 = arrBundle.find(item => item.sku === SKU_COMBO8)
            assert(combo8, 'Combo 204900022 must committed');
            assert(combo8.qty == 133, 'Must committed 133');

            const combo9 = arrBundle.find(item => item.sku === SKU_COMBO9)
            assert(combo9, 'Combo 204900023 must committed');
            assert(combo9.qty == 102, 'Must committed 102');

            const combo10 = arrBundle.find(item => item.sku === SKU_COMBO10)
            assert(combo10, 'Combo 204900024 must committed');
            assert(combo10.qty == 66, 'Must committed 66');

        });
        //Bán204900016 số lượng 100
        it('Sales 204900016 with qty 100', async function () {
            await initStock([{ sku: SKU_COMBO1, in_stock: 160 },
            { sku: SKU_COMBO2, in_stock: 160 },
            { sku: SKU_COMBO3, in_stock: 158 },
            { sku: SKU_COMBO4, in_stock: 158 },
            { sku: SKU_COMBO5, in_stock: 40 },
            { sku: SKU_COMBO6, in_stock: 53 },
            { sku: SKU_COMBO7, in_stock: 21 },
            { sku: SKU_COMBO8, in_stock: 93 },
            { sku: SKU_COMBO9, in_stock: 62 },
            { sku: SKU_COMBO10, in_stock: 46 },
            { sku: SKU_CHILD1, in_stock: 173 },
            { sku: SKU_CHILD2, in_stock: 160 },
            { sku: SKU_CHILD3, in_stock: 186 },
            { sku: SKU_CHILD4, in_stock: 158 },
            { sku: SKU_CHILD5, in_stock: 210 }])
            const arrBundle = await bundleService.getConversionInOut({ sku: SKU_COMBO2, committed: 100, stock_id: 1005 })
            //console.log(arrBundle)
            assert(Array.isArray(arrBundle), 'Bundle error');

            const child1 = arrBundle.find(item => item.sku === SKU_CHILD1)
            assert(child1, 'Child1 100160014 must committed');
            assert(child1.qty == 100, 'Must committed 100');

            const child2 = arrBundle.find(item => item.sku === SKU_CHILD2)
            assert(child2, 'Child2 100100015 must committed');
            assert(child2.qty == 100, 'Must committed 100');

            const child3 = arrBundle.find(item => item.sku === SKU_CHILD3)
            assert(child3, 'Child3 100170026 must committed');
            assert(child3.qty == 100, 'Must committed 100');

            const combo1 = arrBundle.find(item => item.sku === SKU_COMBO1)
            assert(combo1, 'Combo 204900015 must committed');
            assert(combo1.qty == 100, 'Must committed 200');

            const combo3 = arrBundle.find(item => item.sku === SKU_COMBO3)
            assert(combo3, 'Combo 204900017 must committed');
            assert(combo3.qty == 98, 'Must committed 98');

            const combo4 = arrBundle.find(item => item.sku === SKU_COMBO4)
            assert(combo4, 'Combo 204900018 must committed');
            assert(combo4.qty == 98, 'Must committed 98');

            const combo5 = arrBundle.find(item => item.sku === SKU_COMBO5)
            assert(combo5, 'Combo 204900019 must committed');
            assert(combo5.qty == 25, 'Must committed 25');

            const combo6 = arrBundle.find(item => item.sku === SKU_COMBO6)
            assert(combo6, 'Combo 204900020 must committed');
            assert(combo6.qty == 33, 'Must committed 33');

            const combo7 = arrBundle.find(item => item.sku === SKU_COMBO7)
            assert(combo7, 'Combo 204900021 must committed');
            assert(combo7.qty == 12, 'Must committed 12');

            const combo8 = arrBundle.find(item => item.sku === SKU_COMBO8)
            assert(combo8, 'Combo 204900022 must committed');
            assert(combo8.qty == 50, 'Must committed 50');

            const combo9 = arrBundle.find(item => item.sku === SKU_COMBO9)
            assert(combo9, 'Combo 204900023 must committed');
            assert(combo9.qty == 34, 'Must committed 34');

            const combo10 = arrBundle.find(item => item.sku === SKU_COMBO10)
            assert(combo10, 'Combo 204900024 must committed');
            assert(combo10.qty == 25, 'Must committed 25');

        });
        //Bán204900016 số lượng 160
        it('Sales 204900016 with qty 160', async function () {
            await initStock([{ sku: SKU_COMBO1, in_stock: 160 },
            { sku: SKU_COMBO2, in_stock: 160 },
            { sku: SKU_COMBO3, in_stock: 158 },
            { sku: SKU_COMBO4, in_stock: 158 },
            { sku: SKU_COMBO5, in_stock: 40 },
            { sku: SKU_COMBO6, in_stock: 53 },
            { sku: SKU_COMBO7, in_stock: 21 },
            { sku: SKU_COMBO8, in_stock: 93 },
            { sku: SKU_COMBO9, in_stock: 62 },
            { sku: SKU_COMBO10, in_stock: 46 },
            { sku: SKU_CHILD1, in_stock: 173 },
            { sku: SKU_CHILD2, in_stock: 160 },
            { sku: SKU_CHILD3, in_stock: 186 },
            { sku: SKU_CHILD4, in_stock: 158 },
            { sku: SKU_CHILD5, in_stock: 210 }])
            const arrBundle = await bundleService.getConversionInOut({ sku: SKU_COMBO2, committed: 160, stock_id: 1005 })
            //console.log(arrBundle)
            assert(Array.isArray(arrBundle), 'Bundle error');

            const child1 = arrBundle.find(item => item.sku === SKU_CHILD1)
            assert(child1, 'Child1 100160014 must committed');
            assert(child1.qty == 160, 'Must committed 160');

            const child2 = arrBundle.find(item => item.sku === SKU_CHILD2)
            assert(child2, 'Child2 100100015 must committed');
            assert(child2.qty == 160, 'Must committed 160');

            const child3 = arrBundle.find(item => item.sku === SKU_CHILD3)
            assert(child3, 'Child3 100170026 must committed');
            assert(child3.qty == 160, 'Must committed 160');

            const combo1 = arrBundle.find(item => item.sku === SKU_COMBO1)
            assert(combo1, 'Combo 204900015 must committed');
            assert(combo1.qty == 160, 'Must committed 160');

            const combo3 = arrBundle.find(item => item.sku === SKU_COMBO3)
            assert(combo3, 'Combo 204900017 must committed');
            assert(combo3.qty == 158, 'Must committed 158');

            const combo4 = arrBundle.find(item => item.sku === SKU_COMBO4)
            assert(combo4, 'Combo 204900018 must committed');
            assert(combo4.qty == 158, 'Must committed 158');

            const combo5 = arrBundle.find(item => item.sku === SKU_COMBO5)
            assert(combo5, 'Combo 204900019 must committed');
            assert(combo5.qty == 40, 'Must committed 40');

            const combo6 = arrBundle.find(item => item.sku === SKU_COMBO6)
            assert(combo6, 'Combo 204900020 must committed');
            assert(combo6.qty == 53, 'Must committed 53');

            const combo7 = arrBundle.find(item => item.sku === SKU_COMBO7)
            assert(combo7, 'Combo 204900021 must committed');
            assert(combo7.qty == 21, 'Must committed 21');

            const combo8 = arrBundle.find(item => item.sku === SKU_COMBO8)
            assert(combo8, 'Combo 204900022 must committed');
            assert(combo8.qty == 93, 'Must committed 93');

            const combo9 = arrBundle.find(item => item.sku === SKU_COMBO9)
            assert(combo9, 'Combo 204900023 must committed');
            assert(combo9.qty == 62, 'Must committed 62');

            const combo10 = arrBundle.find(item => item.sku === SKU_COMBO10)
            assert(combo10, 'Combo 204900024 must committed');
            assert(combo10.qty == 46, 'Must committed 46');

        });
        //Bán204900016 số lượng 200
        it('Sales 204900016 with qty 200', async function () {
            await initStock([{ sku: SKU_COMBO1, in_stock: 160 },
            { sku: SKU_COMBO2, in_stock: 160 },
            { sku: SKU_COMBO3, in_stock: 158 },
            { sku: SKU_COMBO4, in_stock: 158 },
            { sku: SKU_COMBO5, in_stock: 40 },
            { sku: SKU_COMBO6, in_stock: 53 },
            { sku: SKU_COMBO7, in_stock: 21 },
            { sku: SKU_COMBO8, in_stock: 93 },
            { sku: SKU_COMBO9, in_stock: 62 },
            { sku: SKU_COMBO10, in_stock: 46 },
            { sku: SKU_CHILD1, in_stock: 173 },
            { sku: SKU_CHILD2, in_stock: 160 },
            { sku: SKU_CHILD3, in_stock: 186 },
            { sku: SKU_CHILD4, in_stock: 158 },
            { sku: SKU_CHILD5, in_stock: 210 }])
            const arrBundle = await bundleService.getConversionInOut({ sku: SKU_COMBO2, committed: 200, stock_id: 1005 })
            //console.log(arrBundle)
            assert(Array.isArray(arrBundle), 'Bundle error');

            const child1 = arrBundle.find(item => item.sku === SKU_CHILD1)
            assert(child1, 'Child1 100160014 must committed');
            assert(child1.qty == 200, 'Must committed 200');

            const child2 = arrBundle.find(item => item.sku === SKU_CHILD2)
            assert(child2, 'Child2 100100015 must committed');
            assert(child2.qty == 200, 'Must committed 200');

            const child3 = arrBundle.find(item => item.sku === SKU_CHILD3)
            assert(child3, 'Combo 100170026 must committed');
            assert(child3.qty == 200, 'Must committed 200');

            const combo1 = arrBundle.find(item => item.sku === SKU_COMBO1)
            assert(combo1, 'Combo 204900015 must committed');
            assert(combo1.qty == 200, 'Must committed 200');

            const combo3 = arrBundle.find(item => item.sku === SKU_COMBO3)
            assert(combo3, 'Combo 204900017 must committed');
            assert(combo3.qty == 198, 'Must committed 198');

            const combo4 = arrBundle.find(item => item.sku === SKU_COMBO4)
            assert(combo4, 'Combo 204900018 must committed');
            assert(combo4.qty == 198, 'Must committed 198');

            const combo5 = arrBundle.find(item => item.sku === SKU_COMBO5)
            assert(combo5, 'Combo 204900019 must committed');
            assert(combo5.qty == 67, 'Must committed 67');

            const combo6 = arrBundle.find(item => item.sku === SKU_COMBO6)
            assert(combo6, 'Combo 204900020 must committed');
            assert(combo6.qty == 67, 'Must committed 66');

            const combo7 = arrBundle.find(item => item.sku === SKU_COMBO7)
            assert(combo7, 'Combo 204900021 must committed');
            assert(combo7.qty == 35, 'Must committed 34');

            const combo8 = arrBundle.find(item => item.sku === SKU_COMBO8)
            assert(combo8, 'Combo 204900022 must committed');
            assert(combo8.qty == 133, 'Must committed 133');

            const combo9 = arrBundle.find(item => item.sku === SKU_COMBO9)
            assert(combo9, 'Combo 204900023 must committed');
            assert(combo9.qty == 102, 'Must committed 102');

            const combo10 = arrBundle.find(item => item.sku === SKU_COMBO10)
            assert(combo10, 'Combo 204900024 must committed');
            assert(combo10.qty == 66, 'Must committed 66');

        });
        //Bán 204900017 số lượng 100
        it('Sales 204900017 with qty 100', async function () {
            await initStock([{ sku: SKU_COMBO1, in_stock: 160 },
            { sku: SKU_COMBO2, in_stock: 160 },
            { sku: SKU_COMBO3, in_stock: 158 },
            { sku: SKU_COMBO4, in_stock: 158 },
            { sku: SKU_COMBO5, in_stock: 40 },
            { sku: SKU_COMBO6, in_stock: 53 },
            { sku: SKU_COMBO7, in_stock: 21 },
            { sku: SKU_COMBO8, in_stock: 93 },
            { sku: SKU_COMBO9, in_stock: 62 },
            { sku: SKU_COMBO10, in_stock: 46 },
            { sku: SKU_CHILD1, in_stock: 173 },
            { sku: SKU_CHILD2, in_stock: 160 },
            { sku: SKU_CHILD3, in_stock: 186 },
            { sku: SKU_CHILD4, in_stock: 158 },
            { sku: SKU_CHILD5, in_stock: 210 }])
            const arrBundle = await bundleService.getConversionInOut({ sku: SKU_COMBO3, committed: 100, stock_id: 1005 })
            // console.log(arrBundle)
            assert(Array.isArray(arrBundle), 'Bundle error');

            const child1 = arrBundle.find(item => item.sku === SKU_CHILD1)
            assert(child1, 'Child1 100160014 must committed');
            assert(child1.qty == 100, 'Must committed 100');

            const child2 = arrBundle.find(item => item.sku === SKU_CHILD2)
            assert(child2, 'Child1 100100015 must committed');
            assert(child2.qty == 100, 'Must committed 100');

            const child3 = arrBundle.find(item => item.sku === SKU_CHILD3)
            assert(child3, 'Child3 100170026 must committed');
            assert(child3.qty == 100, 'Must committed 100');

            const child4 = arrBundle.find(item => item.sku === SKU_CHILD4)
            assert(child4, 'Child4 100170027 must committed');
            assert(child4.qty == 100, 'Must committed 100');

            const combo1 = arrBundle.find(item => item.sku === SKU_COMBO1)
            assert(combo1, 'Combo 204900015 must committed');
            assert(combo1.qty == 100, 'Must committed 200');

            const combo2 = arrBundle.find(item => item.sku === SKU_COMBO2)
            assert(combo2, 'Combo 204900016 must committed');
            assert(combo2.qty == 100, 'Must committed 100');

            const combo4 = arrBundle.find(item => item.sku === SKU_COMBO4)
            assert(combo4, 'Combo 204900018 must committed');
            assert(combo4.qty == 100, 'Must committed 100');

            const combo5 = arrBundle.find(item => item.sku === SKU_COMBO5)
            assert(combo5, 'Combo 204900019 must committed');
            assert(combo5.qty == 25, 'Must committed 25');

            const combo6 = arrBundle.find(item => item.sku === SKU_COMBO6)
            assert(combo6, 'Combo 204900020 must committed');
            assert(combo6.qty == 33, 'Must committed 33');

            const combo7 = arrBundle.find(item => item.sku === SKU_COMBO7)
            assert(combo7, 'Combo 204900021 must committed');
            assert(combo7.qty == 12, 'Must committed 12');

            const combo8 = arrBundle.find(item => item.sku === SKU_COMBO8)
            assert(combo8, 'Combo 204900022 must committed');
            assert(combo8.qty == 50, 'Must committed 50');

            const combo9 = arrBundle.find(item => item.sku === SKU_COMBO9)
            assert(combo9, 'Combo 204900023 must committed');
            assert(combo9.qty == 34, 'Must committed 34');

            const combo10 = arrBundle.find(item => item.sku === SKU_COMBO10)
            assert(combo10, 'Combo 204900024 must committed');
            assert(combo10.qty == 25, 'Must committed 25');

        });
        //Bán 204900017 số lượng 158
        it('Sales 204900017 with qty 158', async function () {
            await initStock([{ sku: SKU_COMBO1, in_stock: 160 },
            { sku: SKU_COMBO2, in_stock: 160 },
            { sku: SKU_COMBO3, in_stock: 158 },
            { sku: SKU_COMBO4, in_stock: 158 },
            { sku: SKU_COMBO5, in_stock: 40 },
            { sku: SKU_COMBO6, in_stock: 53 },
            { sku: SKU_COMBO7, in_stock: 21 },
            { sku: SKU_COMBO8, in_stock: 93 },
            { sku: SKU_COMBO9, in_stock: 62 },
            { sku: SKU_COMBO10, in_stock: 46 },
            { sku: SKU_CHILD1, in_stock: 173 },
            { sku: SKU_CHILD2, in_stock: 160 },
            { sku: SKU_CHILD3, in_stock: 186 },
            { sku: SKU_CHILD4, in_stock: 158 },
            { sku: SKU_CHILD5, in_stock: 210 }])
            const arrBundle = await bundleService.getConversionInOut({ sku: SKU_COMBO3, committed: 158, stock_id: 1005 })
            //console.log(arrBundle)
            assert(Array.isArray(arrBundle), 'Bundle error');

            const child1 = arrBundle.find(item => item.sku === SKU_CHILD1)
            assert(child1, 'Child1 100160014 must committed');
            assert(child1.qty == 158, 'Must committed 158');

            const child2 = arrBundle.find(item => item.sku === SKU_CHILD2)
            assert(child2, 'Child2 100100015 must committed');
            assert(child2.qty == 158, 'Must committed 158');

            const child3 = arrBundle.find(item => item.sku === SKU_CHILD3)
            assert(child3, 'Child3 100170026 must committed');
            assert(child3.qty == 158, 'Must committed 158');

            const child4 = arrBundle.find(item => item.sku === SKU_CHILD4)
            assert(child4, 'Child4 100170027 must committed');
            assert(child4.qty == 158, 'Must committed 158');

            const combo1 = arrBundle.find(item => item.sku === SKU_COMBO1)
            assert(combo1, 'Combo 204900015 must committed');
            assert(combo1.qty == 158, 'Must committed 258');

            const combo2 = arrBundle.find(item => item.sku === SKU_COMBO2)
            assert(combo2, 'Combo 204900016 must committed');
            assert(combo2.qty == 158, 'Must committed 158');

            const combo4 = arrBundle.find(item => item.sku === SKU_COMBO4)
            assert(combo4, 'Combo 204900018 must committed');
            assert(combo4.qty == 158, 'Must committed 158');

            const combo5 = arrBundle.find(item => item.sku === SKU_COMBO5)
            assert(combo5, 'Combo 204900019 must committed');
            assert(combo5.qty == 40, 'Must committed 40');

            const combo6 = arrBundle.find(item => item.sku === SKU_COMBO6)
            assert(combo6, 'Combo 204900020 must committed');
            assert(combo6.qty == 53, 'Must committed 53');

            const combo7 = arrBundle.find(item => item.sku === SKU_COMBO7)
            assert(combo7, 'Combo 204900021 must committed');
            assert(combo7.qty == 21, 'Must committed 21');

            const combo8 = arrBundle.find(item => item.sku === SKU_COMBO8)
            assert(combo8, 'Combo 204900022 must committed');
            assert(combo8.qty == 91, 'Must committed 91');

            const combo9 = arrBundle.find(item => item.sku === SKU_COMBO9)
            assert(combo9, 'Combo 204900023 must committed');
            assert(combo9.qty == 60, 'Must committed 60');

            const combo10 = arrBundle.find(item => item.sku === SKU_COMBO10)
            assert(combo10, 'Combo 204900024 must committed');
            assert(combo10.qty == 46, 'Must committed 46');

        });
        //Bán 204900017 số lượng 200
        it('Sales 204900017 with qty 200', async function () {
            await initStock([{ sku: SKU_COMBO1, in_stock: 160 },
            { sku: SKU_COMBO2, in_stock: 160 },
            { sku: SKU_COMBO3, in_stock: 158 },
            { sku: SKU_COMBO4, in_stock: 158 },
            { sku: SKU_COMBO5, in_stock: 40 },
            { sku: SKU_COMBO6, in_stock: 53 },
            { sku: SKU_COMBO7, in_stock: 21 },
            { sku: SKU_COMBO8, in_stock: 93 },
            { sku: SKU_COMBO9, in_stock: 62 },
            { sku: SKU_COMBO10, in_stock: 46 },
            { sku: SKU_CHILD1, in_stock: 173 },
            { sku: SKU_CHILD2, in_stock: 160 },
            { sku: SKU_CHILD3, in_stock: 186 },
            { sku: SKU_CHILD4, in_stock: 158 },
            { sku: SKU_CHILD5, in_stock: 210 }])
            const arrBundle = await bundleService.getConversionInOut({ sku: SKU_COMBO3, committed: 200, stock_id: 1005 })
            //console.log(arrBundle)
            assert(Array.isArray(arrBundle), 'Bundle error');

            const child1 = arrBundle.find(item => item.sku === SKU_CHILD1)
            assert(child1, 'Child1 100160014 must committed');
            assert(child1.qty == 200, 'Must committed 200');

            const child2 = arrBundle.find(item => item.sku === SKU_CHILD2)
            assert(child2, 'Child2 100100015 must committed');
            assert(child2.qty == 200, 'Must committed 200');

            const child3 = arrBundle.find(item => item.sku === SKU_CHILD3)
            assert(child3, 'Child3 100170026 must committed');
            assert(child3.qty == 200, 'Must committed 200');

            const child4 = arrBundle.find(item => item.sku === SKU_CHILD4)
            assert(child4, 'Combo 100170027 must committed');
            assert(child4.qty == 200, 'Must committed 200');

            const combo1 = arrBundle.find(item => item.sku === SKU_COMBO1)
            assert(combo1, 'Combo 204900015 must committed');
            assert(combo1.qty == 200, 'Must committed 200');

            const combo2 = arrBundle.find(item => item.sku === SKU_COMBO2)
            assert(combo2, 'Combo 204900016 must committed');
            assert(combo2.qty == 200, 'Must committed 200');

            const combo4 = arrBundle.find(item => item.sku === SKU_COMBO4)
            assert(combo4, 'Combo 204900018 must committed');
            assert(combo4.qty == 200, 'Must committed 200');

            const combo5 = arrBundle.find(item => item.sku === SKU_COMBO5)
            assert(combo5, 'Combo 204900019 must committed');
            assert(combo5.qty == 67, 'Must committed 67');

            const combo6 = arrBundle.find(item => item.sku === SKU_COMBO6)
            assert(combo6, 'Combo 204900020 must committed');
            assert(combo6.qty == 67, 'Must committed 67');

            const combo7 = arrBundle.find(item => item.sku === SKU_COMBO7)
            assert(combo7, 'Combo 204900021 must committed');
            assert(combo7.qty == 35, 'Must committed 35');

            const combo8 = arrBundle.find(item => item.sku === SKU_COMBO8)
            assert(combo8, 'Combo 204900022 must committed');
            assert(combo8.qty == 133, 'Must committed 133');

            const combo9 = arrBundle.find(item => item.sku === SKU_COMBO9)
            assert(combo9, 'Combo 204900023 must committed');
            assert(combo9.qty == 102, 'Must committed 102');

            const combo10 = arrBundle.find(item => item.sku === SKU_COMBO10)
            assert(combo10, 'Combo 204900024 must committed');
            assert(combo10.qty == 67, 'Must committed 67');

        });
        //Bán 204900018 số lượng 130
        it('Sales 204900018 with qty 130', async function () {
            await initStock([{ sku: SKU_COMBO1, in_stock: 160 },
            { sku: SKU_COMBO2, in_stock: 160 },
            { sku: SKU_COMBO3, in_stock: 158 },
            { sku: SKU_COMBO4, in_stock: 158 },
            { sku: SKU_COMBO5, in_stock: 40 },
            { sku: SKU_COMBO6, in_stock: 53 },
            { sku: SKU_COMBO7, in_stock: 21 },
            { sku: SKU_COMBO8, in_stock: 93 },
            { sku: SKU_COMBO9, in_stock: 62 },
            { sku: SKU_COMBO10, in_stock: 46 },
            { sku: SKU_CHILD1, in_stock: 173 },
            { sku: SKU_CHILD2, in_stock: 160 },
            { sku: SKU_CHILD3, in_stock: 186 },
            { sku: SKU_CHILD4, in_stock: 158 },
            { sku: SKU_CHILD5, in_stock: 210 }])
            const arrBundle = await bundleService.getConversionInOut({ sku: SKU_COMBO4, committed: 130, stock_id: 1005 })
            //console.log(arrBundle)
            assert(Array.isArray(arrBundle), 'Bundle error');

            const child1 = arrBundle.find(item => item.sku === SKU_CHILD1)
            assert(child1, 'Child1 100100015 must committed');
            assert(child1.qty == 130, 'Must committed 130');

            const child2 = arrBundle.find(item => item.sku === SKU_CHILD2)
            assert(child2, 'Child2 100100015 must committed');
            assert(child2.qty == 130, 'Must committed 130');

            const child3 = arrBundle.find(item => item.sku === SKU_CHILD3)
            assert(child3, 'Child3 100170026 must committed');
            assert(child3.qty == 130, 'Must committed 130');

            const child4 = arrBundle.find(item => item.sku === SKU_CHILD4)
            assert(child4, 'Combo 100170027 must committed');
            assert(child4.qty == 130, 'Must committed 130');

            const child5 = arrBundle.find(item => item.sku === SKU_CHILD5)
            assert(child5, 'Child5  100170028 must committed');
            assert(child5.qty == 130, 'Must committed 130');

            const combo1 = arrBundle.find(item => item.sku === SKU_COMBO1)
            assert(combo1, 'Combo 204900015 must committed');
            assert(combo1.qty == 130, 'Must committed 130');

            const combo2 = arrBundle.find(item => item.sku === SKU_COMBO2)
            assert(combo2, 'Combo 204900016 must committed');
            assert(combo2.qty == 130, 'Must committed 130');

            const combo3 = arrBundle.find(item => item.sku === SKU_COMBO3)
            assert(combo3, 'Combo 204900017 must committed');
            assert(combo3.qty == 130, 'Must committed 130');

            const combo5 = arrBundle.find(item => item.sku === SKU_COMBO5)
            assert(combo5, 'Combo 204900019 must committed');
            assert(combo5.qty == 33, 'Must committed 33');

            const combo6 = arrBundle.find(item => item.sku === SKU_COMBO6)
            assert(combo6, 'Combo 204900020 must committed');
            assert(combo6.qty == 43, 'Must committed 43');

            const combo7 = arrBundle.find(item => item.sku === SKU_COMBO7)
            assert(combo7, 'Combo 204900021 must committed');
            assert(combo7.qty == 16, 'Must committed 16');

            const combo8 = arrBundle.find(item => item.sku === SKU_COMBO8)
            assert(combo8, 'Combo 204900022 must committed');
            assert(combo8.qty == 65, 'Must committed 65');

            const combo9 = arrBundle.find(item => item.sku === SKU_COMBO9)
            assert(combo9, 'Combo 204900023 must committed');
            assert(combo9.qty == 44, 'Must committed 44');

            const combo10 = arrBundle.find(item => item.sku === SKU_COMBO10)
            assert(combo10, 'Combo 204900024 must committed');
            assert(combo10.qty == 32, 'Must committed 32');
        });
        //Bán 204900019 số lượng 35
        it('Sales 204900019 with qty 35', async function () {
            await initStock([{ sku: SKU_COMBO1, in_stock: 160 },
            { sku: SKU_COMBO2, in_stock: 160 },
            { sku: SKU_COMBO3, in_stock: 158 },
            { sku: SKU_COMBO4, in_stock: 158 },
            { sku: SKU_COMBO5, in_stock: 40 },
            { sku: SKU_COMBO6, in_stock: 53 },
            { sku: SKU_COMBO7, in_stock: 21 },
            { sku: SKU_COMBO8, in_stock: 93 },
            { sku: SKU_COMBO9, in_stock: 62 },
            { sku: SKU_COMBO10, in_stock: 46 },
            { sku: SKU_CHILD1, in_stock: 173 },
            { sku: SKU_CHILD2, in_stock: 160 },
            { sku: SKU_CHILD3, in_stock: 186 },
            { sku: SKU_CHILD4, in_stock: 158 },
            { sku: SKU_CHILD5, in_stock: 210 }])
            const arrBundle = await bundleService.getConversionInOut({ sku: SKU_COMBO5, committed: 35, stock_id: 1005 })
            // console.log(arrBundle)
            assert(Array.isArray(arrBundle), 'Bundle error');

            const child1 = arrBundle.find(item => item.sku === SKU_CHILD1)
            assert(child1, 'Child1 100160014 must committed');
            assert(child1.qty == 35, 'Must committed 35');

            const child2 = arrBundle.find(item => item.sku === SKU_CHILD2)
            assert(child2, 'Child2 100100015 must committed');
            assert(child2.qty == 140, 'Must committed 140');


            const combo1 = arrBundle.find(item => item.sku === SKU_COMBO1)
            assert(combo1, 'Combo 204900015 must committed');
            assert(combo1.qty == 140, 'Must committed 140');

            const combo2 = arrBundle.find(item => item.sku === SKU_COMBO2)
            assert(combo2, 'Combo 204900016 must committed');
            assert(combo2.qty == 140, 'Must committed 140');

            const combo3 = arrBundle.find(item => item.sku === SKU_COMBO3)
            assert(combo3, 'Combo 204900017 must committed');
            assert(combo3.qty == 138, 'Must committed 138');

            const combo4 = arrBundle.find(item => item.sku === SKU_COMBO4)
            assert(combo4, 'Combo 204900018 must committed');
            assert(combo4.qty == 138, 'Must committed 138');

            const combo6 = arrBundle.find(item => item.sku === SKU_COMBO6)
            assert(combo6, 'Combo 204900020 must committed');
            assert(combo6.qty == 47, 'Must committed 47');

            const combo7 = arrBundle.find(item => item.sku === SKU_COMBO7)
            assert(combo7, 'Combo 204900021 must committed');
            assert(combo7.qty == 15, 'Must committed 15');

            const combo8 = arrBundle.find(item => item.sku === SKU_COMBO8)
            assert(combo8, 'Combo 204900022 must committed');
            assert(combo8.qty == 73, 'Must committed 73');

            const combo9 = arrBundle.find(item => item.sku === SKU_COMBO9)
            assert(combo9, 'Combo 204900023 must committed');
            assert(combo9.qty == 42, 'Must committed 42');

            const combo10 = arrBundle.find(item => item.sku === SKU_COMBO10)
            assert(combo10, 'Combo 204900024 must committed');
            assert(combo10.qty == 36, 'Must committed 36');
        });
        //Bán 204900020 số lượng 50
        it('Sales 204900020 with qty 50', async function () {
            await initStock([{ sku: SKU_COMBO1, in_stock: 160 },
            { sku: SKU_COMBO2, in_stock: 160 },
            { sku: SKU_COMBO3, in_stock: 158 },
            { sku: SKU_COMBO4, in_stock: 158 },
            { sku: SKU_COMBO5, in_stock: 40 },
            { sku: SKU_COMBO6, in_stock: 53 },
            { sku: SKU_COMBO7, in_stock: 21 },
            { sku: SKU_COMBO8, in_stock: 93 },
            { sku: SKU_COMBO9, in_stock: 62 },
            { sku: SKU_COMBO10, in_stock: 46 },
            { sku: SKU_CHILD1, in_stock: 173 },
            { sku: SKU_CHILD2, in_stock: 160 },
            { sku: SKU_CHILD3, in_stock: 186 },
            { sku: SKU_CHILD4, in_stock: 158 },
            { sku: SKU_CHILD5, in_stock: 210 }])
            const arrBundle = await bundleService.getConversionInOut({ sku: SKU_COMBO6, committed: 50, stock_id: 1005 })
            //console.log(arrBundle)
            assert(Array.isArray(arrBundle), 'Bundle error');
        
            const child1 = arrBundle.find(item => item.sku === SKU_CHILD1)
            assert(child1, 'Child1 100160014 must committed');
            assert(child1.qty == 150, 'Must committed 150');

            const child2 = arrBundle.find(item => item.sku === SKU_CHILD2)
            assert(child2, 'Child2 100100015 must committed');
            assert(child2.qty == 150, 'Must committed 150');

            const combo1 = arrBundle.find(item => item.sku === SKU_COMBO1)
            assert(combo1, 'Combo 204900015 must committed');
            assert(combo1.qty == 150, 'Must committed 150');

            const combo2 = arrBundle.find(item => item.sku === SKU_COMBO2)
            assert(combo2, 'Combo 204900016 must committed');
            assert(combo2.qty == 150, 'Must committed 150');

            const combo3 = arrBundle.find(item => item.sku === SKU_COMBO3)
            assert(combo3, 'Combo 204900017 must committed');
            assert(combo3.qty == 148, 'Must committed 148');

            const combo4 = arrBundle.find(item => item.sku === SKU_COMBO4)
            assert(combo4, 'Combo 204900018 must committed');
            assert(combo4.qty == 148, 'Must committed 148');

            const combo5 = arrBundle.find(item => item.sku === SKU_COMBO5)
            assert(combo5, 'Combo 204900019 must committed');
            assert(combo5.qty == 38, 'Must committed 38');

            const combo7 = arrBundle.find(item => item.sku === SKU_COMBO7)
            assert(combo7, 'Combo 204900021 must committed');
            assert(combo7.qty == 19, 'Must committed 19');

            const combo8 = arrBundle.find(item => item.sku === SKU_COMBO8)
            assert(combo8, 'Combo 204900022 must committed');
            assert(combo8.qty == 83, 'Must committed 83');

            const combo9 = arrBundle.find(item => item.sku === SKU_COMBO9)
            assert(combo9, 'Combo 204900023 must committed');
            assert(combo9.qty == 52, 'Must committed 52');

            const combo10 = arrBundle.find(item => item.sku === SKU_COMBO10)
            assert(combo10, 'Combo 204900024 must committed');
            assert(combo10.qty == 41, 'Must committed 41');
        });
        //Bán 204900021 số lượng 20
        it('Sales 204900021 with qty 20', async function () {
            await initStock([{ sku: SKU_COMBO1, in_stock: 160 },
            { sku: SKU_COMBO2, in_stock: 160 },
            { sku: SKU_COMBO3, in_stock: 158 },
            { sku: SKU_COMBO4, in_stock: 158 },
            { sku: SKU_COMBO5, in_stock: 40 },
            { sku: SKU_COMBO6, in_stock: 53 },
            { sku: SKU_COMBO7, in_stock: 21 },
            { sku: SKU_COMBO8, in_stock: 93 },
            { sku: SKU_COMBO9, in_stock: 62 },
            { sku: SKU_COMBO10, in_stock: 46 },
            { sku: SKU_CHILD1, in_stock: 173 },
            { sku: SKU_CHILD2, in_stock: 160 },
            { sku: SKU_CHILD3, in_stock: 186 },
            { sku: SKU_CHILD4, in_stock: 158 },
            { sku: SKU_CHILD5, in_stock: 210 }])
            const arrBundle = await bundleService.getConversionInOut({ sku: SKU_COMBO7, committed: 20, stock_id: 1005 })
           // console.log(arrBundle)
            assert(Array.isArray(arrBundle), 'Bundle error');

            const child1 = arrBundle.find(item => item.sku === SKU_CHILD1)
            assert(child1, 'Child1 100160014 must committed');
            assert(child1.qty == 160, 'Must committed 160');

            const child2 = arrBundle.find(item => item.sku === SKU_CHILD2)
            assert(child2, 'Child2 100100015 must committed');
            assert(child2.qty == 60, 'Must committed 60');

            const combo1 = arrBundle.find(item => item.sku === SKU_COMBO1)
            assert(combo1, 'Combo 204900015 must committed');
            assert(combo1.qty == 147, 'Must committed 147');

            const combo2 = arrBundle.find(item => item.sku === SKU_COMBO2)
            assert(combo2, 'Combo 204900017 must committed');
            assert(combo2.qty == 147, 'Must committed 147');

            const combo3 = arrBundle.find(item => item.sku === SKU_COMBO3)
            assert(combo3, 'Combo 204900017 must committed');
            assert(combo3.qty == 145, 'Must committed 145');

            const combo4 = arrBundle.find(item => item.sku === SKU_COMBO4)
            assert(combo4, 'Combo 204900018 must committed');
            assert(combo4.qty == 145, 'Must committed 145');

            const combo5 = arrBundle.find(item => item.sku === SKU_COMBO5)
            assert(combo5, 'Combo 204900019 must committed');
            assert(combo5.qty == 27, 'Must committed 27');

            const combo6 = arrBundle.find(item => item.sku === SKU_COMBO6)
            assert(combo6, 'Combo 204900020 must committed');
            assert(combo6.qty == 49, 'Must committed 49');

            const combo8 = arrBundle.find(item => item.sku === SKU_COMBO8)
            assert(combo8, 'Combo 204900022 must committed');
            assert(combo8.qty == 80, 'Must committed 80');

            const combo9 = arrBundle.find(item => item.sku === SKU_COMBO9)
            assert(combo9, 'Combo 204900023 must committed');
            assert(combo9.qty == 49, 'Must committed 49');

            const combo10 = arrBundle.find(item => item.sku === SKU_COMBO10)
            assert(combo10, 'Combo 204900021 must committed');
            assert(combo10.qty == 40, 'Must committed 40');
        });
        //Bán 204900022 số lượng 93
        it('Sales 204900022 with qty 93', async function () {
            await initStock([{ sku: SKU_COMBO1, in_stock: 160 },
            { sku: SKU_COMBO2, in_stock: 160 },
            { sku: SKU_COMBO3, in_stock: 158 },
            { sku: SKU_COMBO4, in_stock: 158 },
            { sku: SKU_COMBO5, in_stock: 40 },
            { sku: SKU_COMBO6, in_stock: 53 },
            { sku: SKU_COMBO7, in_stock: 21 },
            { sku: SKU_COMBO8, in_stock: 93 },
            { sku: SKU_COMBO9, in_stock: 62 },
            { sku: SKU_COMBO10, in_stock: 46 },
            { sku: SKU_CHILD1, in_stock: 173 },
            { sku: SKU_CHILD2, in_stock: 160 },
            { sku: SKU_CHILD3, in_stock: 186 },
            { sku: SKU_CHILD4, in_stock: 158 },
            { sku: SKU_CHILD5, in_stock: 210 }])
            const arrBundle = await bundleService.getConversionInOut({ sku: SKU_COMBO8, committed: 93, stock_id: 1005 })
            //console.log(arrBundle)
            assert(Array.isArray(arrBundle), 'Bundle error');
            //assert(arrBundle.length == 5, 'Only one item');

            const child1 = arrBundle.find(item => item.sku === SKU_CHILD1)
            assert(child1, 'Child1 100160014 must committed');
            assert(child1.qty == 93, 'Must committed 93');

            const child2 = arrBundle.find(item => item.sku === SKU_CHILD2)
            assert(child2, 'Child2 100100015 must committed');
            assert(child2.qty == 93, 'Must committed 93');

            const child3 = arrBundle.find(item => item.sku === SKU_CHILD3)
            assert(child3, 'Child3 100100015 must committed');
            assert(child3.qty == 186, 'Must committed 186');

            const combo1 = arrBundle.find(item => item.sku === SKU_COMBO1)
            assert(combo1, 'Combo 204900015 must committed');
            assert(combo1.qty == 93, 'Must committed 93');

            const combo2 = arrBundle.find(item => item.sku === SKU_COMBO2)
            assert(combo2, 'Combo 204900017 must committed');
            assert(combo2.qty == 160, 'Must committed 160');

            const combo3 = arrBundle.find(item => item.sku === SKU_COMBO3)
            assert(combo3, 'Combo 204900018 must committed');
            assert(combo3.qty == 158, 'Must committed 158');

            const combo4 = arrBundle.find(item => item.sku === SKU_COMBO4)
            assert(combo4, 'Combo 204900019 must committed');
            assert(combo4.qty == 158, 'Must committed 158');

            const combo5 = arrBundle.find(item => item.sku === SKU_COMBO5)
            assert(combo5, 'Combo 204900020 must committed');
            assert(combo5.qty == 24, 'Must committed 24');

            const combo6 = arrBundle.find(item => item.sku === SKU_COMBO6)
            assert(combo6, 'Combo 204900021 must committed');
            assert(combo6.qty == 31, 'Must committed 31');

            const combo7 = arrBundle.find(item => item.sku === SKU_COMBO7)
            assert(combo7, 'Combo 204900022 must committed');
            assert(combo7.qty == 11, 'Must committed 11');

            const combo9 = arrBundle.find(item => item.sku === SKU_COMBO9)
            assert(combo9, 'Combo 204900023 must committed');
            assert(combo9.qty == 62, 'Must committed 62');

            const combo10 = arrBundle.find(item => item.sku === SKU_COMBO10)
            assert(combo10, 'Combo 204900021 must committed');
            assert(combo10.qty == 46, 'Must committed 46');
        });
        //Bán 204900023 số lượng 58
        it('Sales 204900023 with qty 58', async function () {
            await initStock([{ sku: SKU_COMBO1, in_stock: 160 },
            { sku: SKU_COMBO2, in_stock: 160 },
            { sku: SKU_COMBO3, in_stock: 158 },
            { sku: SKU_COMBO4, in_stock: 158 },
            { sku: SKU_COMBO5, in_stock: 40 },
            { sku: SKU_COMBO6, in_stock: 53 },
            { sku: SKU_COMBO7, in_stock: 21 },
            { sku: SKU_COMBO8, in_stock: 93 },
            { sku: SKU_COMBO9, in_stock: 62 },
            { sku: SKU_COMBO10, in_stock: 46 },
            { sku: SKU_CHILD1, in_stock: 173 },
            { sku: SKU_CHILD2, in_stock: 160 },
            { sku: SKU_CHILD3, in_stock: 186 },
            { sku: SKU_CHILD4, in_stock: 158 },
            { sku: SKU_CHILD5, in_stock: 210 }])
            const arrBundle = await bundleService.getConversionInOut({ sku: SKU_COMBO9, committed: 58, stock_id: 1005 })
            //console.log(arrBundle)
            assert(Array.isArray(arrBundle), 'Bundle error');

            const child1 = arrBundle.find(item => item.sku === SKU_CHILD1)
            assert(child1, 'Child1 100160014 must committed');
            assert(child1.qty == 58, 'Must committed 58');

            const child2 = arrBundle.find(item => item.sku === SKU_CHILD2)
            assert(child2, 'Child2 100100015 must committed');
            assert(child2.qty == 58, 'Must committed 58');

            const child3 = arrBundle.find(item => item.sku === SKU_CHILD3)
            assert(child3, 'Child3 100170026 must committed');
            assert(child3.qty == 174, 'Must committed 174');

            const combo1 = arrBundle.find(item => item.sku === SKU_COMBO1)
            assert(combo1, 'Combo 204900015 must committed');
            assert(combo1.qty == 58, 'Must committed 58');

            const combo2 = arrBundle.find(item => item.sku === SKU_COMBO2)
            assert(combo2, 'Combo 204900017 must committed');
            assert(combo2.qty == 148, 'Must committed 148');

            const combo3 = arrBundle.find(item => item.sku === SKU_COMBO3)
            assert(combo3, 'Combo 204900018 must committed');
            assert(combo3.qty == 146, 'Must committed 146');

            const combo4 = arrBundle.find(item => item.sku === SKU_COMBO4)
            assert(combo4, 'Combo 204900019 must committed');
            assert(combo4.qty == 146, 'Must committed 146');

            const combo5 = arrBundle.find(item => item.sku === SKU_COMBO5)
            assert(combo5, 'Combo 204900020 must committed');
            assert(combo5.qty == 15, 'Must committed 15');

            const combo6 = arrBundle.find(item => item.sku === SKU_COMBO6)
            assert(combo6, 'Combo 204900021 must committed');
            assert(combo6.qty == 19, 'Must committed 19');

            const combo7 = arrBundle.find(item => item.sku === SKU_COMBO7)
            assert(combo7, 'Combo 204900022 must committed');
            assert(combo7.qty == 7, 'Must committed 7');

            const combo8 = arrBundle.find(item => item.sku === SKU_COMBO8)
            assert(combo8, 'Combo 204900023 must committed');
            assert(combo8.qty == 87, 'Must committed 87');

            const combo10 = arrBundle.find(item => item.sku === SKU_COMBO10)
            assert(combo10, 'Combo 204900024 must committed');
            assert(combo10.qty == 43, 'Must committed 43');
        });
        //Bán 204900024 số lượng 46
        it('Sales 204900024 with qty 46', async function () {
            await initStock([{ sku: SKU_COMBO1, in_stock: 160 },
            { sku: SKU_COMBO2, in_stock: 160 },
            { sku: SKU_COMBO3, in_stock: 158 },
            { sku: SKU_COMBO4, in_stock: 158 },
            { sku: SKU_COMBO5, in_stock: 40 },
            { sku: SKU_COMBO6, in_stock: 53 },
            { sku: SKU_COMBO7, in_stock: 21 },
            { sku: SKU_COMBO8, in_stock: 93 },
            { sku: SKU_COMBO9, in_stock: 62 },
            { sku: SKU_COMBO10, in_stock: 46 },
            { sku: SKU_CHILD1, in_stock: 173 },
            { sku: SKU_CHILD2, in_stock: 160 },
            { sku: SKU_CHILD3, in_stock: 186 },
            { sku: SKU_CHILD4, in_stock: 158 },
            { sku: SKU_CHILD5, in_stock: 210 }])
            const arrBundle = await bundleService.getConversionInOut({ sku: SKU_COMBO10, committed: 46, stock_id: 1005 })
           // console.log(arrBundle)
            assert(Array.isArray(arrBundle), 'Bundle error');
            //assert(arrBundle.length == 5, 'Only one item');

            const child1 = arrBundle.find(item => item.sku === SKU_CHILD1)
            assert(child1, 'Child1 100160014 must committed');
            assert(child1.qty == 92, 'Must committed 92');

            const child2 = arrBundle.find(item => item.sku === SKU_CHILD2)
            assert(child2, 'Child2 100100015 must committed');
            assert(child2.qty == 92, 'Must committed 92');

            const child3 = arrBundle.find(item => item.sku === SKU_CHILD3)
            assert(child3, 'Child3 100170026 must committed');
            assert(child3.qty == 184, 'Must committed 184');

            const child4 = arrBundle.find(item => item.sku === SKU_CHILD4)
            assert(child4, 'Child4 100170027 must committed');
            assert(child4.qty == 92, 'Must committed 92');

            const combo1 = arrBundle.find(item => item.sku === SKU_COMBO1)
            assert(combo1, 'Combo 204900015 must committed');
            assert(combo1.qty == 92, 'Must committed 92');

            const combo2 = arrBundle.find(item => item.sku === SKU_COMBO2)
            assert(combo2, 'Combo 204900016 must committed');
            assert(combo2.qty == 158, 'Must committed 158');

            const combo3 = arrBundle.find(item => item.sku === SKU_COMBO3)
            assert(combo3, 'Combo 204900017 must committed');
            assert(combo3.qty == 156, 'Must committed 156');

            const combo4 = arrBundle.find(item => item.sku === SKU_COMBO4)
            assert(combo4, 'Combo 204900018 must committed');
            assert(combo4.qty == 156, 'Must committed 156');

            const combo5 = arrBundle.find(item => item.sku === SKU_COMBO5)
            assert(combo5, 'Combo 204900019 must committed');
            assert(combo5.qty == 23, 'Must committed 23');

            const combo6 = arrBundle.find(item => item.sku === SKU_COMBO6)
            assert(combo6, 'Combo 204900020 must committed');
            assert(combo6.qty == 31, 'Must committed 31');

            const combo7 = arrBundle.find(item => item.sku === SKU_COMBO7)
            assert(combo7, 'Combo 204900021 must committed');
            assert(combo7.qty == 11, 'Must committed 11');

            const combo8 = arrBundle.find(item => item.sku === SKU_COMBO8)
            assert(combo8, 'Combo 204900022 must committed');
            assert(combo8.qty == 92, 'Must committed 92');

            const combo9 = arrBundle.find(item => item.sku === SKU_COMBO9)
            assert(combo9, 'Combo 204900023 must committed');
            assert(combo9.qty == 62, 'Must committed 62');
        });
    });
});