const assert = require('assert');

const bundleService = require('../../services/bundleService')
const { Bundle, BundleDetail, StockProduct, sequelize } = require('../../models')
const SKU_CHILD1 = 100160014
const SKU_CHILD2 = 100100015
const SKU_CHILD3 = 100170026
const SKU_COMBO1 = 204900015
const SKU_COMBO2 = 204900016
const SKU_COMBO3 = 204900017
const SKU_COMBO4 = 204900018
const SKU_COMBO5 = 204900019
const SKU_COMBO6 = 204900020
const SKU_COMBO7 = 204900021
const SKU_COMBO8 = 204900022
const SKU_COMBO9 = 204900023
const SKU_COMBO10 = 204900024

async function initCombo() {
    const bundle1 = await Bundle.create({ sku: SKU_COMBO1, type: 2, description: '100160014 + 100100015' });
    await BundleDetail.create({ bundle_id: bundle1.id, sku: SKU_CHILD1, qty: 1 });
    await BundleDetail.create({ bundle_id: bundle1.id, sku: SKU_CHILD2, qty: 1 });
    const bundle2 = await Bundle.create({ sku: SKU_COMBO2, type: 2, description: '100100015 + 100170026' });
    await BundleDetail.create({ bundle_id: bundle2.id, sku: SKU_CHILD2, qty: 1 });
    await BundleDetail.create({ bundle_id: bundle2.id, sku: SKU_CHILD3, qty: 1 });
    const bundle3 = await Bundle.create({ sku: SKU_COMBO3, type: 2, description: '100100014 + 2 x 100100015' });
    await BundleDetail.create({ bundle_id: bundle3.id, sku: SKU_CHILD1, qty: 1 });
    await BundleDetail.create({ bundle_id: bundle3.id, sku: SKU_CHILD2, qty: 2 });
    const bundle4 = await Bundle.create({ sku: SKU_COMBO4, type: 2, description: '2 x 100100015 + 100170026' });
    await BundleDetail.create({ bundle_id: bundle4.id, sku: SKU_CHILD2, qty: 2 });
    await BundleDetail.create({ bundle_id: bundle4.id, sku: SKU_CHILD3, qty: 1 });
    const bundle5 = await Bundle.create({ sku: SKU_COMBO5, type: 2, description: '2 x 100160014 + 100100015' });
    await BundleDetail.create({ bundle_id: bundle5.id, sku: SKU_CHILD1, qty: 2 });
    await BundleDetail.create({ bundle_id: bundle5.id, sku: SKU_CHILD2, qty: 1 });
    const bundle6 = await Bundle.create({ sku: SKU_COMBO6, type: 2, description: '100100015 + 2 x 100170026' });
    await BundleDetail.create({ bundle_id: bundle6.id, sku: SKU_CHILD2, qty: 1 });
    await BundleDetail.create({ bundle_id: bundle6.id, sku: SKU_CHILD3, qty: 2 });
    const bundle7 = await Bundle.create({ sku: SKU_COMBO7, type: 2, description: '2 x 100160014 + 3 x 100100015' });
    await BundleDetail.create({ bundle_id: bundle7.id, sku: SKU_CHILD1, qty: 2 });
    await BundleDetail.create({ bundle_id: bundle7.id, sku: SKU_CHILD2, qty: 3 });
    const bundle8 = await Bundle.create({ sku: SKU_COMBO8, type: 2, description: '3 x 100100015 + 2 x 100170026' });
    await BundleDetail.create({ bundle_id: bundle8.id, sku: SKU_CHILD2, qty: 3 });
    await BundleDetail.create({ bundle_id: bundle8.id, sku: SKU_CHILD3, qty: 2 });
    const bundle9 = await Bundle.create({ sku: SKU_COMBO9, type: 2, description: '3 x 100160014 + 2 x 100100015' });
    await BundleDetail.create({ bundle_id: bundle9.id, sku: SKU_CHILD1, qty: 3 });
    await BundleDetail.create({ bundle_id: bundle9.id, sku: SKU_CHILD2, qty: 2 });
    const bundle10 = await Bundle.create({ sku: SKU_COMBO10, type: 2, description: '2 x 100100015 + 3 x 100170026' });
    await BundleDetail.create({ bundle_id: bundle10.id, sku: SKU_CHILD2, qty: 2 });
    await BundleDetail.create({ bundle_id: bundle10.id, sku: SKU_CHILD3, qty: 3 });
}
async function cleanUpStock() {
    await StockProduct.destroy({ where: {}, truncate: true });
}
async function initStock(arrParam) {
    for (let param of arrParam) {
        await StockProduct.create({ stockprod_stock_id: 1005, stockprod_sku: param.sku, stockprod_in_stock: param.in_stock, stockprod_in_comming: 0, stockprod_available: param.in_stock, stockprod_committed: 0, stockprod_utime: Math.floor(+new Date() / 1000)});    
    }
}
async function initData() {
    await initCombo()
}

describe('Complex combo', function () {

    before(async () => {
        await initData()
        await cleanUpStock()
    })
    beforeEach(async () => {
        await cleanUpStock()
    })
    after(async () => {
        await Bundle.destroy({ where: {}, truncate: true });
        await BundleDetail.destroy({ where: {}, truncate: true });
    })
    describe('Combo ', function () {
        // Run one test case: yarn mocha test/in-out/committed-combo-mix.spec.js -g 'Committed combo with qty 1'
    // bán 1 SKU100160014
    it('Sales SKU100160014 with qty 1', async function () {
            await initStock([{ sku: SKU_COMBO1, in_stock: 100 }, 
                             { sku: SKU_COMBO2, in_stock: 100 }, 
                             { sku: SKU_COMBO3, in_stock: 50 },
                             { sku: SKU_COMBO4, in_stock: 50 },
                             { sku: SKU_COMBO5, in_stock: 50 },
                             { sku: SKU_COMBO6, in_stock: 50 },
                             { sku: SKU_COMBO7, in_stock: 33 },
                             { sku: SKU_COMBO8, in_stock: 33 },
                             { sku: SKU_COMBO9, in_stock: 33 },
                             { sku: SKU_COMBO10, in_stock: 33 },
                             { sku: SKU_CHILD1, in_stock: 100 } , 
                             { sku: SKU_CHILD2, in_stock: 100 } , 
                             { sku: SKU_CHILD3, in_stock: 100 }])
            const arrBundle = await bundleService.getConversionInOut({ sku: SKU_CHILD1 , committed: 1, stock_id: 1005 })
            //console.log(arrBundle)
            assert(Array.isArray(arrBundle), 'Bundle error');
            //assert(arrBundle.length == 1, 'Only one item');        
            const combo1 = arrBundle.find(item => item.sku === SKU_COMBO1)
            assert(combo1, 'Combo 204900015 not committed');
            assert(combo1.qty == 1, 'Must committed 1');

            const combo5 = arrBundle.find(item => item.sku === SKU_COMBO5)
            assert(combo5, 'Combo 204900019 must committed');
            assert(combo5.qty == 1, 'Must committed 1');
    });
    // bán 1 SKU100100015
    it('Sales SKU100100015 with qty 1', async function () {
        await initStock([{ sku: SKU_COMBO1, in_stock: 100 }, 
                         { sku: SKU_COMBO2, in_stock: 100 }, 
                         { sku: SKU_COMBO3, in_stock: 50 },
                         { sku: SKU_COMBO4, in_stock: 50 },
                         { sku: SKU_COMBO5, in_stock: 50 },
                         { sku: SKU_COMBO6, in_stock: 50 },
                         { sku: SKU_COMBO7, in_stock: 33 },
                         { sku: SKU_COMBO8, in_stock: 33 },
                         { sku: SKU_COMBO9, in_stock: 33 },
                         { sku: SKU_COMBO10, in_stock: 33 },
                         { sku: SKU_CHILD1, in_stock: 100 } , 
                         { sku: SKU_CHILD2, in_stock: 100 } , 
                         { sku: SKU_CHILD3, in_stock: 100 }])
        const arrBundle = await bundleService.getConversionInOut({ sku: SKU_CHILD2 , committed: 1, stock_id: 1005 })
        //console.log(arrBundle)
        assert(Array.isArray(arrBundle), 'Bundle error');
        //assert(arrBundle.length == 1, 'Only one item');
        
        const combo1 = arrBundle.find(item => item.sku === SKU_COMBO1)
        assert(combo1, 'Combo 204900015 not committed');
       assert(combo1.qty == 1, 'Must committed 1');

        const combo2 = arrBundle.find(item => item.sku === SKU_COMBO2)
        assert(combo2, 'Combo 204900016 must committed');
        assert(combo2.qty == 1, 'Must committed 1');

        const combo3 = arrBundle.find(item => item.sku === SKU_COMBO3)
        assert(combo3, 'Combo 204900017 must committed');
        assert(combo3.qty == 1, 'Must committed 1');

        const combo4 = arrBundle.find(item => item.sku === SKU_COMBO4)
        assert(combo4, 'Combo 204900018 must committed');
        assert(combo4.qty == 1, 'Must committed 1');

    });
    // bán 1 100170026
    it('Sales SKU100170026 with qty 1', async function () {
        await initStock([{ sku: SKU_COMBO1, in_stock: 100 }, 
                         { sku: SKU_COMBO2, in_stock: 100 }, 
                         { sku: SKU_COMBO3, in_stock: 50 },
                         { sku: SKU_COMBO4, in_stock: 50 },
                         { sku: SKU_COMBO5, in_stock: 50 },
                         { sku: SKU_COMBO6, in_stock: 50 },
                         { sku: SKU_COMBO7, in_stock: 33 },
                         { sku: SKU_COMBO8, in_stock: 33 },
                         { sku: SKU_COMBO9, in_stock: 33 },
                         { sku: SKU_COMBO10, in_stock: 33 },
                         { sku: SKU_CHILD1, in_stock: 100 } , 
                         { sku: SKU_CHILD2, in_stock: 100 } , 
                         { sku: SKU_CHILD3, in_stock: 100 }])
        const arrBundle = await bundleService.getConversionInOut({ sku: SKU_CHILD3 , committed: 1, stock_id: 1005 })
        //console.log(arrBundle)
        assert(Array.isArray(arrBundle), 'Bundle error');
        //assert(arrBundle.length == 1, 'Only one item');
        const combo2 = arrBundle.find(item => item.sku === SKU_COMBO2)
        assert(combo2, 'Combo 204900016 must committed');
        assert(combo2.qty == 1, 'Must committed 1');

        const combo6 = arrBundle.find(item => item.sku === SKU_COMBO6)
        assert(combo6, 'Combo 204900020 must committed');
        assert(combo6.qty == 1, 'Must committed 1');
    });
    // bán 1 combo 204900015
     it('Sales combo 204900015 with qty 1', async function () {
        await initStock([{ sku: SKU_COMBO1, in_stock: 100 }, 
                         { sku: SKU_COMBO2, in_stock: 100 }, 
                         { sku: SKU_COMBO3, in_stock: 50 },
                         { sku: SKU_COMBO4, in_stock: 50 },
                         { sku: SKU_COMBO5, in_stock: 50 },
                         { sku: SKU_COMBO6, in_stock: 50 },
                         { sku: SKU_COMBO7, in_stock: 33 },
                         { sku: SKU_COMBO8, in_stock: 33 },
                         { sku: SKU_COMBO9, in_stock: 33 },
                         { sku: SKU_COMBO10, in_stock: 33 },
                         { sku: SKU_CHILD1, in_stock: 100 } , 
                         { sku: SKU_CHILD2, in_stock: 100 } , 
                         { sku: SKU_CHILD3, in_stock: 100 }])
        const arrBundle = await bundleService.getConversionInOut({ sku: SKU_COMBO1 , committed: 1, stock_id: 1005 })
        //console.log(arrBundle)
        assert(Array.isArray(arrBundle), 'Bundle error');
        //assert(arrBundle.length == 1, 'Only one item');
        
        const child1 = arrBundle.find(item => item.sku === SKU_CHILD1)
        assert(child1, 'Child 100160014 must committed');
        assert(child1.qty == 1, 'Must committed 1');

        const child2 = arrBundle.find(item => item.sku === SKU_CHILD2)
        assert(child2, 'Child 100100015 must committed');
        assert(child2.qty == 1, 'Must committed 1');
        
        const combo2 = arrBundle.find(item => item.sku === SKU_COMBO2)
        assert(combo2, 'Combo 204900015 must committed');
        assert(combo2.qty == 1, 'Must committed 1');

        const combo3 = arrBundle.find(item => item.sku === SKU_COMBO3)
        assert(combo3, 'Combo 204900017 must committed');
        assert(combo3.qty == 1, 'Must committed 1');

        const combo4 = arrBundle.find(item => item.sku === SKU_COMBO4)
        assert(combo4, 'Combo 204900018 must committed');
        assert(combo4.qty == 1, 'Must committed 1');

        const combo5 = arrBundle.find(item => item.sku === SKU_COMBO5)
        assert(combo5, 'Combo 204900019 must committed');
        assert(combo5.qty == 1, 'Must committed 1');     
    });
    // bán 1 combo 204900016
    it('Sales combo 204900016 with qty 1', async function () {
        await initStock([{ sku: SKU_COMBO1, in_stock: 100 }, 
                         { sku: SKU_COMBO2, in_stock: 100 }, 
                         { sku: SKU_COMBO3, in_stock: 50 },
                         { sku: SKU_COMBO4, in_stock: 50 },
                         { sku: SKU_COMBO5, in_stock: 50 },
                         { sku: SKU_COMBO6, in_stock: 50 },
                         { sku: SKU_COMBO7, in_stock: 33 },
                         { sku: SKU_COMBO8, in_stock: 33 },
                         { sku: SKU_COMBO9, in_stock: 33 },
                         { sku: SKU_COMBO10, in_stock: 33 },
                         { sku: SKU_CHILD1, in_stock: 100 } , 
                         { sku: SKU_CHILD2, in_stock: 100 } , 
                         { sku: SKU_CHILD3, in_stock: 100 }])
        const arrBundle = await bundleService.getConversionInOut({ sku: SKU_COMBO2 , committed: 1, stock_id: 1005 })
        //console.log(arrBundle)
        assert(Array.isArray(arrBundle), 'Bundle error');
        //assert(arrBundle.length == 1, 'Only one item');
        const child2 = arrBundle.find(item => item.sku === SKU_CHILD2)
        assert(child2, 'Child 100100015 must committed');
        assert(child2.qty == 1, 'Must committed 1');

        const child3 = arrBundle.find(item => item.sku === SKU_CHILD3)
        assert(child3, 'Child 100170026 must committed');
        assert(child3.qty == 1, 'Must committed 1');
        
        const combo1 = arrBundle.find(item => item.sku === SKU_COMBO1)
        assert(combo1, 'Combo 204900015 must committed');
        assert(combo1.qty == 1, 'Must committed 1');
        
        const combo3 = arrBundle.find(item => item.sku === SKU_COMBO3)
        assert(combo3, 'Combo 204900017 must committed');
        assert(combo3.qty == 1, 'Must committed 1');

        const combo4 = arrBundle.find(item => item.sku === SKU_COMBO4)
        assert(combo4, 'Combo 204900018 must committed');
        assert(combo4.qty == 1, 'Must committed 1');

        const combo6 = arrBundle.find(item => item.sku === SKU_COMBO6)
        assert(combo6, 'Combo 204900020 must committed');
        assert(combo6.qty == 1, 'Must committed 1');
    });
    // bán 1 combo 204900017
    it('Sales combo 204900017 with qty 1', async function () {
        await initStock([{ sku: SKU_COMBO1, in_stock: 100 }, 
                         { sku: SKU_COMBO2, in_stock: 100 }, 
                         { sku: SKU_COMBO3, in_stock: 50 },
                         { sku: SKU_COMBO4, in_stock: 50 },
                         { sku: SKU_COMBO5, in_stock: 50 },
                         { sku: SKU_COMBO6, in_stock: 50 },
                         { sku: SKU_COMBO7, in_stock: 33 },
                         { sku: SKU_COMBO8, in_stock: 33 },
                         { sku: SKU_COMBO9, in_stock: 33 },
                         { sku: SKU_COMBO10, in_stock: 33 },
                         { sku: SKU_CHILD1, in_stock: 100 } , 
                         { sku: SKU_CHILD2, in_stock: 100 } , 
                         { sku: SKU_CHILD3, in_stock: 100 }])
        const arrBundle = await bundleService.getConversionInOut({ sku: SKU_COMBO3 , committed: 1, stock_id: 1005 })
        //console.log(arrBundle)
        assert(Array.isArray(arrBundle), 'Bundle error');
        //assert(arrBundle.length == 1, 'Only one item');

        const child1 = arrBundle.find(item => item.sku === SKU_CHILD1)
        assert(child1, 'Child 100160014 must committed');
        assert(child1.qty == 1, 'Must committed 1');

        const child2 = arrBundle.find(item => item.sku === SKU_CHILD2)
        assert(child2, 'Child 100100015 must committed');
        assert(child2.qty == 2, 'Must committed 2');
        
        const combo2 = arrBundle.find(item => item.sku === SKU_COMBO2)
        assert(combo2, 'Combo 204900016 must committed');
        assert(combo2.qty == 2, 'Must committed 2');

        const combo4 = arrBundle.find(item => item.sku === SKU_COMBO4)
        assert(combo4, 'Combo 204900018 must committed');
        assert(combo4.qty == 1, 'Must committed 1');

        const combo5 = arrBundle.find(item => item.sku === SKU_COMBO5)
        assert(combo5, 'Combo 204900019 must committed');
        assert(combo5.qty == 1, 'Must committed 1');

        const combo7 = arrBundle.find(item => item.sku === SKU_COMBO7)
        assert(combo7, 'Combo 204900021 must committed');
        assert(combo7.qty == 1, 'Must committed 1');

        const combo8 = arrBundle.find(item => item.sku === SKU_COMBO4)
        assert(combo8, 'Combo 204900022 must committed');
        assert(combo8.qty == 1, 'Must committed 1');
   
    });
    // bán 1 combo 204900018
    it('Sales combo 204900018 with qty 1', async function () {
        await initStock([{ sku: SKU_COMBO1, in_stock: 100 }, 
                         { sku: SKU_COMBO2, in_stock: 100 }, 
                         { sku: SKU_COMBO3, in_stock: 50 },
                         { sku: SKU_COMBO4, in_stock: 50 },
                         { sku: SKU_COMBO5, in_stock: 50 },
                         { sku: SKU_COMBO6, in_stock: 50 },
                         { sku: SKU_COMBO7, in_stock: 33 },
                         { sku: SKU_COMBO8, in_stock: 33 },
                         { sku: SKU_COMBO9, in_stock: 33 },
                         { sku: SKU_COMBO10, in_stock: 33 },
                         { sku: SKU_CHILD1, in_stock: 100 } , 
                         { sku: SKU_CHILD2, in_stock: 100 } , 
                         { sku: SKU_CHILD3, in_stock: 100 }])
        const arrBundle = await bundleService.getConversionInOut({ sku: SKU_COMBO4 , committed: 1, stock_id: 1005 })
        //console.log(arrBundle)
        assert(Array.isArray(arrBundle), 'Bundle error');
        //assert(arrBundle.length == 1, 'Only one item');
        
        const child2 = arrBundle.find(item => item.sku === SKU_CHILD2)
        assert(child2, 'Child 100100015 must committed');
        assert(child2.qty == 2, 'Must committed 2');

        const child3 = arrBundle.find(item => item.sku === SKU_CHILD3)
        assert(child3, 'Child 100170026 must committed');
        assert(child3.qty == 1, 'Must committed 1');

        const combo1 = arrBundle.find(item => item.sku === SKU_COMBO1)
        assert(combo1, 'Combo 204900015 must committed');
        assert(combo1.qty == 2, 'Must committed 2');

        const combo2 = arrBundle.find(item => item.sku === SKU_COMBO2)
        assert(combo2, 'Combo 204900016 must committed');
        assert(combo2.qty == 2, 'Must committed 2');
  
        const combo3 = arrBundle.find(item => item.sku === SKU_COMBO3)
        assert(combo3, 'Combo 204900017 must committed');
        assert(combo3.qty == 1, 'Must committed 1');

        const combo6 = arrBundle.find(item => item.sku === SKU_COMBO6)
        assert(combo6, 'Combo 204900020 must committed');
        assert(combo6.qty == 1, 'Must committed 1');

        const combo7 = arrBundle.find(item => item.sku === SKU_COMBO7)
        assert(combo7, 'Combo 204900021 must committed');
        assert(combo7.qty == 1, 'Must committed 1');

        const combo8 = arrBundle.find(item => item.sku === SKU_COMBO8)
        assert(combo8, 'Combo 204900022 must committed');
        assert(combo8.qty == 1, 'Must committed 1');
   
    });
    // bán 1 combo 204900019
    it('Sales combo 204900019 with qty 1', async function () {
        await initStock([{ sku: SKU_COMBO1, in_stock: 100 }, 
                         { sku: SKU_COMBO2, in_stock: 100 }, 
                         { sku: SKU_COMBO3, in_stock: 50 },
                         { sku: SKU_COMBO4, in_stock: 50 },
                         { sku: SKU_COMBO5, in_stock: 50 },
                         { sku: SKU_COMBO6, in_stock: 50 },
                         { sku: SKU_COMBO7, in_stock: 33 },
                         { sku: SKU_COMBO8, in_stock: 33 },
                         { sku: SKU_COMBO9, in_stock: 33 },
                         { sku: SKU_COMBO10, in_stock: 33 },
                         { sku: SKU_CHILD1, in_stock: 100 } , 
                         { sku: SKU_CHILD2, in_stock: 100 } , 
                         { sku: SKU_CHILD3, in_stock: 100 }])
        const arrBundle = await bundleService.getConversionInOut({ sku: SKU_COMBO5 , committed: 1, stock_id: 1005 })
       // console.log(arrBundle)
        assert(Array.isArray(arrBundle), 'Bundle error');
        //assert(arrBundle.length == 1, 'Only one item');
        
        const child1 = arrBundle.find(item => item.sku === SKU_CHILD1)
        assert(child1, 'Child 100160014 must committed');
        assert(child1.qty == 2, 'Must committed 2');

        const child2 = arrBundle.find(item => item.sku === SKU_CHILD2)
        assert(child2, 'Child 100100015 must committed');
        assert(child2.qty == 1, 'Must committed 1');

        const combo1 = arrBundle.find(item => item.sku === SKU_COMBO1)
        assert(combo1, 'Combo 204900015 must committed');
        assert(combo1.qty == 2, 'Must committed 2');

        const combo2 = arrBundle.find(item => item.sku === SKU_COMBO2)
        assert(combo2, 'Combo 204900016 must committed');
        assert(combo2.qty == 1, 'Must committed 1');
  
        const combo3 = arrBundle.find(item => item.sku === SKU_COMBO3)
        assert(combo3, 'Combo 204900017 must committed');
        assert(combo3.qty == 1, 'Must committed 1');

        const combo4 = arrBundle.find(item => item.sku === SKU_COMBO4)
        assert(combo4, 'Combo 204900018 must committed');
        assert(combo4.qty == 1, 'Must committed 1');

        const combo9 = arrBundle.find(item => item.sku === SKU_COMBO9)
        assert(combo9, 'Combo 204900023 must committed');
        assert(combo9.qty == 1, 'Must committed 1');
   
    });
     // bán 1 combo 204900020
     it('Sales combo 204900020 with qty 1', async function () {
        await initStock([{ sku: SKU_COMBO1, in_stock: 100 }, 
                         { sku: SKU_COMBO2, in_stock: 100 }, 
                         { sku: SKU_COMBO3, in_stock: 50 },
                         { sku: SKU_COMBO4, in_stock: 50 },
                         { sku: SKU_COMBO5, in_stock: 50 },
                         { sku: SKU_COMBO6, in_stock: 50 },
                         { sku: SKU_COMBO7, in_stock: 33 },
                         { sku: SKU_COMBO8, in_stock: 33 },
                         { sku: SKU_COMBO9, in_stock: 33 },
                         { sku: SKU_COMBO10, in_stock: 33 },
                         { sku: SKU_CHILD1, in_stock: 100 } , 
                         { sku: SKU_CHILD2, in_stock: 100 } , 
                         { sku: SKU_CHILD3, in_stock: 100 }])
        const arrBundle = await bundleService.getConversionInOut({ sku: SKU_COMBO6 , committed: 1, stock_id: 1005 })
       // console.log(arrBundle)
        assert(Array.isArray(arrBundle), 'Bundle error');
        //assert(arrBundle.length == 1, 'Only one item');

        const child2 = arrBundle.find(item => item.sku === SKU_CHILD2)
        assert(child2, 'Child 100100015 must committed');
        assert(child2.qty == 1, 'Must committed 1');

        const child3 = arrBundle.find(item => item.sku === SKU_CHILD3)
        assert(child3, 'Child 100170026 must committed');
        assert(child3.qty == 2, 'Must committed 2');

        const combo1 = arrBundle.find(item => item.sku === SKU_COMBO1)
        assert(combo1, 'Combo 204900015 must committed');
        assert(combo1.qty == 1, 'Must committed 1');

        const combo2 = arrBundle.find(item => item.sku === SKU_COMBO2)
        assert(combo2, 'Combo 204900016 must committed');
        assert(combo2.qty == 2, 'Must committed 2');
  
        const combo3 = arrBundle.find(item => item.sku === SKU_COMBO3)
        assert(combo3, 'Combo 204900017 must committed');
        assert(combo3.qty == 1, 'Must committed 1');

        const combo4 = arrBundle.find(item => item.sku === SKU_COMBO4)
        assert(combo4, 'Combo 204900018 must committed');
        assert(combo4.qty == 1, 'Must committed 1');

        const combo10 = arrBundle.find(item => item.sku === SKU_COMBO10)
        assert(combo10, 'Combo 204900024 must committed');
        assert(combo10.qty == 1, 'Must committed 1');
   
    });
     // bán 1 combo 204900021
     it('Sales combo 204900021 with qty 1', async function () {
        await initStock([{ sku: SKU_COMBO1, in_stock: 100 }, 
                         { sku: SKU_COMBO2, in_stock: 100 }, 
                         { sku: SKU_COMBO3, in_stock: 50 },
                         { sku: SKU_COMBO4, in_stock: 50 },
                         { sku: SKU_COMBO5, in_stock: 50 },
                         { sku: SKU_COMBO6, in_stock: 50 },
                         { sku: SKU_COMBO7, in_stock: 33 },
                         { sku: SKU_COMBO8, in_stock: 33 },
                         { sku: SKU_COMBO9, in_stock: 33 },
                         { sku: SKU_COMBO10, in_stock: 33 },
                         { sku: SKU_CHILD1, in_stock: 100 } , 
                         { sku: SKU_CHILD2, in_stock: 100 } , 
                         { sku: SKU_CHILD3, in_stock: 100 }])
        const arrBundle = await bundleService.getConversionInOut({ sku: SKU_COMBO7 , committed: 1, stock_id: 1005 })
        //console.log(arrBundle)
        assert(Array.isArray(arrBundle), 'Bundle error');
        //assert(arrBundle.length == 1, 'Only one item');
        
        const child1 = arrBundle.find(item => item.sku === SKU_CHILD1)
        assert(child1, 'Child 100160014 must committed');
        assert(child1.qty == 2, 'Must committed 2');

        const child2 = arrBundle.find(item => item.sku === SKU_CHILD2)
        assert(child2, 'Child 100100015 must committed');
        assert(child2.qty == 3, 'Must committed 3');

        const combo1 = arrBundle.find(item => item.sku === SKU_COMBO1)
        assert(combo1, 'Combo 204900015 must committed');
        assert(combo1.qty == 3, 'Must committed 3');

        const combo2 = arrBundle.find(item => item.sku === SKU_COMBO2)
        assert(combo2, 'Combo 204900016 must committed');
        assert(combo2.qty == 3, 'Must committed 3');
  
        const combo3 = arrBundle.find(item => item.sku === SKU_COMBO3)
        assert(combo3, 'Combo 204900017 must committed');
        assert(combo3.qty == 2, 'Must committed 2');

        const combo4 = arrBundle.find(item => item.sku === SKU_COMBO4)
        assert(combo4, 'Combo 204900018 must committed');
        assert(combo4.qty == 2, 'Must committed 2');

        const combo5 = arrBundle.find(item => item.sku === SKU_COMBO5)
        assert(combo5, 'Combo 204900019 must committed');
        assert(combo5.qty == 1, 'Must committed 1');

        const combo6 = arrBundle.find(item => item.sku === SKU_COMBO6)
        assert(!combo6, 'Combo 204900020 not committed');

        const combo8 = arrBundle.find(item => item.sku === SKU_COMBO8)
        assert(combo8, 'Combo 204900022 must committed');
        assert(combo8.qty == 1, 'Must committed 2');

        const combo9 = arrBundle.find(item => item.sku === SKU_COMBO9)
        assert(combo9, 'Combo 204900023 must committed');
        assert(combo9.qty == 1, 'Must committed 2');

        const combo10 = arrBundle.find(item => item.sku === SKU_COMBO10)
        assert(!combo10, 'Combo 204900024 NOT committed');
   
    });
     // bán 1 combo 204900022
     it('Sales combo 204900022 with qty 1', async function () {
        await initStock([{ sku: SKU_COMBO1, in_stock: 100 }, 
                         { sku: SKU_COMBO2, in_stock: 100 }, 
                         { sku: SKU_COMBO3, in_stock: 50 },
                         { sku: SKU_COMBO4, in_stock: 50 },
                         { sku: SKU_COMBO5, in_stock: 50 },
                         { sku: SKU_COMBO6, in_stock: 50 },
                         { sku: SKU_COMBO7, in_stock: 33 },
                         { sku: SKU_COMBO8, in_stock: 33 },
                         { sku: SKU_COMBO9, in_stock: 33 },
                         { sku: SKU_COMBO10, in_stock: 33 },
                         { sku: SKU_CHILD1, in_stock: 100 } , 
                         { sku: SKU_CHILD2, in_stock: 100 } , 
                         { sku: SKU_CHILD3, in_stock: 100 }])
        const arrBundle = await bundleService.getConversionInOut({ sku: SKU_COMBO8 , committed: 1, stock_id: 1005 })
        //console.log(arrBundle)
        assert(Array.isArray(arrBundle), 'Bundle error');
        //assert(arrBundle.length == 1, 'Only one item');

        const child2 = arrBundle.find(item => item.sku === SKU_CHILD2)
        assert(child2, 'Child 100100015 must committed');
        assert(child2.qty == 3, 'Must committed 3');
           
        const child3 = arrBundle.find(item => item.sku === SKU_CHILD3)
        assert(child3, 'Child 100170026 must committed');
        assert(child3.qty == 2, 'Must committed 2');

        const combo1 = arrBundle.find(item => item.sku === SKU_COMBO1)
        assert(combo1, 'Combo 204900015 must committed');
        assert(combo1.qty == 3, 'Must committed 3');

        const combo2 = arrBundle.find(item => item.sku === SKU_COMBO2)
        assert(combo2, 'Combo 204900016 must committed');
        assert(combo2.qty == 3, 'Must committed 3');
  
        const combo3 = arrBundle.find(item => item.sku === SKU_COMBO3)
        assert(combo3, 'Combo 204900017 must committed');
        assert(combo3.qty == 2, 'Must committed 2');

        const combo4 = arrBundle.find(item => item.sku === SKU_COMBO4)
        assert(combo4, 'Combo 204900018 must committed');
        assert(combo4.qty == 2, 'Must committed 2');

        // const combo5 = arrBundle.find(item => item.sku === SKU_COMBO5)
        // assert(combo5, 'Combo 204900019 must committed');
        // assert(combo5.qty == 1, 'Must committed 1');

        const combo6 = arrBundle.find(item => item.sku === SKU_COMBO6)
        assert(combo6, 'Combo 204900020 not committed');
        assert(combo6.qty == 1, 'Must committed 1');

        const combo7 = arrBundle.find(item => item.sku === SKU_COMBO7)
        assert(combo7, 'Combo 204900021 not committed');
        assert(combo7.qty == 1, 'Must committed 1');

        const combo9 = arrBundle.find(item => item.sku === SKU_COMBO9)
        assert(!combo9, 'Combo 204900023 must committed');

        const combo10 = arrBundle.find(item => item.sku === SKU_COMBO10)
        assert(combo10, 'Combo 204900024 must committed');
        assert(combo10.qty == 1, 'Must committed 1');
   
    });
     // bán 1 combo 204900023
     it('Sales combo 204900023 with qty 1', async function () {
        await initStock([{ sku: SKU_COMBO1, in_stock: 100 }, 
                         { sku: SKU_COMBO2, in_stock: 100 }, 
                         { sku: SKU_COMBO3, in_stock: 50 },
                         { sku: SKU_COMBO4, in_stock: 50 },
                         { sku: SKU_COMBO5, in_stock: 50 },
                         { sku: SKU_COMBO6, in_stock: 50 },
                         { sku: SKU_COMBO7, in_stock: 33 },
                         { sku: SKU_COMBO8, in_stock: 33 },
                         { sku: SKU_COMBO9, in_stock: 33 },
                         { sku: SKU_COMBO10, in_stock: 33 },
                         { sku: SKU_CHILD1, in_stock: 100 } , 
                         { sku: SKU_CHILD2, in_stock: 100 } , 
                         { sku: SKU_CHILD3, in_stock: 100 }])
        const arrBundle = await bundleService.getConversionInOut({ sku: SKU_COMBO9 , committed: 1, stock_id: 1005 })
        //console.log(arrBundle)
        assert(Array.isArray(arrBundle), 'Bundle error');
        //assert(arrBundle.length == 1, 'Only one item');
        
        const child1 = arrBundle.find(item => item.sku === SKU_CHILD1)
        assert(child1, 'Child 100100015 must committed');
        assert(child1.qty == 3, 'Must committed 3');

        const child2 = arrBundle.find(item => item.sku === SKU_CHILD2)
        assert(child2, 'Child 100100015 must committed');
        assert(child2.qty == 2, 'Must committed 2');

        const combo1 = arrBundle.find(item => item.sku === SKU_COMBO1)
        assert(combo1, 'Combo 204900015 must committed');
        assert(combo1.qty == 3, 'Must committed 3');

        const combo2 = arrBundle.find(item => item.sku === SKU_COMBO2)
        assert(combo2, 'Combo 204900016 must committed');
        assert(combo2.qty == 2, 'Must committed 2');
  
        const combo3 = arrBundle.find(item => item.sku === SKU_COMBO3)
        assert(combo3, 'Combo 204900017 must committed');
        assert(combo3.qty == 1, 'Must committed 1');

        const combo4 = arrBundle.find(item => item.sku === SKU_COMBO4)
        assert(combo4, 'Combo 204900018 must committed');
        assert(combo4.qty == 1, 'Must committed 1');

        const combo5 = arrBundle.find(item => item.sku === SKU_COMBO5)
        assert(combo5, 'Combo 204900019 must committed');
        assert(combo5.qty == 2, 'Must committed 2');

        const combo6 = arrBundle.find(item => item.sku === SKU_COMBO6)
        assert(!combo6, 'Combo 204900020 not committed');

        const combo7 = arrBundle.find(item => item.sku === SKU_COMBO7)
        assert(combo7, 'Combo 204900021 not committed');
        assert(combo7.qty == 1, 'Must committed 1');

        const combo8 = arrBundle.find(item => item.sku === SKU_COMBO8)
        assert(combo8, 'Combo 204900022 must committed');
        assert(combo8.qty == 1, 'Must committed 1');

        const combo10 = arrBundle.find(item => item.sku === SKU_COMBO10)
        assert(!combo10, 'Combo 204900024 must committed');
     
   
    });
     // bán 1 combo 204900024
     it('Sales combo 2049000234 with qty 1', async function () {
        await initStock([{ sku: SKU_COMBO1, in_stock: 100 }, 
                         { sku: SKU_COMBO2, in_stock: 100 }, 
                         { sku: SKU_COMBO3, in_stock: 50 },
                         { sku: SKU_COMBO4, in_stock: 50 },
                         { sku: SKU_COMBO5, in_stock: 50 },
                         { sku: SKU_COMBO6, in_stock: 50 },
                         { sku: SKU_COMBO7, in_stock: 33 },
                         { sku: SKU_COMBO8, in_stock: 33 },
                         { sku: SKU_COMBO9, in_stock: 33 },
                         { sku: SKU_COMBO10, in_stock: 33 },
                         { sku: SKU_CHILD1, in_stock: 100 } , 
                         { sku: SKU_CHILD2, in_stock: 100 } , 
                         { sku: SKU_CHILD3, in_stock: 100 }])
        const arrBundle = await bundleService.getConversionInOut({ sku: SKU_COMBO10 , committed: 1, stock_id: 1005 })
        //console.log(arrBundle)
        assert(Array.isArray(arrBundle), 'Bundle error');

        const child2 = arrBundle.find(item => item.sku === SKU_CHILD2)
        assert(child2, 'Child 100100015 must committed');
        assert(child2.qty == 2, 'Must committed 2');

        const child3 = arrBundle.find(item => item.sku === SKU_CHILD3)
        assert(child3, 'Child 100100015 must committed');
        assert(child3.qty == 3, 'Must committed 3');

        const combo1 = arrBundle.find(item => item.sku === SKU_COMBO1)
        assert(combo1, 'Combo 204900015 must committed');
        assert(combo1.qty == 2, 'Must committed 2');

        const combo2 = arrBundle.find(item => item.sku === SKU_COMBO2)
        assert(combo2, 'Combo 204900016 must committed');
        assert(combo2.qty == 3, 'Must committed 3');
  
        const combo3 = arrBundle.find(item => item.sku === SKU_COMBO3)
        assert(combo3, 'Combo 204900017 must committed');
        assert(combo3.qty == 1, 'Must committed 1');

        const combo4 = arrBundle.find(item => item.sku === SKU_COMBO4)
        assert(combo4, 'Combo 204900018 must committed');
        assert(combo4.qty == 1, 'Must committed 1');

        const combo5 = arrBundle.find(item => item.sku === SKU_COMBO5)
        assert(!combo5, 'Combo 204900019 must committed');
      
        const combo6 = arrBundle.find(item => item.sku === SKU_COMBO6)
        assert(combo6, 'Combo 204900020 not committed');
        assert(combo6.qty == 2, 'Must committed 2');

        const combo7 = arrBundle.find(item => item.sku === SKU_COMBO7)
        assert(combo7, 'Combo 204900021 not committed');
        assert(combo7.qty == 1, 'Must committed 1');

        const combo8 = arrBundle.find(item => item.sku === SKU_COMBO8)
        assert(combo8, 'Combo 204900022 must committed');
        assert(combo8.qty == 1, 'Must committed 1');

        const combo9 = arrBundle.find(item => item.sku === SKU_COMBO9)
        assert(!combo9, 'Combo 204900024 must committed');
     
   
    });
    });
});     