const assert = require('assert');

const bundleService = require('../../services/bundleService')
const { Bundle, BundleDetail, StockProduct, sequelize } = require('../../models')


const SKU_COMBO_1 = 422203807
const SKU_COMBO_2 = 422203809
const SKU_COMBO_3 = 422203776

const SKU_CHILD = 243300007


async function initCombo() {
    const bundle = await Bundle.create({ sku: SKU_COMBO_1, type: 2, description: '2 x 243300007'});
    await BundleDetail.create({ bundle_id: bundle.id, sku: SKU_CHILD, qty: 2 });
    const bundle2 = await Bundle.create({ sku: SKU_COMBO_2, type: 2, description: '3 x 243300007'});
    await BundleDetail.create({ bundle_id: bundle2.id, sku: SKU_CHILD, qty: 3 });
    const bundle3 = await Bundle.create({ sku: SKU_COMBO_3, type: 2, description: '4 x 243300007'});
    await BundleDetail.create({ bundle_id: bundle3.id, sku: SKU_CHILD, qty: 4 });
}

async function cleanUpStock() {
    await StockProduct.destroy({ where: {}, truncate: true });
}
async function initStock(arrParam) {
    for (let param of arrParam) {
        await StockProduct.create({ stockprod_stock_id: 1005, stockprod_sku: param.sku, stockprod_in_stock: param.in_stock, stockprod_in_comming: 0, stockprod_available: param.in_stock, stockprod_committed: 0, stockprod_utime: Math.floor(+new Date() / 1000)});    
    }
    
}

async function initData() {
    await initCombo()
}

describe('Complex combo', function () {

    before(async () => {
        await initData()
        await cleanUpStock()
    })
    beforeEach(async () => {
        await cleanUpStock()
    })
    after(async () => {
        await Bundle.destroy({ where: {}, truncate: true });
        await BundleDetail.destroy({ where: {}, truncate: true });
    })
    describe('Combo 422203807', function () {
        // Run one test case: yarn mocha test/in-out/committed-combo-mix.spec.js -g 'Committed combo with qty 1'
        it('Committed combo1 with qty 1', async function () {
            await initStock([{ sku: SKU_COMBO_1, in_stock: 0 }, { sku: SKU_COMBO_2, in_stock: 0 }, { sku: SKU_COMBO_3, in_stock: 0 } , { sku: SKU_CHILD, in_stock: 0 }])
            const arrBundle = await bundleService.getConversionInOut({ sku: SKU_COMBO_1, committed: 1, stock_id: 1005 })
            console.log(arrBundle)
            assert(Array.isArray(arrBundle), 'Bundle error');
            //assert(arrBundle.length == 1, 'Only one item');
            const child = arrBundle.find(item => item.sku === SKU_CHILD)
            assert(child, 'Child 243300007 must committed');
            assert(child.qty == 2, 'Must committed 2');

            const combo2 = arrBundle.find(item => item.sku === SKU_COMBO_2)
            assert(combo2, 'Combo 422203809 must committed');
            assert(combo2.qty == 1, 'Must committed 1');

            const combo3 = arrBundle.find(item => item.sku === SKU_COMBO_3)
            assert(combo3, 'Combo 422203776 must committed');
            assert(combo3.qty == 1, 'Must committed 1');
        });
        it('Committed combo1 with qty 1 with empty', async function () {
            //await initStock([{ sku: SKU_COMBO_1, in_stock: 0 }, { sku: SKU_COMBO_2, in_stock: 0 }, { sku: SKU_COMBO_3, in_stock: 0 } , { sku: SKU_CHILD, in_stock: 0 }])
            const arrBundle = await bundleService.getConversionInOut({ sku: SKU_COMBO_1, committed: 1, stock_id: 1005 })
            console.log(arrBundle)
            assert(Array.isArray(arrBundle), 'Bundle error');
            //assert(arrBundle.length == 1, 'Only one item');
            const child = arrBundle.find(item => item.sku === SKU_CHILD)
            assert(child, 'Child 243300007 must committed');
            assert(child.qty == 2, 'Must committed 2');

            /*
            const combo2 = arrBundle.find(item => item.sku === SKU_COMBO_2)
            assert(combo2, 'Combo 422203809 must committed');
            assert(combo2.qty == 1, 'Must committed 1');

            const combo3 = arrBundle.find(item => item.sku === SKU_COMBO_3)
            assert(combo3, 'Combo 422203776 must committed');
            assert(combo3.qty == 1, 'Must committed 1');
            */
        });
        
        it('Committed combo1 with qty 1 exist 1', async function () {
            // Nhung combo nao khong co stock, se khong tinh committed
            await initStock([{ sku: SKU_COMBO_1, in_stock: 1 }, { sku: SKU_COMBO_2, in_stock: 0 }, { sku: SKU_COMBO_3, in_stock: 0 } , { sku: SKU_CHILD, in_stock: 2 }])
            const arrBundle = await bundleService.getConversionInOut({ sku: SKU_COMBO_1, committed: 1, stock_id: 1005 })
            console.log(arrBundle)
            assert(Array.isArray(arrBundle), 'Bundle error');
            //assert(arrBundle.length == 1, 'Only one item');
            const child = arrBundle.find(item => item.sku === SKU_CHILD)
            assert(child, 'Child 243300007 must committed');
            assert(child.qty == 2, 'Must committed 2');

            const combo2 = arrBundle.find(item => item.sku === SKU_COMBO_2)
            console.log(combo2);
            assert(!combo2, 'Combo 422203809 must committed');

            const combo3 = arrBundle.find(item => item.sku === SKU_COMBO_3)
            assert(!combo3, 'Combo 422203776 must committed');
        });
        
        it('Committed combo1 with qty 2 with exist 2', async function () {
            await initStock([{ sku: SKU_COMBO_1, in_stock: 2 }, { sku: SKU_COMBO_2, in_stock: 1 }, { sku: SKU_COMBO_3, in_stock: 1 } , { sku: SKU_CHILD, in_stock: 4 }])
            const arrBundle = await bundleService.getConversionInOut({ sku: SKU_COMBO_1, committed: 2, stock_id: 1005 })
            console.log(arrBundle)
            assert(Array.isArray(arrBundle), 'Bundle error');
            //assert(arrBundle.length == 1, 'Only one item');
            const child = arrBundle.find(item => item.sku === SKU_CHILD)
            assert(child, 'Child 243300007 must committed');
            assert(child.qty == 4, 'Must committed 2');

            const combo2 = arrBundle.find(item => item.sku === SKU_COMBO_2)
            assert(combo2, 'Combo 422203809 must committed');
            assert(combo2.qty == 1, 'Must committed 1');

            const combo3 = arrBundle.find(item => item.sku === SKU_COMBO_3)
            assert(combo3, 'Combo 422203776 must committed');
            assert(combo3.qty == 1, 'Must committed 1');
        });

        it('Committed combo1 with qty 1 with exist 5 child', async function () {
            await initStock([{ sku: SKU_COMBO_1, in_stock: 2 }, { sku: SKU_COMBO_2, in_stock: 1 }, { sku: SKU_COMBO_3, in_stock: 1 } , { sku: SKU_CHILD, in_stock: 5 }])
            const arrBundle = await bundleService.getConversionInOut({ sku: SKU_COMBO_1, committed: 1, stock_id: 1005 })
            console.log(arrBundle)
            assert(Array.isArray(arrBundle), 'Bundle error');
            //assert(arrBundle.length == 1, 'Only one item');
            const child = arrBundle.find(item => item.sku === SKU_CHILD)
            assert(child, 'Child 243300007 must committed');
            assert(child.qty == 2, 'Must committed 2');

            const combo3 = arrBundle.find(item => item.sku === SKU_COMBO_3)
            assert(combo3, 'Combo 422203776 must committed');
            assert(combo3.qty == 1, 'Must committed 1');
        });

        it('Committed combo1 with qty 1 with exist 11 child', async function () {
            // Sau khi commit 1 cb1 -> con 9 sku con ==> 3 combo 2, 2 combo 3
            await initStock([{ sku: SKU_COMBO_1, in_stock: 5 }, { sku: SKU_COMBO_2, in_stock: 3 }, { sku: SKU_COMBO_3, in_stock: 2 } , { sku: SKU_CHILD, in_stock: 11 }])
            const arrBundle = await bundleService.getConversionInOut({ sku: SKU_COMBO_1, committed: 1, stock_id: 1005 })
            console.log(arrBundle)
            assert(Array.isArray(arrBundle), 'Bundle error');
            const child = arrBundle.find(item => item.sku === SKU_CHILD)
            assert(child, 'Child 243300007 must committed');
            assert(child.qty == 2, 'Must committed 2');

            const combo2 = arrBundle.find(item => item.sku === SKU_COMBO_2)
            assert(!combo2, 'Combo 422203809 not committed');

            const combo3 = arrBundle.find(item => item.sku === SKU_COMBO_3)
            assert(!combo3, 'Combo 422203776 must committed');
        });

        it('Committed combo1 with qty 1 with exist 10 child', async function () {
            // Sau khi commit 1 cb1 -> con 8 sku con ==> 2 combo 2, 2 combo 3
            await initStock([{ sku: SKU_COMBO_1, in_stock: 5 }, { sku: SKU_COMBO_2, in_stock: 3 }, { sku: SKU_COMBO_3, in_stock: 2 } , { sku: SKU_CHILD, in_stock: 10 }])
            const arrBundle = await bundleService.getConversionInOut({ sku: SKU_COMBO_1, committed: 1, stock_id: 1005 })
            console.log(arrBundle)
            assert(Array.isArray(arrBundle), 'Bundle error');
            const child = arrBundle.find(item => item.sku === SKU_CHILD)
            assert(child, 'Child 243300007 must committed');
            assert(child.qty == 2, 'Must committed 2');

            const combo2 = arrBundle.find(item => item.sku === SKU_COMBO_2)
            assert(combo2, 'Combo 422203809 must committed');
            assert(combo2.qty == 1, 'Must committed 1');

            const combo3 = arrBundle.find(item => item.sku === SKU_COMBO_3)
            assert(!combo3, 'Combo 422203776 not committed');
        });
    });

    describe('Sku 100150041', function () { 
        it('Committed sku with qty 1', async function () {
            // await initStock([{ sku: SKU_CHILD, in_stock: 3 }, { sku: SKU_COMBO, in_stock: 1 }])
            // const arrBundle = await bundleService.getConversionInOut({ sku: SKU_CHILD, committed: 1, stock_id: 1005 })
            // assert(Array.isArray(arrBundle), 'Bundle error');
            // assert(arrBundle.length == 1, 'Only one item');
            // const child = arrBundle.find(item => item.sku === SKU_COMBO)
            // assert(child, 'Combo 201600075 must committed');
            // assert(child.qty == 1, 'Must committed 1');
        });
    })
})