const assert = require('assert');

const bundleService = require('../../services/bundleService')
const { Bundle, BundleDetail, StockProduct, sequelize } = require('../../models')
const SKU_CHILD = 100150041
const SKU_COMBO = 201600075
async function initCombo201600075() {
    const bundle = await Bundle.create({ sku: SKU_COMBO, type: 2, description: '3 x 100150041' });
    await BundleDetail.create({ bundle_id: bundle.id, sku: SKU_CHILD, qty: 3 });
}

async function cleanUpStock() {
    await StockProduct.destroy({ where: {}, truncate: true });
}
async function initStock(arrParam) {
    for (let param of arrParam) {
        await StockProduct.create({ stockprod_stock_id: 1005, stockprod_sku: param.sku, stockprod_in_stock: param.in_stock, stockprod_in_comming: 0, stockprod_available: param.in_stock, stockprod_committed: 0, stockprod_utime: Math.floor(+new Date() / 1000)});    
    }
    
}

async function initData() {
    await initCombo201600075()
}

describe('Combo simple 3', function () {

    before(async () => {
        await initData()
        await cleanUpStock()
    })
    beforeEach(async () => {
        await cleanUpStock()
    })
    after(async () => {
        await Bundle.destroy({ where: {}, truncate: true });
        await BundleDetail.destroy({ where: {}, truncate: true });
    })
    describe('Combo 201600075', function () {
        // Run one test case: yarn mocha test/in-out/in-comming-combo3.spec.js  -g 'Combo 201600075'
        it('Purchase combo with qty 1', async function () {
            const arrBundle = await bundleService.getConversionInOut({ sku: 201600075, in_comming: 1, stock_id: 1005 })
            assert(Array.isArray(arrBundle), 'Bundle error');
            assert(arrBundle.length == 1, 'Only one item');
            const child = arrBundle.find(item => item.sku === SKU_CHILD)
            assert(child, 'Child 100150041 must in-comming');
            assert(child.qty == 3, 'Must in-comming 3');
        });

        it('Purchase combo with qty 2', async function () {
            const arrBundle = await bundleService.getConversionInOut({ sku: 201600075, in_comming: 2, stock_id: 1005 })
            assert(Array.isArray(arrBundle), 'Bundle error');
            assert(arrBundle.length == 1, 'Only one item');
            const child = arrBundle.find(item => item.sku === SKU_CHILD)
            assert(child, 'Child 100150041 must in-comming');
            assert(child.qty == 6, 'Must in-comming 6');
        });

        it('Purchase combo with qty 5', async function () {
            const arrBundle = await bundleService.getConversionInOut({ sku: 201600075, in_comming: 5, stock_id: 1005 })
            assert(Array.isArray(arrBundle), 'Bundle error');
            assert(arrBundle.length == 1, 'Only one item');
            const child = arrBundle.find(item => item.sku === SKU_CHILD)
            assert(child, 'Child 100150041 must in-comming');
            assert(child.qty == 15, 'Must in-comming 15');
        });

        it('Committed combo with qty 1', async function () {
            const arrBundle = await bundleService.getConversionInOut({ sku: 201600075, committed: 1, stock_id: 1005 })
            assert(Array.isArray(arrBundle), 'Bundle error');
            assert(arrBundle.length == 1, 'Only one item');
            const child = arrBundle.find(item => item.sku === SKU_CHILD)
            assert(child, 'Child 100150041 must committed');
            assert(child.qty == 3, 'Must committed 3');
        });

        it('Committed combo with qty 2', async function () {
            const arrBundle = await bundleService.getConversionInOut({ sku: 201600075, committed: 2, stock_id: 1005 })
            assert(Array.isArray(arrBundle), 'Bundle error');
            assert(arrBundle.length == 1, 'Only one item');
            const child = arrBundle.find(item => item.sku === SKU_CHILD)
            assert(child, 'Child 100150041 must committed');
            assert(child.qty == 6, 'Must committed 6');
        });

        it('Committed combo with qty 4', async function () {
            const arrBundle = await bundleService.getConversionInOut({ sku: 201600075, committed: 4, stock_id: 1005 })
            assert(Array.isArray(arrBundle), 'Bundle error');
            assert(arrBundle.length == 1, 'Only one item');
            const child = arrBundle.find(item => item.sku === SKU_CHILD)
            assert(child, 'Child 100150041 must committed');
            assert(child.qty == 12, 'Must committed 12');
        });
    });

    describe('Sku 100150041', function () { 
        // Run one test case: yarn mocha test/in-out/in-comming-combo.spec.js  -g 'Combo 201600075'
        it('Purchase sku with qty 1', async function () {
            await initStock([{ sku: SKU_CHILD, in_stock: 0 }, { sku: SKU_COMBO, in_stock: 0 }])
            const arrBundle = await bundleService.getConversionInOut({ sku: 100150041, in_comming: 1, stock_id: 1005 })
            assert(Array.isArray(arrBundle), 'Bundle error');
            assert(arrBundle.length == 0, 'Bundle error');
        });

        it('Purchase sku with qty 2', async function () {
            await initStock([{ sku: SKU_CHILD, in_stock: 0 }, { sku: SKU_COMBO, in_stock: 0 }])
            const arrBundle = await bundleService.getConversionInOut({ sku: 100150041, in_comming: 2, stock_id: 1005 })
            assert(Array.isArray(arrBundle), 'Bundle error');
            assert(arrBundle.length == 0, 'Bundle error');
        });

        it('Purchase sku with qty 2 with exist 1', async function () {
            await initStock([{ sku: SKU_CHILD, in_stock: 1 }, { sku: SKU_COMBO, in_stock: 0 }])
            const arrBundle = await bundleService.getConversionInOut({ sku: 100150041, in_comming: 2, stock_id: 1005 })
            assert(Array.isArray(arrBundle), 'Bundle error');
            const combo = arrBundle.find(item => item.sku === SKU_COMBO)
            assert(combo, 'Combo 201600075 must in-comming');
            assert(combo.qty == 1, 'Must in-comming 1');
        });
        it('Purchase sku with qty 3', async function () {
            await initStock([{ sku: SKU_CHILD, in_stock: 0 }, { sku: SKU_COMBO, in_stock: 0 }])
            const arrBundle = await bundleService.getConversionInOut({ sku: 100150041, in_comming: 3, stock_id: 1005 })
            assert(Array.isArray(arrBundle), 'Bundle error');
            const combo = arrBundle.find(item => item.sku === SKU_COMBO)
            assert(combo, 'Combo 201600075 must in-comming');
            assert(combo.qty == 1, 'Must in-comming 1');
        });

        it('Purchase sku with qty 4', async function () {
            await initStock([{ sku: SKU_CHILD, in_stock: 0 }, { sku: SKU_COMBO, in_stock: 0 }])
            const arrBundle = await bundleService.getConversionInOut({ sku: 100150041, in_comming: 4, stock_id: 1005 })
            assert(Array.isArray(arrBundle), 'Bundle error');
            const combo = arrBundle.find(item => item.sku === SKU_COMBO)
            assert(combo, 'Combo 201600075 must in-comming');
            assert(combo.qty == 1, 'Must in-comming 1');
        });

        it('Purchase sku with qty 4 exist 2', async function () {
            await initStock([{ sku: SKU_CHILD, in_stock: 2 }, { sku: SKU_COMBO, in_stock: 0 }])
            const arrBundle = await bundleService.getConversionInOut({ sku: 100150041, in_comming: 4, stock_id: 1005 })
            assert(Array.isArray(arrBundle), 'Bundle error');
            const combo = arrBundle.find(item => item.sku === SKU_COMBO)
            assert(combo, 'Combo 201600075 must in-comming');
            assert(combo.qty == 2, 'Must in-comming 2');
        });
    })
})