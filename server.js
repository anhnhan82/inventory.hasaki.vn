require('dotenv').config();
/*
const amqp = require('amqplib/callback_api');
const { RABBITMQ_HOST, RABBITMQ_PORT, RABBITMQ_VHOST, RABBITMQ_USER, RABBITMQ_PASSWORD, RABBITMQ_QUEUE } = process.env;

const stockImportExportService = require('./services/stockImportExportService')


const insertLogImportExport = async (arrParam) => {
    console.log(arrParam['params'])
    const result = await stockImportExportService.insert(arrParam['params'])
    return { error: 0, data: {update: result}}
}

amqp.connect(`amqp://${RABBITMQ_USER}:${RABBITMQ_PASSWORD}@${RABBITMQ_HOST}:${RABBITMQ_PORT}/${RABBITMQ_VHOST}`, function (error0, connection) {
    if (error0) {
        throw error0;
    }
    connection.createChannel(function (error1, channel) {
        if (error1) {
            throw error1;
        }

        channel.assertQueue(RABBITMQ_QUEUE, {
            durable: false
        });

        console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", RABBITMQ_QUEUE);

        channel.consume(RABBITMQ_QUEUE, function (msg) {
            console.log(" [x] Received %s", msg.content.toString());
            const arrData = JSON.parse(msg.content.toString())
            let result
            switch (arrData['task']) {
                case 'inventory':
                    result = insertLogImportExport(arrData)
                    break;
                default:
                    break
            }
            
            if (result && result.error === 0) {
                channel.ack(msg);
            } else {
                //channel.ack(msg);
            }
        }, {
            noAck: false
        });
    });
});

*/
const { RABBITMQ_HOST, RABBITMQ_PORT, RABBITMQ_VHOST, RABBITMQ_USER, RABBITMQ_PASSWORD, RABBITMQ_QUEUE } = process.env;
const amqplib = require('amqplib');
const stockImportExportService = require('./services/stockImportExportService')

var amqp_url = `amqp://${RABBITMQ_USER}:${RABBITMQ_PASSWORD}@${RABBITMQ_HOST}:${RABBITMQ_PORT}/${RABBITMQ_VHOST}`;

const insertLogImportExport = async (arrParam) => {
    console.log(arrParam)
    const result = await stockImportExportService.insert(arrParam)
    return { error: 0, data: { update: result } }
}

async function do_consume() {
    var conn = await amqplib.connect(amqp_url);
    var ch = await conn.createChannel({durable: true, noAck: false})
    var q = RABBITMQ_QUEUE;
    ch.prefetch(1);
    await ch.consume(q, async function (msg) {
        //console.log(msg.content.toString());
        //ch.ack(msg);
        console.log(" [x] Received %s", msg.content.toString());
        const arrData = JSON.parse(msg.content.toString())
        let result
        switch (arrData['task']) {
            case 'inventory':
                result = await insertLogImportExport(arrData['params'])
                break;
            default:
                break
        }

        if (result && result.error === 0) {
            ch.ack(msg);
        } else {
            ch.reject(msg);
        }
    }, {
        noAck: false
    });

};

do_consume();